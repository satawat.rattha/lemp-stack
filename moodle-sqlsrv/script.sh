add-apt-repository ppa:ondrej/php -y

apt-get update -y

apt install -y apache2


cp -r ./web/moodle /var/www
cp -r ./web/moodledata /var/www
chmod -R 777 /var/www/moodledata

ufw allow in "Apache Full"

cp ./config/000-default.conf /etc/apache2/sites-available/000-default.conf

apt-get install -y php7.4 
apt-get install -y php-pear 
apt-get install -y sendmail 
apt-get install -y libpng-dev 
apt-get install -y libzip-dev
apt-get install -y zlib1g-dev
apt-get install -y libicu-dev
apt-get install -y libxml2-dev
apt-get install -y tdsodbc
apt-get install -y unixodbc
apt-get install -y apt-utils
apt-get install -y curl
apt-get install -y nano
apt-get install -y --no-install-recommends unixodbc-dev libldap2-dev

apt install -y php7.4-dev
apt install -y php7.4-curl
apt install -y php7.4-mysql
apt install -y php7.4-gd
apt install -y php7.4-zip
apt install -y php7.4-mbstring
apt install -y php7.4-mysqli
apt install -y php7.4-intl
apt install -y php7.4-xmlrpc
apt install -y php7.4-soap
apt install -y php7.4-opcache
apt install -y php7.4-xml

apt install -y php7.4-odbc
apt install -y libapache2-mod-php

set -x && cd /usr/src/ && tar -xf php.tar.xz && mv php-7* php && cd /usr/src/php/ext/odbc && phpize && sed -ri 's@^ *test +"\$PHP_.*" *= *"no" *&& *PHP_.*=yes *$@#&@g' configure && ./configure --with-unixODBC=shared,/usr > /dev/null && docker-php-ext-install odbc > /dev/null

apt-get install -y gnupg libgssapi-krb5-2

pecl config-set php_ini /etc/php/7.4/apache2/php.ini

# rm -rf /var/lib/apt/lists/*

service apache2 restart

service apache-htcacheclean start