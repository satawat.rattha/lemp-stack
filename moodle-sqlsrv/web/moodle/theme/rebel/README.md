THEME_Rebel
===========

# Rebel
Radically different, beautifully executed, and intelligently designed.  Rebel is a unique and modern Boost-based theme that isn't afraid to be simple.  A re-imagined, straightforward approach to navigation is focused on teacher and student needs.  A streamlined navigation makes most important links obvious and easily discoverable on the pages they are needed. Everything from the page layout, color choices, and features of Rebel is carefully implemented to benefit learning.  Rebel is an uncomplicated theme that helps students go from login to learning effortlessly! 

REBEL FEATURES
* Intuitive navigation that breaks free of hidden menus
* Clean and consistent layout
* Smart Edit is available anywhere on the course page and keeps you focused
* Dynamic Icon Sidebar navigation never leaves your side
* Custom page image
* Custom login image
* 5 Style Presets to instantly change the look.  Presets are simple enough to modify and change colors.
	* Rebel Preset
	* Shuffled Paper Preset
	* eLearn Preset
	* Grunge Preset
	* University Preset
* Super easy setup and install for site admins
* Super easy interface for users
* Custom login page with image and text features
* Login page alertbox

# Versions and Updates

## Moodle 3.9 Rebel Initial Release 1
* Brand new theme.  Minimal setup.  5 page layouts.  Unique navigation.  Custom background image for pages and login.

### Rebel version 1.2
* Fixed issues related to https://tracker.moodle.org/browse/CONTRIB-8220

### Rebel version 1.3
* Fixed issue: https://github.com/dbnschools/moodle-theme_rebel/issues/1

### Rebel version 1.4
* Fixed issues related to https://tracker.moodle.org/browse/CONTRIB-8220  Additional fixes to certain header links not appearing when on a course page for users.