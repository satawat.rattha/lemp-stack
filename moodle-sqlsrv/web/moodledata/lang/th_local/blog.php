<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://localhost:8000
 *
 * @package    core
 * @subpackage blog
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addnewexternalblog'] = 'ลงทะเบียนบล็อกภายนอก';
$string['assocdescription'] = 'หากคุณกำลังเขียนเกี่ยวกับหลักสูตรและ / หรือโมดูลกิจกรรมให้เลือกที่นี่';
$string['associated'] = 'ที่เกี่ยวข้อง {$a}';
$string['associatewithcourse'] = 'บล็อกเกี่ยวกับหลักสูตร {$a->coursename}';
$string['associatewithmodule'] = 'บล็อกเกี่ยวกับ {$a->modtype}: {$a->modname}';
$string['association'] = 'สมาคม';
$string['associations'] = 'สมาคม';
$string['associationunviewable'] = 'ผู้อื่นไม่สามารถดูรายการนี้ได้จนกว่าจะมีการเชื่อมโยงหลักสูตรหรือฟิลด์ \'เผยแพร่ไปยัง\' จะมีการเปลี่ยนแปลง';
$string['autotags'] = 'เพิ่มแท็กเหล่านี้';
$string['autotags_help'] = 'ป้อนแท็กท้องถิ่นอย่างน้อยหนึ่งแท็ก (คั่นด้วยเครื่องหมายจุลภาค) ที่คุณต้องการเพิ่มโดยอัตโนมัติในแต่ละรายการบล็อกที่คัดลอกมาจากบล็อกภายนอกลงในบล็อกในพื้นที่ของคุณ';
$string['backupblogshelp'] = 'หากเปิดใช้งานบล็อกจะรวมอยู่ในการสำรองข้อมูลอัตโนมัติของไซต์';
$string['blockexternalstitle'] = 'บล็อกภายนอก';
$string['blog'] = 'บล็อก';
$string['blogaboutthis'] = 'บล็อกเกี่ยวกับเรื่องนี้ {$a->type}';
$string['blogaboutthiscourse'] = 'เพิ่มรายการเกี่ยวกับหลักสูตรนี้';
$string['blogaboutthismodule'] = 'เพิ่มรายการเกี่ยวกับเรื่องนี้ {$a}';
$string['blogadministration'] = 'การดูแลระบบบล็อก';
$string['blogdisable'] = 'บล็อกถูกปิดใช้งาน!';
$string['blogentries'] = 'รายการบล็อก';
$string['blogentriesabout'] = 'รายการบล็อกเกี่ยวกับ {$a}';
$string['blogentriesbygroupaboutcourse'] = 'รายการบล็อกเกี่ยวกับ {$a->course} โดย {$a->group}';
$string['blogentriesbygroupaboutmodule'] = 'รายการบล็อกเกี่ยวกับ {$a->mod} โดย {$a->group}';
$string['blogentriesbyuseraboutcourse'] = 'รายการบล็อกเกี่ยวกับ {$a->course} โดย {$a->user}';
$string['blogentriesbyuseraboutmodule'] = 'รายการบล็อกเกี่ยวกับ this {$a->mod} โดย {$a->user}';
$string['blogentrybyuser'] = 'รายการบล็อกโดย {$a}';
$string['blogpreferences'] = 'ค่ากำหนดของบล็อก';
$string['blogs'] = 'บล็อก';
$string['blogscourse'] = 'บล็อกของหลักสูตร';
$string['blogssite'] = 'บล็อกของไซต์';
$string['cannotviewcourseblog'] = 'คุณไม่มีสิทธิ์ที่จำเป็นในการดูบล็อกในหลักสูตรนี้';
$string['cannotviewcourseorgroupblog'] = 'คุณไม่มีสิทธิ์ที่จำเป็นในการดูบล็อกในหลักสูตร / กลุ่มนี้';
$string['cannotviewsiteblog'] = 'คุณไม่มีสิทธิ์ที่จำเป็นในการดูบล็อกของไซต์ทั้งหมด';
$string['cannotviewuserblog'] = 'คุณไม่มีสิทธิ์ที่จำเป็นในการอ่านบล็อกของผู้ใช้';
$string['configexternalblogcrontime'] = 'Moodle ตรวจสอบบล็อกภายนอกสำหรับรายการใหม่บ่อยเพียงใด';
$string['configmaxexternalblogsperuser'] = 'จำนวนบล็อกภายนอกผู้ใช้แต่ละคนได้รับอนุญาตให้เชื่อมโยงไปยังบล็อก Moodle ของตน';
$string['configuseblogassociations'] = 'เปิดใช้งานการเชื่อมโยงรายการบล็อกกับหลักสูตรและโมดูลของหลักสูตร';
$string['configuseexternalblogs'] = 'ช่วยให้ผู้ใช้สามารถระบุฟีดบล็อกภายนอก Moodle จะตรวจสอบฟีดบล็อกเหล่านี้เป็นประจำและคัดลอกรายการใหม่ไปยังบล็อกท้องถิ่นของผู้ใช้รายนั้น';
$string['courseblog'] = 'บล็อกหลักสูตร: {$a}';
$string['courseblogdisable'] = 'ไม่ได้เปิดใช้งานบล็อกของหลักสูตร';
$string['deleteblogassociations'] = 'ลบการเชื่อมโยงบล็อก';
$string['deleteblogassociations_help'] = 'หากทำเครื่องหมายแล้วรายการบล็อกจะไม่เชื่อมโยงกับหลักสูตรนี้หรือกิจกรรมของหลักสูตรหรือแหล่งข้อมูลใด ๆ อีกต่อไป รายการบล็อกจะไม่ถูกลบ';
$string['deleteentry'] = 'ลบรายการ';
$string['deleteexternalblog'] = 'ยกเลิกการลงทะเบียนบล็อกภายนอกนี้';
$string['deleteotagswarn'] = 'คุณแน่ใจหรือไม่ว่าต้องการลบแท็กเหล่านี้ออกจากบล็อกโพสต์ทั้งหมดและลบออกจากระบบ';
$string['description'] = 'คำอธิบาย';
$string['description_help'] = 'ป้อนหนึ่งหรือสองประโยคสรุปเนื้อหาของบล็อกภายนอกของคุณ (หากไม่มีคำอธิบายจะใช้คำอธิบายที่บันทึกไว้ในบล็อกภายนอกของคุณ)';
$string['donothaveblog'] = 'คุณไม่มีบล็อกของคุณเองขออภัย';
$string['editentry'] = 'แก้ไขรายการบล็อก';
$string['editexternalblog'] = 'แก้ไขบล็อกภายนอกนี้';
$string['emptybody'] = 'เนื้อหารายการบล็อกต้องไม่ว่างเปล่า';
$string['emptyrssfeed'] = 'URL ที่คุณป้อนไม่ชี้ไปที่ฟีด RSS ที่ถูกต้อง';
$string['emptytitle'] = 'ชื่อรายการบล็อกต้องไม่ว่างเปล่า';
$string['emptyurl'] = 'คุณต้องระบุ URL ให้กับฟีด RSS ที่ถูกต้อง';
$string['eventblogassociationadded'] = 'สร้างการเชื่อมโยงบล็อกแล้ว';
$string['eventblogassociationdeleted'] = 'ลบการเชื่อมโยงบล็อกแล้ว';
$string['eventblogentriesviewed'] = 'ดูรายการบล็อกแล้ว';
$string['eventblogexternaladded'] = 'บล็อกภายนอกที่ลงทะเบียนแล้ว';
$string['eventblogexternalremoved'] = 'บล็อกภายนอกไม่ได้ลงทะเบียน';
$string['eventblogexternalupdated'] = 'อัปเดตบล็อกภายนอกแล้ว';
$string['evententryadded'] = 'เพิ่มรายการบล็อกแล้ว';
$string['evententrydeleted'] = 'ลบรายการบล็อกแล้ว';
$string['eventexternalblogsviewed'] = 'ดูบล็อกที่ลงทะเบียนภายนอกแล้ว';
$string['externalblogcrontime'] = 'กำหนดการ cron ของบล็อกภายนอก';
$string['externalblogdeleteconfirm'] = 'ยกเลิกการลงทะเบียนบล็อกภายนอกนี้หรือไม่?';
$string['externalblogdeleted'] = 'บล็อกภายนอกไม่ได้ลงทะเบียน';
$string['externalblogs'] = 'บล็อกภายนอก';
$string['feedisinvalid'] = 'ฟีดนี้ไม่ถูกต้อง';
$string['feedisvalid'] = 'ฟีดนี้ถูกต้อง';
$string['filterblogsby'] = 'กรองรายการตาม...';
$string['filtertags'] = 'กรองแท็ก';
$string['filtertags_help'] = 'คุณสามารถใช้คุณสมบัตินี้เพื่อกรองรายการที่คุณต้องการใช้ หากคุณระบุแท็กที่นี่ (คั่นด้วยลูกน้ำ) ระบบจะคัดลอกเฉพาะรายการที่มีแท็กเหล่านี้จากบล็อกภายนอก';
$string['groupblogdisable'] = 'ไม่ได้เปิดใช้งานบล็อกของกลุ่ม';
$string['groupblogentries'] = 'รายการบล็อกที่เกี่ยวข้องกับ {$a->coursename} ตามกลุ่ม {$a->groupname}';
$string['incorrectblogfilter'] = 'ระบุประเภทตัวกรองบล็อกไม่ถูกต้อง';
$string['invalidgroupid'] = 'ID กลุ่มไม่ถูกต้อง';
$string['invalidurl'] = 'ไม่สามารถเข้าถึง URL นี้ได้';
$string['linktooriginalentry'] = 'ลิงก์ไปยังรายการบล็อกเดิม';
$string['maxexternalblogsperuser'] = 'จำนวนบล็อกภายนอกสูงสุดต่อผู้ใช้';
$string['myprofileuserblogs'] = 'ดูรายการบล็อกทั้งหมด';
$string['name'] = 'ชื่อ';
$string['name_help'] = 'ป้อนชื่อที่สื่อความหมายสำหรับบล็อกภายนอกของคุณ (หากไม่มีการระบุชื่อจะใช้ชื่อบล็อกภายนอกของคุณ)';
$string['nopermissionstodeleteentry'] = 'คุณไม่มีสิทธิ์ที่จำเป็นในการลบรายการบล็อกนี้';
$string['nosuchentry'] = 'ไม่มีรายการบล็อกดังกล่าว';
$string['page-blog-edit'] = 'หน้าแก้ไขบล็อก';
$string['page-blog-index'] = 'หน้ารายชื่อบล็อก';
$string['page-blog-x'] = 'หน้าบล็อกทั้งหมด';
$string['permalink'] = 'ลิงก์ถาวร';
$string['preferences'] = 'ค่ากำหนดของบล็อก';
$string['privacy:metadata:core_comments'] = 'ความคิดเห็นที่เกี่ยวข้องกับรายการบล็อก';
$string['privacy:metadata:core_files'] = 'ไฟล์ที่แนบมากับรายการบล็อก';
$string['privacy:metadata:core_tag'] = 'แท็กที่เกี่ยวข้องกับรายการบล็อก';
$string['privacy:metadata:external'] = 'ลิงก์ไปยังฟีด RSS ภายนอก';
$string['privacy:metadata:external:description'] = 'คำอธิบายของฟีด';
$string['privacy:metadata:external:filtertags'] = 'รายการแท็กที่จะกรองรายการด้วย';
$string['privacy:metadata:external:name'] = 'ชื่อของฟีด';
$string['privacy:metadata:external:timefetched'] = 'เวลาที่ดึงข้อมูลฟีดล่าสุด';
$string['privacy:metadata:external:timemodified'] = 'เวลาที่แก้ไขการเชื่อมโยงครั้งล่าสุด';
$string['privacy:metadata:external:url'] = 'URL ของฟีด';
$string['privacy:metadata:external:userid'] = 'ID ของผู้ใช้ที่เพิ่มรายการบล็อกภายนอก';
$string['privacy:metadata:post'] = 'ข้อมูลที่เกี่ยวข้องกับรายการบล็อก';
$string['privacy:metadata:post:content'] = 'เนื้อหาของรายการบล็อกภายนอก';
$string['privacy:metadata:post:created'] = 'วันที่สร้างรายการบล็อก';
$string['privacy:metadata:post:lastmodified'] = 'วันที่แก้ไขรายการบล็อกครั้งล่าสุด';
$string['privacy:metadata:post:publishstate'] = 'ไม่ว่ารายการนั้นจะปรากฏแก่ผู้อื่นหรือไม่ก็ตาม';
$string['privacy:metadata:post:subject'] = 'ชื่อรายการบล็อก';
$string['privacy:metadata:post:summary'] = 'ข้อความเข้าสู่บล็อก';
$string['privacy:metadata:post:uniquehash'] = 'ตัวระบุเฉพาะสำหรับรายการภายนอกโดยทั่วไปคือ URL';
$string['privacy:metadata:post:userid'] = 'ID ของผู้ใช้ที่เพิ่มรายการบล็อก';
$string['privacy:metadata:post:usermodified'] = 'ผู้ใช้ที่แก้ไขรายการล่าสุด';
$string['privacy:path:blogassociations'] = 'บล็อกโพสต์ที่เกี่ยวข้อง';
$string['privacy:unknown'] = 'ไม่ทราบ';
$string['publishto_help'] = 'มี 3 ตัวเลือก:

* ตัวคุณเอง (แบบร่าง) - มีเพียงคุณและผู้ดูแลระบบเท่านั้นที่สามารถเห็นรายการนี้
* ทุกคนในไซต์นี้ - ทุกคนที่ลงทะเบียนในไซต์นี้สามารถอ่านรายการนี้ได้
* ทุกคนในโลก - ทุกคนรวมถึงแขกสามารถอ่านข้อความนี้ได้';
$string['publishtocourse'] = 'ผู้ใช้หลักสูตรร่วมกันกับคุณ';
$string['publishtocourseassoc'] = 'สมาชิกของหลักสูตรที่เกี่ยวข้อง';
$string['publishtocourseassocparam'] = 'สมาชิกของ {$a}';
$string['publishtogroup'] = 'ผู้ใช้ที่แชร์กลุ่มกับคุณ';
$string['publishtogroupassoc'] = 'สมาชิกกลุ่มของคุณในหลักสูตรที่เกี่ยวข้อง';
$string['publishtogroupassocparam'] = 'สมาชิกกลุ่มของคุณใน {$a}';
$string['readfirst'] = 'อ่านสิ่งนี้ก่อน';
$string['relatedblogentries'] = 'รายการบล็อกที่เกี่ยวข้อง';
$string['retrievedfrom'] = 'ดึงมาจาก';
$string['rssfeed'] = 'ฟีด RSS ของบล็อก';
$string['searchterm'] = 'ค้นหา: {$a}';
$string['siteblogdisable'] = 'ไม่ได้เปิดใช้งานบล็อกของไซต์';
$string['siteblogheading'] = 'บล็อกของไซต์';
$string['tagparam'] = 'แท็ก: {$a}';
$string['tags'] = 'แท็ก';
$string['timefetched'] = 'เวลาของการซิงค์ครั้งล่าสุด';
$string['url'] = 'URL ฟีด RSS';
$string['url_help'] = 'ป้อน URL ฟีด RSS สำหรับบล็อกภายนอกของคุณ';
$string['useblogassociations'] = 'เปิดใช้งานการเชื่อมโยงบล็อก';
$string['useexternalblogs'] = 'เปิดใช้งานบล็อกภายนอก';
$string['userblogentries'] = 'รายการบล็อกโดย {$a}';
$string['valid'] = 'ถูกต้อง';
$string['viewallblogentries'] = 'รายการทั้งหมดเกี่ยวกับเรื่องนี้ {$a}';
$string['viewallmodentries'] = 'ดูรายการทั้งหมดเกี่ยวกับเรื่องนี้ {$a->type}';
$string['viewallmyentries'] = 'ดูรายการทั้งหมดของฉัน';
$string['viewblogentries'] = 'คอมเมนต์เกี่ยวกับเรื่องนี้ {$a->type}';
$string['viewblogsfor'] = 'ดูรายการทั้งหมดสำหรับ...';
$string['viewcourseblogs'] = 'ดูรายการทั้งหมดสำหรับหลักสูตรนี้';
$string['viewentriesbyuseraboutcourse'] = 'ดูรายการเกี่ยวกับหลักสูตรนี้โดย {$a}';
$string['viewgroupblogs'] = 'ดูรายการสำหรับกลุ่ม...';
$string['viewgroupentries'] = 'รายการกลุ่ม';
$string['viewmodblogs'] = 'ดูรายการสำหรับโมดูล...';
$string['viewmodentries'] = 'รายการโมดูล';
$string['viewmyentriesaboutcourse'] = 'ดูรายการของฉันเกี่ยวกับหลักสูตรนี้';
$string['viewmyentriesaboutmodule'] = 'ดูรายการของฉันเกี่ยวกับเรื่องนี้ {$a}';
$string['viewuserentries'] = 'ดูรายการทั้งหมดโดย {$a}';
$string['wrongexternalid'] = 'รหัสบล็อกภายนอกไม่ถูกต้อง';
$string['wrongpostid'] = 'รหัสโพสต์บล็อกไม่ถูกต้อง';
