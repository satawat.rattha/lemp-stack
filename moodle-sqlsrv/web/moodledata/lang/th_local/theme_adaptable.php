<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://localhost:8000
 *
 * @package    theme
 * @subpackage adaptable
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['events'] = 'ปฏิทิน';
$string['frnt-footer'] = 'บล็อกในพื้นที่นี้จะมองเห็นได้เฉพาะผู้ใช้ที่เป็นผู้ดูแลระบบเท่านั้น';
$string['fullscreen'] = 'แสดงผลแบบเต็ม';
$string['hideblocks'] = 'ซ่อนบล็อก';
$string['home'] = 'หน้าแรก';
$string['mysites'] = 'รายวิชา';
$string['noenrolments'] = 'ไม่พบรายวิชา';
$string['region-frnt-footer'] = 'ส่วนท้าย';
$string['region-middle'] = 'กลาง';
$string['region-side-post'] = 'ขวา';
$string['region-side-pre'] = 'ซ้าย';
$string['searchcourses'] = 'ค้นหาหลักสูตร';
$string['showblocks'] = 'แสดงบล็อก';
$string['side-post1'] = 'แถบด้านข้างในส่วนท้าย';
$string['slidercaption'] = 'คำอธิบายภาพเลื่อน';
$string['slidercaptiondesc'] = 'เพิ่มคำบรรยายสำหรับสไลด์ของคุณ';
$string['sliderenabled'] = 'เปิดใช้งาน Slider';
$string['sliderenableddesc'] = 'เปิดใช้งานแถบเลื่อนที่ด้านบนของโฮมเพจของคุณ';
$string['sliderfullscreen'] = 'ตัวเลื่อนแบบเต็มหน้าจอ';
$string['sliderfullscreendesc'] = 'เลือกช่องนี้เพื่อทำให้แถบเลื่อนเต็มหน้าจอ (ความกว้าง 100%)';
$string['sliderimage'] = 'รูปภาพตัวเลื่อน';
$string['sliderimagedesc'] = 'เพิ่มรูปภาพสำหรับสไลด์ของคุณ ขนาดที่แนะนำคือ 1600px x 400px หรือสูงกว่า';
$string['slidermarginbottom'] = 'ขอบด้านล่างแถบเลื่อน';
$string['slidermarginbottomdesc'] = 'กำหนดขนาดของระยะขอบด้านล่างแถบเลื่อน';
$string['slidermargintop'] = 'ขอบด้านบนแถบเลื่อน';
$string['slidermargintopdesc'] = 'กำหนดขนาดของระยะขอบเหนือแถบเลื่อน';
$string['slideroption2'] = 'เลือกประเภทตัวเลื่อน';
$string['slideroption2desc'] = 'เลือกประเภทตัวเลื่อน <strong> แล้วคลิกบันทึก </strong> เพื่อดูการตั้งค่าสีสำหรับแถบเลื่อนที่คุณเลือก';
$string['slideroption2snippet'] = '<p>HTML ตัวอย่างสำหรับ Slider Captions:</p>
<pre>
&#x3C;div class=&#x22;span6 col-sm-6&#x22;&#x3E;
&#x3C;h3&#x3E;Hand-crafted&#x3C;/h3&#x3E; &#x3C;h4&#x3E;pixels and code for the Moodle community&#x3C;/h4&#x3E;
&#x3C;a href=&#x22;#&#x22; class=&#x22;submit&#x22;&#x3E;Please favorite our theme!&#x3C;/a&#x3E;
</pre>';
$string['sliderurl'] = 'URL ลิงค์ของสไลด์';
$string['sliderurldesc'] = 'เพิ่ม URL ที่สไลด์ของคุณเชื่อมโยงเมื่อคลิก';
$string['slideshowdesc'] = 'อัปโหลดภาพเพิ่มลิงก์และคำอธิบายสำหรับภาพหมุนในหน้าแรก';
$string['slideshowsettings'] = 'สไลด์โชว์';
$string['slideshowsettingsheading'] = 'ปรับแต่งภาพหมุนในหน้าแรก ดูเค้าโครง <a href="./../theme/adaptable/pix/layout.png" target="_blank"> ที่นี่</a>';
$string['standardview'] = 'แสดงผลแบบมาตรฐาน';
$string['ticker'] = 'ประกาศ';
$string['tickerdefault'] = 'ไม่มีรายการข่าวที่จะแสดง';
