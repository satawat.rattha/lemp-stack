<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://localhost:8000
 *
 * @package    core
 * @subpackage plugin
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actions'] = 'การดำเนินการ';
$string['notes'] = 'บันทึก';
$string['settings'] = 'Settings';
$string['status'] = 'Status';
$string['status_nodb'] = 'No database';
$string['type_auth_plural'] = 'Authentication methods';
$string['type_block'] = 'Block';
$string['type_block_plural'] = 'Blocks';
$string['type_format_plural'] = 'Course formats';
$string['type_mod'] = 'Activity module';
$string['type_qtype'] = 'Question type';
$string['type_qtype_plural'] = 'Question types';
$string['type_theme'] = 'Theme';
$string['type_theme_plural'] = 'Themes';
$string['version'] = 'Version';
