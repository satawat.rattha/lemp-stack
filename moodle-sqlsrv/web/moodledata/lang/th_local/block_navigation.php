<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://localhost:8000
 *
 * @package    block
 * @subpackage navigation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['courseactivities'] = 'หมวดหมู่หลักสูตรและกิจกรรมของหลักสูตร';
$string['courses'] = 'หมวดหมู่และหลักสูตร';
$string['coursestructures'] = 'หมวดหมู่หลักสูตรและโครงสร้างหลักสูตร';
$string['enabledockdesc'] = 'อนุญาตให้ผู้ใช้เชื่อมต่อบล็อกนี้';
$string['everything'] = 'ทุกอย่าง';
$string['expansionlimit'] = 'สร้างการนำทางสำหรับสิ่งต่อไปนี้';
$string['linkcategoriesdesc'] = 'แสดงหมวดหมู่เป็นลิงค์';
$string['navigation:addinstance'] = 'เพิ่มบล็อกการนำทางใหม่';
$string['navigation:myaddinstance'] = 'เพิ่มบล็อกการนำทางใหม่ในแดชบอร์ด';
$string['pluginname'] = 'การนำทาง';
$string['privacy:metadata'] = 'บล็อกการนำทางจะแสดงเฉพาะข้อมูลที่จัดเก็บในตำแหน่งอื่น';
$string['trimlength'] = 'จำนวนอักขระที่จะตัดแต่ง';
$string['trimmode'] = 'โหมดตัดคำ';
$string['trimmodecenter'] = 'ตัดอักขระจากตรงกลาง';
$string['trimmodeleft'] = 'ตัดอักขระจากด้านซ้าย';
$string['trimmoderight'] = 'ตัดอักขระจากด้านขวา';
