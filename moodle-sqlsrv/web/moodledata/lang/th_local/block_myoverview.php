<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://localhost:8000
 *
 * @package    block
 * @subpackage myoverview
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addtofavourites'] = 'ติดดาวหลักสูตรนี้';
$string['all'] = 'ทั้งหมด (ยกเว้นถูกลบออกจากมุมมอง)';
$string['allincludinghidden'] = 'ทั้งหมด';
$string['aria:addtofavourites'] = 'ติดดาวสำหรับ';
$string['aria:allcourses'] = 'แสดงหลักสูตรทั้งหมดยกเว้นรายวิชาที่ถูกลบออกจากมุมมอง';
$string['aria:allcoursesincludinghidden'] = 'แสดงหลักสูตรทั้งหมด';
$string['aria:card'] = 'เปลี่ยนเป็นมุมมองการ์ด';
$string['aria:controls'] = 'การควบคุมภาพรวมของหลักสูตร';
$string['aria:courseactions'] = 'การดำเนินการสำหรับหลักสูตรปัจจุบัน';
$string['aria:courseprogress'] = 'ความก้าวหน้าของหลักสูตร:';
$string['aria:coursesummary'] = 'ข้อความสรุปหลักสูตร:';
$string['aria:customfield'] = 'แสดงหลักสูตร {$a}';
$string['aria:displaydropdown'] = 'แสดงเมนูแบบเลื่อนลง';
$string['aria:favourites'] = 'แสดงหลักสูตรที่ติดดาว';
$string['aria:future'] = 'แสดงหลักสูตรในอนาคต';
$string['aria:groupingdropdown'] = 'การจัดกลุ่มเมนูแบบเลื่อนลง';
$string['aria:hiddencourses'] = 'แสดงหลักสูตรที่ถูกลบออกจากมุมมอง';
$string['aria:hidecourse'] = 'ลบ {$a} ออกจากมุมมอง';
$string['aria:inprogress'] = 'แสดงหลักสูตรที่กำลังดำเนินการ';
$string['aria:lastaccessed'] = 'จัดเรียงหลักสูตรตามวันที่เข้าถึงล่าสุด';
$string['aria:list'] = 'เปลี่ยนเป็นมุมมองรายการ';
$string['aria:past'] = 'แสดงหลักสูตรที่ผ่านมา';
$string['aria:removefromfavourites'] = 'ลบดาวสำหรับ';
$string['aria:shortname'] = 'จัดเรียงหลักสูตรตามชื่อย่อของหลักสูตร';
$string['aria:showcourse'] = 'กู้คืน {$a} เพื่อดู';
$string['aria:sortingdropdown'] = 'การจัดเรียงเมนูแบบเลื่อนลง';
$string['aria:summary'] = 'เปลี่ยนเป็นมุมมองสรุป';
$string['aria:title'] = 'จัดเรียงหลักสูตรตามชื่อหลักสูตร';
$string['availablegroupings'] = 'ตัวกรองที่มี';
$string['availablegroupings_desc'] = 'ตัวกรองหลักสูตรที่ผู้ใช้เลือกได้ หากไม่มีการเลือกหลักสูตรทั้งหมดจะแสดงขึ้น';
$string['card'] = 'การ์ด';
$string['cards'] = 'การ์ด';
$string['completepercent'] = '{$a}% เสร็จสมบูรณ์';
$string['courseprogress'] = 'ความก้าวหน้าของหลักสูตร:';
$string['customfield'] = 'ฟิลด์ที่กำหนดเอง';
$string['customfiltergrouping'] = 'ฟิลด์ที่จะใช้';
$string['customfiltergrouping_nofields'] = 'ตัวเลือกนี้ต้องมีการตั้งค่าฟิลด์ที่กำหนดเองของหลักสูตรและทุกคนสามารถมองเห็นได้';
$string['displaycategories'] = 'แสดงหมวดหมู่';
$string['displaycategories_help'] = 'แสดงหมวดหมู่ของหลักสูตรในรายการหลักสูตรแดชบอร์ดรวมถึงการ์ดรายการและรายการสรุป';
$string['favourites'] = 'ติดดาว';
$string['future'] = 'อนาคต';
$string['hidden'] = 'หลักสูตรถูกลบออกจากมุมมอง';
$string['hiddencourses'] = 'ลบออกจากมุมมอง';
$string['hidecourse'] = 'ลบออกจากมุมมอง';
$string['inprogress'] = 'กำลังดำเนินการ';
$string['lastaccessed'] = 'เข้าถึงล่าสุด';
$string['layouts'] = 'เค้าโครงที่ใช้ได้';
$string['layouts_help'] = 'เค้าโครงภาพรวมหลักสูตรที่ผู้ใช้เลือกได้ หากไม่มีการเลือกเค้าโครงการ์ดจะถูกใช้';
$string['list'] = 'รายการ';
$string['myoverview:myaddinstance'] = 'เพิ่มบล็อกภาพรวมหลักสูตรใหม่ในแดชบอร์ด';
$string['nocustomvalue'] = 'ลำดับที่ {$a}';
$string['past'] = 'ที่ผ่านมา';
$string['pluginname'] = 'ภาพรวมของหลักสูตร';
$string['privacy:metadata:overviewgroupingpreference'] = 'การกำหนดลักษณะการจัดกลุ่มบล็อกภาพรวมของหลักสูตร';
$string['privacy:metadata:overviewpagingpreference'] = 'การกำหนดลักษณะการแบ่งหน้าบล็อกภาพรวมหลักสูตร';
$string['privacy:metadata:overviewsortpreference'] = 'การกำหนดลักษณะการจัดเรียงบล็อกภาพรวมของหลักสูตร';
$string['privacy:metadata:overviewviewpreference'] = 'การตั้งค่ามุมมองบล็อกภาพรวมของหลักสูตร';
$string['privacy:request:preference:set'] = 'ค่าของการตั้งค่า \'{$a->name} \'คือ\'{$a->value}\'';
$string['removefromfavourites'] = 'ยกเลิกการติดดาวหลักสูตรนี้';
$string['shortname'] = 'ชื่อย่อ';
$string['show'] = 'คืนค่าเพื่อดู';
$string['summary'] = 'สรุป';
$string['title'] = 'ชื่อหลักสูตร';
