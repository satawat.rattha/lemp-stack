<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://localhost:8000
 *
 * @package    block
 * @subpackage recentlyaccessedcourses
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['displaycategories'] = 'แสดงหมวดหมู่';
$string['displaycategories_help'] = 'แสดงหมวดหมู่ของหลักสูตรในรายการบล็อกหลักสูตรที่เพิ่งเข้าถึง';
$string['nocourses'] = 'ไม่มีหลักสูตรล่าสุด';
$string['pluginname'] = 'หลักสูตรที่เข้าถึงล่าสุด';
$string['privacy:metadata'] = 'บล็อกหลักสูตรที่เข้าถึงล่าสุดไม่ได้จัดเก็บข้อมูลส่วนบุคคลใด ๆ';
$string['recentlyaccessedcourses:myaddinstance'] = 'เพิ่มบล็อกหลักสูตรใหม่ที่เข้าถึงล่าสุดในแดชบอร์ด';
