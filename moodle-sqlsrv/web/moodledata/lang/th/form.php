<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'form', language 'th', version '3.9'.
 *
 * @package     form
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();




$string['advancedelement'] = 'Advanced element';
$string['close'] = 'Close';
$string['custom'] = 'Custom';
$string['day'] = 'Day';
$string['default'] = 'Default';
$string['display'] = 'Display';
$string['err_alphanumeric'] = 'You must enter only letters or numbers here.';
$string['err_email'] = 'You must enter a valid email address here.';
$string['err_lettersonly'] = 'You must enter only letters here.';
$string['err_maxfiles'] = 'You must not attach more than {$a} files here.';
$string['err_maxlength'] = 'You must enter no more than {$a->format} characters here.';
$string['err_minlength'] = 'You must enter at least {$a->format} characters here.';
$string['err_nonzero'] = 'You must enter a number not starting with a 0 here.';
$string['err_nopunctuation'] = 'You must enter no punctuation characters here.';
$string['err_numeric'] = 'You must enter a number here.';
$string['err_rangelength'] = 'You must enter between {$a->format[0]} and {$a->format[1]} characters here.';
$string['err_required'] = 'You must supply a value here.';
$string['err_wrappingwhitespace'] = 'The value must not start or end with whitespace.';
$string['err_wrongfileextension'] = 'Some files ({$a->wrongfiles}) cannot be uploaded. Only file types {$a->whitelist} are allowed.';
$string['filesofthesetypes'] = 'Accepted file types:';
$string['filetypesany'] = 'All file types';
$string['filetypesnotall'] = 'It is not allowed to select \'All file types\' here';
$string['filetypesnotwhitelisted'] = 'These file types are not allowed here: {$a}';
$string['filetypesothers'] = 'Other files';
$string['filetypesunknown'] = 'Unknown file types: {$a}';
$string['general'] = 'General';
$string['hideadvanced'] = 'Hide advanced';
$string['hour'] = 'Hour';
$string['minute'] = 'Minute';
$string['miscellaneoussettings'] = 'Miscellaneous settings';
$string['modstandardels'] = 'Common module settings';
$string['month'] = 'Month';
$string['mustbeoverriden'] = 'Abstract form_definition() method in class {$a} must be overridden, please fix the code.';
$string['newvaluefor'] = 'New value for {$a}';
$string['nomethodforaddinghelpbutton'] = 'There is no method for adding a help button to form element {$a->name} (class {$a->classname})';
$string['nonexistentformelements'] = 'Trying to add help buttons to non-existent form elements : {$a}';

$string['novalue'] = 'Nothing entered';
$string['novalueclicktoset'] = 'Click to enter text';
$string['optional'] = 'Optional';
$string['othersettings'] = 'Other settings';
$string['passwordunmaskedithint'] = 'Edit password';
$string['passwordunmaskrevealhint'] = 'Reveal';
$string['passwordunmaskinstructions'] = 'Press enter to save changes';
$string['privacy:metadata:preference:filemanager_recentviewmode'] = 'Recently selected view mode of the file picker element.';
$string['privacy:preference:filemanager_recentviewmode'] = 'Your preferred way of displaying files in the file picker is: {$a}';
$string['requiredelement'] = 'Required field';
$string['security'] = 'Security';
$string['selectallornone'] = 'Select all/none';
$string['selected'] = 'Selected';
$string['selecteditems'] = 'Selected items:';
$string['showadvanced'] = 'Show advanced';
$string['showless'] = 'Show less...';
$string['showmore'] = 'Show more...';
$string['somefieldsrequired'] = 'There are required fields in this form marked {$a}.';
$string['time'] = 'Time';
$string['timeunit'] = 'Time unit';
$string['timing'] = 'Timing';
$string['unmaskpassword'] = 'Unmask';
$string['year'] = 'Year';


$string['addfields'] = 'เพิ่มช่อง {$a} ลงในฟอร์ม';
$string['advancedelement'] = 'ตัวเลือกขั้นสูง';
$string['close'] = 'ปิด';
$string['day'] = 'วัน';
$string['display'] = 'แสดงผล';
$string['err_alphanumeric'] = 'คุณสามารถกรอกได้เฉพาะ ตัวเลข หรือ ตัวอักษร เท่านั้น';
$string['err_email'] = 'กรุณากรอกอีเมล์ให้ถูกต้อง';
$string['err_lettersonly'] = 'คุณสามารถกรอกได้เฉพาะตัวอักษรเท่านั้น';
$string['err_maxlength'] = 'คุณไม่สามารถกรอกเกิน {$a->format} ตัวอักษรได้';
$string['err_minlength'] = 'คุณต้องกรอกอย่างน้อย {$a->format} ตัวอักษร';
$string['err_nonzero'] = 'คุณไม่สามารถกรอกตัวเลขขึ้นต้นด้วย 0 ได้';
$string['err_nopunctuation'] = 'คุณไม่สามารถใส่เครื่องหมายวรรคตอนได้';
$string['err_numeric'] = 'คุณต้องใส่ตัวเลขเท่านั้น';
$string['err_rangelength'] = 'คุณสามารถใส่ได้ตั้งแต่ {$a->format[0]} ถึง {$a->format[1]} ตัวอักษร';
$string['err_required'] = 'คุณต้องกรอกช่องนี้';
$string['general'] = 'ทั่วไป';
$string['hideadvanced'] = 'ซ่อนตัวเลือกขั้นสูง';
$string['hour'] = 'ชั่วโมง';
$string['minute'] = 'นาที';
$string['miscellaneoussettings'] = 'การตั้งค่าเบ็ดเตล็ด';
$string['modstandardels'] = 'การตั้งค่าโมดูลปกติ';
$string['month'] = 'เดือน';
$string['nomethodforaddinghelpbutton'] = 'ไม่สามารถสร้างปุ่มช่วยเหลือให้กับช่อง {$a->name} (คลาส {$a->classname} )';
$string['nonexistentformelements'] = 'คุณพยายามสร้างปุ่มช่วยเหลือให้กับช่อง {$a} ที่ไม่มีอยู่';
$string['optional'] = 'ไม่บังคับ';
$string['othersettings'] = 'การตั้งค่าอื่นๆ';
$string['passwordunmaskrevealhint'] = 'แสดงรหัส';
$string['requiredelement'] = 'ช่องที่ต้องกรอก';
$string['security'] = 'ความปลอดภัย';
$string['selectallornone'] = 'เลือกทั้งหมด / ไม่เลือกเลย';
$string['showadvanced'] = 'แสดงตัวเลือกขั้นสูง';
$string['somefieldsrequired'] = 'คุณต้องกรอกข้อมูลในช่องที่ขึ้น {$a}';
$string['timing'] = 'จับเวลา';
$string['unmaskpassword'] = 'ไม่ปกปิด';
$string['year'] = 'ปี';
$string['noselection'] = 'ไม่มีการเลือก';
$string['nosuggestions'] = 'ไม่มีคำแนะนำ';