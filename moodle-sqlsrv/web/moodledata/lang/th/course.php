<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'course', language 'en', branch 'MOODLE_20_STABLE' *
 * @package   core_course
 * @copyright 2018 Adrian Greeve <adriangreeve.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activitychoosercategory'] = 'ตัวเลือกกิจกรรม';

$string['activitychooserrecommendations'] = 'กิจกรรมแนะนำ';

$string['activitychoosersettings'] = 'การตั้งค่าตัวเลือกกิจกรรม';

$string['activitychoosertabmode'] = 'แท็บตัวเลือกกิจกรรม';

$string['activitychoosertabmode_desc'] = 'ตัวเลือกกิจกรรมช่วยให้ครูสามารถเลือกกิจกรรมและแหล่งข้อมูลเพื่อเพิ่มลงในหลักสูตรได้อย่างง่ายดาย การตั้งค่านี้เป็นตัวกำหนดว่าควรแสดงแท็บใดในแท็บนั้น โปรดทราบว่าแท็บที่ติดดาวจะแสดงสำหรับผู้ใช้หากพวกเขาติดดาวกิจกรรมอย่างน้อยหนึ่งกิจกรรมและแท็บที่แนะนำจะแสดงเฉพาะในกรณีที่ผู้ดูแลไซต์ได้ระบุกิจกรรมที่แนะนำบางอย่างเท่านั้น';

$string['activitychoosertabmodeone'] = 'ติดดาวทั้งหมดกิจกรรมทรัพยากรแนะนำ';

$string['activitychoosertabmodetwo'] = 'ติดดาวทั้งหมดแนะนำ';

$string['activitychoosertabmodethree'] = 'ติดดาว, กิจกรรม, แหล่งข้อมูล, แนะนำ';

$string['aria:coursecategory'] = 'หมวดวิชา';

$string['aria:courseimage'] = 'ภาพหลักสูตร';

$string['aria:courseshortname'] = 'ชื่อย่อของหลักสูตร';

$string['aria:coursename'] = 'ชื่อหลักสูตร';

$string['aria:defaulttab'] = 'กิจกรรมเริ่มต้น';

$string['aria:favourite'] = 'ติดดาวหลักสูตรแล้ว';

$string['aria:favouritestab'] = 'กิจกรรมที่ติดดาว';

$string['aria:recommendedtab'] = 'กิจกรรมแนะนำ';

$string['aria:modulefavourite'] = 'ติดดาวกิจกรรม {$a}';

$string['coursealreadyfinished'] = 'จบหลักสูตรแล้ว';

$string['coursenotyetstarted'] = 'หลักสูตรยังไม่เริ่ม';

$string['coursenotyetfinished'] = 'หลักสูตรยังไม่จบ';

$string['coursetoolong'] = 'หลักสูตรยาวเกินไป';

$string['customfield_islocked'] = 'ล็อค';

$string['customfield_islocked_help'] = 'หากฟิลด์ถูกล็อกเฉพาะผู้ใช้ที่มีความสามารถในการเปลี่ยนฟิลด์แบบกำหนดเองที่ถูกล็อก (โดยค่าเริ่มต้นผู้ใช้ที่มีบทบาทเริ่มต้นเป็นผู้จัดการเท่านั้น) จะสามารถเปลี่ยนแปลงได้ในการตั้งค่าหลักสูตร';

$string['customfield_notvisible'] = 'ไม่มีใคร';

$string['customfield_visibility'] = 'ปรากฏแก่';

$string['customfield_visibility_help'] = 'การตั้งค่านี้กำหนดว่าใครสามารถดูชื่อฟิลด์และค่าที่กำหนดเองในรายการหลักสูตรหรือในตัวกรองฟิลด์แบบกำหนดเองที่มีอยู่ของแดชบอร์ด';

$string['customfield_visibletoall'] = 'ทุกคน';

$string['customfield_visibletoteachers'] = 'ครูผู้สอน';

$string['customfieldsettings'] = 'การตั้งค่าฟิลด์ที่กำหนดเองของหลักสูตรทั่วไป';

$string['errorendbeforestart'] = 'วันที่สิ้นสุด ({$a}) อยู่ก่อนวันที่เริ่มหลักสูตร';

$string['favourite'] = 'ติดดาวแน่นอน';

$string['gradetopassnotset'] = 'หลักสูตรนี้ไม่มีการกำหนดเกรดให้ผ่าน อาจกำหนดไว้ในรายการเกรดของหลักสูตร (การตั้งค่าสมุดพก)';

$string['informationformodule'] = 'ข้อมูลเกี่ยวกับกิจกรรม {$a}';

$string['module'] = 'กิจกรรม';

$string['nocourseactivity'] = 'กิจกรรมของหลักสูตรไม่เพียงพอระหว่างจุดเริ่มต้นและจุดสิ้นสุดของหลักสูตร';

$string['nocourseendtime'] = 'หลักสูตรไม่มีเวลาสิ้นสุด';

$string['nocoursesections'] = 'ไม่มีส่วนของหลักสูตร';

$string['nocoursestudents'] = 'ไม่มีนักเรียน';

$string['noaccesssincestartinfomessage'] = 'สวัสดีคุณ {$a->userfirstname}
นักเรียนจำนวนหนึ่งใน {$a->Coursename} ไม่เคยเข้าถึงหลักสูตรนี้';

$string['norecentaccessesinfomessage'] = 'สวัสดีคุณ {$a->userfirstname}
นักเรียนจำนวนหนึ่งใน {$a->Coursename} ไม่ได้เข้าถึงหลักสูตรนี้เมื่อเร็ว ๆ นี้';

$string['noteachinginfomessage'] = 'สวัสดีคุณ {$a->userfirstname}
หลักสูตรที่มีวันเริ่มต้นในสัปดาห์หน้าถูกระบุว่าไม่มีการลงทะเบียนครูหรือนักเรียน';

$string['privacy:perpage'] = 'จำนวนหลักสูตรที่จะแสดงต่อหน้า';

$string['privacy:completionpath'] = 'จบหลักสูตร';

$string['privacy:favouritespath'] = 'ข้อมูลที่ติดดาวของหลักสูตร';

$string['privacy:metadata:activityfavouritessummary'] = 'ระบบหลักสูตรมีข้อมูลเกี่ยวกับรายการจากตัวเลือกกิจกรรมที่ผู้ใช้ติดดาวไว้';

$string['privacy:metadata:completionsummary'] = 'หลักสูตรนี้มีข้อมูลเกี่ยวกับผู้ใช้ที่สมบูรณ์';

$string['privacy:metadata:favouritessummary'] = 'หลักสูตรนี้มีข้อมูลที่เกี่ยวข้องกับหลักสูตรที่ผู้ใช้ติดดาว';

$string['recommend'] = 'แนะนำ';

$string['recommendcheckbox'] = 'แนะนำกิจกรรม: {$a}';

$string['searchactivitiesbyname'] = 'ค้นหากิจกรรมตามชื่อ';

$string['searchresults'] = 'ผลการค้นหา: {$a}';

$string['submitsearch'] = 'ส่งการค้นหา';

$string['studentsatriskincourse'] = 'นักเรียนที่มีความเสี่ยงในหลักสูตร {$a}';

$string['studentsatriskinfomessage'] = 'สวัสดีคุณ {$a->userfirstname}
นักเรียนในหลักสูตร {$a->Coursename} ถูกระบุว่าตกอยู่ในความเสี่ยง';

$string['target:coursecompletion'] = 'นักเรียนที่มีความเสี่ยงที่จะไม่เป็นไปตามเงื่อนไขการจบหลักสูตร';

$string['target:coursecompletion_help'] = 'เป้าหมายนี้อธิบายว่านักเรียนมีความเสี่ยงที่จะไม่เป็นไปตามเงื่อนไขการจบหลักสูตรหรือไม่';

$string['target:coursecompetencies'] = 'นักเรียนที่มีความเสี่ยงที่จะไม่บรรลุความสามารถที่กำหนดให้กับหลักสูตร';

$string['target:coursecompetencies_help'] = 'เป้าหมายนี้อธิบายว่านักเรียนมีความเสี่ยงที่จะไม่บรรลุความสามารถที่กำหนดให้กับหลักสูตรหรือไม่ เป้าหมายนี้พิจารณาว่าความสามารถทั้งหมดที่กำหนดให้กับหลักสูตรจะต้องบรรลุเมื่อจบหลักสูตร';

$string['target:coursedropout'] = 'นักเรียนที่มีความเสี่ยงที่จะออกกลางคัน';

$string['target:coursedropout_help'] = 'เป้าหมายนี้อธิบายว่านักเรียนมีความเสี่ยงที่จะออกกลางคันหรือไม่';

$string['target:coursegradetopass'] = 'นักเรียนที่มีความเสี่ยงที่จะไม่ผ่านเกรดขั้นต่ำที่จะผ่านหลักสูตร';

$string['target:coursegradetopass_help'] = 'เป้าหมายนี้อธิบายว่านักเรียนมีความเสี่ยงที่จะไม่ได้เกรดขั้นต่ำที่จะผ่านหลักสูตรหรือไม่';

$string['target:noaccesssincecoursestart'] = 'นักเรียนที่ยังไม่ได้เข้าเรียน';

$string['target:noaccesssincecoursestart_help'] = 'เป้าหมายนี้อธิบายถึงนักเรียนที่ไม่เคยเข้าถึงหลักสูตรที่ลงทะเบียน';

$string['target:noaccesssincecoursestartinfo'] = 'นักเรียนต่อไปนี้ลงทะเบียนในหลักสูตรที่เริ่มต้นแล้ว แต่ไม่เคยเข้าถึงหลักสูตร';

$string['target:norecentaccesses'] = 'นักเรียนที่ไม่ได้เข้าใช้หลักสูตรเมื่อเร็ว ๆ นี้';

$string['target:norecentaccesses_help'] = 'เป้าหมายนี้ระบุนักเรียนที่ไม่ได้เข้าถึงหลักสูตรที่ลงทะเบียนภายในช่วงการวิเคราะห์ที่ตั้งไว้ (โดยค่าเริ่มต้นในเดือนที่ผ่านมา)';

$string['target:norecentaccessesinfo'] = 'นักเรียนต่อไปนี้ไม่ได้เข้าถึงหลักสูตรที่ลงทะเบียนภายในช่วงการวิเคราะห์ที่ตั้งไว้ (โดยค่าเริ่มต้นคือเดือนที่ผ่านมา)';

$string['target:noteachingactivity'] = 'หลักสูตรที่มีความเสี่ยงที่จะไม่เริ่มต้น';

$string['target:noteachingactivity_help'] = 'เป้าหมายนี้อธิบายว่าหลักสูตรที่จะเริ่มในสัปดาห์หน้าจะมีกิจกรรมการเรียนการสอนหรือไม่';

$string['target:noteachingactivityinfo'] = 'หลักสูตรต่อไปนี้ที่จะเริ่มในอีกไม่กี่วันข้างหน้ามีความเสี่ยงที่จะไม่ได้เริ่มเรียนเนื่องจากไม่มีครูหรือนักเรียนลงทะเบียน';

$string['targetlabelstudentcompletionno'] = 'นักศึกษาที่มีแนวโน้มว่าจะมีคุณสมบัติครบตามเงื่อนไขการจบหลักสูตร';

$string['targetlabelstudentcompletionyes'] = 'นักเรียนที่มีความเสี่ยงที่จะไม่เป็นไปตามเงื่อนไขการสำเร็จหลักสูตร';

$string['targetlabelstudentcompetenciesno'] = 'นักเรียนที่มีแนวโน้มที่จะบรรลุความสามารถที่กำหนดให้กับหลักสูตร';

$string['targetlabelstudentcompetenciesyes'] = 'นักเรียนที่มีความเสี่ยงที่จะไม่บรรลุความสามารถที่กำหนดให้กับหลักสูตร';

$string['targetlabelstudentdropoutyes'] = 'นักเรียนที่มีความเสี่ยงที่จะออกกลางคัน';

$string['targetlabelstudentdropoutno'] = 'ไม่เสี่ยง';

$string['targetlabelstudentgradetopassno'] = 'นักเรียนที่มีแนวโน้มว่าจะผ่านเกณฑ์ขั้นต่ำในการเรียน';

$string['targetlabelstudentgradetopassyes'] = 'นักเรียนที่มีความเสี่ยงที่จะเรียนไม่ถึงเกรดขั้นต่ำที่จะผ่านหลักสูตร';

$string['targetlabelteachingyes'] = 'ผู้ใช้ที่มีความสามารถในการสอนที่สามารถเข้าถึงหลักสูตรได้';

$string['targetlabelteachingno'] = 'หลักสูตรที่มีความเสี่ยงที่จะไม่เริ่มต้น';
