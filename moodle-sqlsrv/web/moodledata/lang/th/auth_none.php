<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_none', language 'th', version '3.9'.
 *
 * @package     auth_none
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['auth_nonedescription'] = 'สมาชิกสามารถ ล็อกอิน และสร้าง account ใหม่ทันที โดยไม่ต้องใช้วิธีการขออนุมัติ การเป็นสมาชิกจากฐานข้อมูลนอก ไม่ต้องยืนยันผ่านอีเมล  ควรระวังในการเลือกใช้วิีธี นี้ เพราะ ว่า ระบบความปลอดภัยนั้นมีน้อย';
$string['pluginname'] = 'ไม่ต้องขออนุมัติ อนุญาตทันที';
