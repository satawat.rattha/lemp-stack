<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'mimetypes', language 'th', version '3.9'.
 *
 * @package     mimetypes
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['application/msword'] = 'เอกสารเวิร์ด';
$string['application/pdf'] = 'เอกสาร pdf';
$string['application/vnd.ms-excel'] = 'ตาราง excel';
$string['application/vnd.ms-powerpoint'] = 'พรีเซนเทชั่น';
$string['document/unknown'] = 'ไฟล์';
$string['text/plain'] = 'ไฟล์ตัวหนังสือ';
$string['text/rtf'] = 'เอกสาร RTF';
