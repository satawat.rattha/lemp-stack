<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'calendar', language 'th', version '3.9'.
 *
 * @package     calendar
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['calendar'] = 'ปฏิทิน';
$string['calendarheading'] = '{$a} ปฏิทิน';
$string['category'] = 'ประเภท';
$string['clickhide'] = 'คลิกเพื่อซ่อน';
$string['clickshow'] = 'คลิกเพื่อแสดง';
$string['colcalendar'] = 'ปฏิทิน';
$string['confirmeventdelete'] = 'คุณแน่ใจหรือเปล่าคะที่จะลบกิจกรรมนี้';
$string['courseevents'] = 'กิจกรรมของรายวิชา';
$string['daywithnoevents'] = 'ไม่มีกิจกรรมวันนี้';
$string['default'] = 'ค่าที่ตั้งไว้';
$string['deleteevent'] = 'ลบกิจกรรม';
$string['durationminutes'] = 'ระยะเวลาเป็นนาที';
$string['durationnone'] = 'ไม่มีช่วงเวลา';
$string['durationuntil'] = 'จนกระทั่ง';
$string['editevent'] = 'แก้ไขกิจกรรม';
$string['errorbeforecoursestart'] = 'ไม่สามารถจัดตารางกิจกรรมก่อนวันที่รายวิชาจะเริ่ม';
$string['errorinvaliddate'] = 'วันที่ไม่ถูกต้อง';
$string['errorinvalidminutes'] = 'กำหนดระยะเวลาเป็นนาทีโดยตั้งค่าระหว่าง 1 ถึง 999';
$string['errorinvalidrepeats'] = 'กำหนดจำนวนกิจกรรมโดยตั้งค่าระหว่าง 1 ถึง 99';
$string['errornodescription'] = 'ต้องใส่คำอธิบาย';
$string['errornoeventname'] = 'ต้องใส่ชื่อ';
$string['eventdate'] = 'วันที่';
$string['eventdescription'] = 'คำอธิบาย';
$string['eventduration'] = 'ระยะเวลา';
$string['eventendtime'] = 'สิ้นสุดเวลา';
$string['eventinstanttime'] = 'เวลา';
$string['eventkind'] = 'ประเภทของกิจกรรม';
$string['eventname'] = 'ชื่อของกิจกรรม';
$string['eventnone'] = 'ไม่มีกิจกรรม';
$string['eventrepeat'] = 'จัดซ้ำ';
$string['eventsfor'] = 'กิจกรรมของวัน{$a}';
$string['eventstarttime'] = 'เริ่มเวลา';
$string['eventtime'] = 'เวลา';
$string['eventview'] = 'รายละเอียดกิจกรรม';
$string['expired'] = 'หมดเวลา';
$string['fri'] = 'ศ.';
$string['friday'] = 'ศุกร์';
$string['globalevents'] = 'กิจกรรมของเว็บ';
$string['gotocalendar'] = 'ไปที่ปฏิทิน';
$string['groupevents'] = 'กิจกรรมกลุ่ม';
$string['manyevents'] = '{$a} กิจกรรม';
$string['mon'] = 'จ.';
$string['monday'] = 'จันทร์';
$string['monthlyview'] = 'มุมมองรายเดือน';
$string['monthnext'] = 'เดือนถัดไป';
$string['never'] = 'ไม่เคย';
$string['newevent'] = 'กิจกรรมใหม่';
$string['noupcomingevents'] = 'ไม่มีกิจกรรมที่กำลังจะเริ่ม';
$string['oneevent'] = '1 กิจกรรม';
$string['pref_lookahead'] = 'กิจกรรมที่กำลังจะเริ่ม';
$string['pref_maxevents'] = 'จำนวนสูงสุดของกิจกรรมที่กำลังจะเริ่ม';
$string['pref_persistflt'] = 'จำการตั้งค่าฟิลเตอร์';
$string['pref_startwday'] = 'วันแรกของสัปดาห์';
$string['pref_timeformat'] = 'รูปแบบการแสดงเวลา';
$string['preferences'] = 'ตั้งค่าที่ต้องการ';
$string['preferences_available'] = 'ค่าที่คุณต้องการ';
$string['repeateditall'] = 'ให้การเปลี่ยนแปลงที่มีต่อกิจกรรม {$a} ที่เกิดซ้ำ ๆ มีผลทันที';
$string['repeateditthis'] = 'ให้การเปลี่ยนแปลงที่มีต่อกิจกรรมนี้เท่านั้นมีผลทันที';
$string['repeatnone'] = 'ไม่มีการซ้ำ';
$string['repeatweeksl'] = 'ซ้ำเป็นรายสัปดาห์, สร้างพร้อมกัน';
$string['repeatweeksr'] = 'กิจกรรม';
$string['sat'] = 'ส.';
$string['saturday'] = 'เสาร์';
$string['shown'] = 'แสดงอยู่';
$string['site'] = 'เว็บไซต์';
$string['spanningevents'] = 'กิจกรรมที่มีอยู่ขณะนี้';
$string['sun'] = 'อา.';
$string['sunday'] = 'อาทิตย์';
$string['thu'] = 'พฤ.';
$string['thursday'] = 'พฤหัสบดี';
$string['timeformat_12'] = '12 ชั่วโมง (am/pm)';
$string['timeformat_24'] = '24 ชั่วโมง';
$string['today'] = 'วันนี้';
$string['tomorrow'] = 'วันพรุ่งนี้';
$string['tt_deleteevent'] = 'ลบกิจกรรม';
$string['tt_editevent'] = 'แก้ไขกิจกรรม';
$string['tue'] = 'อ.';
$string['tuesday'] = 'อังคาร';
$string['typecourse'] = 'กิจกรรมของรายวิชา';
$string['typegroup'] = 'กิจกรรมกลุ่ม';
$string['typesite'] = 'กิจกรรมของเว็บไซต์';
$string['typeuser'] = 'กิจกรรมส่วนตัว';
$string['upcomingevents'] = 'กิจกรรมที่กำลังจะมีขึ้น';
$string['userevents'] = 'กิจกรรมส่วนตัว';
$string['wed'] = 'พ.';
$string['wednesday'] = 'พุธ';
$string['yesterday'] = 'เมื่อวานนี้';
$string['youcandeleteallrepeats'] = 'กิจกรรมนี้เป็นส่วนหนึ่งของกิจกรรมที่เกิดขึ้นซ้ำ ๆ คุณสามารถลบกิจกรรมนี้เท่านั้นหรือจะลบกิจกรรม {$a} ทั้งหมดภายในครั้งเดียว';
