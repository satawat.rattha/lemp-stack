<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tag', language 'en', branch 'MOODLE_20_STABLE' *
 * @package   core_tag
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

$string['added'] = 'เพิ่มแท็กมาตรฐานแล้ว';

$string['addotags'] = 'เพิ่มแท็กมาตรฐาน';

$string['addtagcoll'] = 'เพิ่มคอลเลกชันแท็ก';

$string['addtagtomyinterests'] = 'เพิ่ม "{$a}" ในความสนใจของฉัน';

$string['alltagpages'] = 'หน้าแท็กทั้งหมด';

$string['backtoallitems'] = 'กลับไปที่รายการทั้งหมดที่ติดแท็ก "{$a}"';

$string['changeshowstandard'] = 'เปลี่ยนการแสดงแท็กมาตรฐานในพื้นที่ {$a}';

$string['changessaved'] = 'บันทึกการเปลี่ยนแปลงแล้ว';

$string['changetagcoll'] = 'เปลี่ยนการรวบรวมแท็กของพื้นที่ {$a}';

$string['collnameexplained'] = 'เว้นช่องว่างไว้เพื่อใช้ค่าเริ่มต้น: {$a}';

$string['component'] = 'ส่วนประกอบ';

$string['confirmdeletetag'] = 'แน่ใจไหมว่าต้องการลบแท็กนี้?';

$string['confirmdeletetags'] = 'แน่ใจไหมว่าต้องการลบแท็กที่เลือก?';

$string['count'] = 'จำนวน';

$string['coursetags'] = 'แท็กหลักสูตร';

$string['defautltagcoll'] = 'คอลเลกชันเริ่มต้น';

$string['delete'] = 'ลบ';

$string['deleteselected'] = 'ลบรายการที่เลือก';

$string['deleted'] = 'ลบแท็กแล้ว';

$string['deletedcoursetags'] = 'ลบ - แท็กหลักสูตร';

$string['description'] = 'คำอธิบาย';

$string['editisstandard'] = 'เปลี่ยนการใช้แท็กมาตรฐาน';

$string['editcollname'] = 'แก้ไขชื่อคอลเล็กชันแท็ก';

$string['editname'] = 'แก้ไขชื่อแท็ก';

$string['editsearchable'] = 'เปลี่ยนค้นหาได้';

$string['edittag'] = 'แก้ไขแท็กนี้';

$string['edittagcollection'] = 'เปลี่ยนการรวบรวมแท็ก';

$string['entertags'] = 'ใส่แท็ก...';

$string['edittagcoll'] = 'แก้ไขการรวบรวมแท็ก {$a}';

$string['errortagfrontpage'] = 'ไม่อนุญาตให้ติดแท็กหน้าหลักของไซต์';

$string['errorupdatingrecord'] = 'เกิดข้อผิดพลาดในการอัปเดตบันทึกแท็ก';

$string['eventtagadded'] = 'เพิ่มแท็กในรายการแล้ว';

$string['eventtagcolldeleted'] = 'ลบคอลเล็กชันแท็กแล้ว';

$string['eventtagcollcreated'] = 'สร้างคอลเล็กชันแท็กแล้ว';

$string['eventtagcollupdated'] = 'อัปเดตคอลเล็กชันแท็กแล้ว';

$string['eventtagcreated'] = 'สร้างแท็กแล้ว';

$string['eventtagdeleted'] = 'ลบแท็กแล้ว';

$string['eventtagflagged'] = 'แท็กติดธง';

$string['eventtagremoved'] = 'แท็กถูกลบออกจากรายการ';

$string['eventtagunflagged'] = 'แท็กไม่ติดธง';

$string['eventtagupdated'] = 'อัปเดตแท็กแล้ว';

$string['exclusivemode'] = 'แสดงเฉพาะแท็ก {$a->tagarea}';

$string['flag'] = 'ธง';

$string['flagged'] = 'แท็กติดธง';

$string['flagasinappropriate'] = 'แจ้งว่าไม่เหมาะสม';

$string['helprelatedtags'] = 'จุลภาคคั่นแท็กที่เกี่ยวข้อง';

$string['changename'] = 'เปลี่ยนชื่อแท็ก';

$string['changetype'] = 'เปลี่ยนประเภทแท็ก';

$string['combined'] = 'แท็กจะรวมกัน';

$string['combineselected'] = 'รวมรายการที่เลือก';

$string['id'] = 'id';

$string['inalltagcoll'] = 'ทุกที่';

$string['inputstandardtags'] = 'ป้อนรายการแท็กใหม่ที่คั่นด้วยจุลภาค';

$string['itemstaggedwith'] = '{$a->tagarea} ติดแท็กด้วย "{$a->tag}"';

$string['lesstags'] = 'น้อยกว่า ...';

$string['managestandardtags'] = 'จัดการแท็กมาตรฐาน';

$string['managetags'] = 'จัดการแท็ก';

$string['managetagcolls'] = 'จัดการการรวบรวมแท็ก';

$string['moretags'] = 'มากกว่า...';

$string['name'] = 'ชื่อแท็ก';

$string['namesalreadybeeingused'] = 'มีการใช้ชื่อแท็กแล้ว';

$string['nameuseddocombine'] = 'ชื่อแท็กถูกใช้งานแล้ว คุณต้องการรวมแท็กเหล่านี้หรือไม่';

$string['newcollnamefor'] = 'ชื่อใหม่สำหรับการรวบรวมแท็ก {$a}';

$string['newnamefor'] = 'ชื่อใหม่สำหรับแท็ก {$a}';

$string['nextpage'] = 'มากกว่า';

$string['notagsfound'] = 'ไม่พบแท็กที่ตรงกับ "{$a}"';

$string['noresultsfor'] = 'ไม่มีผลลัพธ์สำหรับ "{$a}"';

$string['nothingtoupdate'] = 'ไม่มีอะไรให้อัปเดต';

$string['owner'] = 'เจ้าของ';

$string['prevpage'] = 'กลับ';

$string['privacy:metadata:tag'] = 'รายละเอียดของแต่ละแท็กที่ไม่ซ้ำกันจะถูกจัดเก็บควบคู่ไปกับคำอธิบายและข้อมูลอื่น ๆ ที่เกี่ยวข้อง';

$string['privacy:metadata:tag:name'] = 'ชื่อของแท็ก - นี่คือเวอร์ชันปกติของชื่อ';

$string['privacy:metadata:tag:rawname'] = 'ชื่อของแท็ก - นี่คือชื่อที่แสดง';

$string['privacy:metadata:tag:description'] = 'คำอธิบายของแท็ก';

$string['privacy:metadata:tag:flag'] = 'แท็กถูกตั้งค่าสถานะว่าไม่เหมาะสมหรือไม่';

$string['privacy:metadata:tag:timemodified'] = 'เวลาที่แก้ไขแท็กครั้งล่าสุด';

$string['privacy:metadata:tag:userid'] = 'ID ของผู้ใช้ที่สร้างแท็ก';

$string['privacy:metadata:taginstance'] = 'ลิงก์ระหว่างแต่ละแท็กและตำแหน่งที่ใช้';

$string['privacy:metadata:taginstance:tagid'] = 'ลิงก์ไปยังแท็ก';

$string['privacy:metadata:taginstance:ordering'] = 'ลำดับสัมพัทธ์ของแท็กนี้';

$string['privacy:metadata:taginstance:timecreated'] = 'เวลาที่แท็กนี้เชื่อมโยงกับเป้าหมาย';

$string['privacy:metadata:taginstance:timemodified'] = 'เวลาที่แก้ไขแท็กนี้สำหรับเป้าหมาย';

$string['privacy:metadata:taginstance:tiuserid'] = 'ในกรณีที่ผู้ใช้สามารถแท็กเนื้อหาที่แชร์แยกกันได้เจ้าของอินสแตนซ์แท็กจะถูกเก็บไว้';

$string['ptags'] = 'แท็กที่ผู้ใช้กำหนด (คั่นด้วยจุลภาค)';

$string['relatedblogs'] = 'รายการบล็อกล่าสุด';

$string['relatedtags'] = 'แท็กที่เกี่ยวข้อง';

$string['removetagfrommyinterests'] = 'ลบ "{$a}" จากความสนใจของฉัน';

$string['reset'] = 'รีเซ็ตค่าสถานะแท็ก';

$string['resetfilter'] = 'รีเซ็ตตัวกรอง';

$string['resetflag'] = 'รีเซ็ตค่าสถานะ';

$string['responsiblewillbenotified'] = 'ผู้รับผิดชอบจะได้รับแจ้ง';

$string['rssdesc'] = 'ฟีด RSS นี้สร้างขึ้นโดยอัตโนมัติโดย Moodle และมีแท็กที่ผู้ใช้สร้างขึ้นสำหรับหลักสูตร';

$string['rsstitle'] = 'แท็กหลักสูตรฟีด RSS สำหรับผู้ใช้: {$a}';

$string['showstandard'] = 'การใช้แท็กมาตรฐาน';

$string['showstandard_help'] = 'เมื่อป้อนแท็กอาจมีการแนะนำแท็กมาตรฐานหรือบังคับซึ่งหมายความว่าไม่สามารถป้อนแท็กใหม่ได้ หรืออาจป้อนแท็กใหม่โดยไม่มีการแนะนำแท็กมาตรฐาน';

$string['search'] = 'ค้นหา';

$string['searchable'] = 'ค้นหาได้';

$string['searchable_help'] = 'หากเลือกแท็กในคอลเล็กชันการค้นหานี้จะพบได้ในหน้า "แท็กการค้นหา" หากไม่เลือกแท็กจะยังคงพบได้ในหน้าการค้นหาอื่น ๆ';

$string['searchresultsfor'] = 'ผลการค้นหา "{$a}"';

$string['searchtags'] = 'ค้นหาแท็ก';

$string['seeallblogs'] = 'ดูรายการบล็อกทั้งหมดที่ติดแท็ก "{$a}"';

$string['select'] = 'เลือก';

$string['selectcoll'] = 'เลือกการรวบรวมแท็ก';

$string['selectmaintag'] = 'เลือกแท็กที่จะใช้หลังจากรวม';

$string['selectmultipletags'] = 'โปรดเลือกมากกว่าหนึ่งแท็ก';

$string['selecttag'] = 'เลือกแท็ก {$a}';

$string['settypedefault'] = 'ลบออกจากแท็กมาตรฐาน';

$string['settypestandard'] = 'สร้างมาตรฐาน';

$string['showingfirsttags'] = 'กำลังแสดงแท็กยอดนิยม {$a} แท็ก';

$string['standardforce'] = 'บังคับ';

$string['standardhide'] = 'ไม่แนะนำ';

$string['standardsuggest'] = 'แนะนำ';

$string['standardtag'] = 'มาตรฐาน';

$string['suredeletecoll'] = 'แน่ใจไหมว่าต้องการลบแท็กคอลเลคชัน "{$a}"';

$string['tag'] = 'แท็ก';

$string['tagarea_blog_external'] = 'โพสต์บล็อกภายนอก';

$string['tagarea_post'] = 'โพสต์บล็อก';

$string['tagarea_user'] = 'ความสนใจของผู้ใช้';

$string['tagarea_course'] = 'หลักสูตร';

$string['tagarea_course_modules'] = 'กิจกรรมและทรัพยากร';

$string['tagareaenabled'] = 'เปิดใช้งาน';

$string['tagareaname'] = 'ชื่อ';

$string['tagareas'] = 'พื้นที่แท็ก';

$string['tagcollection'] = 'การรวบรวมแท็ก';

$string['tagcollection_help'] = 'แท็กคอลเลคชันคือชุดของแท็กสำหรับพื้นที่ต่างๆ ตัวอย่างเช่นชุดของแท็กมาตรฐานสามารถใช้เพื่อแท็กหลักสูตรโดยมีความสนใจของผู้ใช้และแท็กโพสต์บล็อกเก็บไว้ในคอลเล็กชันแยกต่างหาก เมื่อผู้ใช้คลิกที่แท็กหน้าแท็กจะแสดงเฉพาะรายการที่มีแท็กนั้นในคอลเล็กชันเดียวกัน สามารถเพิ่มแท็กลงในคอลเลกชันโดยอัตโนมัติตามพื้นที่ที่ติดแท็กหรือสามารถเพิ่มด้วยตนเองเป็นแท็กมาตรฐาน';

$string['tagcollections'] = 'แท็กคอลเลคชัน';

$string['tagdescription'] = 'คำอธิบายแท็ก';

$string['tags'] = 'แท็ก';

$string['tagsaredisabled'] = 'แท็กถูกปิดใช้งาน';

$string['thingstaggedwith'] = '"{$a->name}" ถูกใช้ไป {$a->count} ครั้ง';

$string['thingtaggedwith'] = '"{$a->name}" ถูกใช้ครั้งเดียว';

$string['timemodified'] = 'แก้ไข';

$string['typechanged'] = 'เปลี่ยนประเภทแท็กแล้ว';

$string['updatetag'] = 'อัปเดต';

$string['page-tag-x'] = 'หน้าแท็กทั้งหมด';

$string['page-tag-index'] = 'หน้าแท็กเดียว';

$string['page-tag-search'] = 'หน้าค้นหาแท็ก';

$string['page-tag-manage'] = 'จัดการหน้าแท็ก';
