<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'accredible', language 'th', version '3.9'.
 *
 * @package     accredible
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activityname'] = 'ชื่อกิจกรรม';
$string['additionalactivitiesone'] = 'โปรดทราบ: คุณกำลังเพิ่มกิจกรรมมากกว่าหนึ่งกิจกรรมลงไปในรายวิชา <br/>กิจกรรมทั้งสองจะถูกเห็นได้โดยนักเรียน ดังนั้นโปรดแน่ใจว่าควรตั้งชื่อกิจกรรมให้ไม่เหมือนกัน';
$string['additionalactivitiesthree'] = 'ชื่อนี้จะปรากฎบนใบประกาศ';
$string['apikeyhelp'] = 'ใส่คีย์ API ที่ได้จาก accredible.com';
$string['apikeylabel'] = 'API คีย์';
$string['completionissuecheckbox'] = 'ใช่ ออกให้หลังจากที่จบรายวิชาแล้ว';
$string['datecreated'] = 'วันที่สร้าง';
$string['description'] = 'คำอธิบาย';
$string['groupselect'] = 'กลุ่ม';
$string['id'] = 'ID';
$string['issued'] = 'ออกเมื่อ';
$string['overview'] = 'ภาพรวม';
