<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'workshop', language 'th', version '3.9'.
 *
 * @package     workshop
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allsubmissions'] = 'งานที่ส่งทั้งหมด';
$string['assess'] = 'ประเมิน';
$string['assessment'] = 'ประเมิน';
$string['assessmentby'] = 'ประเมินโดย {$a}';
$string['assessmentend'] = 'สิ้นสุดการประเมินผล';
$string['assessmentendevent'] = 'สิ้นสุดการประเมินผลสำหรับ{$a}';
$string['assessments'] = 'การประเมิน';
$string['assessmentstart'] = 'เริ่มการประเมินผล';
$string['assessmentstartevent'] = 'เริ่มการประเมินผล {$a}';
$string['editsubmission'] = 'แก้ไขการส่งงาน';
$string['examplesubmissions'] = 'ตัวอย่างผลงาน';
$string['feedbacksettings'] = 'ความเห็นที่มีต่องาน';
$string['gradinggrade'] = 'ให้คะแนนการให้คะแนน';
$string['info'] = 'ข้อมูล';
$string['modulename'] = 'ห้องปฏิบัติการ';
$string['modulenameplural'] = 'ห้องปฏิบัติการ';
$string['pluginname'] = 'ห้องปฏิบัติการ';
$string['reassess'] = 'ประเมินใหม่อีกครั้ง';
$string['submission'] = 'งานที่ส่ง';
$string['submissionend'] = 'สิ้นสุดการส่งงาน';
$string['submissionendevent'] = 'สิ้นสุดการส่งงานสำหรับ {$a}';
$string['submissionstart'] = 'เริ่มให้ส่งงาน';
$string['submissionstartevent'] = 'เริ่มให้ส่งงานสำหรับ  {$a}';
