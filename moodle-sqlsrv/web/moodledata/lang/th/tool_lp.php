<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_lp', language 'en' *
 * @package    tool_lp
 * @copyright  2015 Damyon Wiese
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actions'] = 'การดำเนินการ';

$string['activities'] = 'กิจกรรม';

$string['addcohorts'] = 'เพิ่มกลุ่มประชากรตามรุ่น';

$string['addcohortstosync'] = 'เพิ่มกลุ่มประชากรตามรุ่นเพื่อซิงค์';

$string['addcompetency'] = 'เพิ่มความสามารถ';

$string['addcoursecompetencies'] = 'เพิ่มความสามารถให้กับหลักสูตร';

$string['addcrossreferencedcompetency'] = 'เพิ่มความสามารถในการอ้างอิงข้าม';

$string['addingcompetencywillresetparentrule'] = 'การเพิ่มความสามารถใหม่จะลบกฎที่ตั้งไว้ใน "{$a}" คุณต้องการดำเนินการต่อหรือไม่';

$string['addnewcompetency'] = 'เพิ่มความสามารถใหม่';

$string['addnewcompetencyframework'] = 'เพิ่มกรอบความสามารถใหม่';

$string['addnewplan'] = 'เพิ่มแผนการเรียนรู้ใหม่';

$string['addnewtemplate'] = 'เพิ่มเทมเพลตแผนการเรียนรู้ใหม่';

$string['addnewuserevidence'] = 'เพิ่มหลักฐานใหม่';

$string['addtemplatecompetencies'] = 'เพิ่มสมรรถนะให้กับแม่แบบแผนการเรียนรู้';

$string['aisrequired'] = 'ต้องระบุ "{$a}"';

$string['aplanswerecreated'] = 'มีการสร้างแผนการเรียนรู้ {$a}';

$string['aplanswerecreatedmoremayrequiresync'] = 'มีการสร้างแผนการเรียนรู้ {$a}; เพิ่มเติมจะถูกสร้างขึ้นในระหว่างการซิงโครไนซ์ถัดไป';

$string['assigncohorts'] = 'กำหนดกลุ่มประชากรตามรุ่น';

$string['averageproficiencyrate'] = 'อัตราความเชี่ยวชาญโดยเฉลี่ยสำหรับแผนการเรียนรู้ที่สมบูรณ์ตามเทมเพลตนี้คือ {$a}%';

$string['cancelreviewrequest'] = 'ยกเลิกคำขอตรวจสอบ';

$string['cannotaddrules'] = 'ไม่สามารถกำหนดค่าความสามารถนี้';

$string['cannotcreateuserplanswhentemplateduedateispassed'] = 'ไม่สามารถสร้างแผนการเรียนรู้ใหม่ได้ วันครบกำหนดของเทมเพลตหมดอายุหรือกำลังจะหมดอายุ';

$string['cannotcreateuserplanswhentemplatehidden'] = 'ไม่สามารถสร้างแผนการเรียนรู้ใหม่ได้ในขณะที่แม่แบบนี้ซ่อนอยู่';

$string['category'] = 'ประเภท';

$string['chooserating'] = 'เลือกคะแนน ...';

$string['cohortssyncedtotemplate'] = 'กลุ่มประชากรตามรุ่นที่ซิงค์กับเทมเพลตแผนการเรียนรู้นี้';

$string['competenciesforframework'] = 'ความสามารถสำหรับ {$a}';

$string['competenciesmostoftennotproficient'] = 'ความสามารถส่วนใหญ่มักไม่เชี่ยวชาญในแผนการเรียนรู้ที่สมบูรณ์';

$string['competenciesmostoftennotproficientincourse'] = 'ความสามารถส่วนใหญ่มักไม่เชี่ยวชาญในหลักสูตรนี้';

$string['competencycannotbedeleted'] = 'ไม่สามารถลบความสามารถ "{$a}" ได้';

$string['competencycreated'] = 'สร้างความสามารถแล้ว';

$string['competencycrossreferencedcompetencies'] = 'ความสามารถที่อ้างถึงข้าม {$a}';

$string['competencyframework'] = 'กรอบสมรรถนะ';

$string['competencyframeworkcreated'] = 'สร้างกรอบสมรรถนะ';

$string['competencyframeworkname'] = 'ชื่อ';

$string['competencyframeworkroot'] = 'ไม่มีผู้ปกครอง (ความสามารถระดับบนสุด)';

$string['competencyframeworks'] = 'กรอบความสามารถ';

$string['competencyframeworksrepository'] = 'ที่เก็บกรอบสมรรถนะ';

$string['competencyframeworkupdated'] = 'อัปเดตกรอบสมรรถนะแล้ว';

$string['competencyoutcome_complete'] = 'ทำเครื่องหมายว่าเสร็จสมบูรณ์';

$string['competencyoutcome_evidence'] = 'แนบหลักฐาน';

$string['competencyoutcome_none'] = 'ไม่มี';

$string['competencyoutcome_recommend'] = 'แนะนำความสามารถ';

$string['competencypicker'] = 'ตัวเลือกความสามารถ';

$string['competencyrule'] = 'กฎความสามารถ';

$string['competencyupdated'] = 'อัปเดตความสามารถแล้ว';

$string['completeplan'] = 'ทำแผนการจัดการเรียนรู้นี้ให้สำเร็จ';

$string['completeplanconfirm'] = 'กำหนดแผนการเรียนรู้ "{$a}" ให้เสร็จสมบูรณ์หรือไม่ ในกรณีนี้สถานะปัจจุบันของความสามารถของผู้ใช้ทั้งหมดจะถูกบันทึกไว้และแผนจะกลายเป็นแบบอ่านอย่างเดียว';

$string['configurecoursecompetencysettings'] = 'กำหนดค่าความสามารถของหลักสูตร';

$string['configurescale'] = 'กำหนดค่าเครื่องชั่ง';

$string['coursecompetencies'] = 'ความสามารถของหลักสูตร';

$string['coursecompetencyratingsarenotpushedtouserplans'] = 'การให้คะแนนความสามารถในหลักสูตรนี้ไม่มีผลต่อแผนการเรียนรู้';

$string['coursecompetencyratingsarepushedtouserplans'] = 'คะแนนความสามารถในหลักสูตรนี้ได้รับการอัปเดตทันทีในแผนการเรียนรู้';

$string['coursecompetencyratingsquestion'] = 'เมื่อมีการจัดอันดับความสามารถของหลักสูตรการจัดอันดับจะอัปเดตความสามารถในแผนการเรียนรู้หรือใช้กับหลักสูตรเท่านั้นหรือไม่';

$string['coursesusingthiscompetency'] = 'หลักสูตรที่เชื่อมโยงกับความสามารถนี้';

$string['coveragesummary'] = 'ครอบคลุมความสามารถ {$a->competenciescoveredcount} จาก {$a->competenciescount} ({$a->coveragepercentage}%)';

$string['createplans'] = 'จัดทำแผนการเรียนรู้';

$string['createlearningplans'] = 'จัดทำแผนการเรียนรู้';

$string['crossreferencedcompetencies'] = 'ความสามารถที่อ้างอิงข้ามกัน';

$string['default'] = 'ค่าเริ่มต้น';

$string['deletecompetency'] = 'ลบความสามารถ "{$a}" ไหม';

$string['deletecompetencyframework'] = 'ลบกรอบความสามารถ "{$a}" หรือไม่';

$string['deletecompetencyparenthasrule'] = 'ลบความสามารถ "{$a}" ไหม การดำเนินการนี้จะลบกฎที่ตั้งไว้สำหรับระดับบนสุดด้วย';

$string['deleteplan'] = 'ลบแผนการเรียนรู้ "{$a}" หรือไม่';

$string['deleteplans'] = 'ลบแผนการเรียนรู้';

$string['deletetemplate'] = 'ลบเทมเพลตแผนการเรียนรู้ "{$a}" หรือไม่';

$string['deletetemplatewithplans'] = 'แม่แบบนี้มีแผนการเรียนรู้ที่เกี่ยวข้อง คุณต้องระบุวิธีดำเนินการตามแผนเหล่านั้น';

$string['deletethisplan'] = 'ลบแผนการเรียนรู้นี้';

$string['deletethisuserevidence'] = 'ลบหลักฐานนี้';

$string['deleteuserevidence'] = 'ลบหลักฐานการเรียนรู้ก่อนหน้า "{$a}" หรือไม่';

$string['description'] = 'คำอธิบาย';

$string['duedate'] = 'วันครบกำหนด';

$string['duedate_help'] = 'วันที่แผนการจัดการเรียนรู้ควรจะเสร็จสิ้นภายในวันที่';

$string['editcompetency'] = 'แก้ไขความสามารถ';

$string['editcompetencyframework'] = 'แก้ไขกรอบความสามารถ';

$string['editplan'] = 'แก้ไขแผนการเรียนรู้';

$string['editrating'] = 'แก้ไขคะแนน';

$string['edittemplate'] = 'แก้ไขเทมเพลตแผนการเรียนรู้';

$string['editthisplan'] = 'แก้ไขแผนการเรียนรู้นี้';

$string['editthisuserevidence'] = 'แก้ไขหลักฐานนี้';

$string['edituserevidence'] = 'แก้ไขหลักฐาน';

$string['evidence'] = 'หลักฐาน';

$string['findcourses'] = 'ค้นหาหลักสูตร';

$string['filterbyactivity'] = 'กรองความสามารถตามทรัพยากรหรือกิจกรรม';

$string['frameworkcannotbedeleted'] = 'ไม่สามารถลบกรอบสมรรถนะ "{$a}" ได้';

$string['hidden'] = 'ซ่อนอยู่';

$string['hiddenhint'] = '(ซ่อนไว้)';

$string['idnumber'] = 'เลขประจำตัว';

$string['inheritfromframework'] = 'สืบทอดจากกรอบความสามารถ (ค่าเริ่มต้น)';

$string['itemstoadd'] = 'รายการที่จะเพิ่ม';

$string['jumptocompetency'] = 'ข้ามไปที่ความสามารถ';

$string['jumptouser'] = 'ข้ามไปที่ผู้ใช้';

$string['learningplancompetencies'] = 'สมรรถนะแผนการเรียนรู้';

$string['learningplans'] = 'แผนการเรียนรู้';

$string['levela'] = 'ระดับ {$a}';

$string['linkcompetencies'] = 'เชื่อมโยงความสามารถ';

$string['linkcompetency'] = 'ความสามารถในการเชื่อมโยง';

$string['linkedcompetencies'] = 'ความสามารถที่เชื่อมโยง';

$string['linkedcourses'] = 'หลักสูตรที่เชื่อมโยง';

$string['linkedcourseslist'] = 'หลักสูตรที่เชื่อมโยง:';

$string['listcompetencyframeworkscaption'] = 'รายชื่อกรอบความสามารถ';

$string['listofevidence'] = 'รายการหลักฐาน';

$string['listplanscaption'] = 'รายชื่อแผนการจัดการเรียนรู้';

$string['listtemplatescaption'] = 'รายการแม่แบบแผนการเรียนรู้';

$string['loading'] = 'กำลังโหลด ...';

$string['locatecompetency'] = 'ค้นหาความสามารถ';

$string['managecompetenciesandframeworks'] = 'จัดการความสามารถและกรอบ';

$string['modcompetencies'] = 'ความสามารถของหลักสูตร';

$string['modcompetencies_help'] = 'ความสามารถของหลักสูตรที่เชื่อมโยงกับกิจกรรมนี้';

$string['move'] = 'ย้าย';

$string['movecompetency'] = 'ย้ายความสามารถ';

$string['movecompetencyafter'] = 'ย้ายความสามารถหลัง "{$a}"';

$string['movecompetencyframework'] = 'ย้ายกรอบความสามารถ';

$string['movecompetencytochildofselfwillresetrules'] = 'การย้ายความสามารถจะลบกฎของตัวเองและกฎที่ตั้งไว้สำหรับระดับบนสุดและปลายทาง คุณต้องการดำเนินการต่อหรือไม่';

$string['movecompetencywillresetrules'] = 'การย้ายความสามารถจะลบกฎที่กำหนดไว้สำหรับระดับบนสุดและปลายทาง คุณต้องการดำเนินการต่อหรือไม่';

$string['moveframeworkafter'] = 'ย้ายกรอบความสามารถหลัง "{$a}"';

$string['movetonewparent'] = 'ย้ายที่อยู่';

$string['myplans'] = 'แผนการเรียนรู้ของฉัน';

$string['nfiles'] = '{$a} ไฟล์';

$string['noactivities'] = 'ไม่มีกิจกรรม';

$string['nocompetencies'] = 'ไม่มีการสร้างความสามารถในกรอบนี้';

$string['nocompetenciesincourse'] = 'ไม่มีความสามารถที่เชื่อมโยงกับหลักสูตรนี้';

$string['nocompetenciesinactivity'] = 'ไม่มีการเชื่อมโยงความสามารถกับกิจกรรมหรือทรัพยากรนี้';

$string['nocompetenciesinevidence'] = 'ไม่มีการเชื่อมโยงความสามารถกับหลักฐานนี้';

$string['nocompetenciesinlearningplan'] = 'ไม่มีการเชื่อมโยงสมรรถนะกับแผนการจัดการเรียนรู้นี้';

$string['nocompetenciesintemplate'] = 'ไม่มีการเชื่อมโยงสมรรถนะกับเทมเพลตแผนการเรียนรู้นี้';

$string['nocompetenciesinlist'] = 'ไม่มีการเลือกความสามารถ';

$string['nocompetencyframeworks'] = 'ยังไม่มีการสร้างกรอบสมรรถนะ';

$string['nocompetencyselected'] = 'ไม่ได้เลือกความสามารถ';

$string['nocrossreferencedcompetencies'] = 'ไม่มีการอ้างถึงความสามารถอื่นใดกับความสามารถนี้';

$string['noevidence'] = 'ไม่มีหลักฐาน';

$string['nofiles'] = 'ไม่มีไฟล์';

$string['nolinkedcourses'] = 'ไม่มีหลักสูตรใดที่เชื่อมโยงกับความสามารถนี้';

$string['noparticipants'] = 'ไม่พบผู้เข้าร่วม';

$string['noplanswerecreated'] = 'ไม่มีการสร้างแผนการเรียนรู้';

$string['notemplates'] = 'ยังไม่มีการสร้างเทมเพลตแผนการเรียนรู้';

$string['nourl'] = 'ไม่มี URL';

$string['nouserevidence'] = 'ยังไม่มีการเพิ่มหลักฐานการเรียนรู้ก่อนหน้านี้';

$string['nouserplans'] = 'ยังไม่มีการจัดทำแผนการเรียนรู้';

$string['oneplanwascreated'] = 'มีการสร้างแผนการเรียนรู้';

$string['outcome'] = 'ผล';

$string['path'] = 'เส้นทาง:';

$string['parentcompetency'] = 'ผู้ปกครอง';

$string['parentcompetency_edit'] = 'แก้ไขระดับบนสุด';

$string['parentcompetency_help'] = 'กำหนดผู้ปกครองที่จะเพิ่มความสามารถ อาจเป็นความสามารถอื่นที่อยู่ในกรอบเดียวกันหรือเป็นรากฐานของกรอบความสามารถสำหรับความสามารถระดับบนสุดก็ได้';

$string['planapprove'] = 'ทำให้ใช้งานได้';

$string['plancompleted'] = 'แผนการเรียนรู้เสร็จสิ้น';

$string['plancreated'] = 'แผนการเรียนรู้ที่สร้างขึ้น';

$string['plandescription'] = 'คำอธิบาย';

$string['planname'] = 'ชื่อ';

$string['plantemplate'] = 'เลือกเทมเพลตแผนการเรียนรู้';

$string['plantemplate_help'] = 'แผนการเรียนรู้ที่สร้างจากแม่แบบจะมีรายการความสามารถที่ตรงกับแม่แบบ การอัปเดตเทมเพลตจะแสดงในแผนใด ๆ ที่สร้างจากเทมเพลตนั้น';

$string['planunapprove'] = 'ส่งกลับไปยังร่าง';

$string['planupdated'] = 'อัปเดตแผนการเรียนรู้แล้ว';

$string['pluginname'] = 'แผนการเรียนรู้';

$string['points'] = 'คะแนน';

$string['pointsgivenfor'] = 'คะแนนที่ให้สำหรับ "{$a}"';

$string['proficient'] = 'เชี่ยวชาญ';

$string['progress'] = 'ความคืบหน้า';

$string['rate'] = 'ประเมินค่า';

$string['ratecomment'] = 'บันทึกหลักฐาน';

$string['rating'] = 'คะแนน';

$string['ratingaffectsonlycourse'] = 'การให้คะแนนความสามารถจะอัปเดตความสามารถในหลักสูตรนี้เท่านั้น';

$string['ratingaffectsuserplans'] = 'การให้คะแนนความสามารถยังอัปเดตความสามารถในแผนการเรียนรู้ทั้งหมด';

$string['reopenplan'] = 'เปิดแผนการเรียนรู้นี้อีกครั้ง';

$string['reopenplanconfirm'] = 'เปิดแผนการเรียนรู้ "{$a}" อีกครั้งหรือไม่ หากเป็นเช่นนั้นสถานะความสามารถของผู้ใช้ที่บันทึกไว้ในขณะที่แผนเสร็จสมบูรณ์ก่อนหน้านี้จะถูกลบออกและแผนจะกลับมาใช้งานได้อีกครั้ง';

$string['requestreview'] = 'ขอรับการตรวจสอบ';

$string['reviewer'] = 'ผู้ตรวจทาน';

$string['reviewstatus'] = 'ตรวจสอบสถานะ';

$string['savechanges'] = 'บันทึกการเปลี่ยนแปลง';

$string['scale'] = 'มาตราส่วน';

$string['scale_help'] = 'มาตราส่วนเป็นตัวกำหนดว่าจะวัดความสามารถในความสามารถอย่างไร หลังจากเลือกมาตราส่วนแล้วจะต้องมีการกำหนดค่า * รายการที่เลือกเป็น \'ค่าเริ่มต้น\' คือการให้คะแนนเมื่อความสามารถเสร็จสมบูรณ์โดยอัตโนมัติ * รายการที่เลือกเป็น \'Proficient\' ระบุว่าค่าใดที่จะทำเครื่องหมายความสามารถว่ามีความเชี่ยวชาญเมื่อได้รับการจัดอันดับ';

$string['scalevalue'] = 'ค่าสเกล';

$string['search'] = 'ค้นหา...';

$string['selectcohortstosync'] = 'เลือกกลุ่มประชากรตามรุ่นที่จะซิงค์';

$string['selectcompetencymovetarget'] = 'เลือกสถานที่ที่จะย้ายความสามารถนี้ไปที่:';

$string['selectedcompetency'] = 'ความสามารถที่เลือก';

$string['selectuserstocreateplansfor'] = 'เลือกผู้ใช้เพื่อสร้างแผนการเรียนรู้';

$string['sendallcompetenciestoreview'] = 'ส่งความสามารถทั้งหมดเพื่อตรวจสอบหลักฐานการเรียนรู้ก่อนหน้า \'{$a}\'';

$string['sendcompetenciestoreview'] = 'ส่งความสามารถเพื่อตรวจสอบ';

$string['shortname'] = 'ชื่อ';

$string['sitedefault'] = '(ค่าเริ่มต้นของไซต์)';

$string['startreview'] = 'เริ่มการตรวจทาน';

$string['state'] = 'สถานะ';

$string['status'] = 'สถานะ';

$string['stopreview'] = 'จบการตรวจสอบ';

$string['stopsyncingcohort'] = 'หยุดการซิงค์กลุ่มประชากรตามรุ่น';

$string['taxonomies'] = 'อนุกรมวิธาน';

$string['taxonomy_add_behaviour'] = 'เพิ่มพฤติกรรม';

$string['taxonomy_add_competency'] = 'เพิ่มความสามารถ';

$string['taxonomy_add_concept'] = 'เพิ่มแนวคิด';

$string['taxonomy_add_domain'] = 'เพิ่มโดเมน';

$string['taxonomy_add_indicator'] = 'เพิ่มตัวบ่งชี้';

$string['taxonomy_add_level'] = 'เพิ่มระดับ';

$string['taxonomy_add_outcome'] = 'เพิ่มผลลัพธ์';

$string['taxonomy_add_practice'] = 'เพิ่มการปฏิบัติ';

$string['taxonomy_add_proficiency'] = 'เพิ่มความชำนาญ';

$string['taxonomy_add_skill'] = 'เพิ่มทักษะ';

$string['taxonomy_add_value'] = 'เพิ่มมูลค่า';

$string['taxonomy_edit_behaviour'] = 'แก้ไขพฤติกรรม';

$string['taxonomy_edit_competency'] = 'แก้ไขความสามารถ';

$string['taxonomy_edit_concept'] = 'แก้ไขแนวคิด';

$string['taxonomy_edit_domain'] = 'แก้ไขโดเมน';

$string['taxonomy_edit_indicator'] = 'แก้ไขตัวบ่งชี้';

$string['taxonomy_edit_level'] = 'แก้ไขระดับ';

$string['taxonomy_edit_outcome'] = 'แก้ไขผลลัพธ์';

$string['taxonomy_edit_practice'] = 'แก้ไขการปฏิบัติ';

$string['taxonomy_edit_proficiency'] = 'แก้ไขความชำนาญ';

$string['taxonomy_edit_skill'] = 'แก้ไขทักษะ';

$string['taxonomy_edit_value'] = 'แก้ไขค่า';

$string['taxonomy_parent_behaviour'] = 'พฤติกรรมของผู้ปกครอง';

$string['taxonomy_parent_competency'] = 'ความสามารถของผู้ปกครอง';

$string['taxonomy_parent_concept'] = 'แนวคิดของผู้ปกครอง';

$string['taxonomy_parent_domain'] = 'โดเมนหลัก';

$string['taxonomy_parent_indicator'] = 'ตัวบ่งชี้ผู้ปกครอง';

$string['taxonomy_parent_level'] = 'ระดับผู้ปกครอง';

$string['taxonomy_parent_outcome'] = 'ผลลัพธ์ของผู้ปกครอง';

$string['taxonomy_parent_practice'] = 'การปฏิบัติของผู้ปกครอง';

$string['taxonomy_parent_proficiency'] = 'ความสามารถของผู้ปกครอง';

$string['taxonomy_parent_skill'] = 'ทักษะของผู้ปกครอง';

$string['taxonomy_parent_value'] = 'ค่าหลัก';

$string['taxonomy_selected_behaviour'] = 'พฤติกรรมที่เลือก';

$string['taxonomy_selected_competency'] = 'ความสามารถที่เลือก';

$string['taxonomy_selected_concept'] = 'แนวคิดที่เลือก';

$string['taxonomy_selected_domain'] = 'โดเมนที่เลือก';

$string['taxonomy_selected_indicator'] = 'ตัวบ่งชี้ที่เลือก';

$string['taxonomy_selected_level'] = 'ระดับที่เลือก';

$string['taxonomy_selected_outcome'] = 'ผลลัพธ์ที่เลือก';

$string['taxonomy_selected_practice'] = 'การปฏิบัติที่เลือก';

$string['taxonomy_selected_proficiency'] = 'ความเชี่ยวชาญที่เลือก';

$string['taxonomy_selected_skill'] = 'ทักษะที่เลือก';

$string['taxonomy_selected_value'] = 'ค่าที่เลือก';

$string['template'] = 'แม่แบบแผนการเรียนรู้';

$string['templatebased'] = 'ตามเทมเพลต';

$string['templatecohortnotsyncedwhileduedateispassed'] = 'กลุ่มประชากรตามรุ่นจะไม่ซิงโครไนซ์หากเลยวันครบกำหนดของเทมเพลตไปแล้ว';

$string['templatecohortnotsyncedwhilehidden'] = 'กลุ่มประชากรตามรุ่นจะไม่ถูกซิงโครไนซ์ในขณะที่แม่แบบนี้ซ่อนอยู่';

$string['templatecompetencies'] = 'สมรรถนะแม่แบบแผนการเรียนรู้';

$string['templatecreated'] = 'สร้างเทมเพลตแผนการเรียนรู้แล้ว';

$string['templatename'] = 'ชื่อ';

$string['templates'] = 'เทมเพลตแผนการเรียนรู้';

$string['templateupdated'] = 'อัปเดตเทมเพลตแผนการเรียนรู้แล้ว';

$string['totalrequiredtocomplete'] = 'จำนวนรวมที่ต้องดำเนินการให้เสร็จสมบูรณ์';

$string['unlinkcompetencycourse'] = 'ยกเลิกการลิงก์ความสามารถ "{$a}" จากหลักสูตรหรือไม่';

$string['unlinkcompetencyplan'] = 'ยกเลิกการเชื่อมโยงความสามารถ "{$a}" จากแผนการเรียนรู้หรือไม่';

$string['unlinkcompetencytemplate'] = 'ยกเลิกการเชื่อมโยงความสามารถ "{$a}" จากเทมเพลตแผนการเรียนรู้หรือไม่';

$string['unlinkplanstemplate'] = 'ยกเลิกการลิงก์แผนการเรียนรู้จากเทมเพลต';

$string['unlinkplantemplate'] = 'ยกเลิกการลิงก์จากเทมเพลตแผนการเรียนรู้';

$string['unlinkplantemplateconfirm'] = 'ยกเลิกการลิงก์แผนการเรียนรู้ "{$a}" จากเทมเพลตหรือไม่ การเปลี่ยนแปลงใด ๆ ที่เกิดขึ้นกับเทมเพลตจะไม่มีผลกับแผนอีกต่อไป การดำเนินการนี้ไม่สามารถยกเลิกได้';

$string['uponcoursecompletion'] = 'เมื่อจบหลักสูตร:';

$string['uponcoursemodulecompletion'] = 'เมื่อเสร็จสิ้นกิจกรรม:';

$string['usercompetencyfrozen'] = 'ขณะนี้บันทึกนี้ถูกตรึง สะท้อนให้เห็นถึงสถานะความสามารถของผู้ใช้เมื่อแผนการเรียนรู้ของพวกเขาถูกทำเครื่องหมายว่าเสร็จสมบูรณ์';

$string['userevidence'] = 'หลักฐานการเรียนรู้ก่อน';

$string['userevidencecreated'] = 'สร้างหลักฐานการเรียนรู้ก่อนหน้านี้';

$string['userevidencedescription'] = 'คำอธิบาย';

$string['userevidencefiles'] = 'ไฟล์';

$string['userevidencename'] = 'ชื่อ';

$string['userevidencesummary'] = 'สรุป';

$string['userevidenceupdated'] = 'อัปเดตหลักฐานการเรียนรู้ก่อนหน้าแล้ว';

$string['userevidenceurl'] = 'URL';

$string['userevidenceurl_help'] = 'URL ต้องขึ้นต้นด้วย "http: //" หรือ "https: //"';

$string['viewdetails'] = 'ดูรายละเอียด';

$string['visible'] = 'มองเห็นได้';

$string['visible_help'] = 'กรอบความสามารถสามารถซ่อนอยู่ในขณะที่กำลังตั้งค่าหรืออัปเดตเป็นเวอร์ชันใหม่';

$string['when'] = 'เมื่อไหร่';

$string['xcompetencieslinkedoutofy'] = 'ความสามารถ {$a->x} จาก {$a->y} ที่เชื่อมโยงกับหลักสูตร';

$string['xcompetenciesproficientoutofy'] = 'มีความเชี่ยวชาญ {$a->x} จาก {$a->y}';

$string['xcompetenciesproficientoutofyincourse'] = 'คุณมีความเชี่ยวชาญ {$a->x} จากความสามารถ {$a->y} ในหลักสูตรนี้';

$string['xplanscompletedoutofy'] = 'แผนการเรียนรู้ {$a->x} จาก {$a->y} เสร็จสมบูรณ์สำหรับเทมเพลตนี้';

$string['privacy:metadata'] = 'ปลั๊กอินแผนการเรียนรู้ไม่เก็บข้อมูลส่วนบุคคลใด ๆ';
