<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'journal', language 'th', version '3.9'.
 *
 * @package     journal
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['alwaysopen'] = 'อ่านได้ตลอด';
$string['blankentry'] = 'ข้อมูลว่าง';
$string['daysavailable'] = 'วันที่เริ่มให้เข้าอ่าน';
$string['editingended'] = 'ระยะเวลาในการแก้ไขหมดแล้ว';
$string['editingends'] = 'ระยะเวลาแก้ไขหมดใน';
$string['entries'] = 'งานเขียน';
$string['feedbackupdated'] = 'อัพเดท feedback สำหรับ {$a} บันทึกความก้าวหน้าที่เข้ามาแล้ว';
$string['journalmail'] = '{$a->teacher} ได้ทำการตรวจบันทึกความก้าวหน้า \'{$a->journal}\' ของคุณแล้ว
คุณสามารถดูผลการตรวจได้ที่ :{$a->url}';
$string['journalmailhtml'] = '{$a->teacher} ได้ทำการตรวจบันทึกความก้าวหน้า
\'<i>{$a->journal}</i>\' ของคุณแล้ว <br /><br />

คุณสามารถดูผลการตรวจได้ที่:<a href="{$a->url}">ผลการตรวจ</a>.';
$string['journalname'] = 'ชื่อบันทึกความก้าวหน้า';
$string['journalquestion'] = 'คำถาม';
$string['mailsubject'] = 'ความเห็นต่อบันทึกความก้าวหน้า';
$string['modulename'] = 'บันทึกความก้าวหน้า';
$string['modulenameplural'] = 'บันทึกความก้าวหน้า';
$string['newjournalentries'] = 'บันทึกมาใหม่';
$string['noentry'] = 'ยังไม่มีบันทึก';
$string['noratinggiven'] = 'ไม่มีการให้คะแนน';
$string['notopenuntil'] = 'บันทึกความก้าวหน้านี้จะเข้าอ่านไม่ได้ จนกว่า';
$string['notstarted'] = 'คุณยังไม่ได้เริ่มเขียนบันทึกนี้';
$string['overallrating'] = 'คะแนนโดยรวม';
$string['rate'] = 'ให้คะแนน';
$string['saveallfeedback'] = 'บันทึก feedback ทั้งหมด';
$string['startoredit'] = 'เริ่มเขียนหรือ แก้ไขบันทึกความก้าวหน้า';
$string['viewallentries'] = 'ดู {$a}  บันทึกที่เข้าใหม่';
