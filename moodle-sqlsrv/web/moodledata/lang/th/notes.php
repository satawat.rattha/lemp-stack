<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'notes', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    core_notes
 * @subpackage notes
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

$string['addnewnote'] = 'เพิ่มบันทึกใหม่';
$string['addbulknote'] = 'เพิ่มบันทึกใหม่ให้กับผู้คน {$a} คน';
$string['addbulknotesingle'] = 'เพิ่มบันทึกใหม่ให้กับ 1 คน';
$string['addbulknotedone'] = 'เพิ่มโน้ตให้กับ {$a} คนแล้ว';
$string['addbulknotedonesingle'] = 'เพิ่มหมายเหตุถึง 1 คนแล้ว';
$string['addnewnoteselect'] = 'เลือกผู้ใช้ที่จะเขียนบันทึก';
$string['bynameondate'] = 'โดย {$a->name} - {$a->date}';
$string['configenablenotes'] = 'เปิดใช้งานการจัดเก็บบันทึกเกี่ยวกับผู้ใช้แต่ละคน';
$string['content'] = 'เนื้อหา';
$string['course'] = 'แน่นอน';
$string['coursenotes'] = 'หมายเหตุหลักสูตร';
$string['created'] = 'สร้างขึ้น';
$string['deleteconfirm'] = 'ลบบันทึกนี้?';
$string['deletenotes'] = 'ลบบันทึกทั้งหมด';
$string['editnote'] = 'แก้ไขบันทึก';
$string['enablenotes'] = 'เปิดใช้งานบันทึก';
$string['eventnotecreated'] = 'สร้างโน้ตแล้ว';
$string['eventnoteupdated'] = 'อัปเดตโน้ตแล้ว';
$string['eventnotedeleted'] = 'ลบโน้ตแล้ว';
$string['eventnotesviewed'] = 'ดูโน้ตแล้ว';
$string['invalidid'] = 'ระบุรหัสบันทึกไม่ถูกต้อง';
$string['invaliduserid'] = 'รหัสผู้ใช้ไม่ถูกต้อง : {$a}';
$string['myprofileownnotes'] = 'บันทึกของฉัน';
$string['nocontent'] = 'หมายเหตุเนื้อหาต้องไม่ว่างเปล่า';
$string['nonotes'] = 'ยังไม่มีบันทึกประเภทนี้';
$string['nopermissiontodelete'] = 'คุณไม่สามารถลบบันทึกนี้ได้';
$string['note'] = 'บันทึก';
$string['notes'] = 'บันทึก';
$string['notesdisabled'] = 'โน้ตถูกปิดใช้งานขออภัย';
$string['notesnotvisible'] = 'คุณไม่ได้รับอนุญาตให้ดูบันทึก';
$string['nouser'] = 'คุณต้องเลือกผู้ใช้';
$string['page-notes-x'] = 'หน้าหมายเหตุใด ๆ';
$string['page-notes-index'] = 'หน้าหลักของ บันทึก';
$string['personal'] = 'ส่วนตัว';
$string['personalnotes'] = 'บันทึกส่วนตัว';
$string['privacy:metadata:core_notes'] = 'คอมโพเนนต์ บันทึก เก็บบันทึกย่อของผู้ใช้ภายในระบบย่อยหลัก';
$string['privacy:metadata:core_notes:content'] = 'เนื้อหาของบันทึกย่อ';
$string['privacy:metadata:core_notes:courseid'] = 'รหัสของหลักสูตรที่เกี่ยวข้องกับบันทึกย่อ';
$string['privacy:metadata:core_notes:created'] = 'วันที่ / เวลาที่สร้างสำหรับบันทึกย่อ';
$string['privacy:metadata:core_notes:lastmodified'] = 'วันที่ / เวลาที่แก้ไขล่าสุดสำหรับบันทึกย่อ';
$string['privacy:metadata:core_notes:publishstate'] = 'สถานะการเผยแพร่ของบันทึกย่อ';
$string['privacy:metadata:core_notes:userid'] = 'ID ของผู้ใช้ที่เชื่อมโยงกับบันทึก';
$string['publishstate'] = 'บริบท';
$string['publishstate_help'] = 'บริบทของโน้ตเป็นตัวกำหนดว่าใครสามารถเห็นโน้ตในการใช้งานในชีวิตประจำวัน ผู้ใช้ควรทราบว่าหมายเหตุทั้งหมดรวมถึงบันทึกส่วนตัวอาจถูกเปิดเผยภายใต้กฎหมายของเขตอำนาจศาลของตน

* ส่วนบุคคล - โน้ตจะปรากฏให้คุณเห็นเท่านั้น
* หลักสูตร - ผู้สอนจะมองเห็นบันทึกย่อในหลักสูตรนี้
* ไซต์ - ผู้สอนจะมองเห็นบันทึกย่อในทุกหลักสูตร';
$string['site'] = 'ไซต์';
$string['sitenotes'] = 'บันทึก ไซต์';
$string['unknown'] = 'ไม่ทราบ';
