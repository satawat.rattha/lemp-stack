<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_loginas', language 'th', version '3.9'.
 *
 * @package     local_loginas
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['ajaxnext25'] = 'ถัดไป 25 >>';
$string['ajaxoneuserfound'] = 'พบสมาชิก 1 คน';
$string['ajaxprev25'] = '<< ย้อนกลับ 25';
$string['ajaxxusersfound'] = 'พบสมาชิก {$a}  คน';
$string['configcourseusers'] = 'แสดงลิงก์ เข้าสู่ระบบเป็น ในหน้ารายชื่อนักเรียนในระดับรายวิชา';
$string['configloginasusernames'] = 'ใช้เครื่องหมายจุลภาค (,)  แยกชื่อผู้ใช้ ( เช่น johndoh,janeduh)  เพื่อใช้แสดงในเมนู เข้าสู่ระบบเป็น ในการตั้งค่าการนำทาง';
$string['configloginasusers'] = 'ใช้เครื่องหมายจุลภาค (,)  แยก หมายเลขประจำตัวสมาชิก ( เช่น 101,1212)  เพื่อใช้แสดงในเมนู เข้าสู่ระบบเป็น ในการตั้งค่าการนำทาง';
$string['courseusers'] = 'สมาชิกรายวิชา';
$string['errajaxsearch'] = 'เกิดข้อผิดพลาดในการค้นหาสมาชิก';
$string['loginasuser'] = 'เข้าสู่ระบบในฐานะสมาชิก';
$string['loginasusernames'] = 'เข้าสู่ระบบเป็นสมาชิกโดยใช้  ชื่อผู้ใช้';
$string['loginasusers'] = 'เข้าสู่ระบบเป็นผู้ใช้ โดยใช้หมายเลขประจำตัว';
$string['pluginname'] = 'เข้าสู่ระบบเป็น';
$string['unknowajaxaction'] = 'คำขอผิดพลาด';
