<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignment', language 'th', version '3.9'.
 *
 * @package     assignment
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['assignment:addinstance'] = 'เพิ่มการบ้านใหม่';
$string['assignment:exportownsubmission'] = 'ส่งออกการบ้านของคุณ';
$string['assignment:exportsubmission'] = 'ส่งออกการบ้าน';
$string['assignment:grade'] = 'คะแนนการบ้าน';
$string['assignment:submit'] = 'ส่งการบ้าน';
$string['assignment:view'] = 'ดูการบ้าน';
$string['messageprovider:assignment_updates'] = 'การแจ้งเตือน การบ้าน (2.2)';
$string['modulename'] = 'การบ้าน 2.2 (ถูกปิดใช้งาน)';
$string['modulename_help'] = '<p><img valign="middle" SRC="<?php echo $CFG->wwwroot?>/mod/assignment/icon.gif">&nbsp;<b>การบ้าน</b></p>
<ul>
<p>เป็นส่วนที่ครูมอบหมายการบ้านให้นักเรียนทำ (เป็นเนื้อหาดิจิตอลในรูปแบบใดก็ได้) จากนั้น นักเรียนส่งชิ้นงานโดยการอัพโหลดไฟล์ขึ้นสู่เซิร์ฟเวอร์ ตัวอย่างการบ้าน ได้แก่ เรียงความ งานโปรเจ็คท์ รายงาน และอื่นๆ ซึ่งส่วนนี้จะประมวลผลการให้คะแนนนักเรียนได้ด้วย
</p>
</ul>';
$string['modulenameplural'] = 'การบ้าน 2.2 (ถูกปิดใช้งาน)';
$string['pluginadministration'] = 'การจัดการ การบ้าน 2.2 (ถูกปิดใช้งาน)';
$string['pluginname'] = 'การบ้าน 2.2 (ถูกปิดใช้งาน)';
