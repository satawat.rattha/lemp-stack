<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'moodle', language 'th', version '3.9'.
 *
 * @package     moodle
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['abouttobeinstalled'] = 'กำลังจะทำการติดตั้ง';
$string['accept'] = 'ยอมรับ';
$string['action'] = 'การกระทำ';
$string['actionchoice'] = 'ต้องการทำอย่างไรกับไฟล์ \'{$a}\'?';
$string['actions'] = 'การกระทำ';
$string['actionsmenu'] = 'เมนูการกระทำ';
$string['active'] = 'ใช้งาน';
$string['activeusers'] = 'สมาชิกที่ใช้งานอยู่';
$string['activities'] = 'กิจกรรมทั้งหมด';
$string['activities_help'] = 'เปิดการใช้งานกิจกรรมเช่น กระดานเสวนา แบบทดสอบ วิกิ เปิดการใช้งานเพื่อให้สามารถเพิ่มเข้าไปในรายวิชาได้';
$string['activity'] = 'กิจกรรม';
$string['activityclipboard'] = 'กำลังย้ายกิจกรรมนี้ : {$a}';
$string['activityiscurrentlyhidden'] = 'ขออภัย กิจกรรมนี้ยังไม่เปิดใช้งาน';
$string['activitymodule'] = 'กิจกรรมที่ใช้งาน';
$string['activitymodules'] = 'ชุดกิจกรรม';
$string['activityreport'] = 'รายงานผลกิจกรรม';
$string['activityreports'] = 'รายงานผลกิจกรรม';
$string['activityselect'] = 'ย้ายกิจกรรมที่เลือกไปไว้ที่อื่น';
$string['activitysince'] = 'กิจกรรมตั้งแต่ {$a}';
$string['activitytypetitle'] = '{$a->activity} - {$a->type}';
$string['activityweighted'] = 'กิจกรรมต่อสมาชิก';
$string['add'] = 'เพิ่ม';
$string['addactivity'] = 'เพิ่มกิจกรรม...';
$string['addactivitytosection'] = 'เพิ่มกิจกรรม \'{$a}\'  เข้าไปในส่วนนี้';
$string['addadmin'] = 'เพิ่มผู้ดูแลระบบ';
$string['addblock'] = 'เพิ่มบล็อค';
$string['addcomment'] = 'เพิ่มความคิดเห็น...';
$string['addcountertousername'] = 'สร้างสมาชิกโดยการเพิ่มตัวเลขเข้าไปในชื่อผู้ใช้';
$string['addcreator'] = 'เพิ่มผู้สร้างรายวิชา';
$string['adddots'] = 'เพิ่ม...';
$string['added'] = 'เพิ่ม {$a} แล้ว';
$string['addedrecip'] = 'ได้เพิ่ม  {$a}  เป็นผู้รับใหม่';
$string['addedrecips'] = 'ได้เพิ่ม  {$a}  เป็นผู้รับใหม่';
$string['addedtogroup'] = 'ได้เพิ่มลงในกลุ่ม "{$a}" เรียบร้อยแล้ว';
$string['addedtogroupnot'] = 'ยังไม่ได้เพิ่มเข้าไปในกลุ่ม "{$a}"';
$string['addedtogroupnotenrolled'] = 'ยังไม่ได้เพิ่มเข้าไปยังกลุ่ม "{$a}" เพราะยังไม่ได้สมัครเข้าเรียนในวิชานี้';
$string['addfilehere'] = 'เพิ่มไฟล์ที่นี่';
$string['addinganew'] = 'กำลังเพิ่ม {$a}';
$string['addinganewto'] = 'กำลังเพิ่ม {$a->what}  ลงใน  {$a->to}';
$string['addingdatatoexisting'] = 'เพิ่มข้อมูลลงในที่มีอยู่แล้ว';
$string['additionalnames'] = 'ชื่อเพิ่มเติม';
$string['addlinkhere'] = 'เพิ่มลิงก์ที่นี่';
$string['addnewcategory'] = 'เพิ่มหมวดหมู่ใหม่';
$string['addnewcourse'] = 'เพิ่มรายวิชา';
$string['addnewuser'] = 'เพิ่มสมาชิก';
$string['addnousersrecip'] = 'เพิ่มสมาชิกที่ไม่สามารถเข้าถึง {$a} ลงในรายชื่อผู้รับ';
$string['addpagehere'] = 'เพิ่มข้อความที่นี่';
$string['addresource'] = 'เพิ่มแหล่งข้อมูล';
$string['addresourceoractivity'] = 'เพิ่มกิจกรรมหรือแหล่งข้อมูล';
$string['addresourcetosection'] = 'เพิ่มแหล่งข้อมูลลงใน เข้าไปในส่วน \'{$a}\'';
$string['address'] = 'ที่อยู่';
$string['addsections'] = 'เพิ่มหมวด';
$string['addstudent'] = 'เพิ่มนักเรียน';
$string['addsubcategory'] = 'สร้างประเภทย่อย';
$string['addteacher'] = 'เพิ่มผู้สอน';
$string['admin'] = 'ผู้ดูแลระบบ';
$string['adminhelpaddnewuser'] = 'เพิ่มสมาชิกใหม่ด้วยตนเอง';
$string['adminhelpassignadmins'] = 'ผู้ดูแลระบบสามารถทำอะไรก็ได้ และไปที่ไหนก็ได้ในเว็บ';
$string['adminhelpassigncreators'] = 'ผู้สร้างรายวิชาสามารถเพิ่มรายวิชาใหม่';
$string['adminhelpassignsiteroles'] = 'กำหนดสิทธิ์/บทบาทในระดับเว็บไซต์ให้กับสมาชิกที่เลือก';
$string['adminhelpassignstudents'] = 'ไปยังรายวิชา และเพิ่มนักเรียนจากเมนูผู้ดูแลระบบ';
$string['adminhelpauthentication'] = 'คุณสามารถใช้บัญชีผู้ใช้ภายในหรือนอกระบบฐานข้อมูล';
$string['adminhelpbackup'] = 'ตั้งค่าการสำรองข้อมูลอัตโนมัติ';
$string['adminhelpconfiguration'] = 'ตั้งค่าการแสดงผลของเว็บไซต์';
$string['adminhelpconfigvariables'] = 'ตั้งค่าตัวแปรที่มีผลต่อการทำงานของเว็บไซต์';
$string['adminhelpcourses'] = 'ระบุรายวิชาและประเภทแล้วมอบหมายผู้รับผิดชอบ';
$string['adminhelpeditorsettings'] = 'กำหนดค่าพื้นฐานของ HTML editor';
$string['adminhelpedituser'] = 'browse รายชื่อสมาชิกและแก้ไขของใครก็ได้';
$string['adminhelpenvironment'] = 'ตรวจสอบเซิร์ฟเวอร์ว่าเหมาะสมกับความต้องการในการติดตั้งโปรแกรม';
$string['adminhelpfailurelogs'] = 'ดูบันทึกการล็อกอินเข้าสู่ระบบที่ไม่สำเร็จ';
$string['adminhelplanguage'] = 'สำหรับตรวจสอบและแก้ไขภาษาปัจจุบัน';
$string['adminhelplogs'] = 'ดูบันทึกกิจกรรมทั้งหมดบนเว็บไซต์';
$string['adminhelpmanageblocks'] = 'จัดการบล็อคและตั้งค่า';
$string['adminhelpmanagedatabase'] = 'เข้าไปฐานข้อมูลโดยตรง ( ระวัง ! )';
$string['adminhelpmanagefilters'] = 'เลือกฟิลเตอร์และตั้งค่าที่เกี่ยวข้อง';
$string['adminhelpmanagemodules'] = 'จัดการติดตั้งโมดูลและตั้งค่าต่าง ๆ';
$string['adminhelpmanageroles'] = 'สร้างและกำหนดบทบาทที่ต้องการใช้งานกับสมาชิก';
$string['adminhelpmymoodle'] = 'ตั้งค่าบล็อค Moodle ของฉันสำหรับสมาชิก';
$string['adminhelpreports'] = 'รายงานการใช้งานทั้งเว็บไซต์';
$string['adminhelpsitefiles'] = 'สำหรับเผยแพร่ไฟล์ทั่วไปหรืออัพโหลดไฟล์ข้อมูลสำรอง';
$string['adminhelpsitesettings'] = 'ระบุให้หน้าแรกของเว็บให้แสดงอย่างไร';
$string['adminhelpstickyblocks'] = 'ตั้งค่าบล็อคหลักที่ใช้สำหรับทั้ง Moodle';
$string['adminhelpthemes'] = 'เลือกว่าต้องการให้เว็บแสดงอย่างไร ( สี, ตัวหนังสือ ฯลฯ )';
$string['adminhelpuploadusers'] = 'นำเข้าสมาชิกใหม่จากไฟล์';
$string['adminhelpusers'] = 'เพิ่มสมาชิกและตั้งค่าการอนุมัติ';
$string['adminhelpxmldbeditor'] = 'การแสดงผลในการแก้ไขไฟล์ XMLDB สำหรับนักพัฒนาเท่านั้น';
$string['administration'] = 'การจัดการระบบ';
$string['administrationsite'] = 'การบริหารไซต์';
$string['administrator'] = 'ผู้ดูแลระบบ';
$string['administratordescription'] = 'ผู้ดูแลระบบสามารถทำอะไรก็ได้ในเว็บไซต์และในทุกรายวิชา';
$string['administrators'] = 'ผู้ดูแลระบบ';
$string['administratorsall'] = 'ผู้ดูแลระบบทั้งหมด';
$string['administratorsandteachers'] = 'ผู้ดูแลระบบและอาจารย์';
$string['advanced'] = 'ชั้นสูง';
$string['advancedfilter'] = 'การค้นหาขั้นสูง';
$string['advancedsettings'] = 'การตั้งค่าชั้นสูง';
$string['afterresource'] = 'ตามหลังแหล่งข้อมูล "{$a}"';
$string['aftersection'] = 'ตาหลังส่วน "{$a}"';
$string['again'] = 'อีกครั้ง';
$string['agelocationverification'] = 'การตรวจสอบอายุและสถานที่';
$string['aimid'] = 'AIM ID';
$string['ajaxuse'] = 'ใช้ AJAX และจาวาสคริปต์';
$string['all'] = 'ทั้งหมด';
$string['allactions'] = 'สิ่งที่ทำแล้วทั้งหมด';
$string['allactivities'] = 'กิจกรรมทั้งหมด';
$string['allcategories'] = 'ประเภททั้งหมด';
$string['allchanges'] = 'การเปลี่ยนแปลงทั้งหมด';
$string['alldays'] = 'ตลอดเวลา';
$string['allfieldsrequired'] = 'กรุณาเติมให้ครบทุกช่อง';
$string['allfiles'] = 'ไฟล์ทั้งหมด';
$string['allgroups'] = 'กลุ่มทั้งหมด';
$string['alllogs'] = 'บันทึกการใช้งานเว็บทั้งหมด';
$string['allmods'] = '{$a} ทั้งหมด';
$string['allow'] = 'อนุญาต';
$string['allowinternal'] = 'ให้ใช้วิธีอนุมัติภายในได้ด้วยเช่นกัน';
$string['allownone'] = 'ไม่อนุญาต';
$string['allownot'] = 'ไม่อนุญาต';
$string['allowstealthmodules'] = 'อนุญาตกิจกรรมที่ไม่เปิดเผย';
$string['allparticipants'] = 'สมาชิกทั้งหมด';
$string['allteachers'] = 'อาจารย์ทั้งหมด';
$string['alphanumerical'] = 'สามารถเพิ่มได้เฉพาะตัวอักษรและตัวเลข, ยัติภังค์ (-) หรือ มหัพภาค (.) เท่านั้น';
$string['alreadyconfirmed'] = 'ยืนยันการลงทะเบียนเรียบร้อยแล้ว';
$string['alternatename'] = 'ชื่ออื่น';
$string['always'] = 'ตลอดเวลา';
$string['and'] = '{$a->one} และ {$a->two}';
$string['answer'] = 'คำตอบ';
$string['any'] = 'อันไหนก็ได้';
$string['appearance'] = 'การแสดงผล';
$string['approve'] = 'เห็นควร';
$string['areyousure'] = 'แน่ใจหรือไม่?';
$string['areyousuretorestorethis'] = 'ต้องการทำต่อไปหรือไม่';
$string['areyousuretorestorethisinfo'] = 'หลังจากขั้นตอนนี้จะมีตัวเลือกให้เลือกว่าต้องการเพิ่มข้อมูลสำรองลงในรายวิชาที่มีอยู่หรือว่าสร้างรายวิชาใหม่';
$string['asc'] = 'เรียงจากมากไปน้อย';
$string['assessment'] = 'การประเมิน';
$string['assignadmins'] = 'เพิ่มผู้ดูแลระบบ';
$string['assigncreators'] = 'เพิ่มผู้สร้างรายวิชา';
$string['assignedrolecount'] = '{$a->role}: {$a->count}';
$string['assignsiteroles'] = 'กำหนดบทบาทสำหรับทั้งเว็บไซต์';
$string['authenticateduser'] = 'สมาชิกที่ได้รับการอนุมัติสิทธิ์';
$string['authenticateduserdescription'] = 'สมาชิกที่เข้าสู่ระบบทั้งหมด';
$string['authentication'] = 'การอนุมัติ';
$string['authenticationplugins'] = 'ปลั้กอินการรับรอง';
$string['autosubscribe'] = 'สมัครเป็นสมาชิกกระดานเสวนาอัตโนมัติ';
$string['autosubscribeno'] = 'ไม่ : อย่าสมัครเป็นสมาชิกกระดานใด ๆ ให้ฉันอัตโนมัติ';
$string['autosubscribeyes'] = 'ใช่: สมัครเป็นสมาชิกกระดานทันทีที่ทำการการโพสต์กระทู้';
$string['availability'] = 'ที่ใช้ได้';
$string['availablecourses'] = 'รายวิชาที่มีอยู่';
$string['back'] = 'กลับ';
$string['backto'] = 'กลับไปยัง {$a}';
$string['backtocourselisting'] = 'กลับไปยังหน้าแสดงรายการ รายวิชาทั้งหมด';
$string['backtohome'] = 'กลับสุ่หน้าหลักไซต์';
$string['backtopageyouwereon'] = 'กลับไปยังหน้าที่เข้าไปก่อนหน้า';
$string['backtoparticipants'] = 'กลับไปยังหน้ารายชื่อสมาชิกทั้งหมด';
$string['backup'] = 'การสำรองข้อมูล';
$string['backupactivehelp'] = 'เบือกว่าต้องการทำการสำรองช้อมูลอัตโนมัติหรือไม่';
$string['backupcancelled'] = 'ยกเลิกการสำรองข้อมูลแล้ว';
$string['backupcoursefileshelp'] = 'หากเลือก "ใช่"  ไฟล์ทั้งหมดในรายวิชานั้น ๆ จะมีการสำรองข้อมูลอัตโนมัติ';
$string['backupdate'] = 'วันที่ทำการสำรองข้อมูล';
$string['backupdatenew'] = '  {$a->TAG} ขณะนี้คือ {$a->weekday}, {$a->mday} {$a->month} {$a->year}<br />';
$string['backupdateold'] = '{$a->TAG} เคยเมื่อ {$a->weekday}, {$a->mday} {$a->month} {$a->year}';
$string['backupdaterecordtype'] = '<br />{$a->recordtype} - {$a->recordname}<br />';
$string['backupdetails'] = 'รายละเอียดของการสำรองข้อมูล';
$string['backuperrorinvaliddestination'] = 'ไม่สามารถเขียนข้อมูลลงในแฟ้มปลายทางที่ต้องการสำรองข้อมูล หรือ ไม่มีแฟ้มนั้นอยู่';
$string['backupexecuteathelp'] = 'เลือกเวลาที่ต้องการสำรองข้อมูล';
$string['backupfailed'] = 'บางรายวิชายังไม่ได้รับการบันทึก';
$string['backupfilename'] = 'ชื่อไฟล์ข้อมูลสำรอง';
$string['backupfinished'] = 'การสำรองข้อมูลเสร็จสิ้นค่ะ';
$string['backupfromthissite'] = 'การสำรองข้อมูลที่ทำในเว็บไซต์นี้';
$string['backupgradebookhistoryhelp'] = 'หากเปิดการใช้งาน จะมีการรวมประวัติของคะแนนที่ได้ลงในระบบสำรองข้อมูลอัตโนมัติ  หมายเหตุ:  ไม่ควรปิดการใช้งานประวัติของคะแนนที่ได้ ในหน้าการตั้งค่าของระบบ หากต้องการใช้ฟังก์ชันนี้';
$string['backupincludemoduleshelp'] = 'เลือกว่าต้องการใส่ข้อมูลของสมาชิกของแต่ละรายวิชาลงในการสำรองข้อมูลอัตโนมัติหรือไม่';
$string['backupincludemoduleuserdatahelp'] = 'เลือกว่าต้องการรวมข้อมูลของสมาชิกในแต่ละโมดูลเข้าไปในการสำรองช้อมูลหรือไม่';
$string['backuplogdetailed'] = 'บันทึกรายละเอียดการสำรองข้อมูล';
$string['backuploglaststatus'] = 'บันทึกการสำรองข้อมูลครั้งล่าสุด';
$string['backupmissinguserinfoperms'] = 'หมายเหตุ : การสำรองข้อมูลไม่รวมข้อมูลของสมาชิก  แบบฝึกหัด และ เวิร์กชอป จะไม่รวมอยู่ในการสำรองข้อมูล เนื่องจากโมดูลดังกล่าวไม่เข้ากับประเภทของการสำรองข้อมูลนี้';
$string['backupnext'] = 'การสำรองข้อมูลถัดไป';
$string['backupnonisowarning'] = 'คำเตือน : ข้อมูลสำรองนี้มาจากมูเดิ้ลเวอร์ชันที่ไม่เป็น unicode ( เวอร์ชันก่อนหน้า 1.6) ข้อมูลดังกล่าวประกอบไปด้วยชุดอักขระที่ไม่ใช่ IS-8859-1  อาจทำให้ประสบปัญหาในการกู้คืนได้  ดูรายละเอียดเพิ่มเติมที่<a  href="http://docs.moodle.org/en/Backup_FAQ">FAQ การกู้คืน</a>';
$string['backupnotyetrun'] = 'การสำรองข้อมูลอัตโนมัติ ยังไม่จบ';
$string['backuporiginalname'] = 'ชื่อของข้อมูลสำรอง';
$string['backuproleassignments'] = 'สำรองการกำหนดระดับการเข้าถึง ของระดับต่าง ๆ เหล่านี้';
$string['backupsavetohelp'] = 'ใส่ชื่อ Path เต็มของไดเรกทอรีที่ต้องการบันทึกไฟล์ข้อมูลสำรอง';
$string['backupsitefileshelp'] = 'ถ้าเปิดการทำงาน ไฟล์ของเว็บไซต์ที่ใช้ในรายวิชา จะถูกสำรองอัตโนมัติ';
$string['backuptakealook'] = 'คุณสามารถเปิดดูบันทึกการสำรองข้อมูลใน {$a}';
$string['backupuserfileshelp'] = 'เลือกว่าต้องการบันทึกข้อมูลของสมาชิกลงไปในการสำรองข้อมูลด้วยหรือไม่';
$string['backupversion'] = 'เวอร์ชั่นของข้อมูลสำรอง';
$string['badges'] = 'เครื่องหมาย';
$string['block'] = 'บล็อค';
$string['blockconfiga'] = 'ตั้งค่าบล็อค{$a}';
$string['blockconfigbad'] = 'บล็อคนี้ได้รับการเพิ่มไม่ถูกวิธีจึงไม่สามารถแสดงหน้าการตั้งค่าได้';
$string['blocks'] = 'บล็อค';
$string['blocksaddedit'] = 'เพิ่ม/แก้ไขบล็อค';
$string['blockseditoff'] = 'ปิดการแก้ไขบล็อค';
$string['blocksediton'] = 'เปิดการแก้ไขบล็อค';
$string['blocksetup'] = 'ตั้งค่าตารางบล็อค';
$string['blocksuccess'] = 'ตั้งค่าเสร็จสิ้นแล้ว {$a}  ตาราง';
$string['brief'] = 'อย่างย่อ';
$string['bulkactions'] = 'สิ่งที่ต้องการทำทั้งหมด';
$string['bulkactionselect'] = 'สิ่งที่ต้องการทำทั้งหมด {$a}';
$string['bulkmovecoursessuccess'] = 'ทำการย้ายรายวิชา  {$a->courses} ไปยัง {$a->category} สำเร็จแล้ว';
$string['bycourseorder'] = 'เรียงตามลำดับรายวิชา';
$string['byname'] = 'โดย {$a}';
$string['bypassed'] = 'ข้าม';
$string['cachecontrols'] = 'การควบคุมแคช';
$string['cancel'] = 'ยกเลิก';
$string['cancelled'] = 'ยกเลิก';
$string['categories'] = 'ประเภทของรายวิชา';
$string['categoriesandcourses'] = 'ประเภทของรายวิชาและรายวิชา';
$string['category'] = 'ประเภท';
$string['categoryadded'] = 'เพิ่มประเภท \'{$a}\' เรียบร้อยแล้ว';
$string['categorybulkaction'] = 'สิ่งที่ต้องการทำทั้งหมด สำหรับประเภทที่เลือก';
$string['categorycontents'] = 'ประเภทรายวิชาย่อยและรายวิชาทั้งหมด';
$string['categorycurrentcontents'] = 'เนื้อหาของ  {$a}';
$string['categorydeleted'] = 'ลบประเภท \'{$a}\'เรียบร้อยแล้ว';
$string['categoryduplicate'] = 'ประเภท \'{$a}\' มีอยู่แล้ว';
$string['categoryhidden'] = '(ซ่อน)';
$string['categorymodifiedcancel'] = 'แก้ไขประเภทเรียบร้อยแล้ว กรุณาทดลองใหม่อีกครั้ง';
$string['categoryname'] = 'ชื่อประเภท';
$string['categorysubcategoryof'] = '{$a->category} - ประเภทย่อยของ {$a->parentcategory}';
$string['categoryupdated'] = 'ประเภท \'{$a}\' ได้ถูกอัพเดทแล้ว';
$string['changedpassword'] = 'เปลี่ยนรหัสผ่านเรียบร้อยแล้ว';
$string['changepassword'] = 'เปลี่ยนรหัสผ่าน';
$string['changesmadereallygoaway'] = 'ท่านได้ทำการเปลี่ยนแปลงข้อมูล แน่ใจหรือไม่ว่าต้องการออกจากหน้านี้ ข้อมูลที่เปลี่ยนแปลงทั้งหมดจะหายไป';
$string['changessaved'] = 'บันทึกการเปลี่ยนแปลงแล้ว';
$string['check'] = 'ตรวจสอบ';
$string['checkall'] = 'ตรวจสอบทั้งหมด';
$string['checkingbackup'] = 'ตรวจสอบข้อมูลสำรอง';
$string['checkingcourse'] = 'ตรวจสอบรายวิชา';
$string['checkingforbbexport'] = 'ตรวจสอบการส่งออกไปยัง BlackBoard';
$string['checkinginstances'] = 'ตรวจสอบขั้นตอน';
$string['checkingsections'] = 'ตรวจสอบส่วนต่าง ๆ';
$string['checklanguage'] = 'ตรวจสอบภาษา';
$string['checknone'] = 'ไม่ต้องตรวจสอบ';
$string['childcoursenotfound'] = 'ไม่พบรายวิชาที่ต้องการ';
$string['childcourses'] = 'รายวิชาในหลักสูตรร่วม';
$string['choose'] = 'เลือก';
$string['choosecourse'] = 'เลือกรายวิชา';
$string['choosedots'] = 'เลือก ..';
$string['chooselivelogs'] = 'หรือดูกิจกรรมที่มีขณะนี้';
$string['chooselogs'] = 'เลือกไฟล์บันทึกการใช้งานเว็บไซต์ที่ต้องการดู';
$string['choosereportfilter'] = 'เลือกรายงานของฟิลเตอร์ที่ต้องการ';
$string['choosetheme'] = 'เลือกรูปแบบเว็บไซต์';
$string['chooseuser'] = 'เลือกสมาชิก';
$string['city'] = 'จังหวัด';
$string['cleaningtempdata'] = 'ลบข้อมูลชั่วคราว';
$string['clear'] = 'ลบทั้งหมด';
$string['clickhelpiconformoreinfo'] = 'ทำต่อไป  ให้คลิกที่ไอคอนช่วยเหลือเพื่ออ่านบทความทั้งหมด';
$string['clickhere'] = 'คลิกที่นี่';
$string['clicktochangeinbrackets'] = '{$a} คลิกเพื่อเปลี่ยนแปลง';
$string['clicktohideshow'] = 'คลิกเพื่อขยายหรือย่อส่วนนี้';
$string['closebuttontitle'] = 'ปิด';
$string['closewindow'] = 'ปิดหน้าต่าง';
$string['collapse'] = 'ย่อ';
$string['collapseall'] = 'ย่อทั้งหมด';
$string['collapsecategory'] = 'ย่อ {$a}';
$string['commentincontext'] = 'ค้นหาความเห็นในเนื้อหา';
$string['comments'] = 'ความเห็น';
$string['commentscount'] = 'ความเห็น {$a}';
$string['commentsnotenabled'] = 'ยังไม่เปิดใช้งานการแสดงความคิดเห็น';
$string['commentsrequirelogin'] = 'ท่านต้องเข้าสู่ระบบเพื่ออ่านความคิดเห็น';
$string['comparelanguage'] = 'เปรียบเทียบและแก้ไขภาษาปัจจุบัน';
$string['complete'] = 'เสร็จสิ้น';
$string['completereport'] = 'รายงานแบบสมบูรณ์';
$string['configuration'] = 'การตั้งค่า';
$string['confirm'] = 'ยืนยัน';
$string['confirmcheckfull'] = 'คุณแน่ใจจริงๆหรือ ที่ต้องการจะยืนยัน {$a}';
$string['confirmcoursemove'] = 'แน่ใจหรือไม่ว่าต้องการย้ายรายวิชา  ({$a->course}) ไปยังประเภท ({$a->category})?';
$string['confirmdeletesection'] = 'แน่ใจหรือไม่ว่าต้องการลบ"{$a}"  ทั้งหมด รวมทั้งกิจกรรมทั้งหมดที่มีอยู่ในนี้';
$string['confirmed'] = 'ยืนยันการลงทะเบียนแล้ว';
$string['confirmednot'] = 'การลงทะเบียนของคุณยังไม่ได้รับการยืนยัน กรุณาเซ็คกล่องอีเมล์สำหรับอีเมล์ยืนยัน';
$string['considereddigitalminor'] = 'คุณมีอายุน้อยเกินไปที่จะสร้างบัญชีผู้ใช้บนเว็บไซต์นี้';
$string['content'] = 'เนื้อหา';
$string['continue'] = 'ขั้นต่อไป';
$string['continuetocourse'] = 'คลิกที่นี่เพื่อเข้าไปยังรายวิชาของคุณ';
$string['convertingwikitomarkdown'] = 'เปลี่ยน wiki ให้เป็น Markdown';
$string['cookiesenabled'] = 'เว็บบราวเซอร์ที่คุณใช้ต้องอนุญาตให้รับ cookies';
$string['cookiesenabled_help'] = '<p>เว็บไซค์นี้มีคุกกี้อยู่สองชนิด</p>

<p>คุกกี้ที่จำเป็นคือเซสชั่นคุกกี้ ปกติจะเรียกว่า<b>มูเดิ้ลแซสชั่น</b>.  ต้องมีการยอมให้ใช้คุกกี้นี้โดยการไปตั้งค่า allow ไว้ที่เบราเซอร์ของคุณ  เพื่อให้ระบบเก็บชื่อและรหัสผ่านไว้ ให้คุณคลิ๊กไปดูหน้าต่าง ๆ อย่างต่อเนื่อง คุกกี้นี้จะถูกลบไปเมื่อคุณล็อคเอาท์ออกจากมูเดิ้ลไป</p>

<p> คุกกี้ชนิดนี้ทำหน้าที่้อำนวยความสะดวกให้คุณโดยเฉพาะ เรียกว่า  <b>มูเดิ้ลไอดี</b>. คุกกี้้นี้จะเก็บ ชื่อล็อคอินของคุณไว้ในเบราเซอร์ ดังนั้นเมื่อคุณกลับมาเปิดใช้งานเว๊บไซค์นี้เมื่อไหร่ จะเห็นว่าหน้าจอที่ให้มีช่องให้ใสชื่อล็อคอิน จะมีชื่อล็อคอินเนมของคุณใส่รอไว้อยู่แล้ว แต่มีข้อแนะนำว่าคุณไม่ควรเลือกใช้งานคุกกี้ชนิดนี้ เพื่อความปลอดภัย คุณควรพิมพ์ชื่อและรหัสผ่านใหม่ทุกครั้ง </p>';
$string['cookiesenabledonlysession'] = 'เว็บบราวเซอร์ที่คุณใช้ต้องอนุญาตให้รับ cookies';
$string['cookiesenabledonlysession_help'] = 'ไซต์นี้ใช้คุกกี้แบบเซสชั่นเดียว โดยจะเรียกว่า MoodleSession คุณต้องอนุญาตคุกกี้นี้ในเบราว์เซอร์ที่คุณใช้เพื่อให้อยู่ในระบบได้อย่างต่อเนื่อง เมื่อกำลังเรียกดูเว็บไซต์ เมื่อคุณลงชื่อออก หรือปิดเบราว์เซอร์ คุกกี้จะถูกทำลาย (ทั้งในเบราว์เซอร์ของคุณและบนเซิร์ฟเวอร์)';
$string['cookiesnotenabled'] = 'บราวเซอร์ของคุณไม่อนุญาตให้ใช้งาน cookies ค่ะ';
$string['copy'] = 'สำเนา';
$string['copyasnoun'] = 'สำเนา';
$string['copyingcoursefiles'] = 'ทำสำเนาไฟล์ของแต่ละรายวิชา';
$string['copyingsitefiles'] = 'กำลังทำสำเนาไฟล์ของเว็บไซต์ที่ใช้ในรายวิชา';
$string['copyinguserfiles'] = 'ทำสำเนาไฟล์ของสมาชิก';
$string['copyingzipfile'] = 'ทำสำเนา zip ไฟล์';
$string['copyrightnotice'] = 'การแจ้งเกี่ยวกับลิขสิทธิ์';
$string['coresystem'] = 'ระบบ';
$string['cost'] = 'ราคา';
$string['costdefault'] = 'ราคาที่ตั้งไว้';
$string['counteditems'] = '{$a->count} {$a->items}';
$string['country'] = 'ประเทศ';
$string['course'] = 'รายวิชา';
$string['courseadministration'] = 'การจัดการรายวิชา';
$string['courseapprovedemail'] = 'ท่านได้ทำเรื่องขอสร้างรายวิชา {$a->name} ขณะนี้คำขอได้รับการเห็นชอบแล้วและได้รับการแต่งตั้งเป็น {$a->teacher}   หากต้องการเข้าไปยังรายวิชาของท่านให้ไปยัง  {$a->url}';
$string['courseapprovedemail2'] = 'รายวิชา{$a->name} ที่ท่านได้ยื่นคำขอเอาไว้ ได้รับการอนุมัติแล้ว ท่านสามารถเข้าไปยังรายวิชาใหม่ของท่านได้ที่ {$a->url}';
$string['courseapprovedfailed'] = 'มีข้อผิดพลาดในการอนุมัติให้สร้างรายวิชา';
$string['courseapprovedsubject'] = 'อนุมัติให้สร้างรายวิชา';
$string['courseavailable'] = 'นักเรียนสามารถเข้าศึกษารายวิชานี้ได้';
$string['courseavailablenot'] = 'นักเรียนยังไม่สามารถเข้าศึกษารายวิชานี้ได้';
$string['coursebackup'] = 'การสำรองข้อมูล';
$string['coursebulkaction'] = 'ต้องการทำทั้งหมดนี้กับรายวิชาที่เลือก';
$string['coursecategories'] = 'ประเภทของรายวิชา';
$string['coursecategory'] = 'ประเภทของรายวิชา';
$string['coursecategory_help'] = 'การตั้งค่านี้จะประเมินหมวดหมู่ของรายวิชาว่าจะปรากฎในรายการรายวิชาอย่างไรบ้าง';
$string['coursecategorydeleted'] = 'ลบประเภทของรายวิชา  {$a}';
$string['coursecatmanagement'] = 'การจัดการรายวิชาและประเภท';
$string['coursecompletion'] = 'ความสมบูรณ์ของรายวิชา';
$string['coursecompletions'] = 'ความสมบูรณ์ของรายวิชา';
$string['coursecreators'] = 'ผู้สร้างคอร์ส';
$string['coursecreatorsdescription'] = 'ผู้สร้างหลักสูตรสามารถสร้างรายวิชาใหม่และสอนในรายวิชาดังกล่าวได้';
$string['coursedeleted'] = 'ลบประเภท ความสมบูรณ์ของรายวิชา';
$string['coursedetails'] = 'รายละเอียดของรายวิชา';
$string['coursedisplay'] = 'โครงสร้างรายวิชา';
$string['coursedisplay_help'] = 'ตั้งค่านี้เพื่อเลือกแสดงเนื้อหาทั้งหมดในหนึ่งหน้าหรือว่าแบ่งเป็นหลายหน้า';
$string['coursedisplay_multi'] = 'เลือกแสดงหนึ่งหัวข้อในหนึ่งหน้า';
$string['coursedisplay_single'] = 'แสดงหัวข้อทั้งหมดในหนึ่งหน้า';
$string['courseduration'] = 'ระยะเวลาของคอร์ส';
$string['courseduration_desc'] = 'ระยะเวลาของรายวิชาใช้เพื่อการคำนวนว่าจะจบรายวิชาเมื่อใด. วันจบรายวิชาจะถูกใช้เพื่อการรายงานเท่านั้น ผู้ใช้ยังสามารถเข้าไปยังรายวิชาหลังจากวันที่จบรายวิชาได้';
$string['courseenddateenabled'] = 'วันจบรายวิชาถูกเปิดใช้เป็นค่าเริ่มต้น';
$string['courseenddateenabled_desc'] = 'การตั้งค่านี้จะกำหนดถ้าวันจบรายวิชาจะถูกเปิดใช้โดยอัตโนมัติสำหรับรายวิชาใหม่แล้วตั้งค่าวันที่ตามการคำนวณจากระยะเวลาของหลักสูตร';
$string['courseextendednamedisplay'] = '{$a->shortname} {$a->fullname}';
$string['coursefiles'] = 'ไฟล์คอร์สเก่า';
$string['coursefilesedit'] = 'แก้ไขไฟล์ของรายวิชา';
$string['coursefileswarning'] = 'เลิกใช้งานไฟล์ของรายวิชา';
$string['coursefileswarning_help'] = 'นับตั้งแต่ Moodle 2.0  เป็นต้นมา มีการเลิกใช้งาน ไฟล์ของรายวิชา กรุณาใช้ไฟล์จากคลังภายนอกให้มากที่สุดเท่าที่จะทำได้';
$string['courseformatdata'] = 'ข้อมูลรูปแบบของรายวิชา';
$string['courseformatoptions'] = 'รูปแบบรายวิชาสำหรับ {$a}';
$string['courseformats'] = 'รูปแบบ';
$string['courseformatudpate'] = 'อัพเดทรูปแบบ';
$string['coursegrades'] = 'วิธีการให้คะแนน';
$string['coursehelpcategory'] = 'การวางตำแหน่งของรายวิชาในหน้ารายวิชาทั้งหมดจะทำให้นักเรียนหาวิชานี้ได้ง่ายขึ้น';
$string['coursehelpforce'] = 'บังคับให้ใช้งาน โหมดกลุ่มของรายวิชา สำหรับกิจกรรมทั้งหมดในรายวิชา';
$string['coursehelpformat'] = 'หน้าหลักของรายวิชาจะแสดงในรูปแบบนี้';
$string['coursehelphiddensections'] = 'เลือกที่จะแสดงหัวข้อที่ซ่อนไว้ให้นักเรียนเห็นอย่างไร';
$string['coursehelpmaximumupload'] = 'กำหนดขนาดไฟล์ที่ใหญ่ที่สุดที่สามารถอัพโหลดได้ในรายวิชานี้  ( กำหนดไว้ด้วยระบบกลางแล้วระดับหนึ่ง)';
$string['coursehelpnewsitemsnumber'] = 'จำนวนโพสต์ล่าสุดที่ต้องการแสดง ในบล็อคข่าวล่าสุด หากตั้งค่าเป็น 0  บล็อคข่าวล่าสุดจะถูกปิดการใช้งาน';
$string['coursehelpnumberweeks'] = 'จำนวนหัวข้อที่มีอยู่ในรายวิชา ( ใช้ได้สำหรับบางรูปแบบเท่านั้น)';
$string['coursehelpshowgrades'] = 'เปิดการใช้งาน การแสดงผลคะแนน ไม่ได้ทำให้ป้องกันการแสดงผลคะแนนสำหรับกิจกรรมแต่ละอัน';
$string['coursehidden'] = 'รายวิชานี้ยังไม่เปิดให้นักเรียนเข้าศึกษา';
$string['courseinfo'] = 'รายละเอียดรายวิชา';
$string['courselegacyfiles'] = 'การใช้งานไฟล์ของรายวิชา';
$string['courselegacyfiles_help'] = 'การใช้งานไฟล์สำหรับรายวิชานี้เปิดเพื่อให้ใช้วานได้กับเวอร์ชั่น 1.9  หรือก่อนหน้า นักเรียนสามารถเข้าถึงไฟล์เหล่านี้ได้ ไม่ว่าจะทำการลิงก์ไว้หรือไม่ ซึ่งไม่สามารถรู้ได้เลยว่า ไฟล์ดังกล่าวมีการใช้งานเฉพาะใน Moodle หรือไม่

ถ้าหากท่านเปิดการใช้งานในส่วนนี้ จะทำให้มีปัญหาเรื่องความเป็นส่วนตัวและความปลอดภัยในการใช้งาน และไม่สามารถทำการสำรองข้อมูลไฟล์ดังกล่าวได้ อาจทำให้มีปัญหาในการแชร์รายวิชาหรือใช้งานซ้ำในภายหลัง จึงขอแนะนำให้ให้ปิดการใช้งานในส่วนนี้ไว้ นอกจากว่าท่านแน่ใจว่ากำลังทำอะไรอยู่';
$string['courselegacyfilesofcourse'] = 'การใช้งานไฟล์ของรายวิชา : {$a}';
$string['coursemessage'] = 'ส่งข้อความถึงนักเรียนในรายวิชา';
$string['coursenotaccessible'] = 'รายวิชานี้ไม่อนุญาตให้บุคคลทั่วไปเข้าศึกษา';
$string['courseoverview'] = 'ภาพรวมของรายวิชา';
$string['courseoverviewfiles'] = 'ภาพของรายวิชา';
$string['courseoverviewfiles_help'] = 'ภาพรายวิชาอย่างย่อ เช่น รูปภาพ จะแสดงในส่วนภาพรวมของรายวิชาบนแดชบอร์ด นามสกุลไฟล์ที่สามารถใช้ได้เพิ่มเติมและไฟล์มากกว่าหนึ่งชิ้นอาจจะถูกเปิดใช้ได้โดยผู้บริหารไซต์ ซึ่งไฟล์เหล่านี้จะแสดงอยู่บริเวณถัดไปจากสรุปหลักสูตรบนหน้ารายชื่อรายวิชา';
$string['courseoverviewfilesext'] = 'นามสกุลของไฟล์ภาพสำหรับรายวิชา';
$string['courseoverviewfileslimit'] = 'จำนวนไฟล์ภาพสำหรับรายวิชา';
$string['courseoverviewgraph'] = 'กราฟภาพรวมของรายวิชา';
$string['coursepreferences'] = 'การปรับแต่งรายวิชา';
$string['courseprofiles'] = 'โปรไฟล์สำหรับรายวิชา';
$string['coursereasonforrejecting'] = 'เหตุผลที่ปฏิเสธคำขอนี้';
$string['coursereasonforrejectingemail'] = 'ระบบจะส่งเหตุผลดังกล่าวไปยังผู้ยื่นคำร้อง';
$string['coursereject'] = 'ไม่อนุมัติคำขอสร้างรายวิชา';
$string['courserejected'] = 'รายวิชาไม่ผ่านการอนุมัติและได้แจ้งให้ผู้ส่งคำขอแล้ว';
$string['courserejectemail'] = 'ขออภัยค่ะ  คำขอสร้างรายวิชาของท่านไม่ได้รับการอนุมัติด้วยเหตุผลดังนี้';
$string['courserejectreason'] = 'เหตุผลที่ไม่อนุมัติการสร้างรายวิชานี้ <br/> ( ระบบจะส่งอีเมลแจ้งไปยังผู้ขอ)';
$string['courserejectsubject'] = 'รายวิชาที่ขอสร้างไม่ได้รับการอนุมัติ';
$string['coursereport'] = 'รายงานรายวิชา';
$string['coursereports'] = 'รายงานรายวิชา';
$string['courserequest'] = 'คำขอสร้างรายวิชา';
$string['courserequestdetails'] = 'รายละเอียดของรายวิชาที่ท่านยื่นคำร้อง';
$string['courserequestfailed'] = 'ไม่สามารถบันทึกการขอสร้างรายวิชาได้ในขณะนี้';
$string['courserequestintro'] = 'ใช้แบบฟอร์มนี้ในการขอสร้างรายวิชาของท่าน .<br /> กรุณาใส่ข้อมูลและรายละเอียดให้มากที่สุดเท่าที่จะทำได้ เพื่อให้ง่ายต่อการตัดสินใจของผู้ดูแลระบบในการอนุมัติให้สร้างรายวิชา';
$string['courserequestreason'] = 'เหตุผลในการขอสร้างรายวิชา';
$string['courserequestsuccess'] = 'บันทึกการขอสร้างรายวิชาเรียบร้อยแล้ว คุณจะได้รับอีเมล์ติดต่อถึงคุณขึ้นอยู่กับว่าคำร้องขอจะได้อนุมัติหรือไม่ก็ตาม';
$string['courserequestsupport'] = 'ข้อมูลสนับสนุนเพื่อประกอบการพิจารณาในการขอสร้างรายวิชา';
$string['courserequestwarning'] = 'สมาชิกที่ยื่นคำร้องขอสร้างรายวิชา จะได้รับการตอบรับและกำหนดบทบาท : {$a} ให้โดยอัตโนมัติ';
$string['courserestore'] = 'นำรายวิชาที่สร้างไว้มาใช้ใหม่';
$string['courses'] = 'รายวิชาทั้งหมด';
$string['coursesearch'] = 'ค้นหารายวิชา';
$string['coursesearch_help'] = 'คุณสามารถค้นหาคำหลาย ๆ คำในครั้งเดียว  <p> คำ : ค้นหาคำที่อยู่ในข้อความให้ตรงกับคำที่ใส่ <br> +คำ : ค้นหาเฉพาะคำที่ตรงกับที่ระบุเท่านั้น <br>-คำ: ไม่รวมผลการค้นหาที่มีคำ ๆ นี้อยู่';
$string['coursesectionsummaries'] = 'เนื้อหาย่อของแต่ละหัวข้อในรายวิชา';
$string['coursesectiontitle'] = 'รายวิชา : {$a->course}, {$a->sectionname}: {$a->sectiontitle}';
$string['coursesettings'] = 'ค่าที่ตั้งไว้สำหรับรายวิชา';
$string['coursesmovedout'] = 'รายวิชานี้ย้ายออกจาก {$a}';
$string['coursespending'] = 'กำลังรอผลการอนุมัติ';
$string['coursestart'] = 'วันเริ่มต้นรายวิชา';
$string['coursesummary'] = 'เนื้อหาย่อของรายวิชา';
$string['coursesummary_help'] = 'เนื้อหาย่อของรายวิชาจะแสดงไว้ในหน้าแสดงรายการรวมของรายวิชาทั้งหมด  การค้นหารายวิชา';
$string['coursetitle'] = 'รายวิชา: {$a->course}';
$string['courseupdates'] = 'อัพเดทรายวิชา';
$string['coursevisibility'] = 'การมองเห็นรายวิชา';
$string['create'] = 'สร้าง';
$string['createaccount'] = 'สร้างบัญชใหม่';
$string['createcategory'] = 'สร้างหมวดหมู่';
$string['createfolder'] = 'สร้างโฟเดอร์ใน {$a}';
$string['createnew'] = 'สร้างใหม่';
$string['createnewcategory'] = 'สร้างหมวดหมู่ใหม่';
$string['createnewcourse'] = 'สร้างรายวิชาใหม่';
$string['createnewsubcategory'] = 'สร้างหมวดหมู่ย่อยใหม่';
$string['createsubcategoryof'] = 'สร้างหมวดหมู่ย่อยของ {$a}';
$string['createuser'] = 'สร้างผู้ใช้';
$string['createuserandpass'] = 'สร้างชื่อผู้ใช้และรหัสผ่านใหม่';
$string['createziparchive'] = 'สร้างการบีบอัดแบบ zip';
$string['creatingblocks'] = 'สร้างบล็อค';
$string['creatingblocksroles'] = 'กำลังสร้างบทบาทและหน้าที่ สิทธิการเข้าถึง ในระดับบล็อค';
$string['creatingblogsinfo'] = 'กำลังสร้างข้อมูลบล็อก';
$string['creatingcategoriesandquestions'] = 'กำลังสร้างประเภทและคำถาม';
$string['creatingcoursemodules'] = 'กำลังสร้างโมดูลในแต่ละวิชา';
$string['creatingcourseroles'] = 'กำลังสร้างบทบาทและหน้าที่ สิทธิการเข้าถึง ในระดับรายวิชา';
$string['creatingevents'] = 'สร้างกิจกรรมใหม่';
$string['creatinggradebook'] = 'กำลังสร้างสมุดรายงานคะแนน';
$string['creatinggroupings'] = 'สร้างกลุ่ม';
$string['creatinggroupingsgroups'] = 'เพิ่มกลุ่มเข้าไปในการจัดกลุ่ม';
$string['creatinggroups'] = 'สร้างกลุ่มใหม่';
$string['creatinglogentries'] = 'กำลังบันทึกการใช้เว็บไซต์';
$string['creatingmessagesinfo'] = 'กำลังสร้างข้อความ';
$string['creatingmodroles'] = 'กำลังสร้างบทบาทและหน้าที่ สิทธิการเข้าถึง ในระดับโมดูล';
$string['creatingnewcourse'] = 'กำลังสร้างรายวิชาใหม่';
$string['creatingrolesdefinitions'] = 'กำลังสร้างคำจำกัดความของแต่ละบทบาท';
$string['creatingscales'] = 'กำลังสร้างวิธีการวัด';
$string['creatingsections'] = 'กำลังสร้างหัวข้อ';
$string['creatingtemporarystructures'] = 'กำลังสร้างโครงสร้างชั่วคราว';
$string['creatinguserroles'] = 'กำลังสร้างบทบาทและหน้าที่ สิทธิการเข้าถึง ในระดับสมาชิก';
$string['creatingusers'] = 'กำลังสร้างสมาชิก';
$string['creatingxmlfile'] = 'กำลังสร้าง xml ไฟล์';
$string['currency'] = 'หน่วยเงินตรา';
$string['currentcourse'] = 'หน่วยเงินที่ใช้ในรายวิชา';
$string['currentcourseadding'] = 'รายวิชาปัจจุบัน  : เพิ่มข้อมูลเข้าไป';
$string['currentcoursedeleting'] = 'รายวิชาปัจจุบัน : ลบรายวิชาก่อน';
$string['currentlanguage'] = 'ภาษาปัจจุบัน';
$string['currentlocaltime'] = 'เวลาปัจจุบัน';
$string['currentlyselectedusers'] = 'สมาชิกที่เลือกในขณะนี้';
$string['currentpicture'] = 'รูปปัจจุบัน';
$string['currentrelease'] = 'ข้อมูลเวอร์ชั่นปัจจุบัน';
$string['currentversion'] = 'เวอร์ชั่นปัจจุบัน';
$string['databasechecking'] = 'กำลังปรับปรุงฐานข้อมูล moodle จาก เวอร์ชั่น {$a->oldversion} เป็น  {$a->newversion}...';
$string['databaseperformance'] = 'การแสดงผลของฐานข้อมูล';
$string['databasesetup'] = 'ติดตั้งฐานข้อมูล';
$string['databasesuccess'] = 'ฐานข้อมูลได้รับการอัพเกรดแล้ว';
$string['databaseupgradebackups'] = 'เวอร์ชั่นของข้อมูลสำรองคือ {$a}';
$string['databaseupgradeblocks'] = 'บล็อคเวอร์ชั่น {$a}';
$string['databaseupgradegroups'] = 'เวอร์ชันของกลุ่มปัจจุบันคือ {$a}';
$string['databaseupgradelocal'] = 'การปรับปรุงฐานข้อมูลเวอร์ชันปัจจุบันคือ {$a}';
$string['databaseupgrades'] = 'กำลังอัพเกรดฐานข้อมูล';
$string['date'] = 'วันที่';
$string['datechanged'] = 'เปลี่ยนวันที่แล้ว';
$string['datemostrecentfirst'] = 'วันที่  : วันล่าสุดมาก่อน';
$string['datemostrecentlast'] = 'วันที่ : วันล่าสุดอยู่หลัง';
$string['day'] = 'วัน';
$string['days'] = 'วัน';
$string['decodinginternallinks'] = 'สร้างลิงก์ภายใน';
$string['default'] = 'ค่าที่ตั้งไว้';
$string['defaultcoursestudent'] = 'นักเรียน';
$string['defaultcoursestudentdescription'] = 'นักเรียนมักจะมีสิทธิพิเศษน้อยในรายวิชา';
$string['defaultcoursestudents'] = 'นักเรียน';
$string['defaultcoursesummary'] = 'อธิบายสั้นๆ เกี่ยวกับรายวิชาของท่าน';
$string['defaultcourseteacher'] = 'อาจารย์';
$string['defaultcourseteacherdescription'] = 'ผู้สอนสามารถทำอะไรก็ได้ภายในรายวิชา รวมถึงการเปลี่ยนกิจกรรมและให้คะแนนนักเรียน';
$string['defaultcourseteachers'] = 'อาจารย์';
$string['defaulteditor'] = 'ผู้แก้ไขหลัก';
$string['defaulthomepageuser'] = 'หน้าแรกมาตราฐาน';
$string['delete'] = 'ลบ';
$string['deleteablock'] = 'ลบบล็อค';
$string['deleteall'] = 'ลบทั้งหมด';
$string['deleteallcannotundo'] = 'ลบทั้งหมด -  ไม่สามารถทำได้';
$string['deleteallcomments'] = 'ลบความคิดเห็นทั้งหมด';
$string['deleteallratings'] = 'ลบเรตติ้งทั้งหมด';
$string['deletecategory'] = 'ลบประเภททั้งหมด : {$a}';
$string['deletecategorycheck'] = 'แน่ใจหรือไม่ว่าต้องการลบประเภท<b>\'{$a}\'</b> หากแน่ใจระบบจะย้ายทำการย้ายรายวิชาทั้งหมดในประเภทนี้ไปยังประเภทที่เหนือขึ้นหรือย้ายไปประเภททั่วไป';
$string['deletecategorycheck2'] = 'หากท่านลบประเภทนี้ ท่านต้องเลือกว่าจะทำอย่างไรกับประเภทย่อยที่อยู่ภายใน';
$string['deletecategoryempty'] = 'ประเภทนี้ไม่มีเนื้อหาภายใน';
$string['deletecheck'] = 'ลบ {$a} ?';
$string['deletecheckfiles'] = 'แน่ใจหรือไม่ว่าต้องการลบไฟล์นี้';
$string['deletecheckfull'] = 'คุณแน่ใจจริงๆหรือไม่ว่าต้องการลบผู้ใช้ {$a} นี่จะลบข้อมูลที่รวมไปถึงการลงทะเบียนเรียน กิจกรรม และข้อมูลอื่นๆของผู้ใช้ออกด้วย';
$string['deletechecktype'] = 'แน่ใจหรือไม่ว่าต้องการลบประเภท : {$a} ?';
$string['deletechecktypename'] = 'แน่ใจหรือไม่ว่าต้องการลบ {$a->type} "{$a->name}"?';
$string['deletecheckwarning'] = 'คุณกำลังทำการลบไฟล์ต่อไปนี้';
$string['deletecommentbyon'] = 'ลบคอมเม้นซึ่งโพสต์โดย {$a->user} เมื่อ {$a->time}';
$string['deletecompletely'] = 'ลบทั้งหมด';
$string['deletecourse'] = 'ลบรายวิชา';
$string['deletecoursecheck'] = 'แน่ใจนะคะว่าต้องการลบรายวิชานี้และข้อมูลทั้งหมด ในรายวิชานี้?';
$string['deleted'] = 'ลบแล้ว';
$string['deletedactivity'] = 'ลบ {$a}  แล้ว';
$string['deletedcourse'] = '{$a} ลบทั้งหมดแล้ว';
$string['deletednot'] = 'ไม่สามารถลบ {$a} !';
$string['deletepicture'] = 'ลบ';
$string['deletesection'] = 'ลบหัวข้อ';
$string['deleteselected'] = 'ลบที่เลือก';
$string['deleteselectedkey'] = 'ลบรหัสที่เลือก';
$string['deletingcourse'] = 'กำลังลบ  {$a}';
$string['deletingexistingcoursedata'] = 'ลบข้อมูลรายวิชาที่มีอยู่';
$string['deletingolddata'] = 'ลบข้อมูลเก่า';
$string['department'] = 'หมวด/แผนก';
$string['deprecatedeventname'] = '{$a} (ไม่ได้ถูกใช้งานแล้ว)';
$string['desc'] = 'จากน้อยไปมาก';
$string['description'] = 'คำอธิบาย';
$string['descriptiona'] = 'คำอธิบาย: {$a}';
$string['deselectall'] = 'ยกเลิกการเลือกทั้งหมด';
$string['deselectnos'] = 'ไม่เหลือทั้งหมด \'ไม่\'';
$string['detailedless'] = 'รายละเอียดโดยย่อ';
$string['detailedmore'] = 'รายละเอียดทั้งหมด';
$string['digitalminor_desc'] = 'กรุณาขอผู้ปกครอง/ผู้ค้มครองตามกฏหมาย ให้ติดต่อที่:';
$string['directory'] = 'ไดเรกทอรี';
$string['disable'] = 'ปิดการใช้งาน';
$string['disabledcomments'] = 'ปิดการใช้งานความคิดเห็น';
$string['displayingfirst'] = 'แสดง {$a->count} {$a->things} ข้อมูลแรกเท่านั้น';
$string['displayingrecords'] = 'กำลังแสดง {$a}  ข้อมูล';
$string['displayingusers'] = 'กำลังแสดงสมาชิก {$a->start}  ถึง {$a->end}';
$string['displayonpage'] = 'แสดงในหน้า';
$string['dndcourse'] = 'ท่านสามารถลากและวางรายวิชานี้ไปยังประเภทอื่นที่ต้องการ';
$string['dndenabled_inbox'] = 'ท่านสามารถลากและวางไฟล์นี้ที่นี่หากต้องการเพิ่ม';
$string['dndnotsupported'] = 'ไม่สนับสนุนการอัพโหลดโดยใช้วิธีลากและวาง';
$string['dndnotsupported_help'] = 'บราวเซอร์ของท่านไม่สนับสนุนการอัพโหลดโดยวิธีการลากและวาง<br /> ฟีเจอร์นี้มีใช้งานใน Chrome, Firefox และ Safari เวอร์ชั่นล่าสุด และ ใน  Internet Explorer v 10  หรือสูงกว่า';
$string['dndnotsupported_insentence'] = 'ไม่สนับสนุนการใช้งาน ลากแล้ววาง';
$string['dnduploadwithoutcontent'] = 'ไม่มีเนื้อหาที่จะอัพโหลด';
$string['dndworkingfile'] = 'ลากและวางไฟล์ที่ต้องการลงในรายวิชาที่ต้องการอัพโหลด';
$string['dndworkingfilelink'] = 'ลากและวางไฟล์หรือลิงก์ลงในหัวข้อรายวิชาที่ต้องการอัพโหลด';
$string['dndworkingfiletext'] = 'ลากและวางไฟล์หรือข้อความลงในหัวข้อรายวิชาที่ต้องการอัพโหลด';
$string['dndworkingfiletextlink'] = 'ลากและวางไฟล์หรือข้อความหรือลิงก์ลงในหัวข้อรายวิชาที่ต้องการอัพโหลด';
$string['dndworkinglink'] = 'ลากและวางลิงก์ลงในหัวข้อรายวิชาที่ต้องการอัพโหลด';
$string['dndworkingtext'] = 'ลากและวางข้อความลงในหัวข้อรายวิชาที่ต้องการอัพโหลด';
$string['dndworkingtextlink'] = 'ลากและวางข้อความหรือลิงก์ลงในหัวข้อรายวิชาที่ต้องการอัพโหลด';
$string['documentation'] = 'เอกสารของ Moodle';
$string['dontsortcategories'] = 'ไม่เรียงประเภท';
$string['dontsortcourses'] = 'ไม่เรียงรายวิชา';
$string['down'] = 'ลง';
$string['download'] = 'ดาวน์โหลด';
$string['downloadall'] = 'ดาวน์โหลดทั้งหมด';
$string['downloadexcel'] = 'ดาวน์โหลดรูปแบบตาราง  Excel';
$string['downloadfile'] = 'ดาวน์โหลดไฟล์';
$string['downloadods'] = 'ดาวน์โหลดในรูปแบบ ODS';
$string['downloadtext'] = 'ดาวน์โหลดรูปแบบตัวหนังสือ';
$string['doyouagree'] = 'ท่านอ่านและเข้าใจข้อตกลงหรือไม่';
$string['droptoupload'] = 'วางไฟล์ที่ต้องการอัพโหลดที่นี่';
$string['duplicate'] = 'ทำซ้ำ';
$string['duplicatedmodule'] = '{$a} (copy)';
$string['edhelpaspellpath'] = 'หากต้องการใช้ฟังก์ชันตรวจสอบคำผิดภายใน Editor ต้องติดตั้ง  <strong>aspell 0.50</strong>  หรือเวอร์ชันใหม่กว่าบนเซิร์ฟเวอร์และต้องกำหนด path ที่ถูกต้องในการเรียกใช้โปรแกรมดังกล่ว ใน Unix/Linux ปกติแล้ว path จะอยู่ที่ <strong>/usr/bin/aspell</strong>,  แต่ทั้งนี้อาจจะเป็นค่าอื่นก็ได้';
$string['edhelpbgcolor'] = 'ระบุสีของพื้นหลังของพื้นที่ที่ใช้พิมพ์  ตัวอย่างเช่น #ffffff';
$string['edhelpcleanword'] = 'การตั้งค่าเพื่ออนุญาตให้ใช้หรือไม่ให้ใช้ฟิลเตอร์กรองคำ';
$string['edhelpenablespelling'] = 'เปิดหรือปิดการใช้งานการตรวจคำผิด กรณีที่เปิดการใช้งาน จำเป็นต้องติดตั้ง<strong>aspell</strong> ไว้บนเซิร์เวอร์ ค่าที่สองคือ <strong>default dictionary</strong> ซึ่งจะมีการใช้งานเมื่อไม่มีการระบุภาษาสำหรับใช้งานเอาไว้';
$string['edhelpfontfamily'] = 'คุณสมบัติของ font-family คือรายการของชื่อตัวหนังสือแต่ละชนิด  โดยแต่ละชื่อให้คั่นด้วยเครื่องหมายจุลภาค (comma)';
$string['edhelpfontlist'] = 'ระบุตัวหนังสือที่ใช้ในดรอปดาวน์เมนูบน editor';
$string['edhelpfontsize'] = 'ขนาดของตัวหนังสือที่ตั้งไว้  ตัวอย่างเช่น :: medium, large, smaller, larger,  10pt, 11px';
$string['edit'] = 'แก้ไข ';
$string['edita'] = 'แก้ไข {$a}';
$string['editcategorysettings'] = 'การตั้งค่าประเภท';
$string['editcategorythis'] = 'แก้ไขประเภทนี้';
$string['editcoursesettings'] = 'แก้ไขรายวิชา';
$string['editfiles'] = 'แก้ไขไฟล์';
$string['editgroupprofile'] = 'แก้ไขข้อมูลของกลุ่ม';
$string['editinga'] = 'กำลังแก้ไข  {$a}';
$string['editingteachershort'] = 'Editor';
$string['editlock'] = 'ไม่สามารถแก้ไขค่านี้ได้';
$string['editmyprofile'] = 'แก้ไขข้อมูลส่วนตัว';
$string['editorbgcolor'] = 'สีของพื้นหลัง';
$string['editorcleanonpaste'] = 'ล้างความจำในคลิปบอร์ดหลังจากที่วาง (paste)';
$string['editorcommonsettings'] = 'การตั้งค่าทั่วไป';
$string['editordefaultfont'] = 'ตัวหนังสือที่ตั้งว่า';
$string['editorenablespelling'] = 'อนุญาตให้ใช้การสะกดคำ';
$string['editorfontlist'] = 'รายการตัวหนังสือ';
$string['editorfontsize'] = 'ขนาดตัวหนังสือที่ตั้งไว้';
$string['editorpreferences'] = 'การตั้งค่าที่ต้องการ  Editor';
$string['editorresettodefaults'] = 'รีเซ็ทกลับไปเป็นค่าที่ตั้งไว้';
$string['editorsettings'] = 'การตั้งค่า editor';
$string['editorshortcutkeys'] = 'การตั้งค่า Editor';
$string['editsection'] = 'แก้ไขหัวข้อ';
$string['editsectionname'] = 'แก้ไขชื่อหมวด';
$string['editsettings'] = 'แก้ไขการตั้งค่า';
$string['editsummary'] = 'แก้ไขบทคัดย่อ';
$string['editthisactivity'] = 'แก้ไขกิจกรรมนี้';
$string['editthiscategory'] = 'แก้ไขประเภท';
$string['edittitle'] = 'แก้ไขหัวข้อ';
$string['edittitleinstructions'] = 'กด ESC หากต้องการยกเลิก กด Enter  เมื่องเสร็จสิ้น';
$string['edituser'] = 'แก้ไขบัญชีผู้ใช้';
$string['edulevel'] = 'กิจกรรมทั้งหมด';
$string['edulevel_help'] = '* การสอน - การกระทำที่ทำโดยผู้สอน เช่น การปรับปรุงดูแลทรัพยากรแหล่งเรียนรู้
* การเข้าร่วม - การกระทำที่ทำโดยผู้เรียน เช่น การโพสต์ในกระดาน
* อื่น - การกระทำที่ทำโดยผู้ใช้ที่มีบทบาทต่างจากผู้สอนและผุ้เรียน';
$string['edulevelother'] = 'อื่น ๆ';
$string['edulevelparticipating'] = 'เข้าร่วม';
$string['edulevelteacher'] = 'การสอน';
$string['email'] = 'อีเมล';
$string['emailactive'] = 'อีเมลที่ใช้การได้';
$string['emailagain'] = 'ใส่อีเมลอีกครั้ง';
$string['emailalreadysent'] = 'ระบบได้ส่งอีเมลเพื่อ รีเซ็ทรหัสผ่านให้ท่านแล้ว กรุณาเช็คอีเมล';
$string['emailcharset'] = 'ชุดอักขระที่ต้องการใช้ในอีเมล';
$string['emailconfirm'] = 'ยืนยัน account ของคุณ';
$string['emailconfirmation'] = 'สวัสดีค่ะคุณ{$a->firstname},

ท่านได้สมัครเป็นสมาชิกใหม่ในเว็บไซต์ \'{$a->sitename}\'<br> โดยใช้อีเมลนี้ในการสมัคร

กรุณายืนการใช้งานบัญชีผู้ใช้โดยการคลิกที่ลิงก์ด้านล่างนี้ค่ะ <br>

{$a->link}   </br>

ในโปรแกรมรับส่งอีเมลทั่วไปคุณควรจะเห็นลิงก์ด้านบน <br>ปรากฎเป็นสีน้ำเงิน และสามารถคลิกเพื่อทำการยืนยัน<br>บัญชีผู้ใช้ แต่ในกรณีที่ไม่สามารถคลิกที่ลิงก์ได้ <br> ให้ก้อปปี้ลิงก์ดังกล่าวแล้วนำไปวางไว้ในเว็บบราวเซอร์

จากผู้ดูแลระบบ{$a->sitename} ,
{$a->admin}';
$string['emailconfirmationresend'] = 'ส่งอีเมล์ยืนยันใหม่อีกครั้ง';
$string['emailconfirmationsubject'] = '{$a}: การยืนยันบัญชีผู้ใช้';
$string['emailconfirmsent'] = '<P>อีเมลส่งไปให้คุณที่ <B>{$a}</B>
   <P>ประกอบไปด้วยวิธีการลงทะเบียนขั้นสุดท้าย <P>ถ้าหากคุณมีปัญหาในการลงทะเบียน กรุณาติดต่อผู้ดุแลระบบ';
$string['emailconfirmsentfailure'] = 'อีเมล์ยืนยันตนไม่สามารถส่งได้';
$string['emailconfirmsentsuccess'] = 'อีเมล์ยืนยันตนถูกจัดส่งเรียบร้อยแล้ว';
$string['emaildigest'] = 'ประเภทอีเมลไดเจสท์';
$string['emaildigest_help'] = 'อีเมลนี้เป็นค่าที่ตั้งไว้ สำหรับกระดานเสวนา
* ไม่แยกย่อย - ท่านจะได้รับอีเมลหนึ่งฉบับต่อหนึ่งกระดานเสวนา
* แยกย่อย - กระทู้ทั้งหมด - ท่านจะได้รับอีเมลหนึ่งฉบับต่อวัน ซึ่งจะรวมเนื้อหาทั้งหมดสำหรับทุกกระดานเสวนา
* แยกย่อ - ตามหัวข้อ -  ท่านจะได้รับหนึ่งอีเมลต่อวัน ซึ่งจะมีเนื้อหาของหนึ่งหัวข้อ หรือกระทู้ในแต่ละกระดานเสวนา

ท่านสามารถเลือกตั้งค่าของแต่ละกระดานเสวนาให้แตกต่างกัน';
$string['emaildigestcomplete'] = 'แบบสมบูรณ์ (อีเมลเนื้อหาโพสต์ทั้งหมดทุกวัน)';
$string['emaildigestoff'] = 'แบบไม่ไดเจสท์ (อีเมลเดียวต่อกระดานเสวนา)';
$string['emaildigestsubjects'] = 'แบบหัวข้อ( อีเมลเฉพาะหัวข้อทุกวัน)';
$string['emaildisable'] = 'ไม่สามารถใช้อีเมลนี้ได้';
$string['emaildisableclick'] = 'คลิกที่นี่เพื่อทำการระงับการส่งอีเมลไปยังที่อยู่อีเมลนี้';
$string['emaildisplay'] = 'แสดงอีเมล';
$string['emaildisplay_help'] = 'ผู้ใช้ที่มีพิเศษ (เช่น ผู้สอน และ ผู้จัดการ) จะเห็นชื่อที่อยู่เมล์ของคุณเสมอ';
$string['emaildisplaycourse'] = 'สมาชิกในวิชาที่เรียนเท่านั้นที่จะเห็นอีเมล';
$string['emaildisplayhidden'] = 'ซ่อนอีเมล';
$string['emaildisplayno'] = 'ซ่อนอีเมลจากผู้ใช้ที่ไม่มีสิทธิพิเศษ';
$string['emaildisplayyes'] = 'แสดงอีเมล';
$string['emailenable'] = 'อีเมลนี้ใช้งานได้';
$string['emailenableclick'] = 'คลิกที่นี่เพื่อให้ระบบทำการส่งอีเมลไปยังที่อยู่อีเมลนี้';
$string['emailexists'] = 'มีการลงทะเบียนโดยใช้อีเมลนี้แล้ว';
$string['emailexistshintlink'] = 'กู้คืนชื่อผู้ใช้และรหัสผ่านที่ถูกลืม';
$string['emailexistssignuphint'] = 'คุณน่าจะเคยสร้างบัญชีผู้ใช้ในอดีตหรือเปล่า {$a}';
$string['emailformat'] = 'รูปแบบอีเมล';
$string['emailmustbereal'] = 'หมายเหตุ:  อีเมลของท่านต้องเป็นอีเมลจริง';
$string['emailnotallowed'] = 'ที่อยู่อีเมลที่ใช้โดเมนเหล่านี้ไม่สามารถใช้งานได้ ({$a})';
$string['emailnotfound'] = 'ไม่พบอีเมล์นี้ในฐานข้อมูล';
$string['emailonlyallowed'] = 'ไม่อนุญาตให้ใช้อีเมลนี้ค่ะ ({$a})';
$string['emailpasswordchangeinfo'] = 'สวัสดี {$a->firstname},

ระบบได้รับการแจ้งขอรหัสผ่านใหม่จากเว็บไซต์  \'{$a->sitename}\'.

ในการเปลี่ยนรหัสผ่านของคุณให้เข้าไปยังลิงก์ต่อไปนี้นี้ :

{$a->link}

ในโปรแกรมอีเมลทั่วไปลิงก์ดังกล่าวจะปรากฎเป็นตัวหนังสือสีน้ำเงินและสามารถคลิกที่อยู่ดังกล่าวได้เลย แต่ในกรณีที่ไม่เป็นไปตามที่กล่าวมาให้ทำการก้อปปี้ลิงก์ดังกล่าวไปวางไว้ในเว็บบราวเซอร์ของคุณ

หากต้องการความช่วยเหลือให้ติดต่อผู้ดูแลระบบ

{$a->admin}';
$string['emailpasswordchangeinfodisabled'] = 'สวัสดี {$a->firstname},

ระบบได้รับการแจ้งขอรหัสผ่านใหม่จากเว็บไซต์  \'{$a->sitename}\'.

ทางระบบต้องการแจ้งให้ทราบว่าบัญชีผู้ใช้ของคุณถูกระงับไปแล้วและไม่สามารถทำการรีเซ็ทได้อีก กรุณาติดต่อผู้ดูแลระบบ

{$a->admin}';
$string['emailpasswordchangeinfofail'] = 'สวัสดี {$a->firstname},

ระบบได้รับการแจ้งขอรหัสผ่านใหม่จากเว็บไซต์  \'{$a->sitename}\'.

ทางระบบต้องการแจ้งให้ทราบว่าเราไม่สามารถทำการรีเซ็ทรหัสผ่านให้คุณได้ในเว็บไซต์นี้ กรุณาติดต่อผู้ดูแลระบบ

{$a->admin}';
$string['emailpasswordchangeinfosubject'] = '{$a}: ข้อมูลการเปลี่ยนรหัสผ่าน';
$string['emailpasswordconfirmation'] = 'สวัสดีค่ะ/ครับ คุณ{$a->firstname},

ระบบได้รับการแจ้งขอรหัสผ่านใหม่จากเว็บไซต์  \'{$a->sitename}\'.

โปรดยืนยันว่าต้องการให้ส่งรหัสผ่านใหม่ทางอีเมลโดยการคลิกที่ลิงก์ต่อไปนี้ :

{$a->link}

ในโปรแกรมอีเมลทั่วไปลิงก์ดังกล่าวจะปรากฎเป็นตัวหนังสือสีน้ำเงินและสามารถคลิกที่อยู่ดังกล่าวได้เลย แต่ในกรณีที่ไม่เป็นไปตามที่กล่าวมาให้ทำการก้อปปี้ลิงก์ดังกล่าวไปวางไว้ในเว็บบราวเซอร์ของคุณ

หากต้องการความช่วยเหลือให้ติดต่อผู้ดูแลระบบ

{$a->admin}';
$string['emailpasswordconfirmationsubject'] = '{$a}: ยืนยันการเปลี่ยนรหัสผ่าน';
$string['emailpasswordconfirmmaybesent'] = 'ถ้าหากท่านกรอกชื่อผู้ใช้และอีเมลที่ถูกต้องแล้ว ระบบจะทำการส่งอีเมลถึงท่านในทันที ในอีเมลจะประกอบไปด้วยวิธีการยืนยันและวิธีการเปลี่ยนรหัสผ่านของท่าน  ถ้าหากยังพบปัญหากรุณาติดต่อผู้ดูแลระบบ';
$string['emailpasswordconfirmnoemail'] = '<p>บัญชีผู้ใช้ท่านระบุไม่อยู่ในระบบ</p>
<p>กรุณาติดต่อผู้ดูแลระบบ</p>';
$string['emailpasswordconfirmnotsent'] = '<p>รายละเอียดของผู้ใช้ที่ท่านระบุไม่อยู่ในระบบของสมาชิกที่มีอยู่</p>
<p>กรุณาตรวจสอบรายระเอียดที่ท่านกรอก แล้วลองดูอีกครั้ง ถ้าหากมีปัญหาในการใช้งาน กรุณาติดต่อผู้ดูแลระบบ</p>';
$string['emailpasswordconfirmsent'] = 'ระบบจะส่งอีเมลยืนยันการเปลี่ยนแปลงไปยังที่อยู่ของท่านที่
<b>{$a}</b>

หากพบปัญหาในการใช้งานใด ๆ
โปรดติดต่อ ผู้ดูแลระบบ';
$string['emailpasswordsent'] = 'ขอบคุณสำหรับการยืนยันการเปลี่ยนแปลงรหัสผ่าน
<p> ระบบได้ส่งรหัสผ่านใหม่ไปให้คุณที่ <b>{$a->email}</b>
<p> รหัสผ่านใหม่ของคุณเป็นการกำหนดขึ้นโดยระบบ คุณควรเข้าไปทำการแก้ใขให้จำได้ง่ายที่ <a href={$a->link}>เปลี่ยนรหัสผ่าน</a>';
$string['emailresetconfirmation'] = 'สวัสดีค่ะ/ครับ คุณ{$a->firstname},

ท่านได้ทำการส่งคำขอรีเซ็ทรหัสผ่านจากบัญชี  \'{$a->username}\' at {$a->sitename}

โปรดยืนยันว่าต้องการให้ส่งรหัสผ่านใหม่ทางอีเมลโดยการคลิกที่ลิงก์ต่อไปนี้ :

{$a->link}

(ลิงก์นี้จะใช้การได้เป็นเวลา {$a->reset minutes} นาที นับจากเวลาที่ท่านได้ส่งคำขอไว้)

หากต้องการความช่วยเหลือให้ติดต่อผู้ดูแลระบบ

{$a->admin}';
$string['emailresetconfirmationsubject'] = '{$a}: ส่งคำขอรีเซ็ทรหัสผ่าน';
$string['emailresetconfirmsent'] = 'ระบบได้ส่งอีเมลไปยังอีเมลของท่านที่ <b>{$a}</b>.
<br /> ซึ่งจะมีรายละเอียดและวิธีการยืนยันและการเปลี่ยนแปลงรหัสผ่าน หากต้องการความช่วยเหลือให้ติดต่อผู้ดูแลระบบ';
$string['emailtoprivatefiles'] = 'ท่านสามารถอีเมลไฟล์แนบไปยังที่เก็บไฟล์ส่วนตัวของท่านได้โดยตรง โดยการแนบไฟล์ของท่านแล้วส่งอีเมลไปยัง {$a}';
$string['emailtoprivatefilesdenied'] = 'ผู้ดูแลระบบได้ทำการปิดการใช้งานการอัพโหลดไฟล์ไปยังพื้นที่ส่วนตัว';
$string['emailuserhasnone'] = 'ไม่มีที่อยู่อีเมลสำหรับผู้ใช้นี้';
$string['emailvia'] = '{$a->name} (ผ่าน {$a->siteshortname})';
$string['emptydragdropregion'] = 'ภูมิภาคว่างไว้';
$string['enable'] = 'เปิดการใช้งาน';
$string['encryptedcode'] = 'รหัสผ่านมีการเข้ารหัส (Encrypt)';
$string['enddate'] = 'วันที่จบคอร์ส';
$string['english'] = 'ภาษาอังกฤษ';
$string['enrolmentmethods'] = 'วิธีการสมัครเข้าเรียน';
$string['entercourse'] = 'คลิกที่นี่เพื่อเข้าสู่รายวิชา';
$string['enteremail'] = 'กรุณากรอกที่อยู่อีเมล';
$string['enteremailaddress'] = 'ใส่อีเมลของคุณเพื่อทำการรีเซ็ทรหัสผ่าน และระบบจะส่งรหัสใหม่ให้คุณ';
$string['enterusername'] = 'กรอกชื่อผู้ใช้';
$string['entries'] = 'ข้อมูล';
$string['error'] = 'มีข้อผิดพลาด';
$string['errorcreatingactivity'] = 'ไม่สามารถสร้างข้อมูลในกิจกรรม \'{$a}\'';
$string['errorfiletoobig'] = 'ไฟล์มีขนาดใหญ่กว่าที่กำหนดไว้ \'{$a}\' ไบต์';
$string['errornouploadrepo'] = 'ไม่มีการเปิดการใช้งานไฟล์อัพโหลด';
$string['errorwhenconfirming'] = 'มีข้อผิดพลาด ถ้าคุณมาที่นี่โดยการคลิ๊กที่อีเมลกรุณาตรวจสอบว่า ชื่ออีเมลนั้นถูกต้องไม่ติดเครื่องหมายแปลกปลอมอื่นอยู่  คุณสามารถก๊อปปี้ชื่ออีเมลนั้นมาแปะได้เพื่อความถูกต้อง';
$string['eventcommentcreated'] = 'เพิ่มความคิดเห็นแล้ว';
$string['eventcommentdeleted'] = 'ลบความคิดเห็นแล้ว';
$string['eventcommentsviewed'] = 'ดูความคิดเห็นแล้ว';
$string['eventcontentviewed'] = 'เนื้อหาที่ดูแล้ว';
$string['eventcoursecategorycreated'] = 'สร้างประเภทแล้ว';
$string['eventcoursecategorydeleted'] = 'ลบประเภทแล้ว';
$string['eventcoursecategoryupdated'] = 'อัพเดทประเภทแล้ว';
$string['eventcoursecontentdeleted'] = 'ลบเนื้อหาแล้ว';
$string['eventcoursecreated'] = 'สร้างรายวิชาแล้ว';
$string['eventcoursedeleted'] = 'ลบรายวิชาแล้ว';
$string['eventcoursemodulecreated'] = 'สร้างโมดูลในรายวิชกาแล้ว';
$string['eventcoursemoduledeleted'] = 'ลบโมดูลในรายวิชาแล้ว';
$string['eventcoursemoduleinstancelistviewed'] = 'รายการโมดูลในรายวิชาที่ดูแล้ว';
$string['eventcoursemoduleupdated'] = 'อัพเดทโมดูลแล้ว';
$string['eventcoursemoduleviewed'] = 'โมดูลที่ดูแล้ว';
$string['eventcourseresetended'] = 'รีเซ็ทรายวิชาแล้ว';
$string['eventcourseresetstarted'] = 'เริ่มต้นรีเซ็ทรายวิชา';
$string['eventcourserestored'] = 'นำรายวิชากลับมาใช้ใหม่';
$string['eventcoursesectionupdated'] = 'อัพเดทหัวข้อแล้ว';
$string['eventcourseupdated'] = 'อัพเดทรายวิชาแล้ว';
$string['eventcourseuserreportviewed'] = 'ดูรายงานสมาชิกในรายวิชาแล้ว';
$string['eventcourseviewed'] = 'ดูรายวิชาแล้ว';
$string['eventemailfailed'] = 'ไม่สามารถส่งอีเมลได้';
$string['eventname'] = 'ชื่อเหตุการณ์';
$string['eventrecentactivityviewed'] = 'กิจกรรมล่าสุดที่ดูแล้ว';
$string['eventunknownlogged'] = 'เหตุการณ์ที่ไม่ทราบที่มา';
$string['eventusercreated'] = 'สร้างสมาชิกแล้ว';
$string['eventuserdeleted'] = 'ลบสมาชิกแล้ว';
$string['eventuserinfofieldupdated'] = 'ฟิลล์ผู้ใช้ถูกเปลี่ยนแปลง';
$string['eventuserlistviewed'] = 'รายชื่อสมาชิกที่ดูแล้ว';
$string['eventuserloggedout'] = 'สมาชิกที่ออกจากระบบ';
$string['eventuserpasswordupdated'] = 'รหัสผ่านอัพเดทแล้ว';
$string['eventuserprofileviewed'] = 'ดูโปรไฟล์ของสมาชิกแล้ว';
$string['eventuserupdated'] = 'อัพเดทสมาชิกแล้ว';
$string['everybody'] = 'ทุกคน';
$string['executeat'] = 'ดำเนินการเวลา';
$string['existing'] = 'มีอยู่';
$string['existingadmins'] = 'ผู้ดูแลที่มีอยู่';
$string['existingcourse'] = 'รายวิชาที่มีอยู่';
$string['existingcourseadding'] = 'เพิ่มข้อมูลลงในรายวิชาที่มีอยู่';
$string['existingcoursedeleting'] = 'ลบข้อมูลในรายวิชาที่มีอยู่ก่อน';
$string['existingcreators'] = 'ผู้สร้างรายวิชาที่มีอยู่';
$string['existingstudents'] = 'นักเรียนที่เป็นสมัครเข้าเรียนแล้ว';
$string['existingteachers'] = 'อาจารย์ที่มีอยู่';
$string['expand'] = 'ขยาย';
$string['expandall'] = 'ขยายทั้งหมด';
$string['expandcategory'] = 'ขยาย \'{$a}\'';
$string['explanation'] = 'คำอธิบาย';
$string['extendperiod'] = 'ระยะเวลาที่ต่อ';
$string['failedloginattempts'] = 'ล็อกอินไม่สำเร็จ {$a->attempts}  ครั้งนับแต่คุณล็อกอินครั้งล่าสุด';
$string['favourites'] = 'ติดดาว';
$string['feedback'] = 'ความเห็น';
$string['file'] = 'ไฟล์';
$string['fileexists'] = 'มีไฟล์ที่ชื่อ \'{$a}\' แล้ว';
$string['filemissing'] = '{$a} หายไป';
$string['filereaderror'] = 'ไม่สามารถอ่านไฟล์ \'{$a}\' ได้ โปรดตรวจสอบว่าไฟล์ที่ระบุนั้นเป็นไฟล์จริงๆและไม่ใช่โพสเดอร์';
$string['files'] = 'ไฟล์';
$string['filesanduploads'] = 'ไฟล์และอัพโหลด';
$string['filesfolders'] = 'ไฟล์/แฟ้ม';
$string['fileuploadwithcontent'] = 'ไฟล์ที่ต้องการอัพโหลดไม่ควรมีพารามิเตอร์อยู่ในเนื้อหา';
$string['filloutallfields'] = 'กรุณากรอกแบบฟอร์มนี้';
$string['filter'] = 'ตัวกรอง';
$string['filteroption'] = '{$a->criteria}: {$a->value}';
$string['filters'] = 'ตัวกรอง';
$string['findmorecourses'] = 'ค้นหารายวิชาอื่น ๆ';
$string['first'] = 'ครั้งแรก';
$string['firstaccess'] = 'เข้าเรียนครั้งแรก';
$string['firstname'] = 'ชื่อ';
$string['firstnamephonetic'] = 'ชื่อ (วิธีอ่าน)';
$string['firstsiteaccess'] = 'ครั้งแรกที่เข้ามายังเว็บไซต์';
$string['firsttime'] = 'ท่านเข้ามาที่นี่เป็นครั้งแรกหรือไม่';
$string['folder'] = 'แฟ้ม';
$string['folderclosed'] = 'แฟ้มปิด';
$string['folderopened'] = 'แฟ้มเปิด';
$string['followingoptional'] = 'ไม่จำเป็นต้องกรอก';
$string['followingrequired'] = 'จำเป็นต้องกรอก';
$string['for'] = 'เพื่อ';
$string['force'] = 'บังคับ';
$string['forcelanguage'] = 'ภาษาที่บังคับในการใช้งาน';
$string['forceno'] = 'ไม่บังคับ';
$string['forcepasswordchange'] = 'บังคับให้เปลี่ยนรหัสผ่าน';
$string['forcepasswordchange_help'] = 'ถ้าหากเช็คถูกในช่องนี้ สมาชิกจะต้องเปลี่ยนรหัสผ่านเมื่อเข้าสู่ระบบ';
$string['forcepasswordchangecheckfull'] = 'ท่านแน่ใจหรือไม่ที่จะบังคับให้เปลี่ยนรหัสผ่านเป็น \'{$a}\'';
$string['forcepasswordchangenot'] = 'ไม่สามารถบังคับให้เปลี่ยนรหัสผ่านเป็น \'{$a}\' ได้';
$string['forcepasswordchangenotice'] = 'กรุณาเปลี่ยนรหัสผ่านก่อนไปยังขั้นต่อไป';
$string['forcetheme'] = 'บังคับการใช้รูปแบบเว็บ';
$string['forgotaccount'] = 'ลืมรหัสผ่าน ?';
$string['forgotten'] = 'ลืมชื่อผู้ใช้หรือรหัสผ่าน ?';
$string['forgottenduplicate'] = 'มีการใช้งานอีเมลนี้ในหลายบัญชีผู้ใช้กรุณาใส่ชื่อผู้ใช้แทนค่ะ';
$string['forgotteninvalidurl'] = 'เว็บไซต์สำหรับรีเซ็ทรหัสผ่านไม่ถูกต้อง';
$string['format'] = 'รูปแบบ';
$string['format_help'] = '<P ALIGN=CENTER><B>รูปแบบรายวิชา</B></P>

*  แบบกิจกรรมเดี่ยว  -  แสดงกิจกรรมเดี่ยว หรือ แหล่งข้อมูล เช่น แบบทดสอบ หรือ สกอร์ม ในหน้ารายวิชา

* แบบกระดานเสวนา -  รูปแบบนี้จะเหมือนกับกระดานเสวนาทั่วไป โดยจะปรากฎเป็นรายการในหน้าแรกของรายวิชา

* แบบหัวข้อ - หน้ารายวิชาจะแบ่งออกเป็นหัวข้อ

* แบบรายสัปดาห์ - เป็นการจัดการรายวิชาสัปดาห์ต่อสัปดาห์  โดยมีวันเริ่มต้นและสิ้นสุดที่แน่นอนในแต่ละสัปดาห์จะมีกิจกรรมต่างๆ  ให้นักเรียนทำ';
$string['formathtml'] = 'ใช้โค้ด HTML';
$string['formatmarkdown'] = 'รูปแบบ markdown';
$string['formatplain'] = 'รูปแบบข้อความ(Plain Text)';
$string['formattext'] = 'แบบอัตโนมัติ';
$string['formattexttype'] = 'รูปแบบ';
$string['forumpreferences'] = 'ค่าที่ต้องการสำหรับกระดานเสวนา';
$string['framesetinfo'] = 'เอกสารนี้ประกอบไปด้วย';
$string['from'] = 'จาก';
$string['frontpagecategorycombo'] = 'แสดงประเภทและช่องค้นหา';
$string['frontpagecategorynames'] = 'แสดงประเภท';
$string['frontpagecourselist'] = 'แสดงรายวิชา';
$string['frontpagecoursesearch'] = 'ค้นหารายวิชา';
$string['frontpagedescription'] = 'บทคัดย่อที่แสดงหน้าแรก';
$string['frontpagedescriptionhelp'] = 'บทคัดย่อที่ต้องการแสดงในหน้าแรก ในบล็อคของรายวิชาหรือเว็บไซต์';
$string['frontpageenrolledcourselist'] = 'รายวิชาที่สมัครเรียน';
$string['frontpageformat'] = 'การแสดงหน้าแรกของเว็บ';
$string['frontpageformatloggedin'] = 'การแสดงหน้าแรกหลังเข้าสู่ระบบ';
$string['frontpagenews'] = 'แสดงข่าว';
$string['frontpagesettings'] = 'การตั้งค่าหน้าแรก';
$string['fulllistofcourses'] = 'รายวิชาทั้งหมด';
$string['fullname'] = 'ชื่อเต็ม';
$string['fullnamecourse'] = 'ชื่อเต็ม';
$string['fullnamecourse_help'] = 'ชื่อเต็มของรายวิชา ที่จะแสดงไว้ด้านบนสุดของหน้ารายวิชาและ ในหน้ารวมแสดงรายวิชา';
$string['fullnamedisplay'] = '{$a->firstname} {$a->lastname}';
$string['fullnameuser'] = 'ชื่อเต็ม';
$string['fullprofile'] = 'ประวัติเต็ม';
$string['fullsitename'] = 'ชื่อเต็มของเว็บไซต์';
$string['functiondisabled'] = 'ปิดการใช้งานฟังก์ชันนี้อยู่';
$string['general'] = 'ทั่วไป';
$string['geolocation'] = 'ละติจูด - ลองติจูด';
$string['gettheselogs'] = 'ใช้งานบันทึกการใช้งานเหล่านี้';
$string['go'] = 'เริ่ม';
$string['gpl'] = 'สงวนลิขสิทธิ์ (C) 2001-2002  Martin Dougiamas  (http://dougiamas.com)

โปรแกรมนี้เป็นซอฟท์แวร์เสรี คุณสามารถเผยแพร่หรือแก้ไขภายใต้สัญญานุญาต
GNU GPL ซึ่งเผยแพร่ภายใต้องค์กรซอฟท์แวร์เสรี ทั้งเวอร์ชัน 2 หรือเวอร์ชัน
ต่อไป

โปรแกรมนี้เผยแพร่ด้วยหวังว่าจะเกิดประโยชน์ต่อผู้นำไปใช้แต่ไม่สามารถการันตีผลที่ตามมา
ไม่ว่าจะเป็นผลทางการค้าหรือความเหมาะสมในแต่ละกรณีของการใช้งา อ่านรายละเอียด
เพิ่มเติมเกี่ยวกับสัญญานุญาต GNU GPL ได้ที่
http://www.gnu.org/copyleft/gpl.html';
$string['gpl3'] = 'สงวนลิขสิทธิ์ (C) 1999  Martin Dougiamas  (https://dougiamas.com)

โปรแกรมนี้เป็นซอฟท์แวร์เสรี คุณสามารถเผยแพร่หรือแก้ไขภายใต้สัญญานุญาต
GNU GPL ซึ่งเผยแพร่ภายใต้องค์กรซอฟท์แวร์เสรี ทั้งเวอร์ชัน 2 หรือเวอร์ชัน
ต่อไป

โปรแกรมนี้เผยแพร่ด้วยหวังว่าจะเกิดประโยชน์ต่อผู้นำไปใช้แต่ไม่สามารถการันตีผลที่ตามมา
ไม่ว่าจะเป็นผลทางการค้าหรือความเหมาะสมในแต่ละกรณีของการใช้งา อ่านรายละเอียด
เพิ่มเติมเกี่ยวกับสัญญานุญาต GNU GPL ได้ที่
http://www.gnu.org/copyleft/gpl.html';
$string['gpllicense'] = 'สัญญานุญาต
GPL';
$string['grade'] = 'คะแนนที่ได้';
$string['grades'] = 'คะแนนทั้งหมด';
$string['gravatarenabled'] = 'มีการเปิดใช้งาน <a href="http://www.gravatar.com/">Gravatar</a> ในเว็บไซต์นี้ หากท่านไม่อัพโหลดภาพส่วนตัว ระบบจะเลือกรูปภาพจาก Gravatar';
$string['group'] = 'กลุ่ม';
$string['groupadd'] = 'เพิ่มกลุ่มสมาชิก';
$string['groupaddusers'] = 'เพิ่มสมาชิกที่เลือกลงในกลุ่ม';
$string['groupfor'] = 'สำหรับกลุ่ม';
$string['groupinfo'] = 'ข้อมูลเกี่ยวกับกลุ่ม';
$string['groupinfoedit'] = 'การตั้งค่ากลุ่ม';
$string['groupinfomembers'] = 'ข้อมูลเกี่ยวกับสมาชิก';
$string['groupinfopeople'] = 'ข้อมูลของสมาชิกที่เลือก';
$string['groupmembers'] = 'สมาชิกในกลุ่ม';
$string['groupmemberssee'] = 'ดูรายชื่อสมาชิกในกลุ่ม';
$string['groupmembersselected'] = 'สมาชิกของกลุ่มที่ถูกเลือก';
$string['groupmode'] = 'ระบบกลุ่ม';
$string['groupmodeforce'] = 'บังคับให้ใช้ระบบกลุ่ม';
$string['groupmy'] = 'กลุ่มของฉัน';
$string['groupnonmembers'] = 'บุคคลที่ไม่ได้อยู่ในกลุ่ม';
$string['groupnotamember'] = 'ขออภัยค่ะ คุณไม่ได้เป็นสมาชิกของกลุ่มนี้';
$string['grouprandomassign'] = 'สุ่มเลือกทุกคนเข้ากลุ่ม';
$string['groupremove'] = 'เอากลุ่มที่เลือกออก';
$string['groupremovemembers'] = 'เอาสมาชิกที่เลือกออก';
$string['groups'] = 'กลุ่ม';
$string['groupsnone'] = 'เรียนรวมกันไม่แบ่งกลุ่ม';
$string['groupsseparate'] = 'กลุ่มแบบแยกกันอย่างชัดเจน(ศึกษาข้ามกลุ่มไม่ได้)';
$string['groupsvisible'] = 'กลุ่มแบบศึกษาข้ามกลุ่มได้(แต่ทำกิจกรรมในกลุ่มอื่นไม่ได้)';
$string['guest'] = 'บุคคลทั่วไป';
$string['guestdescription'] = 'บุคคลทั่วไปมีสิทธิพิเศษน้อยที่สุดไม่ว่าจะอยู่ตรงส่วนใดของเว็บและไม่สามารถแสดงความคิดเห็นใด ๆ ได้';
$string['guestskey'] = 'อนุญาตให้บุคคลทั่วไปที่มีรหัสผ่าน';
$string['guestsno'] = 'ไม่อนุญาตให้บุคคลทั่วไปเข้าอ่าน';
$string['guestsnotallowed'] = 'ขออภัยบุคคลทั่วไปไม่สามารถเข้าอ่าน  \'{$a}\' ได้ต้องสมัครสมาชิก';
$string['guestsyes'] = 'อนุญาตให้บุคคลทั่วไปเข้าได้';
$string['guestuser'] = 'บุคคลทั่วไป';
$string['guestuserinfo'] = 'สมาชิกได้รับสิทธิในการอ่านเท่านั้น';
$string['help'] = 'ช่วยเหลือ';
$string['helpprefix2'] = 'ช่วยเหลือ {$a}';
$string['helpwiththis'] = 'ช่วยเหลือในส่วนนี้';
$string['hiddenassign'] = 'การบ้านที่ซ่อนไว้';
$string['hiddenfromstudents'] = 'ซ่อนไม่ให้นักเรียนเห็น';
$string['hiddenoncoursepage'] = 'พร้อมใช้แต่ไม่แสดงบนหน้ารายวิชา';
$string['hiddensections'] = 'ส่วนที่ซ่อนไว้';
$string['hiddensections_help'] = '<p>คุณสามารถใช้ตัวเลือกนี้ซ่อนรายวิชาในส่วนที่คุณไม่ต้องการให้นักเรียนเห็นได้</p>

<p>ตามปกติค่าตั้งต้น ส่วนของรายวิชาไว้จะแสองอยู่ในพื้นที่เล็ก ๆ (รูปแบบพับปิดเปิดได้ สีเทา) จะไม่สามารถมองเห็นรายละเอียดของกิจกรรมและตัวหนังสือที่ถูกซ่อนไว้
   เืมื่อดูแบบรายสัปดาห์ทำให้เห็นได้ง่ายว่าสัปดาห์ไหนไม่มีการเรียนการสอน
   </p>

<p>ถ้าคุณใช้ตัวเลือกนี้ จะทำให้ส่วนของหลักสูตรที่คุณต้องการซ่อนหายไปทั้งส่วน</p>';
$string['hiddensectionscollapsed'] = 'แสดงส่วนที่ซ่อนแบบพับไว้';
$string['hiddensectionsinvisible'] = 'ไม่ให้ใครเห็นส่วนที่ซ่อนไว้';
$string['hiddenwithbrackets'] = '(ถูกซ่อน)';
$string['hide'] = 'ซ่อน';
$string['hideadvancedsettings'] = 'ซ่อนการตั้งค่าชั้นสูง';
$string['hidechartdata'] = 'ซ่อนข้อมูลกราฟ';
$string['hidefromstudents'] = 'ซ่อนจากนักเรียน';
$string['hideoncoursepage'] = 'ทำให้พร้อมใช้แค่ไม่แสดงบนหน้ารายวิชา';
$string['hidepicture'] = 'ซ่อนภาพ';
$string['hidepopoverwindow'] = 'ซ่อนหน้าต่างป๊อปอัพ';
$string['hidesection'] = 'ซ่อนส่วน {$a}';
$string['hidesettings'] = 'ซ่อนการตั้งค่า';
$string['hideshowblocks'] = 'ซ่อน หรือ แสดงบล็อค';
$string['highlight'] = 'ไฮไลท์';
$string['highlightoff'] = 'เอาไฮไลท์ออก';
$string['hits'] = 'ครั้ง';
$string['hitsoncourse'] = 'เข้าใช้งาน {$a->coursename} โดย {$a->username}';
$string['hitsoncoursetoday'] = 'วันนี้เข้าใช้งาน {$a->coursename} โดย {$a->username}';
$string['home'] = 'หน้าหลัก';
$string['hour'] = 'ชั่วโมง';
$string['hours'] = 'ชั่วโมง';
$string['howtomakethemes'] = 'จะสร้างรูปแบบเว็บใหม่ได้อย่างไร';
$string['htmleditor'] = 'ใช้ HTML editor';
$string['htmleditoravailable'] = 'ใช้ HTML editor  ได้';
$string['htmleditordisabled'] = 'คุณเลือกที่จะไม่ใช้ Richtext editor ในหน้าประวัติของคุณ';
$string['htmleditordisabledadmin'] = 'ผู้ดูแลระบบไม่อนุญาตให้ใช้  Richtext editor บนเว็บนี้';
$string['htmleditordisabledbrowser'] = 'ไม่สามารถใช้งาน Richtext editor เพราะไม่ได้ใช้  Internet Explorer 5.5 หรือสูงกว่า';
$string['htmlfilesonly'] = 'ไฟล์ html เท่านั้น';
$string['htmlformat'] = 'Pretty HTML format';
$string['icon'] = 'ไอคอน';
$string['icqnumber'] = 'หมายเลข ICQ';
$string['idnumber'] = 'หมายเลข ID';
$string['idnumbercourse'] = 'รหัสรายวิชา';
$string['idnumbercourse_help'] = 'รหัสรายวิชามีไว้สำหรับเปรียบเทียบกับที่อยู่นอกระบบ เลขนี้จะไม่เห็นในระบบ ถ้าหากมีรหัสรายวิชาอยู่แล้วสามารถใส่รหัสไปได้ หากไม่มีก็ปล่อยทิ้งไว้';
$string['idnumbercoursecategory'] = 'รหัสประเภท';
$string['idnumbercoursecategory_help'] = 'รหัสประเภทใช้เพื่อยืนยันกับระบบภายนอกแต่จะไม่มีการแสดงในส่วนอื่นบนเว็บไซต์ ถ้าหากมีรหัสประเภทอยู่แล้วสามารถใส่รหัสลงไปได้ หากไม่มีก็ปล่อยว่างไว้';
$string['idnumbergroup'] = 'รหัสกลุ่ม';
$string['idnumbergroup_help'] = 'รหัสกลุ่มมีไว้สำหรับเปรียบเทียบกับที่อยู่นอกระบบ เลขนี้จะไม่เห็นในระบบ ถ้าหากมีรหัสรายวิชาอยู่แล้วสามารถใส่รหัสไปได้ หากไม่มีก็ปล่อยทิ้งไว้';
$string['idnumbergrouping'] = 'รหัสการจัดกลุ่ม';
$string['idnumbergrouping_help'] = 'รหัสการจัดกลุ่มมีไว้สำหรับเปรียบเทียบกับที่อยู่นอกระบบ เลขนี้จะไม่เห็นในระบบ ถ้าหากมีรหัสรายวิชาอยู่แล้วสามารถใส่รหัสไปได้ หากไม่มีก็ปล่อยทิ้งไว้';
$string['idnumbermod'] = 'รหัสประจำตัว';
$string['idnumbermod_help'] = 'การตั้งรหัสประจำตัวทำให้ง่ายต่อการออกคะแนนและคำนวณคะแนน ถ้ากิจกรรมใดไม่มีคะแนนให้ปล่อยช่องหมายเลขประจำตัวว่างไว้

รหัสประจำตัวสามารถใช้ในสมุดรายงานคะแนน แต่สามารถแก้ไขได้จากหน้าการตั้งค่ากิจกรรมเท่านั้น';
$string['idnumbertaken'] = 'รหัสนี้ถูกใช้ไปแล้ว';
$string['imagealt'] = 'คำบรรยายภาพ';
$string['import'] = 'นำเข้า';
$string['importdata'] = 'นำเข้าข้อมูลในรายวิชา';
$string['importdataexported'] = 'ส่งออกข้อมูลจากรายวิชานี้ประสบผลสำเร็จ  กำลังจะนำเข้ารายวิชาดังกล่าว';
$string['importdatafinished'] = 'การนำเข้าสำเร็จ คลิกเพื่อเข้าไปยังรายวิชา';
$string['importdatafrom'] = 'พบรายวิชาที่ต้องการนำเข้า';
$string['inactive'] = 'ไม่เปิดใช้งาน';
$string['include'] = 'รวม';
$string['includeallusers'] = 'รวมสมาชิกทั้งหมด';
$string['includecoursefiles'] = 'รวมไฟล์ทั้งหมดทุกวิชา';
$string['includecourseusers'] = 'รวมสมาชิกของแต่ละวิชา';
$string['included'] = 'รวมแล้ว';
$string['includelogentries'] = 'รวมประวัติการใช้งาน';
$string['includemodules'] = 'รวมโมดูล';
$string['includemoduleuserdata'] = 'รวมข้อมูลของสมาชิกในแต่ละโมดูล';
$string['includeneededusers'] = 'รวมสมาชิกที่จำเป็น';
$string['includenoneusers'] = 'ไม่บันทึกสมาชิก';
$string['includeroleassignments'] = 'รวมการกำหนดบทบาท';
$string['includesitefiles'] = 'รวมไฟล์ของเว็บไซต์ที่ใช้ในรายวิชา';
$string['includeuserfiles'] = 'รวมไฟล์สมาชิก';
$string['increasesections'] = 'เพิ่มจำนวนหัวข้อ';
$string['indicator:accessesafterend'] = 'วันที่เข้ารายวิชาได้หลังจากวันที่จบรายวิชา';
$string['indicator:activitiesdue'] = 'กำหนดกิจกรรมที่กำลังจะสิ้นสุด';
$string['indicator:activitiesdue_help'] = 'ผู้ใช้มีกำหนดกิจกรรมที่กำลังจะสิ้นสุด';
$string['indicator:anywrite'] = 'เขียนการกระทำใดๆ';
$string['indicator:completeduserprofile'] = 'ข้อมูลผู้ใช้สมบูรณ์แล้ว';
$string['indicator:nostudent'] = 'ไม่มีนักเรียน';
$string['indicator:noteacher'] = 'ไม่มีผู้สอน';
$string['info'] = 'ข้อมูล';
$string['institution'] = 'สถาบัน';
$string['instudentview'] = 'มุมมองนักเรียน';
$string['interests'] = 'ความสนใจ';
$string['interestslist'] = 'รายการของความสนใจ';
$string['interestslist_help'] = 'ใส่ความสนใจทีละอัน โดยจะมีการแสดงผลในหน้าโปรไฟล์ ในลักษณะของ แท็ก';
$string['invalidemail'] = 'อีเมลไม่ถูกต้อง';
$string['invalidlogin'] = 'ล็อกอินไม่ถูกต้องกรุณาตรวจสอบ';
$string['invalidusername'] = 'ชื่อผู้ใช้สามารถใช้ได้เฉพาะ ตัวอักษร ตัวเลข ตัวพิมพ์เล็ก ขีดล่าง (_)  ขีดกลาง(-)  หรือ เครื่องหมาย @';
$string['invalidusernameupload'] = 'ชื่อผู้ใช้ไม่ถูกต้อง';
$string['ip_address'] = 'หมายเลขไอพี';
$string['jump'] = 'ไป.';
$string['jumpto'] = 'ไปยัง...';
$string['keep'] = 'เก็บ';
$string['keepsearching'] = 'ค้นหาต่อไป';
$string['langltr'] = 'ภาษาจากซ้ายไปขวา';
$string['langrtl'] = 'ภาษาจากขวาไปซ้าย';
$string['language'] = 'ภาษาที่ใช้ในเว็บ';
$string['languagegood'] = 'ภาษาใช้เวอร์ชั่นล่าสุดแล้ว';
$string['last'] = 'สุดท้าย';
$string['lastaccess'] = 'เข้ามาครั้งสุดท้ายเมื่อ';
$string['lastcourseaccess'] = 'เข้ามาในรายวิชาครั้งสุดท้ายเมื่อ';
$string['lastedited'] = 'แก้ไขครั้งสุดท้าย';
$string['lastip'] = 'หมายเลขไอพีที่ใช้ครั้งสุดท้าย';
$string['lastlogin'] = 'เข้ามาครั้งสุดท้ายเมื่อ';
$string['lastmodified'] = 'แก้ไขครั้งสุดท้าย';
$string['lastname'] = 'นามสกุล';
$string['lastnamephonetic'] = 'นามสกุล (วิธีอ่าน)';
$string['lastsiteaccess'] = 'เข้ามายังเว็บไซต์ครั้งสุดท้าย เมื่อ';
$string['lastyear'] = 'ปีที่แล้ว';
$string['latestlanguagepack'] = 'ตรวจสอบหาไฟล์ภาษาล่าสุดใน moodle.org';
$string['layouttable'] = 'ตารางแผนงาน';
$string['leavetokeep'] = 'ว่างไว้หากต้องการใช้รหัสเดิม';
$string['legacythemeinuse'] = 'เว็บไซต์นี้แสดงผลในโหมด compatibility เพราะบราวเซอร์ของท่านเก่าเกินไป';
$string['license'] = 'ลิขสิทธิ';
$string['licenses'] = 'สัญญานุญาต';
$string['liketologin'] = 'ต้องการเข้าสู่ระบบด้วย account ของคุณตอนนี้หรือไม่คะ';
$string['list'] = 'รายชื่อ';
$string['listfiles'] = 'รายชื่อของไฟล์ทั้งหมดใน {$a}';
$string['listofallpeople'] = 'รายชื่อสมาชิก';
$string['listofcourses'] = 'รายชื่อรายวิชา';
$string['loading'] = 'กำลังโหลด';
$string['loadinghelp'] = 'กำลังโหลด';
$string['local'] = 'Local';
$string['localplugins'] = 'ปลั๊กอินที่ใช้';
$string['localpluginsmanage'] = 'จัดการปลั๊กอินในเครื่อง';
$string['location'] = 'ที่ตั้ง';
$string['locktimeout'] = 'เวลาที่ตั้งไว้ก่อนจะทำการล็อกระบบ';
$string['log_excel_date_format'] = 'yyyy mmmm d h:mm';
$string['loggedinas'] = 'ท่านเข้าสู่ระบบในชื่อ {$a}';
$string['loggedinasguest'] = 'ท่านเข้าสู่ระบบในฐานะบุคคลทั่วไป';
$string['loggedinnot'] = 'ท่านยังไม่ได้เข้าสู่ระบบ';
$string['login'] = 'เข้าสู่ระบบ';
$string['login_failure_logs'] = 'บันทึกการเข้าสู่ระบบที่ล้มเหลว';
$string['loginactivity'] = 'กิจกรรมการเข้าสู่ระบบ';
$string['loginalready'] = 'ท่านเข้าสู่ระบบเรียบร้อยแล้ว';
$string['loginas'] = 'เข้าสู่ระบบในนาม';
$string['loginaspasswordexplain'] = '<p>ท่านต้องใส่รหัสผ่านพิเศษ เพื่อใช้งานในส่วนนี้ <br/> ถ้าหากไม่ทราบให้สอบถามที่ผู้ดูแลระบบ</p>';
$string['loginguest'] = 'เข้าสู่ระบบในฐานะบุคคลทั่วไป';
$string['loginsite'] = 'เข้าสู่ระบบของเว็บ';
$string['loginsteps'] = 'สวัสดี <p> กรุณาสมัครสมาชิกใหม่เพื่อที่คุณจะสามารถ เข้าไปยังบทเรียนต่างๆได้ในแต่ละรายวิชานั้นอาจจะต้องการ รหัสผ่านซึ่งคุณยังไม่จำเป็นต้องไปกังวลจนกว่าจะได้เป็นสมาชิกแล้วกรุณาทำตามขั้นตอนต่อไปนี้

<OL size=2>
   <LI>กรอกแบบฟอร์ม<A HREF={$a}>สมัครสมาชิกใหม่</A>
   <LI>ระบบจะทำการส่งอีเมลไปยังอีเมลที่คุณให้ไว้<LI>อ่านอีเมล จากนั้นคลิกที่ ลิงก์ในอีเมลนั้น
   <LI>เมื่อคลิกแล้วบัญชีผู้ใช้ของคุณจะได้รับการยืนยันสามารถล็อกอินเข้าสู่ระบบได้ทันที
   <LI> เลือกรายวิชาที่ต้องการเข้าไปเรียน
   <LI>ถ้าหากมีการถามให้ใส่รหัสในการเข้าเรียน  ให้กรอกรหัสที่อาจารย์ของคุณให้ไว้
<LI> นับจากนี้คุณสามารถเข้าไปศึกษาและทำกิจกรรมในแต่ละรายวิชาได้ โดยครั้งต่อไปเพียงแต่ใส่ชื่อผู้ใช้ (username) และรหัสผ่าน (password)จากหน้านี้ </OL>';
$string['loginstepsnone'] = 'สวัสดี<P>กรุณาสมัครสมาชิกใหม่เพื่อที่ท่านจะสามารถ เข้าไปยังบทเรียนต่างๆได้ <P>ถ้าหากมีการเลือกชื่อผู้ใช้ที่ท่านต้องการไปแล้วกรุณาเลือกชื่อใหม่';
$string['loginto'] = 'เข้าสู่ระบบในชื่อ  {$a}';
$string['logout'] = 'ออกจากระบบ';
$string['logoutconfirm'] = 'แน่ใจหรือว่าต้องการออกจากระบบ?';
$string['logs'] = 'บันทึกการใช้งานเว็บไซต์';
$string['logtoomanycourses'] = '[ <a href="{$a->url}">รายวิชาทั้งหมด</a> ]';
$string['logtoomanyusers'] = '[ <a href="{$a->url}">สมาชิกทั้งหมด</a> ]';
$string['lookback'] = 'ย้อนกลับไปดู';
$string['mailadmins'] = 'แจ้งผู้ดูแลระบบ';
$string['mailstudents'] = 'แจ้งนักเรียน';
$string['mailteachers'] = 'แจ้งอาจารย์';
$string['maincoursepage'] = 'หน้าหลักรายวิชา';
$string['makeafolder'] = 'สร้างโฟลเดอร์';
$string['makeavailable'] = 'ทำให้พร้อมใช้งาน';
$string['makeeditable'] = 'หากต้องการแก้ไข  \'{$a}\'  ผ่านเว็บได้ ควรทำการ chmod ของไฟล์เป็น 777 ก่อน';
$string['makethismyhome'] = 'เลือกหน้านี้ให้เป็นหน้าหลัก';
$string['makeunavailable'] = 'ทำให้ไม่พร้อมใช้งาน';
$string['manageblocks'] = 'จัดการบล็อค';
$string['managecategorythis'] = 'จัดการประเภท';
$string['managecourses'] = 'จัดการรายวิชา';
$string['managedatabase'] = 'จัดการฐานข้อมูล';
$string['manageeditorfiles'] = 'จัดการไฟลืที่ใช้โดย Editor';
$string['managefilters'] = 'จัดการฟิลเตอร์';
$string['managemodules'] = 'จัดการโมดูล';
$string['manageroles'] = 'บทบาทและสิทธิ์การเข้าถึง';
$string['markallread'] = 'ทำเครื่องหมายทั้งหมดว่าอ่านแล้ว';
$string['markedthistopic'] = 'หัวข้อนี้คือหัวข้อปัจจุบัน';
$string['markthistopic'] = 'เลือกหัวข้อนี้ให้เป็นหัวข้อปัจจุบัน';
$string['matchingsearchandrole'] = 'จับคู่ \'{$a->search}\' และ {$a->role}';
$string['maxareabytesreached'] = 'ไฟล์หรือขนาดของไฟล์ทั้งหมดรวมกันมีขนาดใหญ่กว่า พื้นที่ที่เหลืออยู่';
$string['maxfilesize'] = 'ขนาดไฟล์สำหรับไฟล์ใหม่ {$a}';
$string['maxfilesreached'] = 'ท่านสามารถแนบไฟล์ลงในนี้ได้จำนวน {$a}  ไฟล์';
$string['maximumchars'] = 'จำนวนมากที่สุด {$a} ตัวอักษร';
$string['maximumgrade'] = 'คะแนนเต็ม';
$string['maximumgradex'] = 'คะแนนเต็ม {$a}';
$string['maximumshort'] = 'เต็ม';
$string['maximumupload'] = 'ขนาดไฟล์สูงสุด';
$string['maximumupload_help'] = '<p>เป็นการกำหนดขนาดของไฟล์ที่อนุญาตให้นักเรียนนำขึ้นเว็บ (upload) โดยผู้ดูแลระบบจะเป็นคนสร้าง site wide setting ซึ่งจะเป็นตัวที่จำกัดขนาดของไฟล์.

<p> และยังสามารถกำหนดตัวตั้งค่านี้ในแต่่ละโมดูลได้อีก';
$string['maxnumberweeks'] = 'จำนวนหัวข้อสูงสุด';
$string['maxnumberweeks_desc'] = 'จำนวนสูงสุดของหัวข้อที่แสดงในดรอปดาวน์เมนู  ( ใช้ได้ในบางรูปแบบรายวิชาเท่านั้น)';
$string['maxnumcoursesincombo'] = 'ไปยัง  <a href="{$a->link}">{$a->numberofcourses} รายวิชา</a>.';
$string['maxsize'] = 'ขนาดสูงสุด: {$a}';
$string['maxsizeandareasize'] = 'ขนาดของไฟล์ใหม่: {$a->size}, จำกัดที่: {$a->areasize}';
$string['maxsizeandattachments'] = 'ขนาดของไฟล์ใหม่: {$a->size}, จำนวนไฟล์แนบ: {$a->attachments}';
$string['maxsizeandattachmentsandareasize'] = 'ขนาดของไฟล์ใหม่: {$a->size}, จำนวนไฟล์แนบ: {$a->attachments}, จำกัดที่: {$a->areasize}';
$string['memberincourse'] = 'นักเรียนในรายวิชา';
$string['messagebody'] = 'ข้อความ';
$string['messagedselectedcountusersfailed'] = 'มีปัญหา ยังไม่ได้ส่ง {$a}  ข้อความ';
$string['messagedselecteduserfailed'] = 'ยังไม่ได้ส่งข้อความไม่ยังสมาชิก {$a->fullname}.';
$string['messagedselectedusers'] = 'ส่งข้อความไปยังสมาชิกที่เลือกเรียบร้อยแล้ว และระบบทำการรีเซ็ทรายชื่อผู้รับเรียบร้อย';
$string['messagedselectedusersfailed'] = 'มีข้อผิดพลาดในการส่งข้อความถึงสมาชิกบางท่าน สมาชิกที่เลือกอาจไม่ได้รับอีเมลครบทุกคน';
$string['messageprovider:availableupdate'] = 'ข้อความเตือน  มีอัพเดทใหม่';
$string['messageprovider:backup'] = 'ข้อความเตือน สำรองข้อมูล';
$string['messageprovider:badgecreatornotice'] = 'ข้อความเตือน ผู้สร้างเครื่องหมาย';
$string['messageprovider:badgerecipientnotice'] = 'ข้อความเตือน  ผู้รับเครื่องหมาย';
$string['messageprovider:courserequestapproved'] = 'ข้อความเตือน การอนุมัติคำขอสร้างรายวิชา';
$string['messageprovider:courserequested'] = 'ข้อความเตือน  คำขอสร้างรายวิชา';
$string['messageprovider:courserequestrejected'] = 'ข้อความเตือน  การปฏิเสธคำขอสร้างรายวิชา';
$string['messageprovider:errors'] = 'ข้อผิดพลาดในเว็บไซต์';
$string['messageprovider:errors_help'] = 'ข้อผิดพลาดสำคัญที่ผู้ดูแลระบบควรรู้';
$string['messageprovider:instantmessage'] = 'ข้อความส่วนตัวระหว่างสมาชิก';
$string['messageprovider:instantmessage_help'] = 'การตั้งค่าส่วนนี้ เป็นการเลือกว่าจะมีอะไรเกิดขึ้นเมื่อมีข้อความส่งมาถึงท่าน';
$string['messageprovider:notices'] = 'คำเตือนเกี่ยวกับปัญหาปลีกย่อย';
$string['messageprovider:notices_help'] = 'คำเตือนที่ผู้ดูแลระบบควรทราบ';
$string['messageselect'] = 'เลือกส่งอีเมลถึงสมาชิกคนนี้';
$string['messageselectadd'] = 'เพิ่ม / ส่งข้อความ';
$string['middlename'] = 'ชื่อกลาง';
$string['migratinggrades'] = 'ย้ายคะแนน';
$string['min'] = 'นาที';
$string['mins'] = 'นาที';
$string['minute'] = 'นาที';
$string['minutes'] = 'นาที';
$string['miscellaneous'] = 'ทั่วไป';
$string['missingcategory'] = 'กรุณาเลือกประเภท';
$string['missingdescription'] = 'ขาดข้อมูลส่วนตัว';
$string['missingemail'] = 'ขาดอีเมล';
$string['missingfirstname'] = 'ขาดชื่อ';
$string['missingfromdisk'] = 'ขาดดิสก์';
$string['missingfullname'] = 'ขาดชื่อเต็ม';
$string['missinglastname'] = 'ขาดนามสกุล';
$string['missingname'] = 'ขาดชื่อ';
$string['missingnewpassword'] = 'ขาดรหัสใหม่';
$string['missingpassword'] = 'ขาดรหัส';
$string['missingrecaptchachallengefield'] = 'ขาด reCAPTCHA';
$string['missingreqreason'] = 'ขาดเหตุผล';
$string['missingshortname'] = 'ขาดชื่อย่อ';
$string['missingshortsitename'] = 'ขาดชื่อย่อของเว็บ';
$string['missingsitedescription'] = 'ขาดรายละเอียดของเว็บ';
$string['missingsitename'] = 'ขาดชื่อเว็บ';
$string['missingstrings'] = 'ตรวจสอบหา strings ที่หายไป';
$string['missingstudent'] = 'ต้องเลือก';
$string['missingsummary'] = 'ขาดบทคัดย่อ';
$string['missingteacher'] = 'ต้องเลือก';
$string['missingurl'] = 'URL ผิดพลาด';
$string['missingusername'] = 'ขาดชื่อผู้ใช้';
$string['moddoesnotsupporttype'] = 'โมดูล {$a->modname} ไม่สนับสนุนการอัพโหลดประเภท {$a->type}';
$string['modhide'] = 'ซ่อน';
$string['modified'] = 'แก้ไขแล้ว';
$string['modshow'] = 'แสดง';
$string['moduleintro'] = 'คำอธิบาย';
$string['modulesetup'] = 'ติดตั้งตารางโมดูล';
$string['modulesuccess'] = 'ตาราง {$a}  ได้รับการติดตั้งแล้ว';
$string['modulesused'] = 'โมดูลที่ใช้';
$string['modvisible'] = 'รายวิชาที่มีอยู่';
$string['modvisiblehiddensection'] = 'รายวิชาที่มีอยู่';
$string['modvisiblewithstealth'] = 'รายวิชาที่มีอยู่';
$string['month'] = 'เดือน';
$string['months'] = 'เดือน';
$string['moodledocs'] = 'คู่มือการใช้งาน Moodle';
$string['moodledocslink'] = 'เอกสารช่วยเหลือสำหรับหน้านี้';
$string['moodlelogo'] = 'โลโก้ Moodle';
$string['moodlerelease'] = 'Moodle release';
$string['moodleversion'] = 'เวอร์ชั่น Moodle';
$string['more'] = 'มีต่อ..';
$string['morehelp'] = 'ความช่วยเหลือเพิ่มเติม';
$string['moreinfo'] = 'ข้อมูลเพิ่มเติม';
$string['moreinformation'] = 'ข้อมูลเพิ่มเติมเกี่ยวกับข้อผิดพลาดนี้';
$string['morenavigationlinks'] = 'เพิ่มเติม...';
$string['moreprofileinfoneeded'] = 'ข้อมูลเพิ่มเติมเกี่ยวกับตัวท่าน';
$string['mostrecently'] = 'ล่าสุด';
$string['move'] = 'ย้าย';
$string['movecategoriessuccess'] = 'ทำการย้าย {$a->count} ประเภท ไปยัง \'{$a->to}\' สำเร็จแล้ว';
$string['movecategoriestotopsuccess'] = 'ทำการย้าย {$a->count} ประเภท ไปยังระดับสูงสุดแล้ว';
$string['movecategorycontentto'] = 'ย้ายไปยัง';
$string['movecategorysuccess'] = 'ทำการย้ายประเภท  \'{$a->moved}\' ไปยังประเภท \'{$a->to}\' เรียบร้อยแล้ว';
$string['movecategoryto'] = 'ย้ายประเภทไปยัง';
$string['movecategorytotopsuccess'] = 'ทำการย้ายประเภท \'{$a->moved}\' ไปยังระดับสูงสุดแล้ว';
$string['movecontent'] = 'ย้าย {$a}';
$string['movecontentafter'] = 'หลังจาก  "{$a}"';
$string['movecontentstoanothercategory'] = 'ย้ายเนื้อหาไปยังประเภทอื่น';
$string['movecontenttothetop'] = 'ไปยังจุดสูงสุดของรายชื่อ';
$string['movecoursemodule'] = 'ย้ายแหล่งข้อมูล';
$string['movecoursesection'] = 'ย้ายหัวข้อ';
$string['movecourseto'] = 'ย้ายรายวิชานี้ไปยัง';
$string['movedown'] = 'เลื่อนลง';
$string['movefilestohere'] = 'ย้ายไฟล์มาที่นี่';
$string['movefull'] = 'ย้าย {$a}  ไปยัง';
$string['movehere'] = 'ย้ายมาที่นี่';
$string['moveleft'] = 'ย้ายไปทางซ้าย';
$string['moveright'] = 'ย้ายไปทางขวา';
$string['movesection'] = 'ย้ายส่วน {$a}';
$string['moveselectedcategoriesto'] = 'ย้ายประเภทที่เลือกไปยัง';
$string['moveselectedcoursesto'] = 'ย้ายรายวิชาที่เลือกไปยัง';
$string['movetoanotherfolder'] = 'ย้ายไฟล์ไปแฟ้มอื่น';
$string['moveup'] = 'เลื่อนขึ้น';
$string['msnid'] = 'MSN ID';
$string['mustchangepassword'] = 'รหัสผ่านใหม่ต้องต่างไปจากรหัสผ่านเดิมค่ะ';
$string['mustconfirm'] = 'คุณต้องยืนยันการเข้าสู่ระบบ';
$string['mycourses'] = 'วิชาเรียนของฉัน';
$string['myfiles'] = 'ไฟล์ส่วนตัว';
$string['myfilesmanage'] = 'จัดการไฟล์ส่วนตัว';
$string['myhome'] = 'แผงควบคุม';
$string['mymoodledashboard'] = 'แผงควบคุม Moodle ของฉัน';
$string['myprofile'] = 'ประวัติส่วนตัว';
$string['name'] = 'ชื่อ';
$string['namedfiletoolarge'] = 'ไฟล์ \'{$a->filename}\' ใหญ่เกินไป ทำให้ไม่สามารถอัปโหลดได้';
$string['nameforlink'] = 'ท่านเรียกลิงก์นี้ว่าอะไร';
$string['nameforpage'] = 'ชื่อ';
$string['navigation'] = 'Navigation';
$string['needed'] = 'ต้องการ';
$string['networkdropped'] = 'การเชื่อมต่ออินเทอร์เน็ตของท่านไม่เสถียรหรือถูกขัดจังหวะ การเปลี่ยนแปลงที่ท่านได้ทำไปจะไม่ได้การบันทึกจนกว่าการเชื่อมต่อจะได้รับการแก้ไข';
$string['never'] = 'ไม่เคย';
$string['neverdeletelogs'] = 'ไม่ลบไฟล์ประวัติการใช้งาน';
$string['new'] = 'ใหม่';
$string['newaccount'] = 'บัญชีผู้ใช้ใหม่';
$string['newactivityname'] = 'ชื่อใหม่ของกิจกรรม {$a}';
$string['newcourse'] = 'รายวิชาใหม่';
$string['newpassword'] = 'รหัสผ่านใหม่';
$string['newpassword_help'] = 'กรุณาใส่รหัสผ่านใหม่ หรือ ปล่อยว่างไว้หากต้องการใช้รหัสผ่านเดิม';
$string['newpasswordfromlost'] = '<strong> หมายเหตุ :</strong> รหัสผ่านปัจจุบัน จะถูกส่งไปยังอีเมลที่สองของคุณ กรุณาตรวจสอบให้แน่ใจว่าคุณได้รับรหัสผ่าน ก่อนดำเนินการขั้นต่อไป';
$string['newpasswordtext'] = 'สวัสดีค่ะ/ครับ {$a->firstname},

ระบบได้ทำการรีเซ็ทรหัสผ่านของท่าน ที่ \'{$a->sitename}\'
และท่านได้รับรหัสชั่วคราวดังต่อไปนี้

สำหรับการ Login ครั้งต่อไปให้ใช้:
   ชื่อผู้ใช้: {$a->username}
   รหัสผ่าน: {$a->newpassword}

คลิกที่นี่เพื่อแก้ไขรหัสผ่าน:
   {$a->link}

ในโปรแกรมรับส่งอีเมลทั่วไป ท่านควรจะเห็น ลิงก์ข้างบน ปรากฎเป็นสีน้ำเงิน ซึ่งท่านสามารถคลิกเข้าไปได้ แต่ในกรณีที่ มันไม่ทำงาน ให้ทำการก้อปปี้ ลิงก์ดังกล่าว แล้ว นำไป วางไว้ ใน web brower

จาก ผู้ดูแลระบบ  \'{$a->sitename}\' ,
{$a->signoff}';
$string['newpicture'] = 'ภาพใหม่';
$string['newpicture_help'] = 'เพิ่มภาพส่วนตัว สามารถทำได้โดยการบราวซ์และเลือกรูปภาพ ( JPG   หรือ  PNG) จากนั้นให้คลิกที่ " อัพเดทประวัติส่วนตัว" รูปภาพที่ใช้ควรมีขนาด 100 x 100 พิกเซล';
$string['newpictureusernotsetup'] = 'รูปประจำตัวจะถูกเพิ่มได้เมื่อข้อมูลส่วนตัวที่จำเป็นทั้งหมดได้ถูกบันทึกแล้ว';
$string['newsectionname'] = 'ชื่อใหม่สำหรับหมวดหมู่ {$a}';
$string['newsitem'] = 'ข่าว';
$string['newsitems'] = 'ข่าว';
$string['newsitemsnumber'] = 'จำนวนข่าวที่ต้องการแสดง';
$string['newsitemsnumber_help'] = 'การตั้งค่าเพื่อแสดงจำนวนข่าวล่าสุด หากตั้งค่าเป็น 0 จะไม่มีการแสดงบล็อคข่าวล่าสุด';
$string['newuser'] = 'สมาชิกใหม่';
$string['newusernewpasswordsubj'] = 'บัญชีสมาชิกใหม่';
$string['newusernewpasswordtext'] = 'เรียนคุณ {$a->firstname}

ได้มีการสร้างบัญชีผู้ใช้ใหม่ให้ท่านที่  \'{$a->sitename}\'
ระบบได้ทำการสร้างรหัสผ่านชั่วคราวให้ท่านเรียบร้อยแล้ว

ข้อมูลในการเข้าสู่ระบบของท่านมีดังต่อไปนี้ :
ชื่อผู้ใช้: {$a->username}
รหัสผ่าน: {$a->newpassword}
(ท่านต้องทำการเปลี่ยนรหัสผ่านใหม่หลังจากที่ทำการเข้าสู่ระบบในครั้งแรก )

เริ่มใช้งานเว็บไซต์ \'{$a->sitename}\'ได้ที่
{$a->link}

ในโปรแกรมสำหรับอ่านอีเมลส่วนใหญ่แล้ว ลิงก์ด้านบนจะปรากฎเป็นตัวหนังสือสีน้ำเงิน ท่านสามารถคลิกที่ลิงก์ได้โดยตรง ในกรณีที่ไม่สามารถคลิกได้ให้ทำการสำเนาลิงก์ดังกล่าวแล้วนำไปวางในหน้าเว็บบราวเซอร์ใหม่

ด้วยความนับถือ ผู้ดูแลระบบ \'{$a->sitename}\' administrator,
{$a->signoff}';
$string['newusers'] = 'สมาชิกใหม่ทั้งหมด';
$string['newwindow'] = 'หน้าต่างใหม่';
$string['next'] = 'ต่อไป';
$string['nextsection'] = 'ส่วนต่อไป';
$string['no'] = 'ไม่';
$string['noblockstoaddhere'] = 'ไม่สามารถเพิ่มบล็อคในหน้านี้ได้';
$string['nobody'] = 'ไม่มีใครเลย';
$string['nochange'] = 'ไม่มีการเปลี่ยนแปลง';
$string['nocomments'] = 'ไม่มีความคิดเห็น';
$string['nocourses'] = 'ไม่มีประเภท';
$string['nocoursesfound'] = 'ไม่พบรายวิชาที่มีคำว่า \'{$a}\'';
$string['nocoursestarttime'] = 'วิชาไม่มีวันเริ่มต้น';
$string['nocoursesyet'] = 'ยังไม่มีรายวิชาในประเภทนี้';
$string['nodstpresets'] = 'ผู้ดูแลระบบยังไม่ได้เปิดการใช้งาน Daylight Savings Time';
$string['nofilesselected'] = 'ยังไม่ได้เลือกไฟล์ที่ต้องการนำกลับมาใหม่';
$string['nofilesyet'] = 'ยังไม่มีไฟล์อัพโหลดสำหรับรายวิชานี้';
$string['nofiltersapplied'] = 'ไม่มีตัวกรองถูกนำมาใช้';
$string['nograde'] = 'ไม่มีคะแนน';
$string['nohelpforactivityorresource'] = 'ไม่มีความช่วยเหลือที่เกี่ยวข้องกับแหล่งข้อมูลนี้หรือกิจกรรมนี้';
$string['noimagesyet'] = 'ยังไม่มีภาพอัพโหลดมาสำหรับรายวิชานี้';
$string['nologsfound'] = 'ไม่พบบันทึกการใช้งาน';
$string['nomatchingusers'] = 'ไม่มีสมาชิกที่ตรงกับ \'{$a}\'';
$string['nomorecourses'] = 'ไม่พบรายวิชาที่ต้องการ';
$string['nomoreidnumber'] = 'ไม่ใช้ idnumber เพื่อป้องกันการใช้หมายเลขซ้ำกัน';
$string['none'] = 'ไม่มี';
$string['noneditingteacher'] = 'อาจารย์ที่ไม่มีสิทธิ์ในการแก้ไข';
$string['noneditingteacherdescription'] = 'อาจารย์ที่ไม่มีสิทธิ์ในการแก้ไข นั้นเป็นผู้สอนในรายวิชา สามารถให้คะแนนนักเรียนได้แต่ะไม่สามารถเปลี่ยนแปลงกิจกรรมภายในห้องเรียนได้';
$string['nonstandard'] = 'ไม่มาตรฐาน';
$string['nopendingcourses'] = 'ไม่มีรายวิชาที่รอการพิจารณาอนุมัติ';
$string['nopotentialadmins'] = 'ยังไม่มีผู้ที่สามารถเป็นผู้ดูแลระบบ ได้';
$string['nopotentialcreators'] = 'ยังไม่มีผู้ที่สามารถเป็นผู้สร้างรายวิชาได้';
$string['nopotentialstudents'] = 'ยังไม่มีนักเรียน';
$string['nopotentialteachers'] = 'ยังไม่มีผู้ที่สามารถเป็นอาจารย์ผู้สอนได้';
$string['norecentactivity'] = 'ไม่มีกิจกรรมล่าสุด';
$string['noreplybouncemessage'] = 'ท่านได้ตอบกลับอีเมล noreply ถ้าหากต้องการตอบกระทู้ในกระดานเสวนาให้เข้าไปที่กระดานเสวนา  {$a} โดยตรง

เนื้อหาในอีเมลของคุณคือ';
$string['noreplybouncesubject'] = '{$a} - อีเมลย้อนกลับ';
$string['noreplyname'] = 'กรุณาอย่าตอบอีเมลนี้';
$string['noresetrecord'] = 'ไม่มีข้อมูลคำขอรีเซ็ท กรุณาส่งคำขอรีเซ็ทรหัสผ่านใหม่';
$string['noresults'] = 'ไม่พบผลลัพธ์ใด';
$string['normal'] = 'ปกติ';
$string['normalfilter'] = 'ค้นหาธรรมดา';
$string['nosite'] = 'ไม่พบคอร์สในระดับเว็บไซต์';
$string['nostudentsfound'] = 'ไม่พบ{$a}';
$string['nostudentsingroup'] = 'ยังไม่มีนักเรียนในกลุ่มนี้';
$string['nostudentsyet'] = 'ยังไม่มีนักเรียนในรายวิชานี้';
$string['nosuchemail'] = 'อีเมลไม่ถูกต้อง';
$string['notavailable'] = 'ไม่พบ';
$string['notavailablecourse'] = 'ไม่พบ {$a}';
$string['noteachersyet'] = 'ยังไม่มีอาจารย์ในรายวิชานี้';
$string['noteachingupcomingcourses'] = 'วิชาที่ไม่มีทั้งผู้สอนและผู้เรียนที่กำลังจะเปิดเร็วๆนี้';
$string['notenrolled'] = '{$a} ไม่ได้สมัครเข้าศึกษารายวิชานี้';
$string['notenrolledprofile'] = 'ไม่พบประวัติของสมาชิกคนนี้เนื่องจากยังไม่ได้เป็นนักเรียนในรายวิชา';
$string['noteusercannotrolldatesoncontext'] = 'หมายเหตุ : ท่านไม่สามารถย้อนวันที่เมื่อทำการกู้คืนไฟล์สำรองได้เพราะยังไม่ได้สิทธิจากระบบ';
$string['noteuserschangednonetocourse'] = '<strong>โปรดทราบ</strong> ต้องทำการกู้สมาชิกในรายวิชานี้คืน เมื่อทำการกู้ข้อมูลของสมาชิก มีการเปลี่ยนค่านี้สำหรับคุณ';
$string['nothingnew'] = 'ไม่มีกิจกรรมใหม่';
$string['nothingtodisplay'] = 'ไม่มีสิ่งที่ต้องแสดง';
$string['notice'] = 'ประกาศ';
$string['noticenewerbackup'] = 'ไฟล์สำรองข้อมูลนี้สร้างขึ้นโดย  Moodle {$a->backuprelease} ({$a->backupversion})  และใหม่กว่าเวอร์ชันที่คุณติดตั้งอยู่ในขณะนี้  คุณติดตั้ง  Moodle {$a->serverrelease} ({$a->serverversion}) อาจทำให้ระบบไม่เสถียรได้';
$string['notifications'] = 'การแจ้งเตือนจากระบบ';
$string['notifyloginfailuresmessage'] = '{$a->time}, IP: {$a->ip}, ชื่อจริงผู้ใช้: {$a->info}';
$string['notifyloginfailuresmessageend'] = 'คุณสามารถดูบันทึกดังกล่าวได้จาก {$a}';
$string['notifyloginfailuresmessagestart'] = 'รายการของการล็อกอินเข้าสู่ระบบที่ไม่สำเร็จ เมื่อ {$a} นับตั้งแต่ได้รับการแจ้งครั้งสุดท้าย';
$string['notifyloginfailuressubject'] = '{$a} :: แจ้งการล็อกอินเข้าสู่ระบบไม่สำเร็จ';
$string['notincluded'] = 'ไม่รวม';
$string['notingroup'] = 'ขออภัยด้วย คุณต้องเป็นสมาชิกของกลุ่มนี้ก่อนจึงจะเข้าศึกษากิจกรรมในกลุ่มนี้ได้';
$string['notpublic'] = 'ไม่เปิดให้บุคคลทั่วไปใช้งาน';
$string['nousersfound'] = 'ไม่พบสมาชิก';
$string['nousersmatching'] = 'หาสมาชิกที่ตรงกับ \'{$a}\'  ไม่พบ';
$string['nousersyet'] = 'ยังไม่มีสมาชิก';
$string['novalidcourses'] = 'ไม่มีรายวิชาใด ๆแสดง';
$string['now'] = 'ตอนนี้';
$string['numattempts'] = '{$a}  ครั้ง (จำนวนที่ล็อกอินผิดพลาด)';
$string['numberofcourses'] = 'จำนวนรายวิชา';
$string['numberweeks'] = 'จำนวนหัวข้อ';
$string['numday'] = '{$a} วัน';
$string['numdays'] = '{$a} วัน';
$string['numhours'] = '{$a} ชั่วโมง';
$string['numletters'] = '{$a} ตัวอักษร';
$string['numminutes'] = '{$a} นาที';
$string['nummonth'] = '{$a} เดือน';
$string['nummonths'] = '{$a} เดือน';
$string['numseconds'] = '{$a} วินาที';
$string['numviews'] = '{$a} ครั้ง';
$string['numweek'] = '{$a} สัปดาห์';
$string['numweeks'] = '{$a} สัปดาห์';
$string['numwords'] = '{$a} คำ';
$string['numyear'] = '{$a} ปี';
$string['numyears'] = '{$a} ปี';
$string['ok'] = 'เรียบร้อย';
$string['oldpassword'] = 'รหัสผ่านปัจจุบัน';
$string['olduserdirectory'] = 'นี่คือไดเรกทอรีเดิมของผู้ใช้  และไม่ได้ใช้งานแล้ว ท่านสามารถลบทิ้งได้ ไฟล์ทั้งหมดถูกย้ายไปยังไดเรกทอรีใหม่แล้ว';
$string['optional'] = 'ใส่หรือไม่ก็ได้';
$string['options'] = 'ใส่หรือไม่ก็ได้';
$string['order'] = 'ตามลำดับ';
$string['originalpath'] = 'Path เดิม';
$string['orphanedactivitiesinsectionno'] = 'กิจกรรม  (หัวข้อ {$a})';
$string['other'] = 'อื่น ๆ';
$string['outline'] = 'โครงสร้าง';
$string['outlinereport'] = 'โครงสร้างรายงาน';
$string['page'] = 'หน้า';
$string['pagea'] = 'หน้า {$a}';
$string['pagedcontentnavigationitem'] = 'ไปที่หน้า {$a}';
$string['pageheaderconfigablock'] = 'ตั้งค่าบล็อคใน%fullname%';
$string['pagepath'] = 'Path ของหน้านี้';
$string['pageshouldredirect'] = 'หน้านี้จะได้รับการเปลี่ยนอัตโนมัติ หากไม่มีอะไรเกิดขึ้นให้คลิกที่ลิงก์ด้านล่าง';
$string['parentcategory'] = 'ประเภทที่อยู่เหนือกว่า';
$string['parentcoursenotfound'] = 'ไม่พบคอร์สที่เหนือกว่า';
$string['parentfolder'] = 'แฟ้มที่อยู่เหนือกว่า';
$string['participants'] = 'นักเรียนและผู้สนใจ';
$string['participantslist'] = 'รายชื่อผู้เข้าร่วม';
$string['participationratio'] = 'อัตราส่วนการมีส่วนร่วม';
$string['participationreport'] = 'รายงานการมีส่วนร่วม';
$string['password'] = 'รหัสผ่าน';
$string['passwordchanged'] = 'เปลี่ยนรหัสผ่านเรียบร้อยแล้วค่ะ';
$string['passwordconfirmchange'] = 'ยืนยันการเปลี่ยนรหัสผ่าน';
$string['passwordextlink'] = 'ลิงก์ไปยังหน้าที่ต้องการกู้รหัสผ่านคืน';
$string['passwordforgotten'] = 'ลืมรหัสผ่าน';
$string['passwordforgotteninstructions'] = 'ท่านสามารถเลือกใสชื่อผู้ใช้หรือ อีเมล ที่ได้ทำการลงทะเบียนไว้กับทางเว็บไซต์ ไม่จำเป็นต้องใส่ทั้งคู่';
$string['passwordforgotteninstructions2'] = 'ในการรีเซ็ทรหัสผ่าน ให้ใส่ชื่อผู้ใช้หรืออีเมลของท่านด้านล่างนี้ ถ้าหากระบบพบข้อมูลในฐานข้อมูลจะทำการส่งอีเมลไปพร้อมกับคู่มือการเข้าใช้งาน';
$string['passwordnohelp'] = 'ไม่สามารถหารหัสผ่านของท่านได้ กรุณาติดต่อผู้ดูแลระบบ';
$string['passwordrecovery'] = 'ใช่ ,ช่วยในการเข้าสู่ระบบด้วย';
$string['passwordsdiffer'] = 'รหัสผ่านไม่ถูกต้อง';
$string['passwordsent'] = 'ส่งรหัสผ่านเรียบร้อยแล้ว';
$string['passwordsenttext'] = '<P>อีเมลส่งไปให้ท่านแล้วที่    {$a->email}. <P><B>กรุณาเช็คอีเมลเพื่อเช็ครหัสผ่านใหม่</B>
   <P>หาต้องการเปลี่ยนรหัสผ่าน <A HREF={$a->link}>ให้คลิกที่ลิงก์นี้</A>.';
$string['passwordset'] = 'ส่งรหัสผ่านให้ท่านแล้ว';
$string['path'] = 'Path';
$string['pathnotexists'] = 'ไม่มี path นี้ในเซิร์ฟเวอร์';
$string['pathslasherror'] = 'อย่าลงท้าย path ด้วยเครื่องหมายสแลช';
$string['paymentinstant'] = 'ใช้ปุ่มข้างล่างนี้เพื่อทำการชำระค่าสมัครเข้าเรียนและเข้าเรียน';
$string['paymentpending'] = '(<small><b><u>{$a}</u></b> รอการอนุมัติ</small>)';
$string['paymentrequired'] = 'ท่านต้องชำระค่าสมัครเข้าเรียนในรายวิชานี้';
$string['payments'] = 'การชำระเงิน';
$string['paymentsorry'] = 'ขอบคุณที่สมัครเข้าเรียน แต่ระบบยังไม่ได้รับค่าสมัครในขณะนี้ ท่านยังไม่สามารถเข้าไปศึกษาในวิชา "{$a->fullname}" ได้ในขณะนี้ กรุณารอสักครู่แล้วทำการเข้ามายังรายวิชานี้ใหม่ ถ้าหากท่านยังมีปัญหากรุณาแจ้ง  {$a->teacher} หรือผู้ดูแลระบบให้ทราบ';
$string['paymentthanks'] = 'ขอบคุณสำหรับการชำระค่าสมัคร ขณะนี้ท่านเป็นนักเรียนในรายวิชา:<br />"{$a}"';
$string['pendingrequests'] = 'คำร้องที่กำลังดำเนินการ';
$string['percents'] = '{$a}%';
$string['periodending'] = 'ระยะเวลาสิ้นสุด ({$a})';
$string['perpage'] = 'ต่อหน้า';
$string['perpagea'] = 'ต่อหน้า {$a}';
$string['personal'] = 'ส่วนตัว';
$string['personalprofile'] = 'ประวัติสมาชิก';
$string['phone'] = 'โทรศัพท์';
$string['phone1'] = 'โทรศัพท์';
$string['phone2'] = 'มือถือ';
$string['phpinfo'] = 'PHP info';
$string['pictureof'] = 'รูปภาพของ{$a}';
$string['pictureofuser'] = 'รูปภาพส่วนตัว';
$string['pleaseclose'] = 'กรุณาปิดหน้าต่างนี้';
$string['pleasesearchmore'] = 'กรุณาค้นหาเพิ่มเติม';
$string['pleaseusesearch'] = 'กรุณาใช้การค้นหา';
$string['plugin'] = 'ปลั๊กอิน';
$string['plugincheck'] = 'ตรวจสอบปลั๊กอิน';
$string['plugindeletefiles'] = 'ระบบได้ลบข้อมูลทั้งหมดที่เกี่ยวข้อกับปลั๊กอิน{$a->name} ออกจากฐานข้อมูลแล้ว เพื่อป้องกันปลั๊กอินติดตั้งตัวเองกลับเข้ามาในระบบใหม่ ท่านควรทำการลบไดเรกทอรีนี้ออกจากเซิร์ฟเวอร์ : {$a->directory}';
$string['pluginsetup'] = 'ติดตั้งตารางปลั๊กอิน';
$string['policyaccept'] = 'ข้าพเจ้าเข้าใจและยอมรับในเงื่อนไข';
$string['policyagree'] = 'คุณตกลงตามเงื่อนไขการใช้งานเว็บไซต์หรือไม่';
$string['policyagreement'] = 'ข้อตกลงและเงื่อนไขการใช้งาน';
$string['policyagreementclick'] = 'คลิกที่นี่เพื่ออ่านข้อตกลงและเงื่อนไข';
$string['popup'] = 'ป๊อบอัพ';
$string['popupwindow'] = 'เปิดไฟล์ในหน้าต่างใหม่';
$string['popupwindowname'] = 'หน้าต่างป๊อบอัพ';
$string['post'] = 'โพสต์';
$string['posts'] = 'โพสต์';
$string['potentialadmins'] = 'ผู้ที่สามารถเป็นผู้ดูแลระบบได้';
$string['potentialcreators'] = 'รายชื่อผู้ที่สามารถเป็นผู้สร้างรายวิชาได้';
$string['potentialstudents'] = 'รายชื่อผู้ที่สามารถเป็นนักเรียนได้';
$string['potentialteachers'] = 'รายชื่อผู้ที่สามารถเป็นอาจารย์ผู้สอนได้';
$string['preferences'] = 'ค่าที่ต้องการ';
$string['preferredlanguage'] = 'ภาษาที่ต้องการ';
$string['preferredtheme'] = 'รูปแบบที่ต้องการ';
$string['preprocessingbackupfile'] = 'กำลังจัดการไฟล์สำรองข้อมูล';
$string['prev'] = 'ก่อนหน้า';
$string['preview'] = 'แสดงตัวอย่าง';
$string['previewhtml'] = 'ดูตัวอย่างรูปแบบ HTML';
$string['previeworchoose'] = 'แสดงตัวอย่างหรือ เลือกรูปแบบเว็บไซต์';
$string['previous'] = 'หน้าก่อน';
$string['previouslyselectedusers'] = 'สมาชิกที่เลือกก่อนหน้าไม่ตรงกับ\'{$a}\'';
$string['previoussection'] = 'ส่วนก่อนหน้า';
$string['primaryadminsetup'] = 'ตั้งค่าบัญชีผู้ใช้ของผู้ดูแลระบบ';
$string['private_files_handler'] = 'เก็บไฟล์ที่แนบกับอีเมลไว้ในพื้นที่เก็บไฟล์ส่วนตัว';
$string['private_files_handler_name'] = 'อีเมลไปยังไฟล์ส่วนตัว';
$string['privatefiles'] = 'ไฟล์ส่วนตัว';
$string['privatefilesmanage'] = 'จัดการไฟล์ส่วนตัว';
$string['profile'] = 'ประวัติส่วนตัว';
$string['profilenotshown'] = 'ระบบจะไม่แสดงประวัติส่วนตัวจนกว่าสมาชิกจะทำการสมัครเข้าไปยังรายวิชาใดรายวิชาหนึ่ง';
$string['publicprofile'] = 'ประวัติสาธารณะ';
$string['publicsitefileswarning'] = 'หมายเหตุ : ทุกคนสามารถเข้าชมไฟล์ที่อยู่ในหน้านี้ได้';
$string['publicsitefileswarning2'] = 'หมายเหตุ: ไฟล์ที่เก็บไว้ในส่วนนี้ ใครก็ตามที่รู้ที่อยู่ URL สามารถเข้าถึงได้ เพื่อความปลอดภัย ควรลบไฟล์สำรองทันทีที่ทำการกู้คืนเสร็จเรียบร้อย';
$string['publicsitefileswarning3'] = 'หมายเหตุ: ไฟล์ที่เก็บไว้ในส่วนนี้ ใครก็ตามที่รู้ที่อยู่ URL สามารถเข้าถึงได้ เพื่อความปลอดภัย ไฟล์สำรองควรเก็บไว้ในแฟ้มที่ปลอดภัยเท่านั้น';
$string['question'] = 'คำถาม';
$string['questionsinthequestionbank'] = 'คำถามในคลังคำถาม';
$string['readinginfofrombackup'] = 'กำลังอ่านข้อมูลจากข้อมูลสำรอง';
$string['readme'] = 'อ่านที่นี่ก่อน';
$string['recentactivity'] = 'กิจกรรมล่าสุด';
$string['recentactivityreport'] = 'รายงานฉบับสมบูรณ์ของกิจกรรมล่าสุด';
$string['recipientslist'] = 'รายชื่อผู้รับข้อความ';
$string['recreatedcategory'] = 'สร้างประเภท {$a} ขึ้นใหม่';
$string['redirect'] = 'พาไปยัง';
$string['reducesections'] = 'ลดจำนวนหัวข้อ';
$string['refresh'] = 'รีเฟรช';
$string['refreshingevents'] = 'รีเฟรชกิจกรรม';
$string['registration'] = 'การลงทะเบียน Moodle';
$string['registrationcontact'] = 'การติดต่อจากบุคคลทั่วไป';
$string['registrationcontactno'] = 'ไม่ : ไม่ต้องการให้บุคคลอื่นติดต่อผ่านฟอร์มการติดต่อของเว็บไซต์';
$string['registrationcontactyes'] = 'ใช่ :  สร้างฟอร์มให้บุคคลอื่นติดต่อได้ผ่านทางเว็บไซต์';
$string['registrationemail'] = 'แสดงอีเมล';
$string['registrationinfo'] = '<p>ท่านสามารถลงทะเบียนเว็บไซต์ Moodle ของท่านกับทาง moodle.org โดยไม่เสียค่าใช้จ่ายใด ๆ ประโยชน์ของการลงทะเบียนคือท่านจะได้รับอีเมลแจ้งข่าวที่สำคัญ อาทิแจ้งเกี่ยวกับระบบความปลอดภัยของ Moodle หรือเมื่อมีเวอร์ชันใหม่พร้อมให้ดาวน์โหลด

<p>ทาง Moodle.org จะเก็บข้อมูลของท่านไว้เป็นความลับและไม่มีการส่งต่อข้อมูลให้กับเว็บไซต์อื่นใด แต่จะเก็บเป็นข้อมูลสำหรับสถิติผู้ใช้งาน Moodle  ทั้งหมดที่มีอยู่

<p>ท่านสามารถเลือกที่จะให้ชื่อเว็บไซต์ ประเทศ และ URL ปรากฎอยู่ในหน้ารวมลิงก์ของ Moodle.org
<p>ทาง Moodle.org จะทำการตรวจสอบแบบฟอร์มการลงทะเบียนแต่ละอันก่อนที่จะเพิ่มชื่อลงในเว็บไซต์ หลังจากที่เว็บของท่านปรากฎอยู่บน Moodle.org แล้วคุณสามารถทำการเปลี่ยนแปลงข้อมูลต่าง ๆ ได้ในภายหลัง';
$string['registrationinfotitle'] = 'ข้อมูลการลงทะเบียน';
$string['registrationno'] = 'ไม่ต้องการรับอีเมล';
$string['registrationsend'] = 'ส่งข้อมูลการลงทะเบียนไปยัง moodle.org';
$string['registrationyes'] = 'ใช่ โปรดแจ้งข้อมูลสำคัญด้วย';
$string['reject'] = 'ไม่อนุมัติ';
$string['rejectdots'] = 'ปฏิเสธ';
$string['reload'] = 'โหลดใหม่';
$string['remoteappuser'] = 'รีโมตสมาชิก {$a}';
$string['remove'] = 'ปลดออก';
$string['removeadmin'] = 'ปลดผู้ดูแลระบบออก';
$string['removecreator'] = 'ปลดผู้สร้างรายวิชาออก';
$string['removestudent'] = 'ปลดนักเรียนออก';
$string['removeteacher'] = 'ปลดอาจารย์ออก';
$string['rename'] = 'เปลี่ยนชื่อ';
$string['renamefileto'] = 'เปลี่ยนชื่อจาก <b>{$a}</b>  เป็น';
$string['report'] = 'รายงาน';
$string['reports'] = 'รายงาน';
$string['repositories'] = 'คลังเก็บไฟล์';
$string['requestcourse'] = 'ขอสร้างรายวิชา';
$string['requestedby'] = 'ผู้ขอ';
$string['requestedcourses'] = 'รายวิชาที่ขอสร้าง';
$string['requestreason'] = 'เหตุผลในการขอสร้างรายวิชา';
$string['required'] = 'ต้องการ';
$string['requirespayment'] = 'ต้องเสียค่าสมัครเข้าเรียนรายวิชานี้';
$string['resendemail'] = 'ส่งอีเมล์ใหม่อีกครั้ง';
$string['reset'] = 'รีเซ็ท';
$string['resetcomponent'] = 'ส่วนประกอบ';
$string['resetcourse'] = 'รีเซ็ทรายวิชา';
$string['resetinfo'] = 'ท่านสามารถทำการลบข้อมูลสมาชิกในรายวิชาได้ในหน้านี้ ในขณะที่ยังคงเก็บกิจกรรมและการตั้งค่าอื่น ๆ เอาไว้  พึงตระหนักไว้ว่าการเลือกข้อมูลใด ๆ ด้านล่างนี้แล้วกดตกลง  ข้อมูลดังกล่าวจะถูกลบโดยไม่สามารถกู้กลับคืนมาได้';
$string['resetnotimplemented'] = 'รีเซ็ทไม่ได้ถูกตั้งให้ใช้งาน';
$string['resetrecordexpired'] = 'ลิงก์สำหรับรีเซ็ทรหัสผ่าน มีอายุ มากกว่า {$a} นาที และหมดอายุไปแล้ว กรุณาขอรีเซ็ทรหัสผ่านใหม่';
$string['resetstartdate'] = 'รีเซ็ทวันเริ่มต้น';
$string['resetstatus'] = 'สถานะ';
$string['resettable'] = 'รีเซ็ทตารางค่าที่ต้องการ';
$string['resettask'] = 'งาน';
$string['resettodefaults'] = 'รีเซ็ทเป็นค่าที่ตั้งไว้';
$string['resortcourses'] = 'เรียงลำดับรายวิชา';
$string['resortsubcategoriesby'] = 'เรียงลำดับประเภทย่อย {$a} จากน้อยไปมาก';
$string['resortsubcategoriesbyreverse'] = 'เรียงลำดับประเภทย่อย {$a} จากมากไปน้อย';
$string['resource'] = 'แหล่งข้อมูล';
$string['resourcedisplayauto'] = 'อัตโนมัติ';
$string['resourcedisplaydownload'] = 'บังคับดาวน์โหลด';
$string['resourcedisplayembed'] = 'Embed';
$string['resourcedisplayframe'] = 'ในกรอบ';
$string['resourcedisplaynew'] = 'หน้าต่างใหม่';
$string['resourcedisplayopen'] = 'เปิด';
$string['resourcedisplaypopup'] = 'ในป้อปอัพ';
$string['resources'] = 'เนื้อหา';
$string['resources_help'] = 'สามารถใช้เนื้อหาของเว็บเกือบทุกประเภทลงในประเภทของแหล่งข้อมูล ได้';
$string['restore'] = 'กู้คืน';
$string['restorecancelled'] = 'ยกเลิการกู้คืน';
$string['restorecannotassignroles'] = 'การกู้คืนจำเป็นต้องระบุผู้ที่มีสิทธิ์ในการกู้คืน ท่านไม่มีสิทธิ์ที่จะทำได้';
$string['restorecannotcreateorassignroles'] = 'การกู้คืนจำเป็นต้องสร้างหรือระบุผู้ที่มีสิทธิ์ในการกู้คืน ท่านไม่มีสิทธิ์ที่จะทำได้';
$string['restorecannotcreateuser'] = 'การกู้คืนจำเป็นต้องสร้างสมาชิก \'{$a}\' จากไฟล์กู้คืน ท่านไม่มีสิทธิ์ที่จะทำได้';
$string['restorecannotoverrideperms'] = 'การกู้คืนจำเป็นต้องระบุสิทธิ์ใหม่  ท่านไม่มีสิทธิ์ที่จะทำได้';
$string['restorecoursenow'] = 'กู้คืนรายวิชานี้เดี๋ยวนี้';
$string['restoredaccount'] = 'บัญชีผู้ใช้ที่กู้คืน';
$string['restoredaccountinfo'] = 'บัญชีผู้ใช้นี้นำเข้ามาจากเซิร์ฟเวอร์อื่น และรหัสผ่านได้สูญหายไป หากต้องการตั้งรหัสผ่านทางอีเมลให้คลิก "ต่อไป"';
$string['restorefinished'] = 'การกู้คืนสำเร็จแล้ว';
$string['restoremnethostidmismatch'] = 'MNet host id ของสมาชิก \'{$a}\' ไม่ตรงกับ  local MNet host ID.';
$string['restoreto'] = 'กู้คืนที่';
$string['restoretositeadding'] = 'คำเตือน : ท่านกำลังทำการกู้คืนหน้าแรกของเว็บไซต์ กรุณาเพิ่มข้อมูลลงไป';
$string['restoretositedeleting'] = 'คำเตือน : ท่านกำลังทำการกู้คืนหน้าแรกของเว็บไซต์ กรุณาลบข้อมูลก่อน';
$string['restoreuserconflict'] = 'พยายามทำการกู้คืนสมาชิก \'{$a}\' จากไฟล์สำรอง จะทำให้เกิดความขัดแย้ง';
$string['restoreuserinfofailed'] = 'การกู้คืนหยุด เนื่องจากท่านไม่มีสิทธิ์ในการกู้คืนข้อมูลสมาชิก';
$string['restoreusersprecheck'] = 'ตรวจสอบข้อมูลสมาชิก';
$string['restoreusersprecheckerror'] = 'พบปัญหาระหว่างตรวจสอบข้อมูลสมาชิก';
$string['restricted'] = 'ห้าม';
$string['returningtosite'] = 'กลับมาที่เว็บไซต์นี้อีกครั้ง ?';
$string['returntooriginaluser'] = 'ย้อนไปยัง {$a}';
$string['revert'] = 'ย้อนกลับ';
$string['role'] = 'บทบาท';
$string['roleassignments'] = 'กำหนดบทบาท';
$string['rolemappings'] = 'การจับคู่บทบาท';
$string['rolerenaming'] = 'เปลี่ยนชื่อบทบาท';
$string['rolerenaming_help'] = 'การตั้งค่านี้เป็นการอนุญาตให้ เปลี่ยนชื่อบทบาทที่แสดงในรายวิชา โดยเปลี่ยนแต่ชื่อ แต่สิทธิต่าง ๆยังคงเดิม ชื่อใหม่นี้จะปรากฎในหน้ารายชื่อนักเรียน และที่ต่าง ๆ ในรายวิชา ถ้าชื่อที่เปลี่ยนนี้เป็นบทบาทของผู้จัดการรายวิชาชื่อดังกล่าวจะปรากฎในหน้ารวมรายวิชาด้วย';
$string['roles'] = 'บทบาท';
$string['rss'] = 'RSS';
$string['rssarticles'] = 'จำนวนของบทความ RSS';
$string['rsserror'] = 'มีข้อผิดพลาดในการอ่านข้อมูล RSS';
$string['rsserrorauth'] = 'ลิงก์ RSS ไม่ถูกต้อง';
$string['rsserrorguest'] = 'การส่งข่าวนี้ใช้การเข้าถึงแบบบุคคลทั่วไปเพื่อนเข้าถึงข้อมูล แต่บุคคลทั่วไปไม่สามารถอ่านข้อมูลได้ ไปยัง URL  ที่นำข่าวสารนี้มาใช้ และเข้าไปในฐานะสมาชิกที่ถูกต้องและใช้ลิงก์ RSS อันใหม่';
$string['rsskeyshelp'] = '<p>เพื่อความปลอดภัยและความเป็นส่วนตัว ข่าวสาร RSS จะมีข้อมูลของสมาชิกที่นำมาใช้งาน เพื่อป้องกันบุคคลอื่นเข้าไปยังพื้นที่ที่ไม่ได้รับอนุญาตในเว็บไซต์นั้น ๆ</p><p> ลิงก์ RSS ของท่านจะถูกสร้างขึ้นอัตโนมัติแต่หากคิดว่ามีข้อผิดพลาดท่านสามารถขอลิงก์ใหม่โดยคลิกที่ ลิงก์ รีเซ็ท และลิงก์เดิมของท่านจะใช้การไม่ได้ </p>';
$string['rsstype'] = 'RSS สำหรับกิจกรรมนี้';
$string['save'] = 'บันทึก';
$string['saveandnext'] = 'บันทึกและต่อไป';
$string['savechanges'] = 'บันทึกการเปลี่ยนแปลง';
$string['savechangesanddisplay'] = 'บันทึกและแสดงผล';
$string['savechangesandreturn'] = 'บันทึกและกลับไป';
$string['savechangesandreturntocourse'] = 'บันทึกและกลับไปยังรายวิชา';
$string['savecomment'] = 'บันทึกความคิดเห็น';
$string['savedat'] = 'บันทึกไว้ที่';
$string['savepreferences'] = 'บันทึกค่าที่ต้องการ';
$string['saveto'] = 'บันทึกไว้ที่';
$string['scale'] = 'วิธีการวัด';
$string['scale_help'] = 'วิธีการวัด เป็นวิธีการประเมินผลหรือการให้คะแนน ของผลงานในแต่ละกิจกรรม จำเป็นที่จะต้องมีการเรียงลำดับให้ชัดเจน จาก ลบไปบวก น้อยไปมาก และแยกกันโดยใช้คอมม่า เช่น

" แย่มาก, ไม่ดี ,ปานกลาง ,ดี ,ดีมาก , เยี่ยม "';
$string['scales'] = 'วิธีการวัด';
$string['scalescustom'] = 'เลือกวิธีการวัดเอง';
$string['scalescustomcreate'] = 'เพิ่มวิธีการวัด';
$string['scalescustomno'] = 'ยังไม่มีการเพิ่มวิธีการวัดเอง';
$string['scalesstandard'] = 'วิธีการวัดมาตรฐาน';
$string['scalestandard'] = 'วิธีการวัดมาตรฐาน';
$string['scalestandard_help'] = 'มาตรฐานการวัดเปิดใช้งานตลอดทั้งเว็บไซต์สำหรับทุกรายวิชา';
$string['scalestip'] = 'เพื่อที่จะสร้างการวัดเฉพาะ ให้ใช้ลิงก์ "วิธีการวัด" จากเมนูการจัดการรายวิชา';
$string['scalestip2'] = 'สร้างการวัดเฉพาะ คลิกที่  ลิงก์ "คะแนน" จากเมนูการจัดการรายวิชา จากนั้นคลิก "แก้ไข " "วิธีการวัด"';
$string['schedule'] = 'ตั้งเวลา';
$string['screenshot'] = 'ภาพหน้าจอ';
$string['search'] = 'ค้นหา';
$string['search_help'] = 'สำหรับการค้นหาเบื้องต้น ในส่วนต่าง ๆในข้อความ แค่พิมพ์คำที่ต้องการ แยกด้วย เว้นวรรค ต้องค้นหาคำที่ยาวกว่าสองตัวอักษร

สำหรับการค้นหาชั้นสูง ให้กดปุ่นค้นหาโดยไม่ต้องพิมพ์อะไรลงในกล่องค้นหา เพื่อเข้าไปยังฟอร์มค้นหาชั้นสูง';
$string['searchagain'] = 'ค้นอีกครั้ง';
$string['searchbyemail'] = 'ค้นหาจากที่อยู่อีเมล';
$string['searchbyusername'] = 'ค้นหาจากชื่อผู้ใช้';
$string['searchcourses'] = 'ค้นหารายวิชา';
$string['searchoptions'] = 'ตัวเลือกการค้นหา';
$string['searchresults'] = 'ผลการค้นหา';
$string['sec'] = 'วินาที';
$string['seconds'] = 'วินาที';
$string['secondsleft'] = '{$a} วินาที';
$string['secondstotime172800'] = '2 วัน';
$string['secondstotime259200'] = '3 วัน';
$string['secondstotime345600'] = '4 วัน';
$string['secondstotime432000'] = '5 วัน';
$string['secondstotime518400'] = '6 วัน';
$string['secondstotime604800'] = '1 สัปดาห์';
$string['secondstotime86400'] = '1 วัน';
$string['secretalreadyused'] = 'มีการใช้งานลิงก์สำหรับเปลี่ยนรหัสผ่านไปเรียบร้อยแล้ว ยังไม่มีการเปลี่ยนรหัสผ่าน';
$string['secs'] = 'วินาที';
$string['section'] = 'ส่วน';
$string['sectionname'] = 'ชื่อหัวข้อ';
$string['sections'] = 'หัวข้อ';
$string['seealsostats'] = 'ดูรายละเอียด: สถิติ';
$string['selctauser'] = 'เลือกสมาชิก';
$string['select'] = 'เลือก';
$string['selectacategory'] = 'เลือกประเภท';
$string['selectacountry'] = 'เลือกประเทศ';
$string['selectacourse'] = 'เลือกรายวิชา';
$string['selectacoursesite'] = 'เลือกรายวิชาหรือเว็บไซต์';
$string['selectagroup'] = 'เลือกกลุ่ม';
$string['selectall'] = 'เลือกทั้งหมด';
$string['selectamodule'] = 'กรุณาเลือกชุดกิจกรรม';
$string['selectanaction'] = 'เลือกการกระทำ';
$string['selectanoptions'] = 'เลือกตัวเลือก';
$string['selectaregion'] = 'เลือกภูมิภาค';
$string['selectcategorysort'] = 'ประเภทที่ต้องการเรียง';
$string['selectcategorysortby'] = 'ต้องการเรียงประเภทอย่างไร';
$string['selectcoursesortby'] = 'เรียงรายวิชาอย่างไร';
$string['selectdefault'] = 'เลือกค่าที่ตั้งไว้';
$string['selectedcategories'] = 'เลือกประเภท';
$string['selectedfile'] = 'เลือกไฟล์';
$string['selectednowmove'] = 'ไฟล์จำนวน {$a} ไฟล์ถูกเลือกเพื่อย้าย ต่อไปให้ไปเลือกที่ปลายทางและกด "ย้ายไฟล์มาที่นี่"';
$string['selectfiles'] = 'เลือกไฟล์';
$string['selectmoduletoviewhelp'] = 'เลือกกิจกรรมหรือแหล่งข้อมูลเพื่อดูการช่วยเหลือ ดับเบิ้ลคลิกที่กิจกรรมหรือแหล่งข้อมูลเพื่อเพิ่ม';
$string['selectnos'] = 'เลือก \'ไม่\' ทั้งหมด';
$string['selectperiod'] = 'เลือกช่วงเวลา';
$string['senddetails'] = 'ส่งรายละเอียดผ่านอีเมล';
$string['separate'] = 'แยก';
$string['separateandconnected'] = 'วิธีเรียนรู้แบบแยกส่วนและเชื่อมโยง';
$string['separateandconnectedinfo'] = 'การวัดขึ้นอยู่กับทฤษฎีการเรียนรู้แบบแยกส่วนและแบบเชื่อมโยง ทฤษฎีดังกล่าวอธิบายวิธีการประเมินการเรียนการสอนเกี่ยวกับสิ่งที่เราเห็นและได้ยิน
<ul><li><strong>ผู้เรียนรู้แบบแยกส่วน</strong>  มักจะเรียนรู้โดยปราศจากความรู้สึกและอารมณ์ร่วมในการอภิปรายจะใช้ตรรกะ หาช่องโหว่ในความคิดของฝ่ายตรงข้าม เพื่อสนับสนุนความคิดของตนเอง</li><li><strong>ผู้เรียนรู้แบบเชื่อมโยง</strong>  เป็นผู้เรียนที่มักมีอารมณ์และความรู้สึกร่วม รู้สึกเห็นหกเห็นใจ ฟังคำถามและถามคำถามจนกว่าจะรู้สึกเชื่อมโยงเข้ากับประเด็น และ "เข้าใจเรื่องราวในมุมมองของตนเอง"  เป็นผู้เรียนที่พยายามจะแชร์ประสบการณ์ซึ่งนำไปสู่ความรู้ที่ได้รับมาจากบุคคลอื่น</li></ul>';
$string['servererror'] = 'มีข้อผิดพลาดในระหว่างที่ติดต่อกับเซิร์ฟเวอร์';
$string['serverlocaltime'] = 'เวลาของเซิร์ฟเวอร์';
$string['setcategorytheme'] = 'ตั้งค่ารูปแบบเว็บสำหรับประเภท';
$string['setpassword'] = 'ตั้งค่ารหัสผ่าน';
$string['setpasswordinstructions'] = 'กรุณาใส่รหัสผ่านและใส่ซ้ำด้านล่างนี้ จากนั้นคลิก " ตั้งค่ารหัสผ่าน"<br /> รหัสผ่านใหม่ของท่านได้รับการบันทึกแล้ว และท่านได้เข้าสู่ระบบแล้ว';
$string['settings'] = 'การตั้งค่า';
$string['shortname'] = 'ชื่อย่อ';
$string['shortnamecollisionwarning'] = '[*] = มีการใช้ชื่อย่อนี้แล้ว กรุณาเลือกชื่อใหม่ เมื่อได้รับการอนุมัติ';
$string['shortnamecourse'] = 'ชื่อย่อรายวิชา';
$string['shortnamecourse_help'] = '<P ALIGN=CENTER><B>ชื่อย่อของรายวิชา</B></P>

<P>ในหลายๆ สถาบัน มีการเรียกรายวิชาต่างๆ ด้วยชื่อย่อ เช่น  ว 101  ว 102
ส 304 ส 305  ค203  เป็นต้น  ถึงแม้คุณจะไม่มีชื่อรายวิชา ก็ตาม แต่งขึ้นมาซัก
ชื่อค่ะ เพราะว่าจะทำให้สะดวก ในการแสดงผล เช่น ในอีเมลเป็นต้น เพราะ
ชื่อเต็มนั้นจะยาวเกินไป </P>';
$string['shortnametaken'] = 'รายวิชาอื่นใช้ชื่อย่อนี้ไปแล้ว ({$a})';
$string['shortnameuser'] = 'ชื่อย่อของสมาชิก';
$string['shortsitename'] = 'ชื่อย่อของเว็บ';
$string['show'] = 'แสดง';
$string['showactions'] = 'แสดงสิ่งที่ทำ';
$string['showadvancededitor'] = 'ชั้นสูง';
$string['showadvancedsettings'] = 'แสดงการตั้งค่าชั้นสูง';
$string['showall'] = 'แสดง {$a} ทั้งหมด';
$string['showallcourses'] = 'แสดงทุกรายวิชา';
$string['showallusers'] = 'แสดงสมาชิกทั้งหมด';
$string['showblockcourse'] = 'แสดงรายการของรายวิชาที่ประกอบไปด้วยบล็อค';
$string['showcategory'] = 'แสดง {$a}';
$string['showcomments'] = 'แสดง/ซ่อน ความคิดเห็น';
$string['showcommentsnonjs'] = 'แสดงความคิดเห็น';
$string['showdescription'] = 'แสดงคำอธิบายในหน้ารายวิชา';
$string['showdescription_help'] = 'หากเปิดใช้งาน คำอธิบายข้างต้นจะแสดงในหน้ารายวิชาด้านล่างของลิงก์กิจกรรมหรือแหล่งข้อมูล';
$string['showgrades'] = 'แสดงสมุดคะแนนกับนักเรียน';
$string['showgrades_help'] = 'กิจกรรมในรายวิชาสามารถเลือกให้คะแนนด้วยหรือไม่ก็ได้ การตั้งค่านี้จะทำให้นักเรียนมองเห็นคะแนนของตัวเองผ่านลิงก์ในบล็อคการจัดการรายวิชาได้';
$string['showingacourses'] = 'แสดงทั้งหมด {$a} รายวิชา';
$string['showingxofycourses'] = 'แสดงรายวิชา{$a->start} ถึง {$a->end} จากทั้งหมด {$a->total} รายวิชา';
$string['showlistofcourses'] = 'แสดงรายวิชาทั้งหมด';
$string['showmodulecourse'] = 'แสดงรายการของรายวิชาที่ประกอบไปด้วยกิจกรรท';
$string['showonly'] = 'แสดงเฉพาะ';
$string['showperpage'] = 'แสดง {$a} ต่อหน้า';
$string['showrecent'] = 'แสดงกิจกรรมล่าสุด';
$string['showreports'] = 'แสดงรายงานกิจกรรม';
$string['showreports_help'] = 'รายงานกิจกรรม เป็นรายงานที่แสดงการมีส่วนร่วมของนักเรียนในรายวิชา รวมไปถึงการมีส่วนร่วมต่าง ๆ เช่น การโพสต์ในกระดานเสวนา การส่งการบ้าน รวมถึงประวัติการเข้าใช้งาน การตั้งค่านี้เลือกว่านักเรียนสามารถดูรายงานกิจกรรมของตนผ่านหน้าประวัติส่วนตัวได้หรือไม่';
$string['showsettings'] = 'แสดงการตั้งค่า';
$string['showtheselogs'] = 'แสดงบันทึกการใช้งานเว็บไซต์';
$string['showthishelpinlanguage'] = 'แสดงการช่วยเหลือในภาษา {$a}';
$string['since'] = 'ตั้งแต่';
$string['sincelast'] = 'ตั้งแต่เข้าสู่ระบบครั้งล่าสุด';
$string['site'] = 'เว็บไซต์';
$string['sitedefault'] = 'ค่ามาตรฐานของเว็บ';
$string['siteerrors'] = 'ไฟล์ของเว็บไซต์';
$string['sitefiles'] = 'ไฟล์ของเว็บไซต์';
$string['sitefilesused'] = 'ไฟล์ของเว็บไซต์ถูกใช้ในรายวิชานี้';
$string['sitehome'] = 'หน้าแรกของเว็บไซต์';
$string['sitelegacyfiles'] = 'ไฟล์ของเว็บไซต์';
$string['sitelogs'] = 'บันทึกการใช้งานเว็บไซต์';
$string['sitemessage'] = 'ส่งข้อความให้สมาชิก';
$string['sitenews'] = 'ข่าวและประกาศของเว็บ';
$string['sitepages'] = 'ข้อมูลเว็บไซต์';
$string['sitepartlist'] = 'ท่านไม่มีสิทธิ์ในการดูรายชื่อของนักเรียนและผู้สนใจ';
$string['sitepartlist0'] = 'ท่านต้องเป็นอาจารย์ประจำเว็บไซต์จึงจะเห็นรายชื่อของนักเรียน';
$string['sitepartlist1'] = 'คุณต้องเป็นอาจารย์ในรายวิชาใดวิชาหนึ่งจึงจะเห็นรายชื่อของสมาชิก';
$string['sites'] = 'เว็บไซต์';
$string['sitesection'] = 'แสดงส่วนแสดงหัวข้อ';
$string['sitesettings'] = 'การตั้งค่าของเว็บไซต์';
$string['siteteachers'] = 'อาจารย์ประจำเว็บไซต์';
$string['size'] = 'ขนาด';
$string['sizeb'] = 'ไบต์';
$string['sizegb'] = 'กิกะไบต์';
$string['sizekb'] = 'กิโลไบต์';
$string['sizemb'] = 'เมกะไบต์';
$string['skipped'] = 'ข้าม';
$string['skiptocategorylisting'] = 'ข้ามไปยังรายการรวมประเภท';
$string['skiptocoursedetails'] = 'ข้ามไปยังหน้ารายวิชาแบบละเอียด';
$string['skiptocourselisting'] = 'ข้ามไปยังหน้ารวมรายวิชา';
$string['skypeid'] = 'Skype ID';
$string['socialheadline'] = 'กระดานเสวนาหัวข้อล่าสุด';
$string['someallowguest'] = 'บุคคลทั่วไปสามารถเข้าชมได้เฉพาะรายวิชาที่มี สัญลักษณ์หน้าคนติดอยู่ นั่นคือ อนุญาตให้บุคคลทั่วไปเข้าศึกษาได้  นอกนั้น สำหรับท่านที่เป็นสมาชิกเท่านั้น';
$string['someerrorswerefound'] = 'ข้อมูลบางอย่างหายไป ดูรายละเอียดข้างล่าง';
$string['sort'] = 'เรียงลำดับ';
$string['sortby'] = 'เรียงลำดับโดย';
$string['sortbyx'] = 'เรียงลำดับโดย  {$a} จากน้อยไปมาก';
$string['sortbyxreverse'] = 'เรียงลำดับโดย  {$a} จากมากไปน้อย';
$string['sorting'] = 'การเรียงลำดับ';
$string['sourcerole'] = 'บทบาท';
$string['specifyname'] = 'ต้องระบุชื่อ';
$string['standard'] = 'มาตรฐาน';
$string['starpending'] = '([*] = รายวิชาที่รอการอนุมัติ)';
$string['startdate'] = 'วันเริ่มต้นรายวิชา';
$string['startdate_help'] = 'สำหรับระบุเวลาที่ท่านต้องการเริ่มต้นบทเรียนนี้ใน รายวิชาที่ใช้รูปแบบรายสัปดาห์ และระบบจะเลือกวันที่เร็วที่สุดที่จะเก็บบันทึกกิจกรรมเอาไว้ ถ้าหากมีการรีเซ็ทรายวิชาและเปลี่ยนวันเริ่มต้น วันที่ทั้งหมดที่อยู่ในรายวิชาจะเลื่อนไปพร้อมกันด้วย';
$string['startingfrom'] = 'เริ่มจาก';
$string['startsignup'] = 'สมัครเป็นสมาชิก';
$string['state'] = 'จังหวัด';
$string['statistics'] = 'สถิติ';
$string['statisticsgraph'] = 'กราฟสถิติ';
$string['stats'] = 'สถิติ';
$string['statslogins'] = 'เข้าสู่ระบบ';
$string['statsmodedetailed'] = 'มุมมอง (ผู้ใช้) โดยละเอียด';
$string['statsmodegeneral'] = 'มุมมองทั่วไป';
$string['statsnodata'] = 'ไม่มีข้อมูลสำหรับรายวิชาและช่วงเวลาดังกล่าว';
$string['statsnodatauser'] = 'ไม่มีข้อมูลสำหรับรายวิชา,สมาชิก และช่วงเวลาดังกล่าว';
$string['statsoff'] = 'ยังไม่เปิดการใช้งานสถิติ';
$string['statsreads'] = 'ครั้ง';
$string['statsreport1'] = 'เข้าสู่ระบบ';
$string['statsreport10'] = 'กิจกรรมของสมาชิก';
$string['statsreport11'] = 'รายวิชาที่มีความเคลื่อนไหวมากที่สุด';
$string['statsreport12'] = 'รายวิชาที่มีความเคลื่อนไหวมาที่สุด (น้ำหนัก)';
$string['statsreport13'] = 'รายวิชาที่มีการมีส่วนร่วมมากที่สุด (จำนวนนักเรียน)';
$string['statsreport14'] = 'รายวิชาที่มีการมีส่วนร่วมมากที่สุด (การเข้าชม/โพสต์)';
$string['statsreport2'] = 'ดู (ทุกกลุ่ม)';
$string['statsreport3'] = 'โพส (ทุกกลุ่ม)';
$string['statsreport4'] = 'กิจกรรมทั้งหมด (ผู้สอนและนักเรียน)';
$string['statsreport5'] = 'กิจกรรมทั้งหมด (ดูและโพสต์)';
$string['statsreport7'] = 'กิจกรรมของสมาชิก (อ่านมและโพสต์)';
$string['statsreport8'] = 'กิจกรรมของสมาชิกทั้งหมด';
$string['statsreport9'] = 'การเข้าสู่ระบบ (หน้าหลักของเว็บ)';
$string['statsreportactivity'] = 'กิจกรรมทั้งหมด (ผู้สอนและนักเรียน)';
$string['statsreportactivitybyrole'] = 'กิจกรรมทั้งหมด( อ่านและโพสต์)';
$string['statsreportforuser'] = 'สำหรับ';
$string['statsreportlogins'] = 'การเข้าสู่ระบบ';
$string['statsreportreads'] = 'การเข้าอ่าน (ผู้สอนและนักเรียน)';
$string['statsreporttype'] = 'ประเภทของรายงาน';
$string['statsreportwrites'] = 'โพสต์ (ทุกกลุ่ม)';
$string['statsstudentactivity'] = 'กิจกรรมของนักเรียน';
$string['statsstudentreads'] = 'จำนวนเข้าอ่านของนักเรียน';
$string['statsstudentwrites'] = 'จำนวนโพสต์ของนักเรียน';
$string['statsteacheractivity'] = 'กิจกรรมของผู้สอน';
$string['statsteacherreads'] = 'จำนวนเข้าอ่านของผู้สอน';
$string['statsteacherwrites'] = 'จำนวนโพสต์ของผู้สอน';
$string['statstimeperiod'] = 'ช่วงเวลา - ครั้งสุดท้าย';
$string['statsuniquelogins'] = 'การเข้าสู่ระบบแบบเฉพาะ';
$string['statsuseractivity'] = 'กิจกรรมทั้งหมด';
$string['statsuserlogins'] = 'เข้าสู่ระบบ';
$string['statsuserreads'] = 'อ่าน';
$string['statsuserwrites'] = 'โพสต์';
$string['statswrites'] = 'โพสต์';
$string['status'] = 'สถานะ';
$string['stringsnotset'] = 'strings ต่อไปนี้ยังไม่มีการตั้งค่า ใน  {$a}';
$string['studentnotallowed'] = 'ขออภัย ท่านไม่สามารถเข้าไปยังรายวิชานี้ได้ในฐานะ \'{$a}\'';
$string['students'] = 'นักเรียน';
$string['studentsandteachers'] = 'นักเรียนและอาจารย์';
$string['subcategories'] = 'ประเภทย่อย';
$string['subcategory'] = 'ประเภทย่อย';
$string['subcategoryof'] = 'ประเภทย่อยของ {$a}';
$string['submit'] = 'ส่ง';
$string['success'] = 'สำเร็จ';
$string['successduration'] = 'สำเร็จ ({$a} วินาที)';
$string['summary'] = 'บทคัดย่อ';
$string['summary_help'] = '<p align=center><b>ย่อหัวข้อหรือย่อเนื้อหาของแต่ละสัปดาห์</b></p>

<p>หนือ้หาย่อของหัวข้อ คือ เนื้อหาโดยย่อที่เตรียมให้นักเรียนได้เรียนในหัวข้อนั้น หรือ สัปดาห์นั้น </p>
<p>เนื้อหาย่อแต่ละอันควรจะเป็นเนื้อหาที่สั้นจริง ๆ ไม่เช่นนั้นหน้าของรายวิชาจะยาวเกินไป </p>
<p>ถ้าหากเนื้อหาย่อนี้ยาวมาก ๆ  ให้เพิ่มเป็นแหล่งข้อมูลแทนที่จะนำมาใส่ไว้ตรงเนื้อหาย่อ</p>';
$string['summaryof'] = 'บทคัดย่อของ {$a}';
$string['supplyinfo'] = 'รายละเอียดเพิ่มเติม';
$string['suspended'] = 'ระงับไปแล้ว';
$string['suspendedusers'] = 'สมาชิกที่ถูกระงับ';
$string['switchdevicedefault'] = 'เปลี่ยนเป็นรูปแบบมาตรฐาน';
$string['switchdevicerecommended'] = 'เปลี่ยนเป็นรูปแบบแนะนำสำหรับเครื่องมือของท่าน';
$string['switchrolereturn'] = 'รายละเอียดเพิ่มเติม';
$string['switchroleto'] = 'เปลี่ยนบทบาทเป็น';
$string['tag'] = 'แท็ก';
$string['tagalready'] = 'มีแท็กนี้อยู่แล้ว';
$string['tagmanagement'] = 'เพิ่ม/ลบแท็ก';
$string['tags'] = 'สำหรับ {$a} เท่านั้น';
$string['targetrole'] = 'บทบาทเป้าหมาย';
$string['teacheronly'] = 'สำหรับ {$a} เท่านั้น';
$string['teacherroles'] = '{$a}  หน้าที่';
$string['teachers'] = 'ครู อาจารย์';
$string['textediting'] = 'กล่องข้อความ';
$string['textediting_help'] = 'ในการใช้ HTML editor  เช่น Atto หรือ TinyMCE  พื้นที่ในการเขียนจะมีปุ่มเครื่องมือเพิ่มเข้ามาเพื่อทำให้ง่ายต่อการเพิ่มเนื้อหา

ถ้าหากเลือก "ตัวหนังสือธรรมดา" รูปแบบสำหรับตัวหนังสือสามารถเขียนโดยใช้ โค้ด HTML  หรือ Markdown

Text editor ที่เปิดให้ใช้งานขึ้นอยู่กับการตั้งค่าของผู้ดูแลระบบ';
$string['texteditor'] = 'ใช้  standard web forms';
$string['textformat'] = 'รูปแบบ plain text';
$string['thanks'] = 'ขอบคุณค่ะ';
$string['theme'] = 'รูปแบบเว็บ';
$string['themes'] = 'รูปแบบเว็บ';
$string['themesaved'] = 'บันทึกรูปแบบเว็บแล้ว';
$string['therearecourses'] = 'มีจำนวน {$a}  รายวิชา';
$string['thereareno'] = 'ไม่มี {$a} ในรายวิชานี้';
$string['thiscategory'] = 'ประเภทนี้';
$string['thiscategorycontains'] = 'ประเภทนี้ประกอบไปด้วย';
$string['time'] = 'เวลา';
$string['timecreatedcourse'] = 'สร้างรายวิชาเมื่อ';
$string['timesplitting:nosplitting'] = 'ไม่จำกัดเวลา';
$string['timesplitting:singlerange'] = 'จากเริ่มจนจบ';
$string['timesplitting:upcoming3days'] = 'ภายใน 3 วันนี้';
$string['timesplitting:upcomingweek'] = 'อาทิตย์ที่จะถึงนี้';
$string['timezone'] = 'โซนเวลา';
$string['to'] = 'ถึง';
$string['tocontent'] = 'ถึง ส่วนที่ "{$a}"';
$string['tocreatenewaccount'] = 'ข้ามไปสร้างบัญชีผู้ใหญ่ใหม่';
$string['today'] = 'วันนี้';
$string['todaylogs'] = 'บันทึกการใช้งานเว็บไซต์ของวันนี้';
$string['toeveryone'] = 'ส่งถึงทุกคน';
$string['toomanybounces'] = 'ที่อยู่อีเมลดังกล่าวย้อนกลับมาหลายครั้งท่านต้องเปลี่ยนอีเมลก่อนที่จะดำเนินการต่อได้';
$string['toomanytoshow'] = 'มีจำนวนสมาชิกมากเกินกว่าที่จะแสดงทั้งหมด';
$string['toomanyusersmatchsearch'] = 'มีสมาชิกมากเกินไป ({$a->count}) ตรงกับ \'{$a->search}\'';
$string['toomanyuserstoshow'] = 'มีสมาชิกมากเกินไป ({$a}) ที่จะแสดง';
$string['toonly'] = 'ถึง {$a} เท่านั้น';
$string['top'] = 'บนสุด';
$string['topic'] = 'หัวข้อ';
$string['topichide'] = 'ซ่อนหัวข้อนี้จาก {$a}';
$string['topicoutline'] = 'โครงสร้างหัวข้อ';
$string['topicshow'] = 'แสดงหัวข้อนี้แก่ {$a}';
$string['toplevelcategory'] = 'ประเภทสูงสุด';
$string['total'] = 'รวม';
$string['totopofsection'] = 'ไปยังหัวข้อสูงสุด"{$a}"';
$string['trackforums'] = 'การติดตามการอ่านกระดานเสวนา';
$string['trackforumsno'] = 'ไม่ : ไม่ต้องบันทึกว่าอ่านกระทู้ใดไปบ้าง';
$string['trackforumsyes'] = 'ใช่ : แสดงกระทู้ที่ยังไม่ได้อ่าน';
$string['trysearching'] = 'พยายามค้นหาคำอื่นแทน';
$string['turneditingoff'] = 'ปิดการแก้ไขในหน้านี้';
$string['turneditingon'] = 'เริ่มการแก้ไขในหน้านี้';
$string['unauthorisedlogin'] = 'สมาชิก "{$a}" ไม่มีอยู่ในเว็บไซต์นี้';
$string['undecided'] = 'ยังไม่ตัดสินใจ';
$string['unfinished'] = 'ไม่เสร็จ';
$string['unknowncategory'] = 'ไม่สามารถระบุประเภท';
$string['unknownerror'] = 'ไม่สามารถระบุข้อผิดพลาด';
$string['unknownuser'] = 'ไม่สามารถระบุ สมาชิก';
$string['unlimited'] = 'ไม่จำกัด';
$string['unpacking'] = 'unpack {$a}';
$string['unsafepassword'] = 'รหัสนี้ไม่ปลอดภัย หาอันใหม่ดีกว่า';
$string['untilcomplete'] = 'จนกว่าเสร็จสิ้น';
$string['unusedaccounts'] = 'บัญชีผู้ใช้ที่ไม่ได้ใช้เกิน {$a} วันถือว่าหมดสิ้นสมาชิกภาพ';
$string['unzip'] = 'Unzip';
$string['unzippingbackup'] = 'Unzip ข้อมูลสำรอง';
$string['up'] = 'ขึ้นข้างบน';
$string['update'] = 'อัพเดท';
$string['updated'] = 'ปรับปรุง {$a} แล้ว';
$string['updatemymoodleoff'] = 'หยุดการแก้ไขหน้านี้';
$string['updatemymoodleon'] = 'แก้ไขหน้านี้';
$string['updatemyprofile'] = 'อัพเดทประวัติส่วนตัว';
$string['updatesevery'] = 'อัพเดททุกๆ {$a}  วินาที';
$string['updatethis'] = 'แก้ไข {$a}';
$string['updatethiscourse'] = 'แก้ไขรายวิชานี้';
$string['updatinga'] = 'กำลังแก้ไข {$a}';
$string['updatingain'] = 'กำลังแก้ไข {$a->what} ใน {$a->in}';
$string['upload'] = 'อัพโหลด';
$string['uploadafile'] = 'อัพโหลดไฟล์';
$string['uploadcantwrite'] = 'ไม่สามารถเขียนไฟล์ลงดิสก์ได้';
$string['uploadedfile'] = 'ไฟล์อัพโหลดเรียบร้อยแล้ว';
$string['uploadedfileto'] = 'อัพโหลดไฟล์ {$a->file} เข้าแฟ้ม {$a->directory}  แล้ว';
$string['uploadedfiletoobig'] = 'ขออภัย ไฟล์มีขนาดใหญ่เกินไป (จำกัดไว้ที่ {$a} ไบต์)';
$string['uploadextension'] = 'ไฟล์อัพโหลดหยุด เพราะ PHP extension';
$string['uploadfailednotrecovering'] = 'อัพโหลดไฟล์ผิดพลาดเพราะมีปัญหาจากไฟล์ต่อไปนี้  {$a->name}.<br/> บันทึกปัญหาที่เกิดขึ้นคือ:<br />{$a->problem}<br />ไม่สามารถกู้คืนได้';
$string['uploadfilelog'] = 'อัพโหลดบันทึกการใช้งานสำหรับไฟล์ {$a}';
$string['uploadformlimit'] = 'ไฟล์ที่อัพโหลด {$a} มีขนาดเกินกว่าขนาดสูงสุดที่ฟอร์มนี้กำหนดไว้';
$string['uploadlabel'] = 'หัวข้อ :';
$string['uploadlimitwithsize'] = '$a->contextname} จำกัดการอัพโหลดที่  ({$a->displaysize})';
$string['uploadnewfile'] = 'อัพโหลดไฟล์ใหม่';
$string['uploadnofilefound'] = 'ไม่พบไฟล์ดังกล่าว แน่ใจนะว่าเลือกไฟล์ที่จะอัพโหลดเรียบร้อยแล้ว';
$string['uploadnotallowed'] = 'ไม่อนุญาตให้อัพโหลดไฟล์';
$string['uploadnotempdir'] = 'แฟ้มชั่วคราวหายไป';
$string['uploadoldfilesdeleted'] = 'ไฟล์เก่าในพื้นที่อัพโหลดถูกลบไปเรียบร้อยแล้ว';
$string['uploadpartialfile'] = 'อัพโหลดไฟล์เพียงส่วนเดียว';
$string['uploadproblem'] = 'มีปัญหาที่ไม่ทราบสาเหตุระหว่างการอัพโหลด \'{$a}\'  (ไฟล์อาจจะใหญ่เกินไป ลองตรวจสอบดู)';
$string['uploadrenamedchars'] = 'มีการเปลี่ยนชื่อไฟล์จาก {$a->oldname} เป็น {$a->newname} เพราะมีตัวอักษรที่ไม่ถูกต้อง';
$string['uploadrenamedcollision'] = 'มีการเปลี่ยนชื่อไฟล์จาก {$a->oldname} เป็น {$a->newname} เพราะชื่อไฟล์ซ้ำกัน';
$string['uploadserverlimit'] = 'ไฟล์ที่อัพโหลดมีขนาดเกินกว่าขนาดสูงสุดที่เซิร์ฟเวอร์กำหนดไว้';
$string['uploadthisfile'] = 'อัพโหลดไฟล์นี้';
$string['url'] = 'URL';
$string['used'] = 'ใช้ไปแล้ว';
$string['usedinnplaces'] = 'ใช้อยู่ {$a}  แห่ง';
$string['user'] = 'สมาชิก';
$string['useraccount'] = 'บัญชีสมาชิก';
$string['useractivity'] = 'กิจกรรม';
$string['userconfirmed'] = 'ยืนยัน  {$a}';
$string['userdata'] = 'ข้อมูลสมาชิก';
$string['userdeleted'] = 'บัญชีผู้ใช้ถูกลบไปแล้ว';
$string['userdescription'] = 'รายละเอียด';
$string['userdescription_help'] = 'ท่านสามารถเพิ่มข้อมูลเกี่ยวกับตนเองซึ่งจะแสดงในหน้าประวัติส่วนตัว';
$string['userdetails'] = 'รายละเอียดสมาชิก';
$string['userfiles'] = 'ไฟล์ของสมาชิก';
$string['userfilterplaceholder'] = 'ค้นหาคำค้นหรือเลือกตัวกรอง';
$string['userlist'] = 'รายชื่อสมาชิก';
$string['usermenu'] = 'เมนูผู้ใช้';
$string['username'] = 'ชื่อผู้ใช้';
$string['usernameemail'] = 'ชื่อผู้ใช้/อีเมล';
$string['usernameemailmatch'] = 'ชื่อผู้ใช้และอีเมล ไม่ใช่ของบุคคลเดียวกัน';
$string['usernameexists'] = 'ชื่อนี้มีผู้ใช้แล้วค่ะกรุณาเลือกชื่อผู้ใช้ใหม่';
$string['usernamelowercase'] = 'ใช้ได้เฉพาะตัวพิมพ์เล็กเท่านั้น';
$string['usernamenotfound'] = 'ไม่พบชื่อผู้ใช้ในฐานข้อมูล';
$string['usernameoremail'] = 'กรุณาใส่ชื่อผู้ใช้หรืออีเมลของท่าน';
$string['usernotconfirmed'] = 'ไม่สามารถยืนยัน  {$a}';
$string['userpic'] = 'ภาพประจำตัว';
$string['userpreferences'] = 'ตั้งค่าของผู้ใช้';
$string['users'] = 'สมาชิก';
$string['userselectorautoselectunique'] = 'ถ้ามีสมาชิกตรงกับที่คนหาหนึ่งคนให้เลือกโดยอัตโนมัติ';
$string['userselectorpreserveselected'] = 'เลือกสมาชิกที่ใช้ถึงแม้ว่าจะไม่ตรงกับที่ค้นหา';
$string['userselectorsearchanywhere'] = 'จับคู่ข้อความที่ค้นหาในทุกฟิลด์ที่แสดง';
$string['usersnew'] = 'สมาชิกใหม่';
$string['usersnoaccesssince'] = 'แสดงสมาชิกที่ไม่ได้เข้าสู่ระบบเป็นเวลามากกว่า';
$string['userswithfiles'] = 'สมาชิกและไฟล์';
$string['useruploadtype'] = 'ประเภทการอัพโหลดสมาชิก {$a}';
$string['userzones'] = 'มุมสมาชิก';
$string['usetheme'] = 'ใช้รูปแบบ';
$string['usingexistingcourse'] = 'ใช้รายวิชาที่มีอยู่';
$string['valuealreadyused'] = 'ใช้ค่านี้ไปแล้ว';
$string['version'] = 'เวอร์ชัน';
$string['view'] = 'ครั้ง';
$string['viewallcourses'] = 'ดูรายวิชาทั้งหมด';
$string['viewallcoursescategories'] = 'ดูรายวิชาและประเภททั้งหมด';
$string['viewallsubcategories'] = 'ดูประเภทย่อยทั้งหมด';
$string['viewfileinpopup'] = 'ดูไฟล์ในหน้าต่างป้อบอัพ';
$string['viewing'] = 'ดู';
$string['viewmore'] = 'ดูเพิ่มเติม';
$string['viewprofile'] = 'ดูประวัติ';
$string['views'] = 'ครั้ง';
$string['viewsolution'] = 'แสดงวิธีการแก้ปัญหา';
$string['visible'] = 'มองเห็นได้';
$string['visible_help'] = '<p> ท่านสามารถใช้ตัวเลือกนี้ซ่อนรายวิชาของท่าน</p>

<p> ผู้สอนรายวิชานั้นและผู้ดูแลเว็บเท่านั้นที่เห็นหลักสูตรบนหน้ารายชื่อของรายวิชาที่ซ่อนนี้</p>

<p>แม้ว่านักเรียนพยายามเข้ามาดูรายวิชาที่ซ่อนไว้โดยใส่ชื่อลิงก์ (URL) ระบบก็ไม่อนุญาตให้นักเรียนเข้ามาเห็นรายวิชานี้ </p>';
$string['visibletostudents'] = 'แสดงให้กับ {$a}';
$string['warning'] = 'คำเตือน';
$string['warningdeleteresource'] = 'คำเตือน : มีการใช้งาน {$a} ในแหล่งข้อมูล ท่านต้องการอัพเดทแหล่งข้อมูลนี้หรือไม่';
$string['webpage'] = 'เว็บเพจ';
$string['week'] = 'สัปดาห์';
$string['weekhide'] = 'ซ่อนสัปดาห์นี้จาก {$a}';
$string['weeklyoutline'] = 'โครงสร้างรายสัปดาห์';
$string['weeks'] = 'สัปดาห์';
$string['weekshow'] = 'แสดงสัปดาห์นี้แก่ {$a}';
$string['welcometocourse'] = 'ยินดีต้อนรับสู่ {$a}';
$string['welcometocoursetext'] = 'ท่านได้สมัครเป็นนักเรียนวิชา {$a->coursename}!

ข้อแนะนำสำหรับท่านที่ยังไม่ได้ทำการแก้ไขประวัติส่วนตัว ท่านควรแก้ไขประวัติให้ถูกต้อง โดยการเข้าไปแก้ไขในหน้าลิงก์ต่อไปนี้     {$a->profileurl}';
$string['whatforlink'] = 'ท่านต้องการทำอะไรกับลิงก์';
$string['whatforpage'] = 'ท่านต้องการทำอะไรกับข้อความ';
$string['whatisyourage'] = 'อายุของคุณเท่าไร?';
$string['whattocallzip'] = 'ตั้งชื่อ zip ไฟล์ว่า';
$string['whattodo'] = 'ต้องการทำ';
$string['wheredoyoulive'] = 'อาศัยอยู่ที่ประเทศไหนฦ';
$string['whyisthisrequired'] = 'ทำไมถึงต้องการข้อมูลนี้?';
$string['windowclosing'] = 'หน้าต่างนี้ควรปิดอัตโนมัติ หากไม่กรุณาปิดหน้าต่าง';
$string['withchosenfiles'] = 'โดยไฟล์ที่เลือก';
$string['withdisablednote'] = '{$a} (ปิดการใช้งาน)';
$string['withoutuserdata'] = 'ไม่รวมข้อมูลสมาชิก';
$string['withselectedusers'] = 'รวมสมาชิกที่เลือก';
$string['withuserdata'] = 'รวมข้อมูลสมาชิก';
$string['wordforstudent'] = 'คำสำหรับเรียกนักเรียน';
$string['wordforstudenteg'] = 'เช่น นักเรียน นักศึกษา ผู้ร่วมทำกิจกรรม';
$string['wordforstudents'] = 'คำสำหรับเรียกนักเรียน';
$string['wordforstudentseg'] = 'เช่น นักเรียน นักศึกษา ผู้ร่วมทำกิจกรรม';
$string['wordforteacher'] = 'คำสำหรับเรียกผู้สอน';
$string['wordforteachereg'] = 'เช่น  ครู อาจารย์ ผู้สอน';
$string['wordforteachers'] = 'คำสำหรับเรียกผู้สอน';
$string['wordforteacherseg'] = 'เช่น  ครู อาจารย์ ผู้สอน';
$string['writingblogsinfo'] = 'กำลังเขียนข้อมูลบล็อก';
$string['writingcategoriesandquestions'] = 'กำลังเขียนประเภทและคำถาม';
$string['writingcoursedata'] = 'กำลังเขียนข้อมูลรายวิชา';
$string['writingeventsinfo'] = 'กำลังเขียนข้อมูลของกิจกรรม';
$string['writinggeneralinfo'] = 'กำลังเขียนข้อมูลทั่วไป';
$string['writinggradebookinfo'] = 'กำลังเขียนข้อมูลของกิจกรรม';
$string['writinggroupingsgroupsinfo'] = 'กำลังเขียนข้อมูลการจัดกลุ่มของกลุ่ม';
$string['writinggroupingsinfo'] = 'กำลังเขียนข้อมูลการจัดกลุ่ม';
$string['writinggroupsinfo'] = 'กำลังเขียนข้อมูลของกลุ่ม';
$string['writingheader'] = 'กำลังเขียนหัวข้อ';
$string['writingloginfo'] = 'กำลังเขียนบันทึกการดูข้อมูล';
$string['writingmessagesinfo'] = 'กำลังเขียข้อมูลบันทึกการใช้งาน';
$string['writingmoduleinfo'] = 'กำลังเขียนข้อมูลโมดูล';
$string['writingscalesinfo'] = 'กำลังเขียนข้อมูลวิธีการวัด';
$string['writinguserinfo'] = 'กำลังเขียนข้อมูลสมาชิก';
$string['wrongpassword'] = 'รหัสผ่านไม่ถูกต้อง';
$string['yahooid'] = 'Yahoo ID';
$string['year'] = 'ปี';
$string['years'] = 'ปี';
$string['yes'] = 'ใช่';
$string['youareabouttocreatezip'] = 'กำลังสร้าง zip ไฟล์ ประกอบด้วย';
$string['youaregoingtorestorefrom'] = 'กำลังจะเริ่มกระบวนการกู้คืนสำหรับ';
$string['youhaveupcomingactivitiesdue'] = 'คุณมีกิจกรรมที่ใกล้ถึงกำหนด';
$string['youneedtoenrol'] = 'ต้องเป็นนักเรียนในรายวิชานี้ก่อนจึงจะทำได้';
$string['yourlastlogin'] = 'เข้าสู่ระบบครั้งสุดท้ายเมื่อ';
$string['yourself'] = 'ตัวท่านเอง';
$string['yourteacher'] = '{$a} ของท่าน';
$string['yourwordforx'] = 'คำที่ใช้เรียก  \'{$a}\'';
$string['zippingbackup'] = 'zip ข้อมูลสำรอง';
