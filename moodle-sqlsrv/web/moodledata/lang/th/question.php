<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'question', language 'th', version '3.9'.
 *
 * @package     question
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addmorechoiceblanks'] = 'ช่องว่างสำหรับตัวเลือกเพิ่มเติม {no}';

$string['adminreport'] = 'รายงานปัญหาที่อาจเกิดขึ้นในฐานข้อมูลคำถามของคุณ';

$string['alltries'] = 'พยายามทั้งหมด';

$string['broken'] = 'นี่คือ \'ลิงก์เสีย\' ซึ่งชี้ไปที่ไฟล์ที่ไม่มีอยู่จริง';

$string['cannotcreate'] = 'ไม่สามารถสร้างรายการใหม่ในตาราง question_attempts';

$string['cannotdeletebehaviourinuse'] = 'คุณไม่สามารถลบพฤติกรรม \'{$a}\' ได้ ใช้โดยการถามคำถาม';

$string['cannotdeletecate'] = 'คุณไม่สามารถลบหมวดหมู่นั้นซึ่งเป็นหมวดหมู่เริ่มต้นสำหรับบริบทนี้';

$string['cannotdeleteneededbehaviour'] = 'ไม่สามารถลบพฤติกรรมของคำถาม \'{$a}\' มีพฤติกรรมอื่น ๆ ที่ติดตั้งที่ต้องพึ่งพา';

$string['cannotdeletetopcat'] = 'ไม่สามารถลบหมวดหมู่ยอดนิยมได้';

$string['cannotedittopcat'] = 'ไม่สามารถแก้ไขหมวดหมู่ยอดนิยมได้';

$string['cannotenable'] = 'ไม่สามารถสร้างคำถามประเภท {$a} ได้โดยตรง';

$string['cannotenablebehaviour'] = 'ไม่สามารถใช้พฤติกรรมของคำถาม {$a} ได้โดยตรง มีไว้สำหรับใช้ภายในเท่านั้น';

$string['cannotfindcate'] = 'ไม่พบบันทึกหมวดหมู่';

$string['cannotfindquestionfile'] = 'ไม่พบไฟล์ข้อมูลคำถามใน zip';

$string['cannotgetdsfordependent'] = 'ไม่สามารถรับชุดข้อมูลที่ระบุสำหรับคำถามที่ขึ้นอยู่กับชุดข้อมูล! (คำถาม: {$a->id}, datasetitem: {$a->item})';

$string['cannotgetdsforquestion'] = 'ไม่สามารถรับชุดข้อมูลที่ระบุสำหรับคำถามที่คำนวณได้! (คำถาม: {$a})';

$string['cannothidequestion'] = 'ไม่สามารถซ่อนคำถามได้';

$string['cannotimportformat'] = 'ขออภัยยังไม่มีการนำเข้ารูปแบบนี้!';

$string['cannotinsertquestion'] = 'ไม่สามารถแทรกคำถามใหม่ได้!';

$string['cannotinsertquestioncatecontext'] = 'ไม่สามารถแทรกหมวดหมู่คำถามใหม่ {$a->cat} contextid ที่ไม่ถูกต้อง {$a->ctx}';

$string['cannotloadquestion'] = 'ไม่สามารถโหลดคำถาม';

$string['cannotmovequestion'] = 'คุณไม่สามารถใช้สคริปต์นี้เพื่อย้ายคำถามที่มีไฟล์เชื่อมโยงกับคำถามเหล่านี้จากพื้นที่ต่างๆ';

$string['cannotopenforwriting'] = 'ไม่สามารถเปิดให้เขียนได้: {$a}';

$string['cannotpreview'] = 'คุณไม่สามารถดูคำถามเหล่านี้ได้!';

$string['cannotread'] = 'ไม่สามารถอ่านไฟล์นำเข้า (หรือไฟล์ว่างเปล่า)';

$string['cannotretrieveqcat'] = 'ไม่สามารถเรียกข้อมูลหมวดหมู่คำถาม';

$string['cannotunhidequestion'] = 'ยกเลิกการซ่อนคำถามไม่สำเร็จ';

$string['cannotunzip'] = 'ไม่สามารถแตกไฟล์ได้';

$string['cannotwriteto'] = 'ไม่สามารถเขียนคำถามที่ส่งออกไปยัง {$a}';

$string['categories'] = 'หมวดหมู่';

$string['categorycurrent'] = 'หมวดหมู่ปัจจุบัน';

$string['categorycurrentuse'] = 'ใช้หมวดหมู่นี้';

$string['categorydoesnotexist'] = 'ไม่มีหมวดหมู่นี้';

$string['categoryinfo'] = 'ข้อมูลหมวดหมู่';

$string['categorymove'] = 'หมวดหมู่ \'{$a->name}\' มี {$a->count} คำถาม (ซึ่งบางคำถามอาจเป็นคำถามที่ซ่อนอยู่หรือคำถามสุ่มที่ยังคงใช้อยู่ในแบบทดสอบ) โปรดเลือกหมวดหมู่อื่นเพื่อย้ายไป';

$string['categorymoveto'] = 'บันทึกในหมวดหมู่';

$string['categorynamecantbeblank'] = 'ชื่อหมวดหมู่ต้องไม่เว้นว่าง';

$string['categorynamewithcount'] = '{$a->name} ({$a->questioncount})';

$string['categorynamewithidnumber'] = '{$a->name} [{$a->idnumber}]';

$string['categorynamewithidnumberandcount'] = '{$a->name} [{$a->idnumber}] ({$a->questioncount})';

$string['clickflag'] = 'ตั้งค่าสถานะคำถาม';

$string['clicktoflag'] = 'ตั้งค่าสถานะคำถามนี้เพื่อใช้อ้างอิงในอนาคต';

$string['clicktounflag'] = 'ลบแฟล็ก';

$string['clickunflag'] = 'ลบแฟล็ก';

$string['contexterror'] = 'คุณไม่ควรมาที่นี่หากคุณไม่ได้ย้ายหมวดหมู่ไปยังบริบทอื่น';

$string['copy'] = 'คัดลอกจาก {$a} และเปลี่ยนลิงก์';

$string['created'] = 'สร้าง';

$string['createdby'] = 'สร้างโดย';

$string['createdmodifiedheader'] = 'สร้าง / บันทึกล่าสุด';

$string['createnewquestion'] = 'สร้างคำถามใหม่ ...';

$string['cwrqpfs'] = 'คำถามสุ่มเลือกคำถามจากหมวดย่อย';

$string['cwrqpfsinfo'] = 'ในระหว่างการอัปเกรดเป็น Moodle 1.9 เราจะแยกหมวดหมู่คำถามออกเป็นบริบทต่างๆ หมวดหมู่คำถามและคำถามบางข้อในไซต์ของคุณจะต้องมีการเปลี่ยนแปลงสถานะการแบ่งปัน นี่เป็นสิ่งจำเป็นในกรณีที่ไม่ค่อยเกิดขึ้นบ่อยนักที่จะตั้งคำถามแบบ \'สุ่ม\' อย่างน้อยหนึ่งข้อในแบบทดสอบเพื่อเลือกจากการผสมระหว่างหมวดหมู่ที่ใช้ร่วมกันและไม่ได้แชร์ (เช่นเดียวกับในไซต์นี้) สิ่งนี้เกิดขึ้นเมื่อคำถาม \'สุ่ม\' ถูกตั้งค่าให้เลือกจากหมวดหมู่ย่อยและหมวดหมู่ย่อยหนึ่งหมวดหมู่ขึ้นไปมีสถานะการแบ่งปันที่แตกต่างกันไปยังหมวดหมู่หลักที่คำถามสุ่มถูกสร้างขึ้น หมวดหมู่คำถามต่อไปนี้ซึ่งคำถาม \'สุ่ม\' ในหมวดหมู่ผู้ปกครองเลือกคำถามจะมีการเปลี่ยนสถานะการแบ่งปันเป็นสถานะการแบ่งปันเช่นเดียวกับหมวดหมู่ที่มีคำถาม \'สุ่ม\' ในการอัปเกรดเป็น Moodle 1.9 หมวดหมู่ต่อไปนี้จะมีการเปลี่ยนแปลงสถานะการแบ่งปัน คำถามที่ได้รับผลกระทบจะยังคงใช้งานได้ในแบบทดสอบที่มีอยู่ทั้งหมดจนกว่าคุณจะนำออกจากแบบทดสอบเหล่านี้';

$string['cwrqpfsnoprob'] = 'ไม่มีหมวดหมู่คำถามในไซต์ของคุณได้รับผลกระทบจากปัญหา \'คำถามสุ่มเลือกคำถามจากหมวดหมู่ย่อย\'';

$string['defaultfor'] = 'ค่าเริ่มต้นสำหรับ {$a}';

$string['defaultmarkmustbepositive'] = 'เครื่องหมายเริ่มต้นต้องเป็นค่าบวก';

$string['deletecoursecategorywithquestions'] = 'มีคำถามในคลังคำถามที่เกี่ยวข้องกับหมวดหมู่หลักสูตรนี้ หากคุณดำเนินการต่อพวกเขาจะถูกลบ คุณอาจต้องการย้ายก่อนโดยใช้อินเทอร์เฟซคลังคำถาม';

$string['deletequestioncheck'] = 'คุณแน่ใจหรือไม่ว่าต้องการลบ \'{$a}\'';

$string['deletequestionscheck'] = 'คุณแน่ใจหรือไม่ว่าต้องการลบคำถามต่อไปนี้ {$a}';

$string['deletingbehaviour'] = 'การลบพฤติกรรมของคำถาม \'{$a}\'';

$string['deletingqtype'] = 'การลบคำถามประเภท \'{$a}\'';

$string['didnotmatchanyanswer'] = '[ไม่ตรงกับคำตอบใด ๆ ]';

$string['disabled'] = 'ปิดการใช้งาน';

$string['disterror'] = 'การแจกจ่าย {$a} ทำให้เกิดปัญหา';

$string['donothing'] = 'อย่าคัดลอกหรือย้ายไฟล์หรือเปลี่ยนลิงก์';

$string['editcategories'] = 'แก้ไขหมวดหมู่';

$string['editcategories_help'] = 'แทนที่จะเก็บทุกอย่างไว้ในรายการใหญ่รายการเดียวอาจมีการจัดเรียงคำถามเป็นหมวดหมู่และหมวดหมู่ย่อย แต่ละหมวดหมู่มีบริบทที่กำหนดว่าจะใช้คำถามในหมวดหมู่ใดได้บ้าง: * บริบทกิจกรรม - คำถามที่มีเฉพาะในโมดูลกิจกรรมเท่านั้น * บริบทของหลักสูตร - คำถามที่มีอยู่ในโมดูลกิจกรรมทั้งหมดในหลักสูตร * บริบทหมวดหมู่หลักสูตร - คำถามที่มีอยู่ในทั้งหมด โมดูลกิจกรรมและหลักสูตรในหมวดหมู่ของหลักสูตร * บริบทของระบบ - คำถามที่มีอยู่ในหลักสูตรและกิจกรรมทั้งหมดบนไซต์ยังใช้หมวดหมู่สำหรับคำถามแบบสุ่มเนื่องจากคำถามจะถูกเลือกจากหมวดหมู่เฉพาะ';

$string['editcategories_link'] = 'คำถาม / หมวดหมู่';

$string['editcategory'] = 'แก้ไขหมวดหมู่';

$string['editingcategory'] = 'การแก้ไขหมวดหมู่';

$string['editingquestion'] = 'แก้ไขคำถาม';

$string['editquestion'] = 'แก้ไขคำถาม';

$string['editthiscategory'] = 'แก้ไขหมวดหมู่นี้';

$string['emptyxml'] = 'ข้อผิดพลาดที่ไม่รู้จัก - imsmanifest.xml ว่างเปล่า';

$string['enabled'] = 'เปิดใช้งาน';

$string['erroraccessingcontext'] = 'ไม่สามารถเข้าถึงบริบท';

$string['errordeletingquestionsfromcategory'] = 'เกิดข้อผิดพลาดในการลบคำถามจากหมวดหมู่ {$a}';

$string['errorduringpost'] = 'เกิดข้อผิดพลาดระหว่างการประมวลผล!';

$string['errorduringpre'] = 'เกิดข้อผิดพลาดระหว่างการประมวลผลล่วงหน้า!';

$string['errorduringproc'] = 'เกิดข้อผิดพลาดระหว่างการประมวลผล!';

$string['errorduringregrade'] = 'ไม่สามารถให้คะแนนคำถาม {$a->qid} ใหม่ได้โดยไปที่สถานะ {$a->stateid}';

$string['errorfilecannotbecopied'] = 'ข้อผิดพลาด: ไม่สามารถคัดลอกไฟล์ {$a}';

$string['errorfilecannotbemoved'] = 'ข้อผิดพลาด: ไม่สามารถย้ายไฟล์ {$a}';

$string['errorfileschanged'] = 'ข้อผิดพลาด: ไฟล์ที่เชื่อมโยงจากคำถามมีการเปลี่ยนแปลงตั้งแต่แสดงแบบฟอร์ม';

$string['erroritemappearsmorethanoncewithdifferentweight'] = 'คำถาม ({$a}) ปรากฏขึ้นมากกว่าหนึ่งครั้งโดยมีน้ำหนักต่างกันในตำแหน่งต่างๆของการทดสอบ ขณะนี้รายงานสถิติยังไม่รองรับและอาจทำให้สถิติของคำถามนี้ไม่น่าเชื่อถือ';

$string['errormanualgradeoutofrange'] = 'เกรด {$a->grade} ไม่อยู่ระหว่าง 0 ถึง {$a->maxgrade} สำหรับคำถาม {$a->name} ยังไม่ได้บันทึกคะแนนและความคิดเห็น';

$string['errormovingquestions'] = 'เกิดข้อผิดพลาดขณะย้ายคำถามด้วยรหัส {$a}';

$string['errorpostprocess'] = 'เกิดข้อผิดพลาดระหว่างการประมวลผล!';

$string['errorpreprocess'] = 'เกิดข้อผิดพลาดระหว่างการประมวลผลล่วงหน้า!';

$string['errorprocess'] = 'เกิดข้อผิดพลาดระหว่างการประมวลผล!';

$string['errorprocessingresponses'] = 'เกิดข้อผิดพลาดขณะประมวลผลคำตอบของคุณ ({$a}) คลิกดำเนินการต่อเพื่อกลับไปยังหน้าที่คุณอยู่และลองอีกครั้ง';

$string['errorsavingcomment'] = 'เกิดข้อผิดพลาดในการบันทึกความคิดเห็นสำหรับคำถาม {$a->name} ในฐานข้อมูล';

$string['errorupdatingattempt'] = 'เกิดข้อผิดพลาดในการอัปเดตความพยายาม {$a->id} ในฐานข้อมูล';

$string['eventquestioncategorycreated'] = 'สร้างหมวดหมู่คำถามแล้ว';

$string['eventquestioncategorydeleted'] = 'ลบหมวดหมู่คำถามแล้ว';

$string['eventquestioncategorymoved'] = 'ย้ายหมวดหมู่คำถามแล้ว';

$string['eventquestioncategoryupdated'] = 'อัปเดตหมวดคำถามแล้ว';

$string['eventquestioncategoryviewed'] = 'ดูหมวดคำถามแล้ว';

$string['eventquestioncreated'] = 'สร้างคำถามแล้ว';

$string['eventquestiondeleted'] = 'ลบคำถามแล้ว';

$string['eventquestionmoved'] = 'ย้ายคำถามแล้ว';

$string['eventquestionviewed'] = 'ดูคำถามแล้ว';

$string['eventquestionsexported'] = 'คำถามที่ส่งออก';

$string['eventquestionsimported'] = 'คำถามที่นำเข้า';

$string['eventquestionupdated'] = 'อัปเดตคำถามแล้ว';

$string['export'] = 'ส่งออก';

$string['exportasxml'] = 'ส่งออกเป็น Moodle XML';

$string['exportcategory'] = 'ประเภทการส่งออก';

$string['exportcategory_help'] = 'การตั้งค่านี้จะกำหนดหมวดหมู่ที่จะใช้คำถามที่ส่งออก รูปแบบการนำเข้าบางรูปแบบเช่น GIFT และ Moodle XML อนุญาตให้รวมหมวดหมู่และข้อมูลบริบทในไฟล์ส่งออกทำให้สามารถสร้าง (ไม่บังคับ) ขึ้นใหม่ได้เมื่อนำเข้า หากจำเป็นควรเลือกช่องทำเครื่องหมายที่เหมาะสม';

$string['exporterror'] = 'เกิดข้อผิดพลาดระหว่างการส่งออก!';

$string['exportfilename'] = 'คำถาม';

$string['exportnameformat'] = '%Y%m%d-%H%M';

$string['exportonequestion'] = 'ดาวน์โหลดคำถามนี้ในรูปแบบ Moodle XML';

$string['exportquestions'] = 'ส่งออกคำถามไปยังไฟล์';

$string['exportquestions_help'] = 'ฟังก์ชันนี้ช่วยให้สามารถส่งออกหมวดหมู่ทั้งหมด (และหมวดหมู่ย่อย) ของคำถามไปยังไฟล์ได้ โปรดทราบว่าขึ้นอยู่กับรูปแบบไฟล์ที่เลือกข้อมูลคำถามและคำถามบางประเภทอาจไม่สามารถส่งออกได้ทั้งนี้ขึ้นอยู่กับรูปแบบไฟล์ที่เลือก';

$string['exportquestions_link'] = 'คำถาม / ส่งออก';

$string['filecantmovefrom'] = 'ไม่สามารถย้ายไฟล์คำถามได้เนื่องจากคุณไม่ได้รับอนุญาตให้ลบไฟล์ออกจากที่ที่คุณพยายามย้ายคำถาม';

$string['filecantmoveto'] = 'ไม่สามารถย้ายหรือคัดลอกไฟล์คำถามได้เนื่องจากคุณไม่ได้รับอนุญาตให้เพิ่มไฟล์ไปยังสถานที่ที่คุณพยายามจะย้ายคำถามไป';

$string['fileformat'] = 'รูปแบบไฟล์';

$string['filesareacourse'] = 'พื้นที่ไฟล์หลักสูตร';

$string['filesareasite'] = 'พื้นที่ไฟล์ไซต์';

$string['filestomove'] = 'ย้าย / คัดลอกไฟล์ไปที่ {$a} ไหม';

$string['filterbytags'] = 'กรองตามแท็ก ...';

$string['firsttry'] = 'ครั้งแรกลอง';

$string['flagged'] = 'ติดธง';

$string['flagthisquestion'] = 'ตั้งค่าสถานะคำถามนี้';

$string['formquestionnotinids'] = 'แบบฟอร์มมีคำถามที่ไม่อยู่ในคำถาม';

$string['fractionsnomax'] = 'คำตอบข้อใดข้อหนึ่งควรมีคะแนน 100%จึงเป็นไปได้ที่จะได้คะแนนเต็มสำหรับคำถามนี้';

$string['getcategoryfromfile'] = 'รับหมวดหมู่จากไฟล์';

$string['getcontextfromfile'] = 'รับบริบทจากไฟล์';

$string['changepublishstatuscat'] = 'หมวดหมู่ \'{$a->name}\'ในหลักสูตร \'{$a->coursename}\' จะมีมันสถานะร่วมกันเปลี่ยนจาก{$a->changefrom} เพื่อ {$a->changeto}';

$string['chooseqtypetoadd'] = 'เลือกประเภทคำถามที่จะเพิ่ม';

$string['editquestions'] = 'แก้ไขคำถาม';

$string['idnumber'] = 'เลขประจำตัว';

$string['idnumber_help'] = 'หากใช้หมายเลขประจำตัวจะต้องไม่ซ้ำกันในแต่ละหมวดหมู่คำถาม เป็นอีกวิธีหนึ่งในการระบุคำถามซึ่งบางครั้งก็มีประโยชน์ แต่โดยปกติแล้วสามารถเว้นว่างไว้ได้';

$string['ignorebroken'] = 'ละเว้นลิงก์ที่ใช้งานไม่ได้';

$string['impossiblechar'] = 'ตรวจพบอักขระที่เป็นไปไม่ได้ {$a} เป็นอักขระในวงเล็บ';

$string['import'] = 'นำเข้า';

$string['importcategory'] = 'นำเข้าหมวดหมู่';

$string['importcategory_help'] = 'การตั้งค่านี้จะกำหนดหมวดหมู่ของคำถามที่นำเข้า รูปแบบการนำเข้าบางรูปแบบเช่น GIFT และ Moodle XML อาจรวมถึงหมวดหมู่และข้อมูลบริบทในไฟล์นำเข้า ในการใช้ข้อมูลนี้แทนที่จะเลือกหมวดหมู่ที่เลือกควรเลือกช่องทำเครื่องหมายที่เหมาะสม หากไม่มีหมวดหมู่ที่ระบุในไฟล์นำเข้าจะถูกสร้างขึ้น';

$string['importerror'] = 'เกิดข้อผิดพลาดระหว่างการประมวลผลการนำเข้า';

$string['importerrorquestion'] = 'เกิดข้อผิดพลาดในการนำเข้าคำถาม';

$string['importingquestions'] = 'นำเข้าคำถาม {$a} จากไฟล์';

$string['importparseerror'] = 'พบข้อผิดพลาดในการแยกวิเคราะห์ไฟล์นำเข้า ไม่มีการนำเข้าคำถาม หากต้องการนำเข้าคำถามดีๆให้ลองตั้งค่า \'Stop on error\' เป็น \'No\' อีกครั้ง';

$string['importquestions'] = 'นำเข้าคำถามจากไฟล์';

$string['importquestions_help'] = 'ฟังก์ชันนี้ช่วยให้สามารถนำเข้าคำถามในรูปแบบต่างๆผ่านไฟล์ข้อความได้ โปรดทราบว่าไฟล์ต้องใช้การเข้ารหัส UTF-8';

$string['importquestions_link'] = 'คำถาม / นำเข้า';

$string['importwrongfiletype'] = 'ประเภทของไฟล์ที่คุณเลือก ({$a->actualtype}) ไม่ตรงกับประเภทที่ต้องการโดยรูปแบบการนำเข้านี้ ({$a->expectedtype})';

$string['invalidarg'] = 'ไม่มีอาร์กิวเมนต์ที่ถูกต้องหรือการกำหนดค่าเซิร์ฟเวอร์ไม่ถูกต้อง';

$string['invalidcategoryidforparent'] = 'รหัสหมวดหมู่ไม่ถูกต้องสำหรับผู้ปกครอง!';

$string['invalidcategoryidtomove'] = 'รหัสหมวดหมู่ไม่ถูกต้องที่จะย้าย!';

$string['invalidconfirm'] = 'สตริงการยืนยันไม่ถูกต้อง';

$string['invalidcontextinhasanyquestions'] = 'ส่งบริบทไม่ถูกต้องไปยัง question_context_has_any_questions';

$string['invalidgrade'] = 'เกรด ({$a}) ไม่ตรงกับตัวเลือกเกรด - ข้ามคำถาม';

$string['invalidpenalty'] = 'บทลงโทษไม่ถูกต้อง';

$string['invalidwizardpage'] = 'ไม่ถูกต้องหรือไม่ได้ระบุหน้าตัวช่วยสร้าง!';

$string['lastmodifiedby'] = 'แก้ไขล่าสุดโดย';

$string['lasttry'] = 'ลองครั้งสุดท้าย';

$string['linkedfiledoesntexist'] = 'ไม่มีไฟล์ที่เชื่อมโยง {$a}';

$string['makechildof'] = 'กำหนดให้เป็นลูกของ \'{$a}\'';

$string['maketoplevelitem'] = 'ย้ายไปที่ระดับบนสุด';

$string['manualgradeinvalidformat'] = 'นั่นไม่ใช่ตัวเลขที่ถูกต้อง';

$string['matchgrades'] = 'จับคู่เกรด';

$string['matchgradeserror'] = 'เกิดข้อผิดพลาดหากเกรดไม่อยู่ในรายการ';

$string['matchgradesnearest'] = 'เกรดที่ใกล้ที่สุดหากไม่อยู่ในรายการ';

$string['matchgrades_help'] = 'เกรดที่นำเข้าจะต้องตรงกับรายการเกรดที่ถูกต้องคงที่ - 100, 90, 80, 75, 70, 66.666, 60, 50, 40, 33.333, 30, 25, 20, 16.666, 14.2857, 12.5, 11.111, 10, 5 , 0 (ค่าลบด้วย) หากไม่มีจะมีสองตัวเลือก: * ข้อผิดพลาดหากเกรดไม่อยู่ในรายการ - หากคำถามมีเกรดใด ๆ ที่ไม่พบในรายการข้อผิดพลาดจะปรากฏขึ้นและคำถามนั้นจะไม่ถูกนำเข้า * เกรดที่ใกล้ที่สุดหากไม่อยู่ในรายการ - หากพบว่าเกรด ไม่ตรงกับค่าในรายการเกรดจะเปลี่ยนเป็นค่าที่ใกล้เคียงที่สุดในรายการ';

$string['missingcourseorcmid'] = 'ต้องระบุ courseid หรือ cmid เพื่อ print_question';

$string['missingcourseorcmidtolink'] = 'ต้องระบุ courseid หรือ cmid เพื่อ get_question_edit_link';

$string['missingimportantcode'] = 'คำถามประเภทนี้ไม่มีรหัสสำคัญ: {$a}';

$string['missingoption'] = 'คำถามปิดบัง {$a} ไม่มีตัวเลือก';

$string['modified'] = 'บันทึกล่าสุด';

$string['move'] = 'ย้ายจาก {$a} และเปลี่ยนลิงก์';

$string['movecategory'] = 'ย้ายหมวดหมู่';

$string['movedquestionsandcategories'] = 'ย้ายคำถามและหมวดหมู่คำถามจาก {$a->oldplace} ไปที่ {$a->newplace}';

$string['movelinksonly'] = 'เพียงแค่เปลี่ยนตำแหน่งที่ลิงก์ชี้ไปอย่าย้ายหรือคัดลอกไฟล์';

$string['moveq'] = 'ย้ายคำถาม';

$string['moveqtoanothercontext'] = 'ย้ายคำถามไปยังบริบทอื่น';

$string['moveto'] = 'ย้ายไปที่ >>';

$string['movingcategory'] = 'การย้ายหมวดหมู่';

$string['movingcategoryandfiles'] = 'แน่ใจไหมว่าต้องการย้ายหมวดหมู่ {$a->name} และหมวดหมู่ย่อยทั้งหมดไปยังบริบทสำหรับ \'{$a->contextto}\' เราตรวจพบไฟล์ {$a->urlcount} ที่เชื่อมโยงจากคำถามใน {$a->fromareaname} คุณต้องการคัดลอกหรือย้ายไฟล์เหล่านี้ไปที่ {$a->toareaname} หรือไม่';

$string['movingcategorynofiles'] = 'แน่ใจไหมว่าต้องการย้ายหมวดหมู่ \'{$a->name}\' และหมวดหมู่ย่อยทั้งหมดไปยังบริบทสำหรับ \'{$a->contextto}\'';

$string['movingquestions'] = 'การย้ายคำถามและไฟล์ใด ๆ';

$string['movingquestionsandfiles'] = 'คุณแน่ใจหรือว่าต้องการย้ายคำถาม (s) {$a->คำถาม} กับบริบทสำหรับ\'{$a->tocontext}\' ? เราตรวจพบไฟล์{$a->urlcount} ไฟล์ที่เชื่อมโยงจากคำถามเหล่านี้ใน {$a->fromareaname} คุณต้องการคัดลอกหรือย้ายไฟล์เหล่านี้ไปที่ {$a->toareaname} หรือไม่';

$string['movingquestionsnofiles'] = 'คุณแน่ใจหรือว่าต้องการย้ายคำถาม (s) {$a->คำถาม} กับบริบทสำหรับ\'{$a->tocontext}\' ? มีไฟล์ที่ไม่มีการเชื่อมโยงจากคำถามเหล่านี้ (s) ใน {$a->fromareaname}';

$string['needtochoosecat'] = 'คุณต้องเลือกหมวดหมู่เพื่อย้ายคำถามนี้ไปที่หรือกด \'ยกเลิก\'';

$string['nocate'] = 'ไม่มีหมวดหมู่ดังกล่าว {$a}!';

$string['nopermissionadd'] = 'คุณไม่ได้รับอนุญาตให้เพิ่มคำถามที่นี่';

$string['nopermissionmove'] = 'คุณไม่ได้รับอนุญาตให้ย้ายคำถามจากที่นี่ คุณต้องบันทึกคำถามในหมวดหมู่นี้หรือบันทึกเป็นคำถามใหม่';

$string['noprobs'] = 'ไม่พบปัญหาในฐานข้อมูลคำถามของคุณ';

$string['noquestions'] = 'ไม่พบคำถามที่สามารถส่งออกได้ ตรวจสอบให้แน่ใจว่าคุณได้เลือกหมวดหมู่ที่จะส่งออกซึ่งมีคำถาม';

$string['noquestionsinfile'] = 'ไม่มีคำถามในไฟล์นำเข้า';

$string['notagfiltersapplied'] = 'ไม่ได้ใช้ตัวกรองแท็ก';

$string['notenoughanswers'] = 'คำถามประเภทนี้ต้องการคำตอบอย่างน้อย {$a} คำตอบ';

$string['notenoughdatatoeditaquestion'] = 'ไม่ได้ระบุรหัสคำถามหรือรหัสหมวดหมู่และประเภทคำถาม';

$string['notenoughdatatomovequestions'] = 'คุณต้องระบุรหัสคำถามของคำถามที่คุณต้องการย้าย';

$string['notflagged'] = 'ไม่ถูกตั้งค่าสถานะ';

$string['novirtualquestiontype'] = 'ไม่มีประเภทคำถามเสมือนจริงสำหรับคำถามประเภท {$a}';

$string['numqas'] = 'ไม่ได้พยายามถาม';

$string['numquestions'] = 'ไม่มีคำถาม';

$string['numquestionsandhidden'] = '{$a->numquestions} (ซ่อนไว้ + {$a->numhidden})';

$string['page-question-x'] = 'หน้าคำถามใด ๆ';

$string['page-question-edit'] = 'หน้าแก้ไขคำถาม';

$string['page-question-category'] = 'หน้าหมวดหมู่คำถาม';

$string['page-question-import'] = 'หน้านำเข้าคำถาม';

$string['page-question-export'] = 'หน้าการส่งออกคำถาม';

$string['parentcategory'] = 'หมวดหมู่ผู้ปกครอง';

$string['parentcategory_help'] = 'หมวดหมู่หลักคือหมวดหมู่ที่จะวางหมวดหมู่ใหม่ \'ด้านบน\' หมายความว่าหมวดหมู่นี้ไม่มีอยู่ในหมวดหมู่อื่นใด บริบทของหมวดหมู่จะแสดงเป็นตัวหนา ต้องมีอย่างน้อยหนึ่งหมวดหมู่ในแต่ละบริบท';

$string['parentcategory_link'] = 'คำถาม / หมวดหมู่';

$string['parenthesisinproperclose'] = 'วงเล็บก่อน ** ปิดไม่ถูกต้องใน {$a} **';

$string['parenthesisinproperstart'] = 'วงเล็บก่อน ** เริ่มต้นไม่ถูกต้องใน {$a} **';

$string['parsingquestions'] = 'การแยกคำถามจากไฟล์นำเข้า';

$string['penaltyfactor'] = 'ปัจจัยโทษ';

$string['penaltyfactor_help'] = 'การตั้งค่านี้จะกำหนดว่าเศษของคะแนนที่ได้รับจะถูกลบออกสำหรับการตอบสนองที่ไม่ถูกต้องแต่ละครั้ง จะใช้ได้ก็ต่อเมื่อแบบทดสอบทำงานในโหมด Adaptive ปัจจัยการลงโทษควรเป็นตัวเลขระหว่าง 0 ถึง 1 ปัจจัยการลงโทษเท่ากับ 1 หมายความว่านักเรียนต้องได้รับคำตอบที่ถูกต้องในการตอบครั้งแรกเพื่อให้ได้รับเครดิตใด ๆ ค่าปรับเป็น 0 หมายความว่านักเรียนสามารถลองได้บ่อยเท่าที่ต้องการและยังได้คะแนนเต็ม';

$string['permissionedit'] = 'แก้ไขคำถามนี้';

$string['permissionmove'] = 'ย้ายคำถามนี้';

$string['permissionsaveasnew'] = 'บันทึกเป็นคำถามใหม่';

$string['permissionto'] = 'คุณได้รับอนุญาตให้:';

$string['published'] = 'แชร์';

$string['qtypeveryshort'] = 'ที';

$string['questionaffected'] = 'คำถาม \'{$a->name}\' ({$a->qtype})อยู่ในหมวดคำถามนี้ แต่ยังใช้ในแบบทดสอบ \'{$a->quizname}\'ในหลักสูตรอื่นด้วย \'{$a->Coursename } \'.';

$string['questionbank'] = 'คลังคำถาม';

$string['questioncategory'] = 'หมวดคำถาม';

$string['questioncatsfor'] = 'หมวดคำถามสำหรับ \'{$a}\'';

$string['questiondoesnotexist'] = 'ไม่มีคำถามนี้';

$string['questionname'] = 'ชื่อคำถาม';

$string['questionno'] = 'คำถาม {$a}';

$string['questionsaveerror'] = 'เกิดข้อผิดพลาดระหว่างการบันทึกคำถาม - ({$a})';

$string['questionsinuse'] = '(* คำถามที่มีเครื่องหมายดอกจันจะถูกนำไปใช้ที่ใดที่หนึ่งเช่นในแบบทดสอบดังนั้นหากคุณดำเนินการต่อคำถามเหล่านี้จะไม่ถูกลบจริงๆคำถามเหล่านี้จะถูกซ่อนไว้เท่านั้น)';

$string['questionsmovedto'] = 'คำถามที่ยังคงใช้อยู่ย้ายไปที่ \'{$a}\' ในหมวดหมู่หลักสูตรผู้ปกครอง';

$string['questionsrescuedfrom'] = 'คำถามบันทึกจากบริบท {$a}';

$string['questionsrescuedfrominfo'] = 'คำถามเหล่านี้ (บางส่วนอาจถูกซ่อนไว้) จะถูกบันทึกไว้เมื่อบริบท {$a} ถูกลบเนื่องจากคำถามหรือกิจกรรมอื่น ๆ ยังคงใช้อยู่';

$string['questiontags'] = 'แท็กคำถาม';

$string['questiontype'] = 'ประเภทคำถาม';

$string['questionuse'] = 'ใช้คำถามในกิจกรรมนี้';

$string['questionvariant'] = 'รูปแบบคำถาม';

$string['reviewresponse'] = 'ตรวจสอบคำตอบ';

$string['save'] = 'บันทึก';

$string['savechangesandcontinueediting'] = 'บันทึกการเปลี่ยนแปลงและแก้ไขต่อไป';

$string['saveflags'] = 'บันทึกสถานะของแฟล็ก';

$string['selectacategory'] = 'เลือกหมวดหมู่:';

$string['selectaqtypefordescription'] = 'เลือกประเภทคำถามเพื่อดูคำอธิบาย';

$string['selectcategoryabove'] = 'เลือกหมวดหมู่ด้านบน';

$string['selectquestionsforbulk'] = 'เลือกคำถามสำหรับการดำเนินการจำนวนมาก';

$string['shareincontext'] = 'แบ่งปันในบริบทสำหรับ {$a}';

$string['stoponerror'] = 'หยุดข้อผิดพลาด';

$string['stoponerror_help'] = 'การตั้งค่านี้จะพิจารณาว่ากระบวนการนำเข้าหยุดเมื่อตรวจพบข้อผิดพลาดทำให้ไม่มีคำถามใด ๆ ที่นำเข้าหรือไม่ว่าคำถามใด ๆ ที่มีข้อผิดพลาดจะถูกละเว้นและคำถามที่ถูกต้องจะถูกนำเข้าหรือไม่';

$string['tofilecategory'] = 'เขียนหมวดหมู่ลงในไฟล์';

$string['tofilecontext'] = 'เขียนบริบทลงในไฟล์';

$string['topfor'] = 'ยอดนิยมในราคา {$a}';

$string['uninstallbehaviour'] = 'ถอนการติดตั้งลักษณะการทำงานของคำถามนี้';

$string['uninstallqtype'] = 'ถอนการติดตั้งคำถามประเภทนี้';

$string['unknown'] = 'ไม่ทราบ';

$string['unknownquestiontype'] = 'ประเภทคำถามที่ไม่รู้จัก: {$a}';

$string['unknowntolerance'] = 'ประเภทความอดทนที่ไม่รู้จัก {$a}';

$string['unpublished'] = 'ยกเลิกการแชร์';

$string['upgradeproblemcategoryloop'] = 'ตรวจพบปัญหาเมื่ออัปเกรดหมวดหมู่คำถาม มีการวนซ้ำในต้นไม้หมวดหมู่ รหัสหมวดหมู่ที่ได้รับผลกระทบคือ {$a}';

$string['upgradeproblemcouldnotupdatecategory'] = 'ไม่สามารถอัปเดตหมวดคำถาม {$a->name} ({$a->id})';

$string['upgradeproblemunknowncategory'] = 'ตรวจพบปัญหาเมื่ออัปเกรดหมวดหมู่คำถาม หมวดหมู่ {$a->id} หมายถึง parent {$a->parent} ซึ่งไม่มีอยู่ ผู้ปกครองเปลี่ยนเพื่อแก้ไขปัญหา';

$string['wrongprefix'] = 'nameprefix ที่จัดรูปแบบไม่ถูกต้อง {$a}';

$string['youmustselectaqtype'] = 'คุณต้องเลือกประเภทคำถาม';

$string['yourfileshoulddownload'] = 'ไฟล์ส่งออกของคุณควรจะเริ่มดาวน์โหลดในไม่ช้า ถ้าไม่ได้โปรดคลิกที่นี่';

$string['addanotherhint'] = 'เพิ่มคำใบ้อื่น';

$string['answer'] = 'ตอบ';

$string['answersaved'] = 'บันทึกคำตอบแล้ว';

$string['attemptfinished'] = 'ความพยายามเสร็จสิ้น';

$string['attemptfinishedsubmitting'] = 'ความพยายามในการส่งเสร็จสิ้น:';

$string['behaviourbeingused'] = 'พฤติกรรมที่ใช้: {$a}';

$string['category'] = 'ประเภท';

$string['changeoptions'] = 'เปลี่ยนตัวเลือก';

$string['attemptoptions'] = 'ลองใช้ตัวเลือก';

$string['displayoptions'] = 'ตัวเลือกการแสดงผล';

$string['check'] = 'ตรวจสอบ';

$string['clearwrongparts'] = 'ล้างคำตอบที่ไม่ถูกต้อง';

$string['closepreview'] = 'ปิดการแสดงตัวอย่าง';

$string['combinedfeedback'] = 'ความคิดเห็นรวม';

$string['commented'] = 'แสดงความคิดเห็น: {$a}';

$string['comment'] = 'แสดงความคิดเห็น';

$string['commentormark'] = 'แสดงความคิดเห็นหรือลบเครื่องหมาย';

$string['comments'] = 'ความคิดเห็น';

$string['commentx'] = 'ความคิดเห็น: {$a}';

$string['complete'] = 'เสร็จสมบูรณ์';

$string['correct'] = 'แก้ไข';

$string['correctfeedback'] = 'สำหรับคำตอบที่ถูกต้อง';

$string['correctfeedbackdefault'] = 'คำตอบของคุณถูกต้อง';

$string['decimalplacesingrades'] = 'ตำแหน่งทศนิยมในเกรด';

$string['defaultmark'] = 'เครื่องหมายเริ่มต้น';

$string['errorsavingflags'] = 'เกิดข้อผิดพลาดในการบันทึกสถานะแฟล็ก';

$string['feedback'] = 'ข้อเสนอแนะ';

$string['fillincorrect'] = 'กรอกคำตอบที่ถูกต้อง';

$string['generalfeedback'] = 'ข้อเสนอแนะทั่วไป';

$string['generalfeedback_help'] = 'ข้อเสนอแนะทั่วไปจะแสดงให้นักเรียนเห็นหลังจากที่พวกเขาตอบคำถามเสร็จแล้ว ซึ่งแตกต่างจากคำติชมที่เฉพาะเจาะจงซึ่งขึ้นอยู่กับประเภทคำถามและคำตอบของนักเรียนที่ให้ไว้ข้อความแสดงความคิดเห็นทั่วไปเดียวกันจะแสดงต่อนักเรียนทุกคน คุณสามารถใช้คำติชมทั่วไปเพื่อให้คำตอบแก่นักเรียนได้อย่างสมบูรณ์และอาจเป็นลิงค์ไปยังข้อมูลเพิ่มเติมที่พวกเขาสามารถใช้ได้หากพวกเขาไม่เข้าใจคำถาม';

$string['hintn'] = 'คำใบ้';

$string['hintnoptions'] = 'คำแนะนำตัวเลือก {no}';

$string['hinttext'] = 'ข้อความคำใบ้';

$string['howquestionsbehave'] = 'คำถามมีพฤติกรรมอย่างไร';

$string['howquestionsbehave_help'] = 'นักเรียนสามารถโต้ตอบกับคำถามในแบบทดสอบได้หลายวิธี ตัวอย่างเช่นคุณอาจต้องการให้นักเรียนป้อนคำตอบสำหรับคำถามแต่ละข้อจากนั้นจึงส่งแบบทดสอบทั้งหมดก่อนที่จะให้คะแนนหรือได้รับคำติชมใด ๆ นั่นจะเป็นโหมด \'คำติชมรอตัดบัญชี\' หรือคุณอาจต้องการให้นักเรียนส่งคำถามแต่ละข้อในขณะที่พวกเขาดำเนินการไปพร้อมกันเพื่อรับคำติชมในทันทีและถ้าพวกเขาทำไม่ถูกต้องในทันทีให้ลองใหม่เพื่อให้ได้คะแนนน้อยลง นั่นจะเป็นโหมด \'โต้ตอบกับการพยายามหลายครั้ง\' สิ่งเหล่านี้อาจเป็นสองโหมดพฤติกรรมที่ใช้บ่อยที่สุด';

$string['howquestionsbehave_link'] = 'คำถาม / พฤติกรรม';

$string['importfromcoursefiles'] = '... หรือเลือกไฟล์หลักสูตรที่จะนำเข้า';

$string['importfromupload'] = 'เลือกไฟล์ที่จะอัพโหลด ...';

$string['includesubcategories'] = 'แสดงคำถามจากหมวดหมู่ย่อยด้วย';

$string['incorrect'] = 'ไม่ถูกต้อง';

$string['incorrectfeedback'] = 'สำหรับการตอบสนองที่ไม่ถูกต้อง';

$string['incorrectfeedbackdefault'] = 'คำตอบของคุณไม่ถูกต้อง';

$string['information'] = 'ข้อมูล';

$string['invalidanswer'] = 'คำตอบที่ไม่สมบูรณ์';

$string['makecopy'] = 'ทำสำเนา';

$string['manualgradeoutofrange'] = 'เกรดนี้อยู่นอกช่วงที่ถูกต้อง';

$string['manuallygraded'] = 'ให้คะแนน {$a->mark} ด้วยตนเองพร้อมความคิดเห็น: {$a->comment}';

$string['mark'] = 'เครื่องหมาย';

$string['markedoutof'] = 'ทำเครื่องหมายจาก';

$string['markedoutofmax'] = 'ทำเครื่องหมายจาก {$a}';

$string['markoutofmax'] = 'ทำเครื่องหมาย {$a->mark} จาก {$a->max}';

$string['marks'] = 'เครื่องหมาย';

$string['noresponse'] = '[ไม่มีการตอบกลับ]';

$string['notanswered'] = 'ไม่ได้รับคำตอบ';

$string['notgraded'] = 'ไม่มีคะแนน';

$string['notshown'] = 'ไม่แสดง';

$string['notyetanswered'] = 'ยังไม่ตอบ';

$string['notchanged'] = 'ไม่มีการเปลี่ยนแปลงตั้งแต่ความพยายามครั้งล่าสุด';

$string['notyourpreview'] = 'ตัวอย่างนี้ไม่ได้เป็นของคุณ';

$string['options'] = 'ตัวเลือก';

$string['parent'] = 'ผู้ปกครอง';

$string['partiallycorrect'] = 'ถูกต้องบางส่วน';

$string['partiallycorrectfeedback'] = 'สำหรับคำตอบที่ถูกต้องบางส่วน';

$string['partiallycorrectfeedbackdefault'] = 'คำตอบของคุณถูกต้องบางส่วน';

$string['penaltyforeachincorrecttry'] = 'บทลงโทษสำหรับการลองผิดแต่ละครั้ง';

$string['penaltyforeachincorrecttry_help'] = 'เมื่อคำถามถูกเรียกใช้โดยใช้พฤติกรรม \'โต้ตอบกับการพยายามหลายครั้ง\' หรือ \'โหมดปรับเปลี่ยน\' เพื่อให้นักเรียนพยายามหลายครั้งเพื่อให้คำถามถูกต้องจากนั้นตัวเลือกนี้จะควบคุมว่าจะถูกลงโทษเท่าใดสำหรับการลองผิดแต่ละครั้ง การลงโทษเป็นสัดส่วนของคะแนนคำถามทั้งหมดดังนั้นหากคำถามมีค่าสามคะแนนและบทลงโทษคือ 0.3333333 นักเรียนจะได้คะแนน 3 หากพวกเขาได้รับคำถามในครั้งแรกอย่างถูกต้อง 2 หากพวกเขาได้รับการลองครั้งที่สองถูกต้อง และ 1 ในนั้นทำให้ถูกต้องในการลองครั้งที่สาม สำหรับคำถามหลายส่วนตรรกะการให้คะแนนนี้จะใช้แยกกันกับแต่ละส่วนของคำถาม รายละเอียดขึ้นอยู่กับประเภทคำถามและอาจมีความซับซ้อน แต่หลักการคือให้เครดิตนักเรียนสำหรับความรู้ที่พวกเขาแสดงให้เป็นธรรมมากที่สุด';

$string['previewquestion'] = 'ดูตัวอย่างคำถาม: {$a}';

$string['privacy:metadata:database:question'] = 'รายละเอียดเกี่ยวกับคำถามเฉพาะ';

$string['privacy:metadata:database:question:createdby'] = 'คนที่สร้างคำถาม.';

$string['privacy:metadata:database:question:generalfeedback'] = 'ข้อเสนอแนะทั่วไปสำหรับคำถามนี้';

$string['privacy:metadata:database:question:modifiedby'] = 'ผู้ที่อัปเดตคำถามล่าสุด';

$string['privacy:metadata:database:question:name'] = 'ชื่อของคำถาม';

$string['privacy:metadata:database:question:questiontext'] = 'ข้อความคำถาม';

$string['privacy:metadata:database:question:timecreated'] = 'วันที่และเวลาที่สร้างคำถามนี้';

$string['privacy:metadata:database:question:timemodified'] = 'วันที่และเวลาที่อัปเดตคำถามนี้';

$string['privacy:metadata:database:question_attempt_step_data'] = 'ขั้นตอนการถามคำถามอาจมีข้อมูลเพิ่มเติมเฉพาะสำหรับขั้นตอนนั้น ข้อมูลจะถูกเก็บไว้ในตาราง step_data';

$string['privacy:metadata:database:question_attempt_step_data:name'] = 'ชื่อของรายการข้อมูล';

$string['privacy:metadata:database:question_attempt_step_data:value'] = 'ค่าของรายการข้อมูล';

$string['privacy:metadata:database:question_attempt_steps'] = 'การถามคำถามแต่ละครั้งจะมีขั้นตอนต่างๆเพื่อระบุระยะที่แตกต่างกันตั้งแต่ต้นจนจบจนถึงการทำเครื่องหมาย ตารางนี้จัดเก็บข้อมูลสำหรับแต่ละขั้นตอนเหล่านี้';

$string['privacy:metadata:database:question_attempt_steps:fraction'] = 'คะแนนที่ได้รับสำหรับคำถามนี้พยายามปรับขนาดเป็นค่าจาก 1';

$string['privacy:metadata:database:question_attempt_steps:state'] = 'สถานะของคำถามนี้จะพยายามทำตามขั้นตอนเมื่อสิ้นสุดการเปลี่ยนขั้นตอน';

$string['privacy:metadata:database:question_attempt_steps:timecreated'] = 'วันที่และเวลาที่เริ่มการเปลี่ยนแปลงขั้นตอนนี้';

$string['privacy:metadata:database:question_attempt_steps:userid'] = 'ผู้ใช้ที่ทำการเปลี่ยนขั้นตอน';

$string['privacy:metadata:database:question_attempts'] = 'ข้อมูลเกี่ยวกับความพยายามในคำถามที่เฉพาะเจาะจง';

$string['privacy:metadata:database:question_attempts:flagged'] = 'ข้อบ่งชี้ว่าผู้ใช้ได้ตั้งค่าสถานะคำถามนี้ภายในความพยายาม';

$string['privacy:metadata:database:question_attempts:responsesummary'] = 'สรุปการตอบคำถาม';

$string['privacy:metadata:database:question_attempts:timemodified'] = 'เวลาที่อัปเดตคำถาม';

$string['privacy:metadata:link:qbehaviour'] = 'ระบบย่อยคำถามใช้ประโยชน์จากปลั๊กอินประเภทพฤติกรรมคำถาม';

$string['privacy:metadata:link:qformat'] = 'ระบบย่อยคำถามใช้ปลั๊กอินประเภทคำถามเพื่อวัตถุประสงค์ในการนำเข้าและส่งออกคำถามในรูปแบบต่างๆ';

$string['privacy:metadata:link:qtype'] = 'ระบบย่อยคำถามโต้ตอบกับปลั๊กอินประเภทคำถามซึ่งมีคำถามประเภทต่างๆ';

$string['questionbehaviouradminsetting'] = 'การตั้งค่าพฤติกรรมของคำถาม';

$string['questionbehavioursdisabled'] = 'คำถามพฤติกรรมเพื่อปิดใช้งาน';

$string['questionbehavioursdisabledexplained'] = 'ป้อนรายการพฤติกรรมที่คั่นด้วยจุลภาคที่คุณไม่ต้องการให้ปรากฏในเมนูแบบเลื่อนลง';

$string['questionbehavioursorder'] = 'ลำดับพฤติกรรมของคำถาม';

$string['questionbehavioursorderexplained'] = 'ป้อนรายการพฤติกรรมที่คั่นด้วยจุลภาคตามลำดับที่คุณต้องการให้ปรากฏในเมนูแบบเลื่อนลง';

$string['questionidmismatch'] = 'รหัสคำถามไม่ตรงกัน';

$string['questionformtagheader'] = '{$a} แท็ก';

$string['questionnamecopy'] = '{$a} (สำเนา)';

$string['questionpreviewdefaults'] = 'ค่าเริ่มต้นของการแสดงตัวอย่างคำถาม';

$string['questionpreviewdefaults_desc'] = 'ค่าเริ่มต้นเหล่านี้จะใช้เมื่อผู้ใช้ดูตัวอย่างคำถามในคลังคำถามเป็นครั้งแรก เมื่อผู้ใช้ดูตัวอย่างคำถามแล้วการตั้งค่าส่วนบุคคลของพวกเขาจะถูกจัดเก็บเป็นค่ากำหนดของผู้ใช้';

$string['questions'] = 'คำถาม';

$string['questionx'] = 'คำถาม {$a}';

$string['questiontext'] = 'ข้อความคำถาม';

$string['requiresgrading'] = 'ต้องมีการให้คะแนน';

$string['responsehistory'] = 'ประวัติการตอบกลับ';

$string['restart'] = 'เริ่มต้นอีกครั้ง';

$string['restartwiththeseoptions'] = 'เริ่มต้นใหม่อีกครั้งด้วยตัวเลือกเหล่านี้';

$string['restoremultipletopcats'] = 'ไฟล์สำรองประกอบด้วยหมวดหมู่คำถามระดับบนสุดมากกว่าหนึ่งหมวดหมู่สำหรับบริบท {$a}';

$string['rightanswer'] = 'คำตอบที่ถูกต้อง';

$string['rightanswer_help'] = 'สรุปคำตอบที่ถูกต้องที่สร้างขึ้นโดยอัตโนมัติ อาจมีข้อ จำกัด ดังนั้นคุณอาจต้องการพิจารณาอธิบายวิธีแก้ไขปัญหาที่ถูกต้องในความคิดเห็นทั่วไปสำหรับคำถามและปิดตัวเลือกนี้';

$string['saved'] = 'บันทึกแล้ว: {$a}';

$string['settingsformultipletries'] = 'พยายามหลายครั้ง';

$string['showhidden'] = 'แสดงคำถามเก่าด้วย';

$string['showmarkandmax'] = 'แสดงเครื่องหมายและสูงสุด';

$string['showmaxmarkonly'] = 'แสดงเครื่องหมายสูงสุดเท่านั้น';

$string['showquestiontext'] = 'แสดงข้อความคำถามในรายการคำถาม';

$string['shown'] = 'แสดง';

$string['shownumpartscorrect'] = 'แสดงจำนวนคำตอบที่ถูกต้อง';

$string['shownumpartscorrectwhenfinished'] = 'แสดงจำนวนคำตอบที่ถูกต้องเมื่อคำถามเสร็จสิ้น';

$string['specificfeedback'] = 'ข้อเสนอแนะเฉพาะ';

$string['specificfeedback_help'] = 'ข้อเสนอแนะที่ขึ้นอยู่กับสิ่งที่นักเรียนตอบสนอง';

$string['started'] = 'เริ่มแล้ว';

$string['state'] = 'สถานะ';

$string['step'] = 'ขั้นตอน';

$string['steps'] = 'ขั้นตอน';

$string['submissionoutofsequence'] = 'เข้าถึงไม่เป็นลำดับ โปรดอย่าคลิกปุ่มย้อนกลับเมื่อทำคำถามแบบทดสอบ';

$string['submissionoutofsequencefriendlymessage'] = 'คุณได้ป้อนข้อมูลนอกลำดับปกติ สิ่งนี้สามารถเกิดขึ้นได้หากคุณใช้ปุ่มย้อนกลับหรือปุ่มไปข้างหน้าของเบราว์เซอร์ โปรดอย่าใช้สิ่งเหล่านี้ในระหว่างการทดสอบ นอกจากนี้ยังสามารถเกิดขึ้นได้หากคุณคลิกบางสิ่งบางอย่างในขณะที่หน้ากำลังโหลด คลิกดำเนินการต่อเพื่อดำเนินการต่อ';

$string['submit'] = 'ส่ง';

$string['submitandfinish'] = 'ส่งและเสร็จสิ้น';

$string['submitted'] = 'ส่ง: {$a}';

$string['tagarea_question'] = 'คำถาม';

$string['technicalinfo'] = 'ข้อมูลทางเทคนิค';

$string['technicalinfo_help'] = 'ข้อมูลทางเทคนิคนี้น่าจะเป็นประโยชน์สำหรับนักพัฒนาที่ทำงานเกี่ยวกับคำถามประเภทใหม่เท่านั้น นอกจากนี้ยังอาจเป็นประโยชน์เมื่อพยายามวินิจฉัยปัญหาเกี่ยวกับคำถาม';

$string['technicalinfominfraction'] = 'เศษส่วนขั้นต่ำ: {$a}';

$string['technicalinfomaxfraction'] = 'เศษส่วนสูงสุด: {$a}';

$string['technicalinfoquestionsummary'] = 'สรุปคำถาม: {$a}';

$string['technicalinforesponsesummary'] = 'สรุปคำตอบ: {$a}';

$string['technicalinforightsummary'] = 'สรุปคำตอบที่ถูกต้อง: {$a}';

$string['technicalinfostate'] = 'สถานะคำถาม: {$a}';

$string['technicalinfovariant'] = 'รูปแบบคำถาม: {$a}';

$string['unknownbehaviour'] = 'พฤติกรรมที่ไม่รู้จัก: {$a}';

$string['unknownorunhandledtype'] = 'ประเภทคำถามที่ไม่รู้จักหรือไม่ได้รับการจัดการ: {$a}';

$string['unknownquestion'] = 'คำถามที่ไม่รู้จัก: {$a}';

$string['unknownquestioncatregory'] = 'หมวดหมู่คำถามที่ไม่รู้จัก: {$a}';

$string['unusedcategorydeleted'] = 'หมวดหมู่นี้ถูกลบไปแล้วเนื่องจากหลังจากลบหลักสูตรแล้วคำถามของมันจะไม่ถูกนำไปใช้อีกต่อไป';

$string['updatedisplayoptions'] = 'อัปเดตตัวเลือกการแสดงผล';

$string['whethercorrect'] = 'ว่าถูกต้องหรือไม่';

$string['whethercorrect_help'] = 'ซึ่งครอบคลุมทั้งคำอธิบายที่เป็นข้อความ \'ถูกต้อง\' \'ถูกต้องบางส่วน\' หรือ \'ไม่ถูกต้อง\' และการเน้นสีใด ๆ ที่สื่อถึงข้อมูลเดียวกัน';

$string['whichtries'] = 'ซึ่งพยายาม';

$string['withselected'] = 'ด้วยการเลือก';

$string['xoutofmax'] = '{$a->mark} จาก {$a->max}';

$string['yougotnright'] = 'คุณได้เลือก {$a->num} อย่างถูกต้อง';

$string['action'] = 'การดำเนินการ';

$string['addcategory'] = 'เพิ่มประเภท';

$string['advancedsearchoptions'] = 'ตัวเลือกการค้นหา';

$string['answers'] = 'คำตอบ';

$string['availableq'] = 'พร้อม?';

$string['behaviour'] = 'การกระทำ';

$string['byandon'] = 'โดย {$a->user} เมื่อ {$a->time}';

$string['cannotcopybackup'] = 'ไม่สามารถคัดลอกแฟ้มสำรองข้อมูล';

$string['cannotcreatepath'] = 'ไม่สามารถสร้างพาธ: {$a}';

$string['cannotdeleteqtypeinuse'] = 'ไม่สามารถลบประเภทของคำถาม \'{$a}\' นี้ได้เพราะยังมีคำถามอยู่ในคลังข้อสอบ';

$string['cannotdeleteqtypeneeded'] = 'ไม่สามารถลบประเภทของคำถาม \'{$a}\' นี้ได้ ยังมีการใช้งานคำถามประเภทอื่นที่ยังใช้งานเกี่ยวเนื่องกันอยู่';
