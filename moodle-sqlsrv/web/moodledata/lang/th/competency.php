<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Competency language file.
 *
 * @package    core_competency
 * @copyright  2016 Frédéric Massart - FMCorz.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allchildrenarecomplete'] = 'เด็ก ๆ ทุกคนอยู่ครบ';

$string['competencies'] = 'สมรรถนะ';

$string['competenciesarenotenabled'] = 'ไม่ได้เปิดใช้งานสมรรถนะ';

$string['competenciessettings'] = 'การตั้งค่าความสามารถ';

$string['completeplanstask'] = 'แผนการจัดการเรียนรู้ที่ครบกำหนด';

$string['coursecompetencyoutcome_complete'] = 'เติมเต็มสมรรถนะ';

$string['coursecompetencyoutcome_evidence'] = 'แนบหลักฐาน';

$string['coursecompetencyoutcome_none'] = 'ไม่ทำอะไร';

$string['coursecompetencyoutcome_recommend'] = 'ส่งเพื่อตรวจสอบ';

$string['coursemodulecompetencyoutcome_complete'] = 'เติมเต็มสมรรถนะ';

$string['coursemodulecompetencyoutcome_evidence'] = 'แนบหลักฐาน';

$string['coursemodulecompetencyoutcome_none'] = 'ไม่ทำอะไร';

$string['coursemodulecompetencyoutcome_recommend'] = 'ส่งเพื่อตรวจสอบ';

$string['deletecompetencyratings'] = 'ลบคะแนนความสามารถ';

$string['duplicateditemname'] = '{$a} (สำเนา)';

$string['enablecompetencies'] = 'เปิดใช้งานความสามารถ';

$string['enablecompetencies_desc'] = 'สมรรถนะช่วยให้ผู้ใช้ได้รับการประเมินตามแผนการเรียนรู้';

$string['errorcannotchangeapastduedate'] = 'วันที่ครบกำหนดผ่านไปแล้ว ไม่สามารถเปลี่ยนแปลงได้';

$string['errorcannotsetduedateinthepast'] = 'ไม่สามารถกำหนดวันที่ครบกำหนดย้อนหลังได้';

$string['errorcannotsetduedatetoosoon'] = 'วันที่ครบกำหนดเร็วเกินไป';

$string['errorcompetencyrule'] = 'ไม่ทราบกฎความสามารถของหลักสูตร "{$a}"';

$string['errorcoursecompetencyrule'] = 'ไม่ทราบกฎความสามารถของหลักสูตร "{$a}"';

$string['errorinvalidcourse'] = 'หลักสูตรไม่ถูกต้อง';

$string['errornocompetency'] = 'ไม่พบความสามารถของ {$a}';

$string['errorplanstatus'] = 'แผนการเรียนรู้ไม่ทราบสถานะ "{$a}"';

$string['errorscalealreadyused'] = 'ไม่สามารถเปลี่ยนมาตราส่วนได้เนื่องจากมีการใช้งานอยู่แล้ว';

$string['errorscaleconfiguration'] = 'ต้องกำหนดค่ามาตราส่วนโดยการเลือกรายการเริ่มต้นและรายการที่เชี่ยวชาญ';

$string['errorusercomptencystatus'] = 'ไม่ทราบสถานะความสามารถของผู้ใช้ "{$a}"';

$string['eventcompetencycreated'] = 'สร้างความสามารถแล้ว';

$string['eventcompetencydeleted'] = 'ลบความสามารถแล้ว';

$string['eventcompetencyframeworkcreated'] = 'สร้างกรอบสมรรถนะ';

$string['eventcompetencyframeworkdeleted'] = 'ลบกรอบสมรรถนะแล้ว';

$string['eventcompetencyframeworkupdated'] = 'อัปเดตกรอบสมรรถนะแล้ว';

$string['eventcompetencyframeworkviewed'] = 'ดูกรอบสมรรถนะ';

$string['eventcompetencyupdated'] = 'อัปเดตความสามารถแล้ว';

$string['eventcompetencyviewed'] = 'ดูความสามารถแล้ว';

$string['eventevidencecreated'] = 'สร้างหลักฐานแล้ว';

$string['eventplanapproved'] = 'แผนการเรียนรู้ได้รับการอนุมัติ';

$string['eventplancompleted'] = 'แผนการเรียนรู้เสร็จสิ้น';

$string['eventplancreated'] = 'แผนการเรียนรู้ที่สร้างขึ้น.';

$string['eventplandeleted'] = 'ลบแผนการเรียนรู้แล้ว';

$string['eventplanreopened'] = 'แผนการเรียนรู้เปิดอีกครั้ง';

$string['eventplanreviewrequestcancelled'] = 'คำขอทบทวนแผนการเรียนรู้ถูกยกเลิก';

$string['eventplanreviewrequested'] = 'ขอให้มีการทบทวนแผนการเรียนรู้';

$string['eventplanreviewstarted'] = 'เริ่มการทบทวนแผนการเรียนรู้';

$string['eventplanreviewstopped'] = 'หยุดทบทวนแผนการเรียนรู้แล้ว';

$string['eventplanunapproved'] = 'แผนการเรียนรู้ไม่ได้รับการอนุมัติ';

$string['eventplanunlinked'] = 'ยกเลิกการลิงก์แผนการเรียนรู้แล้ว';

$string['eventplanupdated'] = 'อัปเดตแผนการเรียนรู้แล้ว';

$string['eventplanviewed'] = 'ดูแผนการเรียนรู้';

$string['eventtemplatecreated'] = 'สร้างเทมเพลตแผนการเรียนรู้แล้ว';

$string['eventtemplatedeleted'] = 'ลบเทมเพลตแผนการเรียนรู้แล้ว';

$string['eventtemplateupdated'] = 'อัปเดตเทมเพลตแผนการเรียนรู้แล้ว';

$string['eventtemplateviewed'] = 'ดูเทมเพลตแผนการเรียนรู้แล้ว';

$string['eventusercompetencyplanviewed'] = 'ดูแผนความสามารถของผู้ใช้';

$string['eventusercompetencyrated'] = 'คะแนนความสามารถของผู้ใช้';

$string['eventusercompetencyratedincourse'] = 'คะแนนความสามารถของผู้ใช้ในหลักสูตร';

$string['eventusercompetencyratedinplan'] = 'คะแนนความสามารถของผู้ใช้ในแผนการเรียนรู้';

$string['eventusercompetencyreviewrequestcancelled'] = 'คำขอตรวจสอบความสามารถของผู้ใช้ถูกยกเลิก';

$string['eventusercompetencyreviewrequested'] = 'ร้องขอการตรวจสอบความสามารถของผู้ใช้';

$string['eventusercompetencyreviewstarted'] = 'เริ่มการตรวจสอบความสามารถของผู้ใช้แล้ว';

$string['eventusercompetencyreviewstopped'] = 'การตรวจสอบความสามารถของผู้ใช้หยุดลง';

$string['eventusercompetencyviewed'] = 'ดูความสามารถของผู้ใช้';

$string['eventusercompetencyviewedincourse'] = 'ความสามารถของผู้ใช้ที่ดูในหลักสูตร';

$string['eventusercompetencyviewedinplan'] = 'ความสามารถของผู้ใช้ที่ดูในแผนการเรียนรู้';

$string['eventuserevidencecreated'] = 'สร้างหลักฐานการเรียนรู้ก่อนหน้านี้';

$string['eventuserevidencedeleted'] = 'ลบหลักฐานการเรียนรู้ก่อนหน้านี้แล้ว';

$string['eventuserevidenceupdated'] = 'อัปเดตหลักฐานการเรียนรู้ก่อนหน้าแล้ว';

$string['evidence_competencyrule'] = 'เป็นไปตามกฎของความสามารถ';

$string['evidence_coursecompleted'] = 'หลักสูตร "{$a}" เสร็จสมบูรณ์แล้ว';

$string['evidence_coursemodulecompleted'] = 'กิจกรรม "{$a}" เสร็จสมบูรณ์';

$string['evidence_courserestored'] = 'คะแนนถูกเรียกคืนพร้อมกับหลักสูตร "{$a}"';

$string['evidence_evidenceofpriorlearninglinked'] = 'มีการเชื่อมโยงหลักฐานการเรียนรู้ก่อนหน้า "{$a}"';

$string['evidence_evidenceofpriorlearningunlinked'] = 'ไม่มีการลิงก์หลักฐานการเรียนรู้ก่อนหน้า "{$a}"';

$string['evidence_manualoverride'] = 'การจัดระดับความสามารถถูกตั้งค่าด้วยตนเอง';

$string['evidence_manualoverrideincourse'] = 'คะแนนความสามารถถูกตั้งค่าด้วยตนเองในหลักสูตร "{$a}"';

$string['evidence_manualoverrideinplan'] = 'คะแนนความสามารถถูกกำหนดด้วยตนเองในแผนการเรียนรู้ "{$a}"';

$string['invalidevidencedesc'] = 'คำอธิบายหลักฐานไม่ถูกต้อง';

$string['invalidgrade'] = 'คะแนนไม่ถูกต้อง';

$string['invalidpersistenterror'] = 'ข้อผิดพลาด: {$a}';

$string['invalidplan'] = 'แผนการเรียนรู้ไม่ถูกต้อง';

$string['invalidtaxonomy'] = 'การจัดหมวดหมู่ไม่ถูกต้อง: {$a}';

$string['invalidurl'] = 'URL ไม่ถูกต้อง ตรวจสอบให้แน่ใจว่าขึ้นต้นด้วย "http: //" หรือ "https: //"';

$string['nouserplanswithcompetency'] = 'ไม่มีแผนการเรียนรู้ใดที่ประกอบด้วยความสามารถนี้';

$string['planstatusactive'] = 'คล่องแคล่ว';

$string['planstatuscomplete'] = 'เสร็จสมบูรณ์';

$string['planstatusdraft'] = 'ร่าง';

$string['planstatusinreview'] = 'อยู่ระหว่างตรวจสอบ';

$string['planstatuswaitingforreview'] = 'รอการตรวจสอบ';

$string['pointsrequiredaremet'] = 'ตรงตามจุดที่ต้องการ';

$string['privacy:evidence:action:complete'] = 'ความสามารถที่สมบูรณ์หากไม่มีการให้คะแนน';

$string['privacy:evidence:action:log'] = 'บันทึกการดำเนินการ';

$string['privacy:evidence:action:override'] = 'ลบล้างคะแนนความสามารถ';

$string['privacy:metadata:competency'] = 'บันทึกความสามารถ';

$string['privacy:metadata:competency_coursecomp'] = 'บันทึกความสามารถที่เชื่อมโยงกับหลักสูตร';

$string['privacy:metadata:competency_coursecompsetting'] = 'บันทึกการตั้งค่าความสามารถในหลักสูตร';

$string['privacy:metadata:competency_evidence'] = 'บันทึกหลักฐานที่มีผลต่อสถานะของความสามารถ';

$string['privacy:metadata:competency_framework'] = 'บันทึกกรอบความสามารถ';

$string['privacy:metadata:competency_modulecomp'] = 'บันทึกความสามารถที่เชื่อมโยงกับโมดูล';

$string['privacy:metadata:competency_plan'] = 'บันทึกแผนการจัดการเรียนรู้';

$string['privacy:metadata:competency_plancomp'] = 'แบบบันทึกสมรรถนะในแผนการจัดการเรียนรู้';

$string['privacy:metadata:competency_relatedcomp'] = 'บันทึกความสัมพันธ์ระหว่างความสามารถ';

$string['privacy:metadata:competency_template'] = 'บันทึกแม่แบบแผนการเรียนรู้';

$string['privacy:metadata:competency_templatecohort'] = 'บันทึกกลุ่มประชากรตามรุ่นที่เกี่ยวข้องกับเทมเพลตแผนการเรียนรู้';

$string['privacy:metadata:competency_templatecomp'] = 'บันทึกความสามารถในแม่แบบแผนการเรียนรู้';

$string['privacy:metadata:competency_usercomp'] = 'บันทึกสถานะความสามารถของผู้ใช้';

$string['privacy:metadata:competency_usercompcourse'] = 'บันทึกสถานะความสามารถของผู้ใช้ในหลักสูตร';

$string['privacy:metadata:competency_usercompplan'] = 'บันทึกสถานะสมรรถนะในแผนการจัดการเรียนรู้';

$string['privacy:metadata:competency_userevidence'] = 'บันทึกหลักฐานการเรียนรู้ก่อนหน้านี้';

$string['privacy:metadata:competency_userevidencecomp'] = 'บันทึกความสามารถที่เกี่ยวข้องกับหลักฐานการเรียนรู้ก่อนหน้านี้';

$string['privacy:metadata:core_comments'] = 'ข้อคิดเห็นเกี่ยวกับแผนการเรียนรู้และสมรรถนะ';

$string['privacy:metadata:evidence:action'] = 'ประเภทของการดำเนินการกับหลักฐาน';

$string['privacy:metadata:evidence:actionuserid'] = 'ผู้ใช้ดำเนินการ';

$string['privacy:metadata:evidence:desca'] = 'พารามิเตอร์ทางเลือกของคำอธิบายหลักฐานที่แปลได้';

$string['privacy:metadata:evidence:desccomponent'] = 'ส่วนประกอบของคำอธิบายหลักฐานที่แปลได้';

$string['privacy:metadata:evidence:descidentifier'] = 'ตัวระบุคำอธิบายหลักฐานที่แปลได้';

$string['privacy:metadata:evidence:grade'] = 'เกรดที่เกี่ยวข้องกับหลักฐาน';

$string['privacy:metadata:evidence:note'] = 'บันทึกที่ไม่ใช่ภาษาท้องถิ่นแนบมากับหลักฐาน';

$string['privacy:metadata:evidence:url'] = 'URL ที่เชื่อมโยงกับหลักฐาน';

$string['privacy:metadata:plan:description'] = 'คำอธิบายแผนการจัดการเรียนรู้';

$string['privacy:metadata:plan:duedate'] = 'วันครบกำหนดแผนการเรียนรู้';

$string['privacy:metadata:plan:name'] = 'ชื่อแผนการจัดการเรียนรู้';

$string['privacy:metadata:plan:reviewerid'] = 'รหัสของผู้ตรวจสอบแผนการจัดการเรียนรู้';

$string['privacy:metadata:plan:status'] = 'สถานะของแผนการจัดการเรียนรู้';

$string['privacy:metadata:plan:userid'] = 'ID ของผู้ใช้ที่มีแผนการเรียนรู้';

$string['privacy:metadata:timecreated'] = 'เวลาที่สร้างเรกคอร์ด';

$string['privacy:metadata:timemodified'] = 'เวลาที่แก้ไขบันทึก';

$string['privacy:metadata:usercomp:grade'] = 'เกรดที่กำหนดสำหรับความสามารถ';

$string['privacy:metadata:usercomp:proficiency'] = 'ไม่ว่าจะเป็นความชำนาญหรือไม่';

$string['privacy:metadata:usercomp:reviewerid'] = 'รหัสของผู้ตรวจสอบ';

$string['privacy:metadata:usercomp:status'] = 'สถานะของความสามารถ';

$string['privacy:metadata:usercomp:userid'] = 'ID ของผู้ใช้ที่มีความสามารถ';

$string['privacy:metadata:userevidence:description'] = 'คำอธิบายของหลักฐาน';

$string['privacy:metadata:userevidence:name'] = 'ชื่อของหลักฐานการเรียนรู้ก่อนหน้านี้';

$string['privacy:metadata:userevidence:url'] = 'URL ที่เชื่อมโยงกับหลักฐาน';

$string['privacy:metadata:usermodified'] = 'ผู้ใช้ที่สร้างหรือแก้ไขเรกคอร์ด';

$string['privacy:path:plans'] = 'แผนการเรียนรู้';

$string['privacy:path:relatedtome'] = 'ที่เกี่ยวข้องกับฉัน';

$string['privacy:path:userevidence'] = 'หลักฐานการเรียนรู้ก่อน';

$string['pushcourseratingstouserplans'] = 'ผลักดันการจัดอันดับหลักสูตรไปยังแผนการเรียนรู้รายบุคคล';

$string['pushcourseratingstouserplans_desc'] = 'ค่าเริ่มต้นสำหรับการตั้งค่าหลักสูตรเพื่อปรับปรุงแผนการเรียนรู้ของแต่ละบุคคลเมื่อมีการจัดอันดับความสามารถของหลักสูตร';

$string['syncplanscohorts'] = 'ซิงค์แผนจากกลุ่มแม่แบบแผนการเรียนรู้';

$string['taxonomy_behaviour'] = 'พฤติกรรม';

$string['taxonomy_competency'] = 'สมรรถนะ';

$string['taxonomy_concept'] = 'แนวคิด';

$string['taxonomy_domain'] = 'โดเมน';

$string['taxonomy_indicator'] = 'ตัวบ่งชี้';

$string['taxonomy_level'] = 'ระดับ';

$string['taxonomy_outcome'] = 'ผล';

$string['taxonomy_practice'] = 'การปฏิบัติ';

$string['taxonomy_proficiency'] = 'ความชำนาญ';

$string['taxonomy_skill'] = 'ทักษะ';

$string['taxonomy_value'] = 'ค่า';

$string['usercommentedonacompetency'] = '{$a->fullname} แสดงความคิดเห็นเกี่ยวกับความสามารถ "{$a->competency}": {$a->comment} ดู: {$a->url}';

$string['usercommentedonacompetencyhtml'] = '{$a->fullname} แสดงความคิดเห็นเกี่ยวกับความสามารถ "{$a->competency}":

{$a->comment}
ดู: {$a->URLNAME}';

$string['usercommentedonacompetencysmall'] = '{$a->fullname} แสดงความคิดเห็นเกี่ยวกับความสามารถ "{$a->competency}"';

$string['usercommentedonacompetencysubject'] = '{$a} แสดงความคิดเห็นเกี่ยวกับความสามารถ';

$string['usercommentedonaplan'] = '{$a->fullname} แสดงความคิดเห็นเกี่ยวกับแผนการเรียนรู้ "{$a->plan}": {$a->comment} ดู: {$a->url}';

$string['usercommentedonaplanhtml'] = '{$a->fullname} แสดงความคิดเห็นเกี่ยวกับแผนการเรียนรู้ "{$a->plan}":

{$a->comment}
ดู: {$a->URLNAME}';

$string['usercommentedonaplansmall'] = '{$a->fullname} แสดงความคิดเห็นเกี่ยวกับแผนการเรียนรู้ "{$a->plan}"';

$string['usercommentedonaplansubject'] = '{$a} แสดงความคิดเห็นเกี่ยวกับแผนการเรียนรู้';

$string['usercompetencystatus_idle'] = 'ไม่ได้ใช้งาน';

$string['usercompetencystatus_inreview'] = 'อยู่ระหว่างตรวจสอบ';

$string['usercompetencystatus_waitingforreview'] = 'รอการตรวจสอบ';

$string['userplans'] = 'แผนการเรียนรู้';