<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lang strings
 *
 * @package    report
 * @subpackage completion
 * @copyright  2009 Catalyst IT Ltd
 * @author     Aaron Barnes <aaronb@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

$string['clicktomarkusercomplete'] = 'คลิกเพื่อทำเครื่องหมายว่าผู้ใช้เสร็จสมบูรณ์';

$string['completion:view'] = 'ดูรายงานการจบหลักสูตร';

$string['completiondate'] = 'วันที่เสร็จสมบูรณ์';

$string['id'] = 'ID';

$string['name'] = 'ชื่อ';

$string['nocapability'] = 'ไม่สามารถเข้าถึงรายงานการเสร็จสิ้นของผู้ใช้';

$string['page-report-completion-x'] = 'รายงานความสมบูรณ์ใด ๆ';

$string['page-report-completion-index'] = 'รายงานการจบหลักสูตร';

$string['page-report-completion-user'] = 'รายงานการจบหลักสูตรของผู้ใช้';

$string['pluginname'] = 'จบหลักสูตร';

$string['privacy:metadata'] = 'รายงานการจบหลักสูตรจะแสดงเฉพาะข้อมูลที่จัดเก็บในสถานที่อื่น ๆ';

$string['eventreportviewed'] = 'ดูรายงานความสมบูรณ์แล้ว';

$string['eventuserreportviewed'] = 'ดูรายงานผู้ใช้ที่เสร็จสมบูรณ์แล้ว';

