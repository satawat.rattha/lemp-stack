<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'access', language 'th', version '3.9'.
 *
 * @package     access
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['access'] = 'การเข้าถึง';
$string['accesshelp'] = 'ความช่วยเหลือในการเข้าถึง';
$string['accesskey'] = 'รหัสการเข้าถึง {$ a}';
$string['accessstatement'] = 'คำสั่งการเข้าถึง';
$string['activitynext'] = 'กิจกรรมต่อไป';
$string['activityprev'] = 'กิจกรรมก่อนหน้า';
$string['breadcrumb'] = 'แถบนำทาง';
$string['eventcontextlocked'] = 'บริบทถูกแช่แข็ง';
$string['eventcontextunlocked'] = 'บริบทไม่ได้แช่แข็ง';
$string['hideblocka'] = 'ซ่อน {$ a} บล็อค';
$string['showblocka'] = 'แสดง {$ a} บล็อก';
$string['sitemap'] = 'แผนผังเว็บไซต์';
$string['skipa'] = 'ข้าม {$ a}';
$string['skipblock'] = 'ข้ามบล็อก';
$string['skipnavigation'] = 'ข้ามการนำทาง';
$string['skipto'] = 'ข้ามไปที่ {$ a}';
$string['tocontent'] = 'ข้ามไปที่เนื้อหาหลัก';
$string['tonavigation'] = 'ไปที่การนำทาง';
$string['youarehere'] = 'คุณอยู่ที่นี่';
