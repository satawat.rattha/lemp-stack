<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'install', language 'th', version '3.9'.
 *
 * @package     install
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['admindirerror'] = 'ไดเรกทอรี admin ที่ระบุไม่ถูกต้อง';
$string['admindirname'] = 'ไดเรกทอรี admin';
$string['admindirsetting'] = 'มีเว็บโฮสต์จำนวนน้อยที่ใช้ /admin  ในการติดตั้งระบบควบคุมเว็บไซต์เอาไว้ ซึ่งเป็นชื่อเดียวกับหน้าผู้ดูแลระบบใน moodle  วิธีหลีกเลี่ยงปัญหาคือให้คุณเปลี่ยนชื่อ admin ใน Moodle เป็นชื่ออื่นในระหว่างการติดตั้ง และใส่ชื่อใหม่ที่ต้องการตัวอย่างเช่น moodleadmin';
$string['admindirsettinghead'] = 'ตั้งค่าไดเรกทอรี admin';
$string['admindirsettingsub'] = 'เว็บโฮสติ้งจำนวนน้อยที่ใช้ /admin เป็น url พิเศษเพื่อที่จะเข้าไปยังหน้า control panel หรืออื่น ๆ ซึ่งอาจทำให้เกิดปัญหากับหน้า admin ของ Moodle ท่านสามารถทำการแก้ไขได้โดยการเปลี่ยนชื่อไดเรกทอรี admin ในการติดตั้งและใส่ชื่อใหม่ลงไป  เช่น <br /> <br /><b>moodleadmin</b><br /> <br />
การเปลี่ยนค่านี้จะเป็นการแก้ไขหน้าลิงก์ admin ใน moodle';
$string['availablelangs'] = 'ภาษาทั้งหมด';
$string['caution'] = 'คำเตือน';
$string['chooselanguage'] = 'เลือกภาษา';
$string['chooselanguagehead'] = 'เลื่อกภาษา';
$string['chooselanguagesub'] = 'กรุณาเลือกภาษาที่ใช้ในการติดตั้งเท่านั้น คุณจะสามารถเลือกตั้งค่าภาษาสำหรับเว็บไซต์และสำหรับสมาชิกแต่ละคนในหน้าจอต่อไป';
$string['compatibilitysettings'] = 'ตรวจสอบการตั้งค่า PHP';
$string['compatibilitysettingshead'] = 'ตรวจสอบการตั้งค่า PHP';
$string['compatibilitysettingssub'] = 'เซิร์ฟเวอร์ของท่านควรผ่านการทดสอบทั้งหมดนี้เพื่อให้ Moodle สามารถทำงานได้เป็นปกติ';
$string['configfilenotwritten'] = 'ตัวติดตั้งอัตโนมัติไม่สามารถสร้างไฟล์ config.php ได้ อาจเป็นเพราะว่าไม่สามารถเขียนลงไดเรกทอรี moodle ได้ คุณสามารถสร้างไฟล์ดังกล่าวได้เองโดยการก้อปปี้โค้ดต่อไปนี้ลงในไฟล์ที่ต้องการสร้างใหม่';
$string['configfilewritten'] = 'สร้าง config.php เรียบร้อยแล้ว';
$string['configurationcomplete'] = 'ตั้งค่าตัวแปรเสร็จสิ้นแล้ว';
$string['configurationcompletehead'] = 'ตั้งค่าตัวแปรเสร็จสิ้นแล้ว';
$string['configurationcompletesub'] = 'Moodle ทำการบันทึกไฟล์การตั้งค่าลงในโฟลเดอร์ moodle หลังจากทำการติดตั้ง';
$string['database'] = 'ฐานข้อมูล';
$string['dataroot'] = 'ไดเรกทอรีข้อมูล';
$string['datarooterror'] = 'ไม่พบไดเรกทอรีข้อมูลที่คุณระบุไว้หรือไม่สามารถสร้างได้ กรุณาแก้ไข Path ให้ถูกต้องหรือสร้างไดเรกทอรีนี้ใหม่';
$string['dbconnectionerror'] = 'ไม่สามารถติดต่อฐานข้อมูลที่คุณระบุไว้ได้ กรุณาตรวจสอบค่าที่ตั้งไว้ของฐานข้อมูล';
$string['dbcreationerror'] = 'มีข้อผิดพลาดในการสร้างฐานข้อมูล ไม่สามารถสร้างฐานข้อมูลที่ระบุด้วยค่าที่ให้ไว้ได้';
$string['dbhost'] = 'โฮสต์เซิร์ฟเวอร์';
$string['dbpass'] = 'รหัสผ่าน';
$string['dbprefix'] = 'คำนำหน้าตาราง (Table Prefix)';
$string['dbtype'] = 'ประเภท';
$string['directorysettings'] = '<p>กรุณายืนยันที่ตั้งของการติดตั้ง  Moodle .</p>

<p><b>ที่อยู่ของเว็บ (Web Address):</b>

ระบุที่อยู่ของเว็บไซต์ที่คุณจะนำ Moodle ไปใช้ ถ้าหากเว็บของคุณเข้าผ่าน URLs หลายขั้นให้เลือกที่นักเรียนของคุณจะเข้าไปใช้ ไม่ต้องใส่เครื่องหมาย /  ปิดท้าย</p>

<p><b>ไดเรกทอรี moodle </b>

ระบุ path ของไดเรกทอรีเต็ม ๆ ที่ใช้ในการติดตั้ง ระวังเรื่องการใช้ชื่อตัวพิมพ์ใหญ่พิมพ์เล็กให้ดี ให้แน่ใจว่าถูกต้อง </p>

<p><b>ไดเรกทอรีข้อมูล:</b>

ไดเรกทอรีนี้จะเป็นที่เก็บไฟล์ที่ moodle จะทำการบันทึกไว้ เป็นข้อมูลของเว็บ ดังนั้นควรให้สิทธิ์ในการอ่าน และ เขียนลงไดเรกทอรีนี้  (ทั่วไปแล้ว \'nobody\' หรือ \'apache\') แต่ไม่ควรเข้าไปตรง ๆ ผ่านเว็บได้
</p>';
$string['directorysettingshead'] = 'กรุณายืนยันที่ตั้งของการติดตั้ง Moodle';
$string['directorysettingssub'] = '<p><b>ที่อยู่ของเว็บ (Web Address):</b>

ระบุที่อยู่ของเว็บไซต์ที่คุณจะนำ Moodle ไปใช้ ถ้าหากเว็บของคุณเข้าผ่าน URLs หลายขั้นให้เลือกที่นักเรียนของคุณจะเข้าไปใช้ ไม่ต้องใส่เครื่องหมาย /  ปิดท้าย</p>

<p><b>ไดเรกทอรี moodle </b>

ระบุ path ของไดเรกทอรีเต็ม ๆ ที่ใช้ในการติดตั้ง ระวังเรื่องการใช้ชื่อตัวพิมพ์ใหญ่พิมพ์เล็กให้ดี ให้แน่ใจว่าถูกต้อง </p>

<p><b>ไดเรกทอรีข้อมูล:</b>

ไดเรกทอรีนี้จะเป็นที่เก็บไฟล์ที่ moodle จะทำการบันทึกไว้ เป็นข้อมูลของเว็บ ดังนั้นควรให้สิทธิ์ในการอ่าน และ เขียนลงไดเรกทอรีนี้  (ทั่วไปแล้ว \'nobody\' หรือ \'apache\') แต่ไม่ควรเข้าไปตรง ๆ ผ่านเว็บได้
</p>';
$string['dirroot'] = 'Moodle ไดเรกทอรี';
$string['dirrooterror'] = 'การตั้งค่า ไดเรกทอรี moodle ไม่ถูกต้อง ไม่พบไฟล์ติดตั้งที่ระบุ  ระบบทำการรีเซ็ตค่าด้านล่างนี้';
$string['download'] = 'ดาวน์โหลด';
$string['downloadlanguagebutton'] = 'ดาวน์โหลด "{$a}" ไฟล์ภาษา';
$string['downloadlanguagehead'] = 'ดาวน์โหลดไฟล์ภาษา';
$string['downloadlanguagenotneeded'] = 'คุณสามารถดำเนินการติดตั้งโดยใช้ภาษาที่ตั้งค่าไว้ "{$a}"';
$string['downloadlanguagesub'] = 'คุณมีตัวเลือกในการดาวน์โหลดไฟล์ภาษาและดำเนินการติดตั้งด้วยภาษาดังกล่าว <br /><br /> ถ้าหากคุณไม่สามารถดาวน์โหลดภาษาได้การติดตั้งจะดำเนินการต่อด้วยภาษาอังกฤษ (หลังจากที่ติดตั้งสำเร็จแล้วท่านสามารถดาวน์โหลดภาษาได้ในภายหลัง)';
$string['environmenthead'] = 'ตรวจสอบความพร้อมของระบบ';
$string['environmentsub'] = 'กำลังทำการตรวจสอบคอมโพเนนท์ต่าง ๆ ของระบบว่าตรงตามความต้องการของ Moodle หรือไม่';
$string['fail'] = 'ล้มเหลว';
$string['fileuploads'] = 'ไฟล์อัพโหลด';
$string['fileuploadserror'] = 'ควรจะเปิด(on)';
$string['fileuploadshelp'] = '<p>เซิร์ฟเวอร์ไม่ให้ใช้ไฟล์อัพโหลด</p>

<p>คุณสามารถติดตั้ง Moodle ได้ถึงแม้ค่านี้จะยังไม่ได้มีการอนุญาตแต่จะไม่สามารถอัพโหลดไฟล์ในด ๆ หรือรูปภาพประกอบประวัติส่วนตัวของสมาชิกได้

<p>ให้ติดต่อเว็บเซิร์ฟเวอร์ของท่านเพื่อให้ทำการเปิดให้ใช้ ไฟล์อัพโหลดซึ่งปกติทำได้โดยแก้ไขไฟล์  php.ini โดยเปลี่ยนค่า <b>file_uploads</b> เป็น\'1\'.</p>';
$string['installation'] = 'การติดตั้ง';
$string['langdownloaderror'] = 'ภาษา "{$a}" ไม่ได้รับการติดตั้ง กระบวนการติดตั้งจะดำเนินไปด้วยภาษาอังกฤษ';
$string['langdownloadok'] = 'ภาษา"{$a}" ได้รับการติดตั้งเรียบร้อยแล้ว กระบวนการติดตั้งจะดำเนินไปด้วยภาษานี้';
$string['memorylimit'] = 'ความจำสูงสุด (Memory Limit)';
$string['memorylimiterror'] = 'ความจำสูงสุดที่คุณตั้งไว้ค่อนข้างต่ำ อาจมีปัญหาในภายหลังค่ะ';
$string['memorylimithelp'] = '<p>ค่าความจำสูงสุดของเซิร์ฟเวอร์ของคุณตั้งไว้ที่  {$a}</p>

<p>ความจำดังกล่าวมีค่าน้อยไปค่ะอาจทำให้มีปัญหาในการใช้งาน moodle ในภายหลังโดยเฉพาะเมื่อคุณใช้โมดูลหลาย ๆ ตัวรวมไปถึงมีสมาชิกจำนวนมาก

<p>ค่าที่ตั้งไว้นี้ควรตั้งให้มากที่สุดเท่าที่จะมากได้ ค่าทั่วไปแนะนำไว้ที่ 40M มีอยู่หลายวิธีในการเพิ่มค่าความจำสูงสุด กล่าวคือ:

<ol>

<li>รีคอมไพล์ PHP ใหม่ โดยเพิ่มคำสั่ง <i>--enable-memory-limit</i> ซึ่งเป็นการตั้งค่าให้ moodle กำหนดขีดจำกัดค่าสูงสุดเอง

<li>ถ้าคุณสามารถแก้ไขไฟล์  php.ini ได้ด้วยตนเองก็สามารถเปลี่ยนค่า <b>memory_limit</b> ให้เป็นค่าอื่นได้เช่น  40M แต่ถ้าไม่สามารถเปลี่ยนค่านี้ได้ด้วยตนเองให้แจ้งผู้ดูแลระบบแก้ไข

<li>ในเซิร์ฟเวอร์ PHP บางตัวคุณสามารถสร้าง ไฟล์ .htaccess ภายใต้ไดเรกทอรี moodle ซึ่งมีบรรทัดต่อไปนี้อยู่:

<p><blockquote>php_value memory_limit 40M</blockquote></p>

<p>อย่างไรก็ตามในบางเซิร์ฟเวอร์คุณไม่สามารถใช้ วิธีนี้ได้ โดยจะมีการแสดง error ขึ้นมาคุณจำเป็นต้องลบไฟล์ดังกล่าวนี้ทิ้ง
</ol>';
$string['pass'] = 'สำเร็จ';
$string['phpversion'] = 'PHP เวอร์ชัน';
$string['phpversionhelp'] = '<p>Moodle จำเป็นต้องใช้ PHP เวอร์ชัน 4.1.0 เป็นอย่างน้อย</p>

<p>คุณกำลังใช้เวอร์ชัน {$a}</p>

<p>คุณต้องอัพเกรด  PHP หรือย้ายโฮสต์ใหม่ที่มี PHP เวอร์ชันใหม่กว่า</p>';
$string['safemode'] = 'Safe Mode';
$string['safemodeerror'] = 'moodle อาจมีปัญหาหาก safe mode on';
$string['safemodehelp'] = '<p>Moodle อาจมีปัญหาหาก safe mode on ซึ่งจะทำให้คุณไม่สามารถสร้างไฟล์ใหม่ได้</p>

<p>Safe mode โดยทั่วไปแล้วจะเปิดใช้ในบางเว็บโฮสติ้ง อาจจำเป็นต้องหาโฮสต์ใหม่ที่เหมาะสมสำหรับการใช้งาน Moodle </p>

<p>คุณสามารถเริ่มการติดตั้งในตอนนี้แต่อาจมีปัญหาตามมาภายหลัง</p>';
$string['sessionautostart'] = 'Session Auto Start';
$string['sessionautostarterror'] = 'ควรจะปิด (off)';
$string['sessionautostarthelp'] = '<p>Moodle จะทำงานก็ต่อเมื่อเซิร์ฟเวอร์สนับสนุน session </p>

<p>คุณสามารถทำให้ Sessions ทำงานได้โดยการแก้ไขไฟล์  php.ini ดูภายใต้ตัวแปร  session.auto_start </p>';
$string['welcomep10'] = '{$a->installername} ({$a->installerversion})';
$string['welcomep20'] = 'ท่านได้ทำการติดตั้ง<strong>{$a->packname} {$a->packversion}</strong> สำเร็จแล้ว';
$string['welcomep30'] = 'เวอร์ชั่น <strong>{$a->installername}</strong>รวมโปรแกรมสำหรับสร้างความให้กับระบบซึ่ง Moodle สามารถทำงานได้';
$string['welcomep40'] = 'แพ็กเกจนี้รวม <strong>Moodle {$a->moodlerelease} ({$a->moodleversion})</strong>.';
$string['welcomep50'] = 'การใช้งานโปรแกรมต่าง ๆ ในแพ็กเกจนี้สามารถทำได้โดยไม่ละเมิดสัญญานุญาตของแต่ละโปรแกรม  โปแกรม<strong>{$a->installername}</strong>เต็มรูปแบบนั้นจัดเป็นโปรแกรมประเภท
<a href="http://www.opensource.org/docs/definition_plain.html">โอเพ่นซอร์ส</a> และเผยแพร่ภายใต้สํญญานุญาต <a href="http://www.gnu.org/copyleft/gpl.html">GPL</a>';
$string['welcomep60'] = 'หน้าถัดจากนี้ไปจะเป็นการตั้งค่า Moodle บนคอมพิวเตอร์คุณสามารถยอมรับค่าที่ตั้งไว้ทั้งหมดหรือเปลี่ยนแปลงให้เหมาะกับความต้องการ';
$string['welcomep70'] = 'คลิกที่ "ต่อไป" เพื่อติดตั้ง Moodle ต่อไป';
$string['wwwroot'] = 'ที่อยู่ของเว็บ';
$string['wwwrooterror'] = 'ที่อยู่ของเว็บไม่ถูกต้อง ระบบไม่พบว่ามี Moodle อยู่ที่นั่น';
