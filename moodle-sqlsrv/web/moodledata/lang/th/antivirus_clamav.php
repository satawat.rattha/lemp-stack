<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'antivirus_clamav', language 'th', version '3.9'.
 *
 * @package     antivirus_clamav
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['clamfailed'] = 'Clam AV ไม่ทำงานค่ะข้อความแสดงข้อผิดพลาดมีดังนี้ {$a}  การแสดงผลจาก Clam มีดังนี้';
$string['invalidpathtoclam'] = 'มีการตั้งค่าให้เปิดการสแกนไวรัสสำหรับไฟล์อัพโหลดบน Moodle แต่ path ที่ชี้ไปยัง Clam AV  {$a} ไม่ถูกต้องค่ะ';
$string['unknownerror'] = 'มีข้อผิดพลาดเกิดขึ้นกับ Clam';
