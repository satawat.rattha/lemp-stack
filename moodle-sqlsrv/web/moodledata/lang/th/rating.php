<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'rating', language 'th', version '3.9'.
 *
 * @package     rating
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['aggregateavg'] = 'การประเมินโดยเฉลี่ย';
$string['aggregatecount'] = 'นับจำนวนการประเมิน';
$string['aggregatemax'] = 'การประเมินโดยใช้คะแนนอย่างสูง';
$string['aggregatemin'] = 'ประเมินโดยใช้คะแนนอย่างต่ำ';
$string['aggregatenone'] = 'ไม่มีการประเมิน';
$string['aggregatesum'] = 'ผลรวมการประเมิน';
$string['aggregatetype'] = 'ประเภทการสรุป';
$string['aggregatetype_help'] = 'ประเภทการสรุปจะกำหนดว่าการประเมินการให้คะแนนจะถูกรวมเพื่อกำหนดคะแนนสุดท้ายในสมุดเกรด

*การประเมินโดยเฉลี่ย - คะแนนเฉลี่ยจากการประเมินทั้งหมด
*นับจำนวนการประเมิน - จำนวนชิ้นงานที่ได้รับการประเมินจะเป็นคะแนนสุดท้าย โดยจำไว้ว่าคะแนนที่ได้ทั้งหมดจะไม่เกินคะแนนสูงสุดสำหรับกิจกรรม
* อย่างสูง - คะแนนที่มากที่สุดจะเป็นคะแนนสุดท้าย
* อย่างต่ำ - คะแนนที่น้อยที่สุดจะเป็นคะแนนสุดท้าย
* ผลรวม - คะแนนจากการประเมินทั้งหมดจะนำมารวมกัน โดยจำไว้ว่าคะแนนที่ได้ทั้งหมดจะไม่เกินคะแนนสูงสุดสำหรับกิจกรรม

ถ้าเลือก "ไม่มีการประเมิน" ไว้อยู่ กิจกรรมนั้นจะไม่ปรากฏในสมุดเกรด';
$string['allowratings'] = 'อนุญาตให้ชิ้นงานถูกประเมินได้หรือไม่?';
$string['allratingsforitem'] = 'การประเมินที่ถูกส่งมาทั้งหมด';
$string['capabilitychecknotavailable'] = 'การตรวจสอบสมรรถนะจะไม่สามารถใช้ได้จนกว่ากิจกรรมจะได้รับการบันทึกแล้ว';
$string['couldnotdeleteratings'] = 'ขออภัย ไม่สามารถลบได้เนื่องจากมีการให้คะแนนไปแล้ว';
$string['norate'] = 'การประเมินให้คะแนนชิ้นงานไม่ได้รับอนุญาต!';
$string['noratings'] = 'ไม่มีการประเมินใดๆ ส่งมา';
$string['noviewanyrate'] = 'คุณสามารถดูผลลัพธ์สำหรับชิ้นงานที่คุณสร้างเท่านั้น';
$string['noviewrate'] = 'คุณไม่สามารถดูการประเมินชิ้นงานนี้';
$string['privacy:metadata:rating'] = 'การประเมินให้คะแนนที่ผู้ใช้เป็นคนให้เองจะถูกเก็บไว้กับแผนผังของชิ้นงานที่ประเมินแล้ว';
$string['privacy:metadata:rating:rating'] = 'การประเมินให้คะแนนแบบตัวเลขที่ผู้ใช้ได้ใส่ไป';
$string['privacy:metadata:rating:timecreated'] = 'เวลาที่มีการประเมินให้คะแนนครั้งแรก';
$string['privacy:metadata:rating:timemodified'] = 'เวลาที่มีการประเมินให้คะแนนครั้งล่าสุด';
$string['privacy:metadata:rating:userid'] = 'ผู้ใช้ที่เป็นคนประเมินให้คะแนน';
$string['rate'] = 'ให้คะแนน';
$string['ratepermissiondenied'] = 'คุณไม่ได้รับอนุญาตให้ประเมินให้คะแนนชิ้นนี้';
$string['rating'] = 'การประเมินให้คะแนน';
$string['ratinginvalid'] = 'การให้คะแนนไม่ถูกต้อง';
$string['ratings'] = 'การประเมินให้คะแนน';
$string['ratingtime'] = 'จำกัดการประเมินให้คะแนนด้วยวันที่ในระยะนี้:';
$string['rolewarning'] = 'บทบาทที่มีสิทธิในการให้คะแนน';
$string['rolewarning_help'] = 'ในการส่งการประเมินให้คะแนน ผู้ใช้ต้องการ moodle/rating:rate capability และโมดูลที่มีสมรรถนะโดยเฉพาะ ผู้ใช้ที่ได้รับการแต่งตั้งบทบาทควรที่จะสามารถประเมินให้คะแนนชิ้นงานได้ รายชื่อบทบาทนั้นสามารถแก้ไขได้จากลิงก์สิทธิอนุญาตในส่วนของการบริหารจัดการ';
$string['scaleselectionrequired'] = 'เมื่อเลือกประเภิทการสรุปประเมินให้คะแนน คุณต้องเลือกด้วยว่าจะใช้มาตราส่วนหรือคะแนนสูงสุด';
