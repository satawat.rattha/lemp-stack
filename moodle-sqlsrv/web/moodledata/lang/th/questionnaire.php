<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'questionnaire', language 'th', version '3.9'.
 *
 * @package     questionnaire
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['alreadyfilled'] = 'คุณกรอกแบบฟอร์มแบบสำรวจเรียบร้อยแล้ว ขอบคุณค่ะ';
$string['modulename'] = 'แบบสำรวจ';
$string['modulenameplural'] = 'แบบสำรวจ';
$string['notavail'] = 'ยังไม่สามารถตอบแบบสำรวจในขณะนี้ได้ กรุณาลองใหม่อีกครั้ง';
$string['qtype'] = 'ประเภท';
$string['respondenttype'] = 'ประเภทของผู้ตอบแบบสำรวจ';
