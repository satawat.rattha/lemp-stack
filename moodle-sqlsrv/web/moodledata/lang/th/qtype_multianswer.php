<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_multianswer', language 'th', version '3.9'.
 *
 * @package     qtype_multianswer
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['confirmsave'] = 'ยืนยันที่จะบันทึก{$a}';
$string['correctanswer'] = 'คำตอบที่ถูกต้อง';
$string['correctanswerandfeedback'] = 'คำตอบที่ถูกต้อง และคำติชม';
$string['layout'] = 'รูปแบบ';
$string['questionsadded'] = 'เพิ่มคำถามแล้ว';
$string['questionsaveasedited'] = 'บันทึกการแก้ไขคำถามแล้ว';
$string['questiontypechanged'] = 'เปลี่ยนประเภทคำถามแล้ว';
$string['warningquestionmodified'] = '<b>ระวัง!</b>';
$string['youshouldnot'] = '<b>คุณไม่สามารถ</b>';
