<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'feedback', language 'th', version '3.9'.
 *
 * @package     feedback
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['add_item'] = 'เพิ่มคำถามลงในกิจกรรม';
$string['analysis'] = 'วิเคราะห์';
$string['anonymous'] = 'ไม่บันทึกชื่อผู้ตอบแบบสำรวจ';
$string['anonymous_edit'] = 'บันทึกชื่อของสมาชิก';
$string['anonymous_entries'] = 'ไม่บันทึกชื่อผู้ตอบคำถาม';
$string['check'] = 'หลายตัวเลือก หลายคำตอบ';
$string['check_values'] = 'คำตอบที่สามารถเลือกได้';
$string['checkbox'] = 'หลายตัวเลือก สามารถตอบได้หลายข้อ (เช็คในช่องสี่เหลี่ยม)';
$string['completed'] = 'เสร็จสิ้น';
$string['completed_feedbacks'] = 'ส่งคำตอบ';
$string['creating_templates'] = 'จัดเก็บคำถามนี้ในรูปแบบใหม่';
$string['delete_entry'] = 'ลบการตอบแบบสำรวจ';
$string['delete_template'] = 'ลบรูปแบบ';
$string['delete_templates'] = 'ลบรูปแบบ';
$string['description'] = 'คำอธิบาย';
$string['dropdown'] = 'หลายตัวเลือก-เลือกได้ตัวเดียว(ดรอปดาวน์ลิสต์)';
$string['dropdownlist'] = 'หลายตัวเลือก-คำตอบเดีวย(ดรอปดาวน์)';
$string['dropdownrated'] = 'ดรอปดาวน์ลิสต์';
$string['edit_item'] = 'แก้ไขคำถาม';
$string['edit_items'] = 'แก้ไขคำถาม';
$string['email_notification'] = 'ส่งอีเมลแจ้ง';
$string['emailteachermail'] = '{$a->username} ได้ตอบแบบสำรวจเรื่อง : \'{$a->feedback}\' เสร็จแล้ว คุณสามารถดูได้ที่ {$a->url}';
$string['emailteachermailhtml'] = '{$a->username} ได้ตอบแบบสำรวจเรื่อง : \'{$a->feedback}\' เสร็จแล้ว คุณสามารถดูได้ที่ {$a->url}';
$string['entries_saved'] = 'บันทึกคำตอบของท่านเรียบร้อยแล้ว ขอบคุณค่ะ';
$string['export_to_excel'] = 'ส่งออกเป็น Excel';
$string['feedback_is_not_open'] = 'ยังไม่เปิดให้ทำแบบสำรวจนี้';
$string['feedbackclose'] = 'ปิดการทำแบบสำรวจเวลา';
$string['feedbackopen'] = 'เปิดให้ทำแบบสำรวจเวลา';
$string['handling_error'] = 'มีข้อผิดพลาดในโมดูลแบบสำรวจ';
$string['item_name'] = 'คำถาม';
$string['label'] = 'Label';
$string['mapcourse'] = 'จับคู่รายวิชา';
$string['mapcourseinfo'] = 'แบบสำรวจนี้เป็นแบบสำรวจมที่สามารถนำไปใช้งานได้ทั้งเว็บและเปิดให้ตอบในทุกรายวิชาโดยการใช้งานบล็อคแบบสำรวจ ท่านสามารถจำกัดว่าต้องการให้แบบสำรวจนี้ปรากฎอยู่ในรายวิชาใดบ้างโดยการจับคู่รายวิชาเข้ากับแบบสำรวจนี้';
$string['mapcoursenone'] = 'ไม่มีการจับคู่รายวิชา แบบสำรวจเปิดใชงานสำหรับทุกรายวิชา';
$string['mapcourses'] = 'จับคู่แบบสอบถามเข้ากับรายวิชา';
$string['mappedcourses'] = 'รายวิชาที่จับคู่ไว้';
$string['modulename'] = 'แบบสำรวจ';
$string['modulenameplural'] = 'แบบสำรวจ';
$string['move_item'] = 'ย้ายคำถามนี้';
$string['multiplesubmit'] = 'ส่งคำตอบหลายหลัง สำหรับโพลล์ที่ไม่ต้องการการระบุชื่อสามารถทำการตอบได้ไม่จำกัดจำนวนครั้ง สำหรับโพลล์ที่มีการบันทึกชื่อผู้ใช้งานอนุญาตให้สมาชิกส่งคำตอบใหม่ได้หลายครั้ง';
$string['name'] = 'ชื่อแบบสำรวจ';
$string['name_required'] = 'ต้องใส่ชื่อ';
$string['no_handler'] = 'ยังไม่ได้กระทำการใดๆ สำหรับ';
$string['no_items_available_yet'] = 'ยังไม่ได้เพิ่มคำถาม';
$string['no_templates_available_yet'] = 'ยังไม่มีรูปแบบที่สร้างไว้';
$string['non_anonymous'] = 'ชื่อของสมาชิกจะถูกบันทึกไว้และแสดงพร้อมกับคำตอบ';
$string['not_completed_yet'] = 'ยังทำไม่เสร็จ';
$string['of'] = 'จากทั้งหมด';
$string['page'] = 'หน้า';
$string['page_after_submit'] = 'หน้าหลังจากส่งแบบสำรวจ';
$string['pagebreak'] = 'แบ่งหน้า';
$string['pluginname'] = 'แบบสำรวจ';
$string['position'] = 'ตำแหน่ง';
$string['public'] = 'สาธารณะ';
$string['question'] = 'คำถาม';
$string['questions'] = 'คำถาม';
$string['radio'] = 'หลายตัวเลือก - คำตอบเดียว';
$string['radio_values'] = 'คำตอบ';
$string['ready_feedbacks'] = 'แบบสำรวจที่พร้อมใช้งาน';
$string['required'] = 'ต้องใส่';
$string['responses'] = 'คำตอบ';
$string['save_as_new_template'] = 'บันทึกเป็นรูปแบบใหม่';
$string['save_entries'] = 'ส่งแบบสอบถาม';
$string['save_item'] = 'บันทึกคำถาม';
$string['saving_failed'] = 'การบันทึกล้มเหลว';
$string['selected_dump'] = 'เลือกดัชนีของตัวแปร $SESSION ข้างล่างนี้';
$string['show_all'] = 'แสดงทั้งหมด';
$string['show_entries'] = 'แสดงคำตอบ';
$string['show_entry'] = 'แสดงคำตอบ';
$string['sort_by_course'] = 'เรียงตามรายวิชา';
$string['switch_item_to_not_required'] = 'เปลี่ยนเป็น : ไม่จำเป็นต้องตอบ';
$string['switch_item_to_required'] = 'เปลี่ยนเป็น : ต้องตอบคำถามนี้';
$string['template'] = 'รูปแบบ';
$string['template_saved'] = 'บันทึกรูปแบบแล้ว';
$string['templates'] = 'รูปแบบ';
$string['textarea'] = 'กล่องข้อความ';
$string['textarea_height'] = 'จำนวนบรรทัด';
$string['textarea_width'] = 'กว้าง';
$string['textfield'] = 'ข้อความ';
$string['textfield_maxlength'] = 'จำนวนตัวอักษรสูงสุด';
$string['textfield_size'] = 'ความกว้างของฟิลด์ข้อความ';
$string['this_feedback_is_already_submitted'] = 'ท่านส่งแบบสอบถามไปเรียบร้อยแล้ว';
$string['update_item'] = 'บันทึกการเปลี่ยนแปลงคำถาม';
$string['use_one_line_for_each_value'] = 'ใช้หนึ่งบรรทัดต่อหนึ่งคำตอบ';
$string['use_this_template'] = 'ใช้รูปแบบนี้';
$string['using_templates'] = 'ใช้รูปแบบ';
