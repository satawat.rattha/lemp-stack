<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_radius', language 'th', version '3.9'.
 *
 * @package     auth_radius
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['auth_radiusdescription'] = 'วิธีนี้เป็นการใช้งาน <a href="http://en.wikipedia.org/wiki/RADIUS" target="_blank">RADIUS</a> เซิร์ฟเวอร์ เพื่อตรวจสอบว่าชื่อผู้ใช้และรหัสผ่านถูกต้อง';
$string['auth_radiushost'] = 'ที่อยู่ของเซิร์ฟเวอร์  RADIUS';
$string['auth_radiusnasport'] = 'พอร์ตที่ใช้ติดต่อ';
$string['auth_radiussecret'] = 'ใช้รหัสลับร่วมกัน';
$string['pluginname'] = 'ใช้ RADIUS เซิร์ฟเวอร์';
