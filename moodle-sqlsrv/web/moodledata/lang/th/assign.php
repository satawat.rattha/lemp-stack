<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'assign', language 'th', version '3.9'.
 *
 * @package     assign
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addattempt'] = 'อนุญาตให้ลองทำอีกครั้ง';
$string['addnewattempt'] = 'เพิ่มการพยายามทำ';
$string['assign:viewgrades'] = 'ดูคะแนน';
$string['assignmentisdue'] = 'ถึงกำหนดส่ง';
$string['assignmentname'] = 'หัวข้อการบ้าน';
$string['attemptnumber'] = 'จำนวนความพยายาม';
$string['attemptreopenmethod_manual'] = 'ทำโดยไม่ใช้ตัวช่วย';
$string['comment'] = 'ความคิดเห็น';
$string['defaultteam'] = 'กลุ่มเริ่มต้น';
$string['description'] = 'รายละเอียด';
$string['duedate'] = 'กำหนดส่ง';
$string['duedateno'] = 'ไม่มีกำหนดส่ง';
$string['duedatereached'] = 'วันกำหนดส่งงานผ่านไปแล้ว';
$string['editsubmission'] = 'แก้ไขงานที่ส่ง';
$string['extensionduedate'] = 'ขยายเวลากำหนดส่ง';
$string['feedback'] = 'ความเห็นที่มีต่องาน';
$string['filtersubmitted'] = 'ทำการส่งเรียบร้อยแล้ว';
$string['graded'] = 'ตรวจแล้ว';
$string['gradedby'] = 'ให้คะแนนโดย';
$string['gradedon'] = 'ให้คะแนนตาม';
$string['gradersubmissionupdatedhtml'] = '{$a->username} ทำการแก้ไขการบ้าน <i>\'{$a->assignment}\'</i><br /><br />
เข้าไปตรวจการบ้าน <a href="{$a->url}">คลิกที่นี่</a>.';
$string['gradersubmissionupdatedtext'] = '{$a->username} ได้แก้ไขการบ้าน \'{$a->assignment}\'';
$string['gradingstatus'] = 'สถานะของคะแนน';
$string['hiddenuser'] = 'ผู้เข้าร่วม';
$string['latesubmissions'] = 'ส่งงานล่าช้า';
$string['markingworkflowstatereadyforrelease'] = 'พร้อมที่จะจัดส่ง';
$string['markingworkflowstatereleased'] = 'จัดส่งแล้ว';
$string['modulenameplural'] = 'การบ้าน';
$string['newsubmissions'] = 'การบ้านที่ส่งแล้ว';
$string['nosavebutnext'] = 'ต่อไป';
$string['notgradedyet'] = 'ยังไม่ได้ให้คะแนน';
$string['notsubmittedyet'] = 'ยังไม่ได้ส่งการบ้าน';
$string['numberofdraftsubmissions'] = 'ดราฟ';
$string['numberofparticipants'] = 'ผู้เข้าร่วม';
$string['pluginname'] = 'งาน';
$string['previous'] = 'หน้าก่อน';
$string['savechanges'] = 'บันทึกการเปลี่ยนแปลง';
$string['status'] = 'สถานะ';
$string['submission'] = 'การบ้านที่ส่ง';
$string['submissionstatus_marked'] = 'ตรวจแล้ว';
$string['submitted'] = 'ทำการส่งเรียบร้อยแล้ว';
$string['submittedlateshort'] = '{$a} ช้ากว่ากำหนด';
