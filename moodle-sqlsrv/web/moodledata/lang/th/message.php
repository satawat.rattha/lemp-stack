<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'message', language 'th', version '3.9'.
 *
 * @package     message
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcontact'] = 'เพิ่มรายชื่อติดต่อ';
$string['ago'] = '{$a} ที่ผ่านมา';
$string['allusers'] = 'ข้อความจากสมาชิกทั้งหมด';
$string['backupmessageshelp'] = 'ถ้าหากเปิดกรใช้งาน เมื่อระบบทำการสำรองข้อมูลจะทำการสำรองข้อมูลในการฝากข้อความเอาไว้ด้วย';
$string['blockcontact'] = 'บล็อคการติดต่อจากบุคคลนี้';
$string['blocknoncontacts'] = 'บล็อคข้อความจากสมาชิกที่ไม่อยู่ในรายชื่อติดต่อ';
$string['blockuserconfirmbutton'] = 'บล็อค';
$string['contacts'] = 'รายชื่อติดต่อ';
$string['emailtagline'] = 'อีเมลสำเนาข้อความแล้วส่งไปยัง {$a}';
$string['message'] = 'ข้อความ';
$string['messages'] = 'ข้อความ';
$string['nomessages'] = 'ไม่มีข้อความใหม่';
$string['nomessagesfound'] = 'ไม่พบข้อความ';
$string['offline'] = 'ออฟไลน์';
$string['online'] = 'ออนไลน์';
$string['removecontact'] = 'ลบรายชื่อติดต่อ';
$string['searchmessages'] = 'ค้นหาข้อความ';
$string['sendmessage'] = 'ส่งข้อความ';
$string['sendmessageto'] = 'ส่งข้อความถึง {$a}';
$string['settings'] = 'การตั้งค่า';
$string['unblockcontact'] = 'เปิดบล็อค';
$string['unreadmessages'] = '{$a} ข้อความใหม่';
$string['userisblockingyou'] = 'สมาชิกดังกล่าวไม่ต้องการรับข้อความจากคุณ';
$string['userisblockingyounoncontact'] = 'สมาชิกดังกล่าวจะรับข้อความเฉพาะจากบุคคลที่อยู่ในรายชื่อติดต่อส่วนตัวเท่านั้น  คุณไม่ได้อยู่ในรายชื่อติดต่อดังกล่าว';
