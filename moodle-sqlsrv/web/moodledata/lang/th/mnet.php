<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'mnet', language 'th', version '3.9'.
 *
 * @package     mnet
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['mnetpeers'] = 'เครื่องลูกข่าย';
$string['mnetservices'] = 'บริการ';
$string['mnetsettings'] = 'การตั้งค่าระบบเครือข่ายของ Moodle';
$string['net'] = 'ระบบเครือข่าย';
$string['networksettings'] = 'การตั้งค่าเครือข่าย';
$string['settings'] = 'การตั้งค่า';
$string['ssoaccesscontrol'] = 'การควบคุมการเข้าถึง SSO';
$string['trustedhosts'] = 'โฮสต์ XML-RPC';
