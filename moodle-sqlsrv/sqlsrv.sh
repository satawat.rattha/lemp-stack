curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -

curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list

apt-get update -y

ACCEPT_EULA=Y apt-get -y install msodbcsql17

ACCEPT_EULA=Y apt-get -y install mssql-tools

echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile

echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc

pecl config-set php_ini /etc/php/7.4/apache2/php.ini

pecl install sqlsrv

pecl install pdo_sqlsrv

# printf "; priority=20\nextension=sqlsrv.so\n" > /etc/php/7.4/mods-available/sqlsrv.ini
# printf "; priority=30\nextension=pdo_sqlsrv.so\n" > /etc/php/7.4/mods-available/pdo_sqlsrv.ini

# cp ./config/php.ini /etc/php/7.4/apache2/php.ini

service apache2 restart

service apache-htcacheclean restart