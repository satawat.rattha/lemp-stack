apt update

apt install -y nginx

ufw allow 'Nginx HTTP'

service nginx start

apt install -y php-fpm

apt install -y php-mysql 

mkdir /var/www/html

chown -R $USER:$USER /var/www/html